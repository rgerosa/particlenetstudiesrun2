def AddXtoHHSamples(samples):

    samples['XtoHH_pythia8_13p6TeV_part1'] = [
        'XtoHH_pythia8_13p6TeV_part1',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=300','resonanceMassMax=600','resonanceMassStepSize=25','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part2'] = [
        'XtoHH_pythia8_13p6TeV_part2',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=600','resonanceMassMax=900','resonanceMassStepSize=25','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part3'] = [
        'XtoHH_pythia8_13p6TeV_part3',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=900','resonanceMassMax=1400','resonanceMassStepSize=50','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part4'] = [
        'XtoHH_pythia8_13p6TeV_part4',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=1400','resonanceMassMax=2000','resonanceMassStepSize=50','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part5'] = [
        'XtoHH_pythia8_13p6TeV_part5',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=2000','resonanceMassMax=3000','resonanceMassStepSize=50','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

def AddQCDSamples(samples):

    samples['QCD_PtBin-100to250_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-100to250_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=100','pTHatMax=250'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['QCD_PtBin-250to400_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-250to400_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=250','pTHatMax=400'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['QCD_PtBin-400to600_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-400to600_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=400','pTHatMax=600'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['QCD_PtBin-600to800_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-600to800_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=600','pTHatMax=800'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['QCD_PtBin-800to1200_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-800to1200_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=800','pTHatMax=1200'],
        1000,
        500000,
        "tree.root"
    ]

    samples['QCD_PtBin-1200to1600_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-1200to1600_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=1200','pTHatMax=1600'],
        1000,
        500000,
        "tree.root"
    ]

    samples['QCD_PtBin-1600to2000_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-1600to2000_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=1600','pTHatMax=2000'],
        1000,
        500000,
        "tree.root"
    ]

    samples['QCD_PtBin-2000to2500_TuneCP5_13p6TeV_pythia8'] = [
        'QCD_PtBin-2000to2500_TuneCP5_13p6TeV_pythia8',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','pTHatMin=2000','pTHatMax=2500'],
        1000,
        500000,
        "tree.root"
    ]
       
def AddAllSamples(samples):
    AddXtoHHSamples(samples)
    AddQCDSamples(samples)
