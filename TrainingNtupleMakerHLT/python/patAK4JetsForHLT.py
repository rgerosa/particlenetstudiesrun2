import FWCore.ParameterSet.Config as cms
import os, sys

def customizeHLTForAK4PNET(process,
                           options,
                           jetCollection,
                           pathName="",
                           outputModuleName="",
                           isMC=True,                             
                           addLastPNETEvaluation=False,
                           minPtGenJets=10,
                           maxEtaGenJets=5.0):

    print("customizeHLTForAK4PNET --> in order to create PAT containers for AK4 PNET")

    ## Build the AK4 GenJets
    if isMC:
        from PhysicsTools.PatAlgos.slimming.prunedGenParticles_cfi import prunedGenParticles;
        setattr(process,"hltPrunedGenParticlesWithStatusOne",prunedGenParticles.clone());
        getattr(process,"hltPrunedGenParticlesWithStatusOne").select.append("keep status == 1");
        setattr(process,"hltPrunedGenParticles",prunedGenParticles.clone());
        getattr(process,"hltPrunedGenParticles").src = cms.InputTag("hltPrunedGenParticlesWithStatusOne")
    
        ## packed gen particles
        from PhysicsTools.PatAlgos.slimming.packedGenParticles_cfi import packedGenParticles
        setattr(process,"hltPackedGenParticles",packedGenParticles.clone());
        getattr(process,"hltPackedGenParticles").inputCollection = cms.InputTag("hltPrunedGenParticlesWithStatusOne")
        getattr(process,"hltPackedGenParticles").map = cms.InputTag("hltPrunedGenParticles");
        getattr(process,"hltPackedGenParticles").inputOriginal = cms.InputTag("genParticles")

        ## slimmedGenJetoptions.s including neutrinos
        from PhysicsTools.PatAlgos.slimming.slimmedGenJets_cfi import slimmedGenJets
        setattr(process,"hltSlimmedGenJets",slimmedGenJets.clone());
        getattr(process,"hltSlimmedGenJets").cut = cms.string("pt > "+str(minPtGenJets)+" && abs(eta) < "+str(maxEtaGenJets));
        getattr(process,"hltSlimmedGenJets").src = cms.InputTag("ak4GenJets");
        getattr(process,"hltSlimmedGenJets").packedGenParticles = cms.InputTag("hltPackedGenParticles");

        ## Hadron flavor for GEN jets                                                                                                                                                         
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetPartons
        setattr(process,"hltSlimmedGenJetsPartons",patJetPartons.clone());
        getattr(process,"hltSlimmedGenJetsPartons").particles = cms.InputTag("hltPrunedGenParticles");
  
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetFlavourAssociation
        setattr(process,"hltSlimmedGenJetsFlavourAssociation",patJetFlavourAssociation.clone());
        getattr(process,"hltSlimmedGenJetsFlavourAssociation").jets = cms.InputTag("ak4GenJets");
        getattr(process,"hltSlimmedGenJetsFlavourAssociation").bHadrons = cms.InputTag("hltSlimmedGenJetsPartons","bHadrons");
        getattr(process,"hltSlimmedGenJetsFlavourAssociation").cHadrons = cms.InputTag("hltSlimmedGenJetsPartons","cHadrons");
        getattr(process,"hltSlimmedGenJetsFlavourAssociation").partons = cms.InputTag("hltSlimmedGenJetsPartons","physicsPartons");
        getattr(process,"hltSlimmedGenJetsFlavourAssociation").leptons = cms.InputTag("hltSlimmedGenJetsPartons","leptons");

        ## GenJets flavor info
        from PhysicsTools.PatAlgos.slimming.slimmedGenJetsFlavourInfos_cfi import slimmedGenJetsFlavourInfos
        setattr(process,"hltSlimmedGenJetsFlavourInfos",slimmedGenJetsFlavourInfos.clone());
        getattr(process,"hltSlimmedGenJetsFlavourInfos").genJetFlavourInfos = cms.InputTag("hltSlimmedGenJetsFlavourAssociation");
        getattr(process,"hltSlimmedGenJetsFlavourInfos").genJets = cms.InputTag("ak4GenJets");
        getattr(process,"hltSlimmedGenJetsFlavourInfos").slimmedGenJets = cms.InputTag("hltSlimmedGenJets");
        getattr(process,"hltSlimmedGenJetsFlavourInfos").slimmedGenJetAssociation = cms.InputTag("hltSlimmedGenJets","slimmedGenJetAssociation");

        ## Sequence for operations
        if hasattr(process,pathName):            
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPrunedGenParticlesWithStatusOne")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPrunedGenParticles")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPackedGenParticles")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedGenJets")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedGenJetsPartons")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedGenJetsFlavourAssociation")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedGenJetsFlavourInfos")+process.hltBoolEnd);

        ## Update output file
        if hasattr(process,outputModuleName):
            getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltPrunedGenParticles_*_*"+options.hltProcessName+"*");
            getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltPackedGenParticles_*_*"+options.hltProcessName+"*");
            getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltSlimmedGenJets_*_*"+options.hltProcessName+"*");
            getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltSlimmedGenJetsFlavourInfos_*_*"+options.hltProcessName+"*");

            getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
                branch = cms.untracked.string('recoGenParticles_hltPrunedGenParticles__*'),
                splitLevel = cms.untracked.int32(99)
            ));
            getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
                branch = cms.untracked.string('patPackedGenParticles_hltPackedGenParticles__*'),
                splitLevel = cms.untracked.int32(99)
            ));
            getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
                branch = cms.untracked.string('recoGenJets_hltSlimmedGenJets__*'),
                splitLevel = cms.untracked.int32(99)
            ));

    ##### Packed PF candidates
    from PhysicsTools.PatAlgos.slimming.offlineSlimmedPrimaryVertices_cfi import offlineSlimmedPrimaryVertices
    setattr(process,"hltOfflineSlimmedPrimaryVertices",offlineSlimmedPrimaryVertices.clone());
    getattr(process,"hltOfflineSlimmedPrimaryVertices").src = cms.InputTag("hltVerticesPFFilter")
    getattr(process,"hltOfflineSlimmedPrimaryVertices").score = cms.InputTag("hltPrimaryVertexAssociation","original")
        
    ## Packed PF candidates
    from PhysicsTools.PatAlgos.slimming.packedPFCandidates_cfi import packedPFCandidates
    setattr(process,"hltPackedPFCandidates",packedPFCandidates.clone());
    getattr(process,"hltPackedPFCandidates").inputCollection = cms.InputTag("hltParticleFlow");
    getattr(process,"hltPackedPFCandidates").inputVertices = cms.InputTag("hltOfflineSlimmedPrimaryVertices");
    getattr(process,"hltPackedPFCandidates").originalVertices = cms.InputTag("hltVerticesPFFilter");
    getattr(process,"hltPackedPFCandidates").originalTracks = cms.InputTag("hltPFMuonMerging");
    getattr(process,"hltPackedPFCandidates").vertexAssociator = cms.InputTag("hltPrimaryVertexAssociation","original");
    getattr(process,"hltPackedPFCandidates").PuppiSrc = cms.InputTag("");
    getattr(process,"hltPackedPFCandidates").PuppiNoLepSrc = cms.InputTag("");
    getattr(process,"hltPackedPFCandidates").chargedHadronIsolation = cms.InputTag("");
    getattr(process,"hltPackedPFCandidates").secondaryVerticesForWhiteList =  cms.VInputTag(cms.InputTag("hltDeepInclusiveMergedVerticesPF"));
    getattr(process,"hltPackedPFCandidates").covarianceVersion = cms.int32(0);
    ## Slimmed secondary vertexes
    from PhysicsTools.PatAlgos.slimming.slimmedSecondaryVertices_cfi import slimmedSecondaryVertices
    setattr(process,"hltSlimmedSecondaryVertices",slimmedSecondaryVertices.clone());
    getattr(process,"hltSlimmedSecondaryVertices").src = cms.InputTag("hltDeepInclusiveMergedVerticesPF");
    getattr(process,"hltSlimmedSecondaryVertices").packedPFCandidates = cms.InputTag("hltPackedPFCandidates");
  
    if hasattr(process,pathName):
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPrimaryVertexAssociation")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltOfflineSlimmedPrimaryVertices")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPackedPFCandidates")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedSecondaryVertices")+process.hltBoolEnd);

    if(hasattr(process,outputModuleName)):
        getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltOfflineSlimmedPrimaryVertices"+"*_*_*"+options.hltProcessName+"*");
        getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltPackedPFCandidates"+"*_*_*"+options.hltProcessName+"*");
        getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltSlimmedSecondaryVertices"+"*_*_*"+options.hltProcessName+"*");

    getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
        branch = cms.untracked.string('recoVertexs_hltSlimmedSecondaryVertices__*'),
        splitLevel = cms.untracked.int32(99)
    ));

    getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
        branch = cms.untracked.string('recoVertexs_hltOfflineSlimmedPrimaryVertices__*'),
        splitLevel = cms.untracked.int32(99)
    ));
    
    getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
        branch = cms.untracked.string('patPackedCandidates_hltPackedPFCandidates__*'),
        splitLevel = cms.untracked.int32(99)
    ));

    if isMC:
        ## parton jet flavour
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetPartonsLegacy
        setattr(process,"hltPatJetPartonsLegacy",patJetPartonsLegacy.clone());
        getattr(process,"hltPatJetPartonsLegacy").src = cms.InputTag("hltPrunedGenParticles");

        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetPartonAssociationLegacy
        setattr(process,"hltPatJetPartonAssociationLegacy",patJetPartonAssociationLegacy.clone());
        getattr(process,"hltPatJetPartonAssociationLegacy").jets = cms.InputTag(jetCollection);                              
        getattr(process,"hltPatJetPartonAssociationLegacy").partons = cms.InputTag("hltPatJetPartonsLegacy");                              

        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetFlavourAssociationLegacy                                                            
        setattr(process,"hltPatJetFlavourAssociationLegacy",patJetFlavourAssociationLegacy.clone());
        getattr(process,"hltPatJetFlavourAssociationLegacy").srcByReference = cms.InputTag("hltPatJetPartonAssociationLegacy");

        ## hadron jet flavour
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetPartons
        setattr(process,"hltPatJetPartons",patJetPartons.clone());
        getattr(process,"hltPatJetPartons").particles = cms.InputTag("hltPrunedGenParticles");
  
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetFlavourAssociation
        setattr(process,"hltPatJetFlavourAssociation",patJetFlavourAssociation.clone());
        getattr(process,"hltPatJetFlavourAssociation").jets = cms.InputTag(jetCollection);
        getattr(process,"hltPatJetFlavourAssociation").bHadrons = cms.InputTag("hltPatJetPartons","bHadrons");
        getattr(process,"hltPatJetFlavourAssociation").cHadrons = cms.InputTag("hltPatJetPartons","cHadrons");
        getattr(process,"hltPatJetFlavourAssociation").partons = cms.InputTag("hltPatJetPartons","physicsPartons");
        getattr(process,"hltPatJetFlavourAssociation").leptons = cms.InputTag("hltPatJetPartons","leptons");
        
        ## matching with gen-jets
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetMatch_cfi import patJetGenJetMatch
        setattr(process,"hltPatJetGenJetMatch",patJetGenJetMatch.clone());
        getattr(process,"hltPatJetGenJetMatch").src = cms.InputTag(jetCollection);
        getattr(process,"hltPatJetGenJetMatch").matched = cms.InputTag("hltSlimmedGenJets");

        ## matching with partons
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetMatch_cfi import patJetPartonMatch
        setattr(process,"hltPatJetPartonMatch",patJetPartonMatch.clone());
        getattr(process,"hltPatJetPartonMatch").src = cms.InputTag(jetCollection);
        getattr(process,"hltPatJetPartonMatch").matched = cms.InputTag("hltPrunedGenParticles");
    
    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    setattr(process,"hltPatJetCorrFactorsProducer",JetCorrFactorsProducer.clone());
    getattr(process,"hltPatJetCorrFactorsProducer").extraJPTOffset = cms.string('L1FastJet');
    if isMC:
        getattr(process,"hltPatJetCorrFactorsProducer").levels = cms.vstring('L1FastJet','L2Relative','L3Absolute');
    else:
        getattr(process,"hltPatJetCorrFactorsProducer").levels = cms.vstring('L1FastJet','L2Relative','L3Absolute','L2L3Residual');
    getattr(process,"hltPatJetCorrFactorsProducer").payload = cms.string('AK4PFHLT');
    getattr(process,"hltPatJetCorrFactorsProducer").primaryVertices = cms.InputTag("hltVerticesPFFilter");
    getattr(process,"hltPatJetCorrFactorsProducer").rho = cms.InputTag("hltFixedGridRhoFastjetAll");
    getattr(process,"hltPatJetCorrFactorsProducer").useNPV = cms.bool(False);
    getattr(process,"hltPatJetCorrFactorsProducer").useRho = cms.bool(True);
    getattr(process,"hltPatJetCorrFactorsProducer").src = cms.InputTag(jetCollection);   

    ## DeepCSV and DeepJet
    setattr(process,"hltDeepCSVJetTags",process.hltDeepCombinedSecondaryVertexBJetTagsPF.clone());
    setattr(process,"hltDeepFlavourJetTags",process.hltPFDeepFlavourJetTags.clone());

    if addLastPNETEvaluation:
        setattr(process,"hltParticleNetONNXLastJetTags",process.hltParticleNetONNXJetTags.clone());
        getattr(process,"hltParticleNetONNXLastJetTags").preprocess_json = cms.string("ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/ParticleNetAK4/preprocess.json")
        getattr(process,"hltParticleNetONNXLastJetTags").model_path = cms.FileInPath("ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/ParticleNetAK4/particle-net.onnx")
        getattr(process,"hltParticleNetONNXLastJetTags").flav_names = cms.vstring(
            "probtauhp",
            "probtauhm",
            "probb",
            "probc",
            "probuds",
            "probg",
            "ptcorr"
        )
        
    ## pat-jets
    from PhysicsTools.PatAlgos.producersLayer1.jetProducer_cfi import _patJets
    setattr(process,"hltPatJets",_patJets.clone());
    getattr(process,"hltPatJets").jetSource = cms.InputTag(jetCollection);
    getattr(process,"hltPatJets").addAssociatedTracks = cms.bool(False);
    getattr(process,"hltPatJets").addJetCharge = cms.bool(False);
    getattr(process,"hltPatJets").addGenJetMatch = cms.bool(True);
    getattr(process,"hltPatJets").addJetCorrFactors    = cms.bool(True);
    getattr(process,"hltPatJets").jetCorrFactorsSource = cms.VInputTag("hltPatJetCorrFactorsProducer");
    getattr(process,"hltPatJets").addJetFlavourInfo = cms.bool(True);

    if isMC:
        getattr(process,"hltPatJets").embedGenJetMatch = cms.bool(True);
        getattr(process,"hltPatJets").genJetMatch = cms.InputTag("hltPatJetGenJetMatch");
        getattr(process,"hltPatJets").addGenPartonMatch = cms.bool(True);
        getattr(process,"hltPatJets").embedGenPartonMatch = cms.bool(False);
        getattr(process,"hltPatJets").genPartonMatch = cms.InputTag("hltPatJetPartonMatch");
        getattr(process,"hltPatJets").JetFlavourInfoSource = cms.InputTag("hltPatJetFlavourAssociation");
        getattr(process,"hltPatJets").getJetMCFlavour = cms.bool(True);
        getattr(process,"hltPatJets").useLegacyJetMCFlavour = cms.bool(False);
        getattr(process,"hltPatJets").JetPartonMapSource = cms.InputTag("hltPatJetFlavourAssociationLegacy");
    else:
        getattr(process,"hltPatJets").addGenJetMatch = cms.bool(False);
        getattr(process,"hltPatJets").embedGenJetMatch = cms.bool(False);
        getattr(process,"hltPatJets").genJetMatch = cms.InputTag("");
        getattr(process,"hltPatJets").addGenPartonMatch = cms.bool(False);
        getattr(process,"hltPatJets").embedGenPartonMatch = cms.bool(False);
        getattr(process,"hltPatJets").genPartonMatch = cms.InputTag("");
        getattr(process,"hltPatJets").addJetFlavourInfo = cms.bool(False);
        getattr(process,"hltPatJets").JetFlavourInfoSource = cms.InputTag("");
        getattr(process,"hltPatJets").getJetMCFlavour = cms.bool(False);
        getattr(process,"hltPatJets").useLegacyJetMCFlavour = cms.bool(False);
        getattr(process,"hltPatJets").JetPartonMapSource = cms.InputTag("");

    
    getattr(process,"hltPatJets").addBTagInfo          = cms.bool(True);                                                                                                   
    getattr(process,"hltPatJets").addDiscriminators    = cms.bool(True);                                                                                                    
    getattr(process,"hltPatJets").addTagInfos          = cms.bool(True);
    getattr(process,"hltPatJets").discriminatorSources = cms.VInputTag(                                                                                                                 
        cms.InputTag("hltParticleNetONNXJetTags","probb"),                                                                                                                                 
        cms.InputTag("hltParticleNetONNXJetTags","probc"),                                                                                                                                 
        cms.InputTag("hltParticleNetONNXJetTags","probuds"),                                                                                                                                 
        cms.InputTag("hltParticleNetONNXJetTags","probg"),                                                                                                                                 
        cms.InputTag("hltParticleNetONNXJetTags","probtauh"),                                     
        cms.InputTag("hltDeepCSVJetTags","probb"),                                                                                                                                 
        cms.InputTag("hltDeepCSVJetTags","probc"),                                                                                                                                 
        cms.InputTag("hltDeepCSVJetTags","probudsg"),
        cms.InputTag("hltDeepFlavourJetTags","probb"),                                                                                                                                 
        cms.InputTag("hltDeepFlavourJetTags","probbb"),                                                                                                                                 
        cms.InputTag("hltDeepFlavourJetTags","problepb"),                                                                                                                                 
        cms.InputTag("hltDeepFlavourJetTags","probc"),                                                                                                                                 
        cms.InputTag("hltDeepFlavourJetTags","probuds"),                                                                                                                              
        cms.InputTag("hltDeepFlavourJetTags","probg")                                                                                                                              
    );   

    if addLastPNETEvaluation:
        getattr(process,"hltPatJets").discriminatorSources.extend([
            cms.InputTag("hltParticleNetONNXLastJetTags","probtauhp"), 
            cms.InputTag("hltParticleNetONNXLastJetTags","probtauhm"),                                                                                                                              
            cms.InputTag("hltParticleNetONNXLastJetTags","probb"),                                                                                                                                 
            cms.InputTag("hltParticleNetONNXLastJetTags","probc"),                                                                                                                                 
            cms.InputTag("hltParticleNetONNXLastJetTags","probuds"),                                                                                                                                 
            cms.InputTag("hltParticleNetONNXLastJetTags","probg"),                                                                                                                                 
            cms.InputTag("hltParticleNetONNXLastJetTags","ptcorr")
        ])

    ## selected patJets with minimum pT threshold
    from PhysicsTools.PatAlgos.selectionLayer1.jetSelector_cfi import selectedPatJets
    setattr(process,"hltSelectedPatJets",selectedPatJets.clone());
    getattr(process,"hltSelectedPatJets").src = cms.InputTag("hltPatJets");
    getattr(process,"hltSelectedPatJets").cut = cms.string("pt > "+str(options.jetPtMin)+" && abs(eta) < "+str(options.jetEtaMax));
    
    ## convert to slimmed jets without dropping info
    from PhysicsTools.PatAlgos.slimming.slimmedJets_cfi import slimmedJets
    setattr(process,"hltSlimmedJets",slimmedJets.clone());
    getattr(process,"hltSlimmedJets").src = cms.InputTag("hltSelectedPatJets");
    getattr(process,"hltSlimmedJets").packedPFCandidates = cms.InputTag("hltPackedPFCandidates");
    getattr(process,"hltSlimmedJets").dropTagInfos = cms.string("0");
    getattr(process,"hltSlimmedJets").dropJetVars  = cms.string("0");
    getattr(process,"hltSlimmedJets").dropTrackRefs = cms.string('0');
    
    if(hasattr(process,pathName)):
        if isMC:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetPartonsLegacy")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetPartonAssociationLegacy")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetFlavourAssociationLegacy")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetPartons")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetFlavourAssociation")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetGenJetMatch")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetPartonMatch")+process.hltBoolEnd);
            
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetCorrFactorsProducer")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltDeepCSVJetTags")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltDeepFlavourJetTags")+process.hltBoolEnd);
        if addLastPNETEvaluation:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltParticleNetONNXLastJetTags")+process.hltBoolEnd);            
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJets")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSelectedPatJets")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedJets")+process.hltBoolEnd);
        
    if(hasattr(process,outputModuleName)):
        getattr(process,outputModuleName).outputCommands.append("keep *pat*Jets*_*"+"hltSlimmedJets"+"*_*_*"+options.hltProcessName+"*");
        getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
            branch = cms.untracked.string('patJets_hltSlimmedJets__*'),
            splitLevel = cms.untracked.int32(99)
        ));

    return process;
    


