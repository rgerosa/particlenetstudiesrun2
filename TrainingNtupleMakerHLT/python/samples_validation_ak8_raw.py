def AddQCDSamples(samples):

    samples['QCD_PT-170to300'] = [
        '/QCD_PT-170to300_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_PT-300to470'] = [
        '/QCD_PT-300to470_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
    samples['QCD_PT-470to600'] = [
        '/QCD_PT-470to600_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_PT-600to800'] = [
        '/QCD_PT-600to800_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_PT-800to1000'] = [
        '/QCD_PT-800to1000_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_PT-1000to1400'] = [
        '/QCD_PT-1000to1400_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddHHSamples(samples):


    samples['GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-RnD_126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddXtoHHSamples(samples):

    samples['XtoHH_pythia8_13p6TeV_part1'] = [
        'XtoHH_pythia8_13p6TeV_part1',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=300','resonanceMassMax=600','resonanceMassStepSize=25','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part2'] = [
        'XtoHH_pythia8_13p6TeV_part2',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=600','resonanceMassMax=900','resonanceMassStepSize=25','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part3'] = [
        'XtoHH_pythia8_13p6TeV_part3',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=900','resonanceMassMax=1400','resonanceMassStepSize=50','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]

    samples['XtoHH_pythia8_13p6TeV_part4'] = [
        'XtoHH_pythia8_13p6TeV_part4',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True','resonanceMassMin=1400','resonanceMassMax=2000','resonanceMassStepSize=50','higgsMassMin=40','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=20'],
        1000,
        1000000,
        "tree.root"
    ]
    
    
def AddEphemeralHLTSamples(samples):
    
    samples["EphemeralHLTPhysics0-Run2022G"] = [
        '/EphemeralHLTPhysics0/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics1-Run2022G"] = [
        '/EphemeralHLTPhysics1/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics2-Run2022G"] = [
        '/EphemeralHLTPhysics2/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics3-Run2022G"] = [
        '/EphemeralHLTPhysics3/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics4-Run2022G"] = [
        '/EphemeralHLTPhysics4/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics5-Run2022G"] = [
        '/EphemeralHLTPhysics5/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics6-Run2022G"] = [
        '/EphemeralHLTPhysics6/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics7-Run2022G"] = [
        '/EphemeralHLTPhysics7/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics8-Run2022G"] = [
        '/EphemeralHLTPhysics8/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

def AddEphemeralZeroBiasSamples(samples):
    
    samples["EphemeralZeroBias0-Run2022G"] = [
        '/EphemeralZeroBias0/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias1-Run2022G"] = [
        '/EphemeralZeroBias1/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias2-Run2022G"] = [
        '/EphemeralZeroBias2/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias3-Run2022G"] = [
        '/EphemeralZeroBias3/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias4-Run2022G"] = [
        '/EphemeralZeroBias4/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias5-Run2022G"] = [
        '/EphemeralZeroBias5/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias6-Run2022G"] = [
        '/EphemeralZeroBias6/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias7-Run2022G"] = [
        '/EphemeralZeroBias7/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias8-Run2022G"] = [
        '/EphemeralZeroBias8/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

def AddAllSamples(samples):
    AddHHSamples(samples)
    AddQCDSamples(samples)
    AddXtoHHSamples(samples)
    AddEphemeralHLTSamples(samples)
    AddEphemeralZeroBiasSamples(samples)
    
