### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register(
    'useLocalInputFile', False, VarParsing.multiplicity.singleton,VarParsing.varType.bool,
    "use local input files");

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'datasetType',"MC126X",VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'identifier of the dataset that needs to be processed');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'hltProcessName',"HLTX",VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'name of the hlt process to be considered');

options.register (
    'saveL1Objects',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True, L1 objects are saved --> only valid for private miniAOD');

options.register (
    'saveLHEObjects',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True, LHE objects are saved --> turn-on only for simulated samples coming from ME');

options.register (
    'useOfflinePuppiJets',True,VarParsing.multiplicity.singleton,VarParsing.varType.bool,
    "use puppi ak4 jets offline rather then CHS");

options.register (
    'muonPtMin',15.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',15.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for offline and online jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for offline and online jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for offline and online jets');

options.register (
    'dumpOnlyJetMatchedToGen', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'dRJetGenMatch',0.4,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets');


options.parseArguments()

# Define the CMSSW process
process = cms.Process("DnnTree")

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

if options.useLocalInputFile:
    file_list = ["file:"+s for s in options.inputFiles]
    process.source.fileNames = cms.untracked.vstring(file_list)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

isMC = False;
from Configuration.AlCa.GlobalTag import GlobalTag
if options.datasetType == "MC126X":
    process.GlobalTag = GlobalTag(process.GlobalTag, '126X_mcRun3_2023_forPU65_v4', '')
    isMC=True;
elif options.datasetType == "MC124X":
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic_postEE', '')
    isMC=True;
elif options.datasetType == "Data2022" or options.datasetType == "ZeroBias2022":
    process.GlobalTag = GlobalTag(process.GlobalTag, '130X_dataRun3_HLT_v2','')
    isMC=False;
else:
    sys.exit("Dataset type not known, abort");


### produce GEN jets with neutrinos
if isMC:
    from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
    process.genParticlesForJets = genParticlesForJets.clone(
        src = cms.InputTag("packedGenParticles")
    )

    from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
    process.ak4GenJetsWithNu = ak4GenJets.clone(
    src = "genParticlesForJets"
    )

## deep tau evaluation                                                                                                                                                                                
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.                                                                                                                           
tauIdEmbedder.runTauID()

#######################################
### Evaluate the version of particleNet
#######################################

pnetDiscriminatorLabels = [];
pnetDiscriminatorNames = [];
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator

process.pfParticleNetAK4LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
    muons = cms.InputTag("slimmedMuons"),                                                            
    electrons = cms.InputTag("slimmedElectrons"),                                                            
    photons = cms.InputTag("slimmedPhotons"),                                                            
    taus = cms.InputTag("slimmedTaus"),                                                            
    vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
    secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
    jets = cms.InputTag("slimmedJetsPuppi" if options.useOfflinePuppiJets else "slimmedJets"),
    losttracks = cms.InputTag("lostTracks"),                                                         
    jet_radius = cms.double(0.4),
    min_jet_pt = cms.double(options.jetPtMin),
    min_jet_eta = cms.double(options.jetEtaMin),
    max_jet_eta = cms.double(options.jetEtaMax),
    min_pt_for_pfcandidates = cms.double(0.1), ## arbitrary                                                                                                            
    min_pt_for_track_properties = cms.double(-1),
    min_pt_for_losttrack = cms.double(1.0),
    max_dr_for_losttrack = cms.double(0.2),
    min_pt_for_taus = cms.double(options.tauPtMin),
    max_eta_for_taus = cms.double(2.5),
    dump_feature_tree = cms.bool(False)
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK4LastJetTags = boostedJetONNXJetTagsProducer.clone();
process.pfParticleNetAK4LastJetTags.src = cms.InputTag("pfParticleNetAK4LastJetTagInfos");
process.pfParticleNetAK4LastJetTags.flav_names = cms.vstring('probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p','probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow');
if options.useOfflinePuppiJets:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/particle-net.onnx');
else:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegOneTarget/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegOneTarget/particle-net.onnx');

pnetDiscriminatorNames.extend([
    "pfParticleNetAK4LastJetTags:probmu",
    "pfParticleNetAK4LastJetTags:probele",
    "pfParticleNetAK4LastJetTags:probtaup1h0p",
    "pfParticleNetAK4LastJetTags:probtaup1h1p",
    "pfParticleNetAK4LastJetTags:probtaup1h2p",
    "pfParticleNetAK4LastJetTags:probtaup3h0p",
    "pfParticleNetAK4LastJetTags:probtaup3h1p",
    "pfParticleNetAK4LastJetTags:probtaum1h0p",
    "pfParticleNetAK4LastJetTags:probtaum1h1p",
    "pfParticleNetAK4LastJetTags:probtaum1h2p",
    "pfParticleNetAK4LastJetTags:probtaum3h0p",
    "pfParticleNetAK4LastJetTags:probtaum3h1p",
    "pfParticleNetAK4LastJetTags:probb",
    "pfParticleNetAK4LastJetTags:probc",
    "pfParticleNetAK4LastJetTags:probuds",
    "pfParticleNetAK4LastJetTags:probg",
    "pfParticleNetAK4LastJetTags:ptcorr",
    "pfParticleNetAK4LastJetTags:ptreslow",
    "pfParticleNetAK4LastJetTags:ptreshigh"
])

pnetDiscriminatorLabels = [name.replace("pfParticleNetAK4LastJetTags:","") for name in pnetDiscriminatorNames]

## Update final jet collection                                                                                                                                                    
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = "slimmedJetsPuppi" if options.useOfflinePuppiJets else "slimmedJets",
    addJetCorrFactors = False,
    discriminatorSources = pnetDiscriminatorNames        
)

#### Final dumper
process.dnntree = cms.EDAnalyzer('ValidationNtupleMakerAK4',
        #### General flags
        xsec              = cms.double(options.xsec),
        isMC              = cms.bool(isMC),
        saveLHEObjects    = cms.bool(options.saveLHEObjects),
        dumpOnlyJetMatchedToGen = cms.bool(options.dumpOnlyJetMatchedToGen),
        pnetDiscriminatorLabels = cms.vstring(),
        pnetDiscriminatorNames  = cms.vstring(),
        ### Object selection (applied on miniAOD, HLT and Gen objects for jets)
        muonPtMin         = cms.double(options.muonPtMin),
        muonEtaMax        = cms.double(2.4),
        electronPtMin     = cms.double(options.electronPtMin),
        electronEtaMax    = cms.double(2.5),
        tauPtMin          = cms.double(options.tauPtMin),
        tauEtaMax         = cms.double(2.5),
        jetPtMin          = cms.double(options.jetPtMin),
        jetEtaMax         = cms.double(options.jetEtaMax),
        jetEtaMin         = cms.double(options.jetEtaMin),
        dRJetGenMatch     = cms.double(options.dRJetGenMatch),
        usePuppiJets      = cms.bool(options.useOfflinePuppiJets),
        ### Simulation quantities from miniAOD
        pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
        genEventInfo      = cms.InputTag("generator"),
        genParticles      = cms.InputTag("prunedGenParticles"),
        lheInfo           = cms.InputTag("externalLHEProducer"),
        ### miniAOD objects
        filterResults     = cms.InputTag("TriggerResults","", "PAT"),
        triggerResults    = cms.InputTag("TriggerResults","", options.hltProcessName),
        triggerL1Algos    = cms.InputTag("gtStage2Digis"),         
        stageL1Trigger    = cms.uint32(2),
        rho               = cms.InputTag("fixedGridRhoFastjetAll"),
        pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
        sVertices         = cms.InputTag("slimmedSecondaryVertices"),
        muons             = cms.InputTag("slimmedMuons"),
        electrons         = cms.InputTag("slimmedElectrons"),
        taus              = cms.InputTag("slimmedTaus"),
        jets              = cms.InputTag("slimmedJetsUpdated"),
        met               = cms.InputTag("slimmedMETs"),
        ### HLT objects
        hltJets           = cms.InputTag("hltSlimmedJets"),
        hltPVertices      = cms.InputTag("hltOfflineSlimmedPrimaryVertices"),
        hltSVertices      = cms.InputTag("hltSlimmedSecondaryVertices"),
        hltRho            = cms.InputTag("hltFixedGridRhoFastjetAll"),
        hltPixelJets      = cms.InputTag("hltAK4PixelOnlyPFJetsTightIDCorrected"),
        ### L1 objects                                                                                                                                          
        saveL1Objects     = cms.bool(options.saveL1Objects),
        triggerL1Jets     = cms.InputTag("caloStage2Digis","Jet"),
        triggerL1EtSum    = cms.InputTag("caloStage2Digis","EtSum"),
        triggerL1Muons    = cms.InputTag("gmtStage2Digis","Muon"),
        ### Gen jets
        genJetsWnu        = cms.InputTag("ak4GenJetsWithNu"),
        genJets           = cms.InputTag("slimmedGenJets"),
        genJetsFlavour    = cms.InputTag("slimmedGenJetsFlavourInfos"),
        hltGenJets        = cms.InputTag("hltSlimmedGenJets"),
        hltGenJetsFlavour = cms.InputTag("hltSlimmedGenJetsFlavourInfos"),
                            
)

for element in pnetDiscriminatorNames:
    process.dnntree.pnetDiscriminatorNames.append(element);
for element in pnetDiscriminatorLabels:
    process.dnntree.pnetDiscriminatorLabels.append(element);

process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

process.dnnTreePath = cms.Path(process.dnntree,process.edTask)
