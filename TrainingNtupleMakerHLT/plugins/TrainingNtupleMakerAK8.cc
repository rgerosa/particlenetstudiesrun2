// Standard C++ includes
#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>

// ROOT includes
#include <TTree.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

// CMSSW framework includes
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/ESGetToken.h"

// CMSSW data formats
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/METReco/interface/PFMET.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/Candidate/interface/VertexCompositePtrCandidate.h"
#include "DataFormats/L1Trigger/interface/Jet.h"
#include "DataFormats/L1Trigger/interface/Muon.h"
#include "DataFormats/L1Trigger/interface/EtSum.h"
#include "DataFormats/JetMatching/interface/JetFlavourInfoMatching.h"
#include "DataFormats/L1TGlobal/interface/GlobalAlgBlk.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/GenLumiInfoHeader.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/GenLumiInfoHeader.h"

// Other relevant CMSSW includes
#include "CommonTools/UtilAlgos/interface/TFileService.h" 

#include "CondFormats/L1TObjects/interface/L1TUtmTriggerMenu.h"
#include "CondFormats/DataRecord/interface/L1TUtmTriggerMenuRcd.h"

#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "HLTrigger/HLTcore/interface/HLTPrescaleProvider.h"

// For dumping b-tagging info
#include "DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h"
#include "RecoBTag/FeatureTools/interface/TrackInfoBuilder.h"
#include "RecoVertex/VertexTools/interface/VertexDistanceXY.h"
#include "RecoVertex/VertexTools/interface/VertexDistance3D.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"

class TrainingNtupleMakerAK8 : public edm::one::EDAnalyzer<edm::one::SharedResources, edm::one::WatchRuns> {

public:
  explicit TrainingNtupleMakerAK8(const edm::ParameterSet&);
  ~TrainingNtupleMakerAK8();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  bool applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi);
  bool applyPileupJetID(const pat::Jet & jet, const std::string & level);
    
private:

  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;  
  virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
  virtual void endRun(edm::Run const&, edm::EventSetup const&) override;

  void initializeBranches();

  // MC information
  const edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupInfoToken;
  const edm::EDGetTokenT<reco::GenParticleCollection > gensToken;
  const edm::EDGetTokenT<GenEventInfoProduct> genEvtInfoToken;
  const edm::EDGetTokenT<LHEEventProduct> lheInfoToken;

  // Filter result
  const edm::InputTag filterResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> filterResultsToken;
  const edm::InputTag triggerResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> triggerResultsToken;

  // HLT result
  const edm::EDGetTokenT<GlobalAlgBlkBxCollection> l1AlgosToken;
  const edm::EDGetTokenT<l1t::JetBxCollection> l1JetToken;
  const edm::EDGetTokenT<l1t::MuonBxCollection> l1MuonToken;
  const edm::EDGetTokenT<l1t::EtSumBxCollection> l1EtSumToken;
  const edm::ESGetToken<L1TUtmTriggerMenu, L1TUtmTriggerMenuRcd> l1GtMenuToken;

  // Vertices from miniAOD
  const edm::EDGetTokenT<reco::VertexCollection > primaryVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> secondaryVerticesToken;
  edm::EDGetTokenT< double > rhoToken;

  // Pat objects from miniAOD
  const edm::EDGetTokenT<pat::MuonCollection> muonsToken;
  const edm::EDGetTokenT<pat::ElectronCollection> electronsToken;
  const edm::EDGetTokenT<pat::TauCollection> tausToken;
  const edm::EDGetTokenT<pat::METCollection> metToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsAK4Token;
  const edm::EDGetTokenT<pat::JetCollection> jetsAK8Token;
  const std::string subJetCollectionName;
  
  // Vertices from HLT
  const edm::EDGetTokenT<reco::VertexCollection > primaryVerticesHLTToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> secondaryVerticesHLTToken;
  edm::EDGetTokenT< double > rhoHLTToken;

  // Trigger level objects
  const edm::EDGetTokenT<reco::PFJetCollection> jetsAK4HLTToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsAK8HLTToken;
  const std::string subJetCollectionNameHLT;

  // Transient track
  const edm::ESGetToken<TransientTrackBuilder, TransientTrackRecord> trackBuilderToken;

  // Gen jets
  const edm::EDGetTokenT<pat::JetCollection> genJetsAK8Token;
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsToken;
  const std::string genSubJetCollectionName;

  // MET filters
  std::vector<std::string>   filterPathsVector;
  std::map<std::string, int> filterPathsMap;

  // HLT paths
  std::map<std::string,int> triggerPathsMap;
  
  // L1 triggers
  std::map<std::string,std::vector<std::string> > triggerL1SeedMap;
  std::unique_ptr<HLTPrescaleProvider> hltPrescaleProvider;

  // Flag for the analyzer
  float muonPtMin, muonEtaMax;
  float electronPtMin, electronEtaMax;
  float tauPtMin, tauEtaMax;
  float jetPtAK4Min, jetPtAK8Min;
  float jetEtaMax, jetEtaMin;
  float jetPFCandidatePtMin;
  float dRJetGenMatch;
  bool  filterEvents;
  bool  dumpOnlyJetMatchedToGen;
  bool  saveLHEObjects;
  bool  saveL1Objects;
  bool  isMC;

  // Event coordinates
  unsigned event, run, lumi;
  // Pileup information
  unsigned int putrue;
  // Flags for various event filters
  unsigned int flags;
  // Cross-section and event weight information for MC events
  float xsec, wgt;
  // Rho
  float rho;
  float rho_hlt;
  // Generator-level information (Gen and LHE particles)
  std::vector<float>  gen_particle_pt;
  std::vector<float>  gen_particle_eta;
  std::vector<float>  gen_particle_phi;
  std::vector<float>  gen_particle_mass;
  std::vector<int>    gen_particle_id;
  std::vector<unsigned int> gen_particle_status;
  std::vector<int> gen_particle_mothers_id;
  std::vector<int>   gen_particle_daughters_id;
  std::vector<unsigned int> gen_particle_daughters_igen;
  std::vector<unsigned int> gen_particle_daughters_status;
  std::vector<float>  gen_particle_daughters_pt;
  std::vector<float>  gen_particle_daughters_eta;
  std::vector<float>  gen_particle_daughters_phi;
  std::vector<float>  gen_particle_daughters_mass;
  std::vector<int>    gen_particle_daughters_charge;
  std::vector<float>  lhe_particle_pt;
  std::vector<float>  lhe_particle_eta;
  std::vector<float>  lhe_particle_phi;
  std::vector<float>  lhe_particle_mass;
  std::vector<int>    lhe_particle_id;
  std::vector<unsigned int> lhe_particle_status;
  // HLT trigger
  std::vector<unsigned int> trigger_hlt_pass;
  std::vector<std::string>  trigger_hlt_path;
  std::vector<int>  trigger_hlt_prescale;  
  // L1 trigger
  std::vector<unsigned int> trigger_l1_pass;
  std::vector<std::string>  trigger_l1_seed;
  std::vector<int>  trigger_l1_prescale;
  std::vector<float> trigger_l1_jet_pt;
  std::vector<float> trigger_l1_jet_eta;
  std::vector<float> trigger_l1_jet_phi;
  std::vector<float> trigger_l1_jet_mass;
  std::vector<float> trigger_l1_muon_pt;
  std::vector<float> trigger_l1_muon_eta;
  std::vector<float> trigger_l1_muon_phi;
  std::vector<float> trigger_l1_muon_mass;
  std::vector<float> trigger_l1_muon_qual;
  std::vector<float> trigger_l1_muon_iso;
  float trigger_l1_ht;
  float trigger_l1_mht;
  // Vertexes
  unsigned int npv;
  unsigned int nsv;
  unsigned int npv_hlt;
  unsigned int nsv_hlt;
  // Collection of muon 4-vectors, muon ID bytes, muon isolation values
  std::vector<float>  muon_pt;
  std::vector<float>  muon_eta;
  std::vector<float>  muon_phi;
  std::vector<float>  muon_mass;
  std::vector<unsigned int> muon_id;
  std::vector<int>    muon_charge;
  std::vector<unsigned int> muon_iso;
  std::vector<float>  muon_d0;
  std::vector<float>  muon_dz;
  // Collection of electron 4-vectors, electron ID bytes, electron isolation values
  std::vector<float>  electron_pt;
  std::vector<float>  electron_eta;
  std::vector<float>  electron_phi;
  std::vector<float>  electron_mass;
  std::vector<unsigned int> electron_id;
  std::vector<int>    electron_charge;
  std::vector<float>  electron_d0;
  std::vector<float>  electron_dz;  
  // Collection of taus 4-vectors, tau ID bytes, tau isolation values
  std::vector<float>  tau_pt;
  std::vector<float>  tau_eta;
  std::vector<float>  tau_phi;
  std::vector<float>  tau_mass;
  std::vector<unsigned int>  tau_decaymode;
  std::vector<float>  tau_idjet;
  std::vector<float>  tau_idele;
  std::vector<float>  tau_idmu;
  std::vector<unsigned int> tau_idjet_wp;
  std::vector<unsigned int> tau_idmu_wp;
  std::vector<unsigned int> tau_idele_wp;
  std::vector<int>    tau_charge;  
  std::vector<float>  tau_genmatch_pt;
  std::vector<float>  tau_genmatch_eta;
  std::vector<float>  tau_genmatch_phi;
  std::vector<float>  tau_genmatch_mass;
  std::vector<int> tau_genmatch_decaymode;
  // MET
  float met, met_phi;
  // AK4 jet
  std::vector<float> jet_ak4_pt;
  std::vector<float> jet_ak4_eta;
  std::vector<float> jet_ak4_phi;
  std::vector<float> jet_ak4_mass;
  std::vector<float> jet_ak4_genmatch_pt;
  std::vector<float> jet_ak4_genmatch_eta;
  std::vector<float> jet_ak4_genmatch_phi;
  std::vector<float> jet_ak4_genmatch_mass;
  std::vector<float> jet_hlt_ak4_pt;
  std::vector<float> jet_hlt_ak4_eta;
  std::vector<float> jet_hlt_ak4_phi;
  std::vector<float> jet_hlt_ak4_mass;
  std::vector<float> jet_hlt_ak4_genmatch_pt;
  std::vector<float> jet_hlt_ak4_genmatch_eta;
  std::vector<float> jet_hlt_ak4_genmatch_phi;
  std::vector<float> jet_hlt_ak4_genmatch_mass;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_pt;
  std::vector<float> jet_eta;
  std::vector<float> jet_phi;
  std::vector<float> jet_mass;
  std::vector<float> jet_pt_raw;
  std::vector<float> jet_mass_raw;
  std::vector<float> jet_chf;
  std::vector<float> jet_nhf;
  std::vector<float> jet_elf;
  std::vector<float> jet_phf;
  std::vector<float> jet_muf;
  std::vector<float> jet_pnet_probXbb;
  std::vector<float> jet_pnet_probXcc;
  std::vector<float> jet_pnet_probXqq;
  std::vector<float> jet_pnet_probXgg;
  std::vector<float> jet_pnet_probQCDbb;
  std::vector<float> jet_pnet_probQCDcc;
  std::vector<float> jet_pnet_probQCDb;
  std::vector<float> jet_pnet_probQCDc;
  std::vector<float> jet_pnet_probQCDothers;
  std::vector<float> jet_pnet_probQCD;
  std::vector<float> jet_pnet_regression_mass;
  std::vector<unsigned int> jet_id;
  std::vector<unsigned int> jet_ncand;
  std::vector<unsigned int> jet_nch;
  std::vector<unsigned int> jet_nnh;
  std::vector<unsigned int> jet_nel;
  std::vector<unsigned int> jet_nph;
  std::vector<unsigned int> jet_nmu;
  std::vector<unsigned int> jet_hflav;
  std::vector<int> jet_pflav;
  std::vector<unsigned int> jet_nbhad;
  std::vector<unsigned int> jet_nchad;
  std::vector<float> jet_genmatch_pt;
  std::vector<float> jet_genmatch_eta;
  std::vector<float> jet_genmatch_phi;
  std::vector<float> jet_genmatch_mass;
  std::vector<float> jet_tau1;
  std::vector<float> jet_tau2;
  std::vector<float> jet_tau3;
  std::vector<float> jet_tau4;
  std::vector<float> jet_softdrop_pt;
  std::vector<float> jet_softdrop_eta;
  std::vector<float> jet_softdrop_phi;
  std::vector<float> jet_softdrop_mass;
  std::vector<float> jet_softdrop_pt_raw;
  std::vector<float> jet_softdrop_mass_raw;
  std::vector<float> jet_softdrop_genmatch_pt;
  std::vector<float> jet_softdrop_genmatch_eta;
  std::vector<float> jet_softdrop_genmatch_phi;
  std::vector<float> jet_softdrop_genmatch_mass;  
  std::vector<float> jet_softdrop_subjet_pt;
  std::vector<float> jet_softdrop_subjet_eta;
  std::vector<float> jet_softdrop_subjet_phi;
  std::vector<float> jet_softdrop_subjet_mass;
  std::vector<float> jet_softdrop_subjet_pt_raw;
  std::vector<float> jet_softdrop_subjet_mass_raw;
  std::vector<unsigned int> jet_softdrop_subjet_ijet; 
  std::vector<unsigned int> jet_softdrop_subjet_nbhad;
  std::vector<unsigned int> jet_softdrop_subjet_nchad;
  std::vector<unsigned int> jet_softdrop_subjet_hflav;
  std::vector<int> jet_softdrop_subjet_pflav;
  std::vector<float> jet_softdrop_subjet_genmatch_pt;
  std::vector<float> jet_softdrop_subjet_genmatch_eta;
  std::vector<float> jet_softdrop_subjet_genmatch_phi;
  std::vector<float> jet_softdrop_subjet_genmatch_mass; 
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_hlt_pt;
  std::vector<float> jet_hlt_eta;
  std::vector<float> jet_hlt_phi;
  std::vector<float> jet_hlt_mass;
  std::vector<float> jet_hlt_pt_raw;
  std::vector<float> jet_hlt_mass_raw;
  std::vector<float> jet_hlt_chf;
  std::vector<float> jet_hlt_nhf;
  std::vector<float> jet_hlt_elf;
  std::vector<float> jet_hlt_phf;
  std::vector<float> jet_hlt_muf;
  std::vector<float> jet_hlt_pnethlt_probXtt;
  std::vector<float> jet_hlt_pnethlt_probXbb;
  std::vector<float> jet_hlt_pnethlt_probXcc;
  std::vector<float> jet_hlt_pnethlt_probXqq;
  std::vector<float> jet_hlt_pnethlt_probXgg;
  std::vector<float> jet_hlt_pnethlt_probQCD;
  std::vector<unsigned int> jet_hlt_id;
  std::vector<unsigned int> jet_hlt_ncand;
  std::vector<unsigned int> jet_hlt_nch;
  std::vector<unsigned int> jet_hlt_nnh;
  std::vector<unsigned int> jet_hlt_nel;
  std::vector<unsigned int> jet_hlt_nph;
  std::vector<unsigned int> jet_hlt_nmu;
  std::vector<unsigned int> jet_hlt_hflav;
  std::vector<int> jet_hlt_pflav;
  std::vector<unsigned int> jet_hlt_nbhad;
  std::vector<unsigned int> jet_hlt_nchad;
  std::vector<float> jet_hlt_genmatch_pt;
  std::vector<float> jet_hlt_genmatch_eta;
  std::vector<float> jet_hlt_genmatch_phi;
  std::vector<float> jet_hlt_genmatch_mass;
  std::vector<float> jet_hlt_tau1;
  std::vector<float> jet_hlt_tau2;
  std::vector<float> jet_hlt_tau3;
  std::vector<float> jet_hlt_tau4;
  std::vector<float> jet_hlt_softdrop_pt;
  std::vector<float> jet_hlt_softdrop_eta;
  std::vector<float> jet_hlt_softdrop_phi;
  std::vector<float> jet_hlt_softdrop_mass;
  std::vector<float> jet_hlt_softdrop_pt_raw;
  std::vector<float> jet_hlt_softdrop_mass_raw;
  std::vector<float> jet_hlt_softdrop_genmatch_pt;
  std::vector<float> jet_hlt_softdrop_genmatch_eta;
  std::vector<float> jet_hlt_softdrop_genmatch_phi;
  std::vector<float> jet_hlt_softdrop_genmatch_mass;  
  std::vector<float> jet_hlt_softdrop_subjet_pt;
  std::vector<float> jet_hlt_softdrop_subjet_eta;
  std::vector<float> jet_hlt_softdrop_subjet_phi;
  std::vector<float> jet_hlt_softdrop_subjet_mass;
  std::vector<float> jet_hlt_softdrop_subjet_pt_raw;
  std::vector<float> jet_hlt_softdrop_subjet_mass_raw;
  std::vector<unsigned int> jet_hlt_softdrop_subjet_ijet; 
  std::vector<unsigned int> jet_hlt_softdrop_subjet_nbhad;
  std::vector<unsigned int> jet_hlt_softdrop_subjet_nchad;
  std::vector<unsigned int> jet_hlt_softdrop_subjet_hflav;
  std::vector<int> jet_hlt_softdrop_subjet_pflav;
  std::vector<float> jet_hlt_softdrop_subjet_genmatch_pt;
  std::vector<float> jet_hlt_softdrop_subjet_genmatch_eta;
  std::vector<float> jet_hlt_softdrop_subjet_genmatch_phi;
  std::vector<float> jet_hlt_softdrop_subjet_genmatch_mass; 
  // jet pf candidates
  std::vector<float> jet_hlt_pfcandidate_pt;
  std::vector<float> jet_hlt_pfcandidate_eta;
  std::vector<float> jet_hlt_pfcandidate_phi; 
  std::vector<float> jet_hlt_pfcandidate_mass;
  std::vector<float> jet_hlt_pfcandidate_energy;
  std::vector<float> jet_hlt_pfcandidate_calofraction;
  std::vector<float> jet_hlt_pfcandidate_hcalfraction;
  std::vector<float> jet_hlt_pfcandidate_dz;
  std::vector<float> jet_hlt_pfcandidate_dxy;
  std::vector<float> jet_hlt_pfcandidate_dzsig;
  std::vector<float> jet_hlt_pfcandidate_dxysig;
  std::vector<unsigned int> jet_hlt_pfcandidate_frompv;
  std::vector<unsigned int> jet_hlt_pfcandidate_npixhits;
  std::vector<unsigned int> jet_hlt_pfcandidate_nstriphits;
  std::vector<unsigned int> jet_hlt_pfcandidate_highpurity;
  std::vector<unsigned int> jet_hlt_pfcandidate_id;
  std::vector<unsigned int> jet_hlt_pfcandidate_ijet;
  std::vector<int> jet_hlt_pfcandidate_nlostinnerhits;
  std::vector<int> jet_hlt_pfcandidate_charge;
  std::vector<float> jet_hlt_pfcandidate_candjet_pperp_ratio;
  std::vector<float> jet_hlt_pfcandidate_candjet_ppara_ratio;
  std::vector<float> jet_hlt_pfcandidate_candjet_deta;
  std::vector<float> jet_hlt_pfcandidate_candjet_dphi;
  std::vector<float> jet_hlt_pfcandidate_candjet_etarel;
  std::vector<unsigned int> jet_hlt_pfcandidate_track_chi2;
  std::vector<unsigned int> jet_hlt_pfcandidate_track_qual;
  std::vector<float> jet_hlt_pfcandidate_trackjet_dxy;
  std::vector<float> jet_hlt_pfcandidate_trackjet_dxysig;
  std::vector<float> jet_hlt_pfcandidate_trackjet_d3d;
  std::vector<float> jet_hlt_pfcandidate_trackjet_d3dsig;
  std::vector<float> jet_hlt_pfcandidate_trackjet_dist;
  std::vector<float> jet_hlt_pfcandidate_trackjet_decayL;
  // jet-to-secondary vertex
  std::vector<float> jet_hlt_sv_pt;
  std::vector<float> jet_hlt_sv_eta;
  std::vector<float> jet_hlt_sv_phi;
  std::vector<float> jet_hlt_sv_mass;
  std::vector<float> jet_hlt_sv_energy;
  std::vector<float> jet_hlt_sv_chi2;
  std::vector<float> jet_hlt_sv_dxy;
  std::vector<float> jet_hlt_sv_dxysig;
  std::vector<float> jet_hlt_sv_d3d;
  std::vector<float> jet_hlt_sv_d3dsig;
  std::vector<unsigned int> jet_hlt_sv_ntrack;
  std::vector<unsigned int> jet_hlt_sv_ijet;


  // TTree carrying the event weight information
  TTree* tree;

  // Sorters to order object collections in decreasing order of pT
  template<typename T> 
  class PatPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i.pt() > j.pt());
    }
  };

  PatPtSorter<pat::Muon>     muonSorter;
  PatPtSorter<pat::Electron> electronSorter;
  PatPtSorter<pat::Tau>      tauSorter;
  PatPtSorter<pat::PackedCandidate>   packedPFCandidateSorter;
  PatPtSorter<reco::PFCandidate>  recoPFCandidateSorter;

  template<typename T> 
  class PatRefPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i->pt() > j->pt());
    }
  };

  PatRefPtSorter<pat::JetRef>      jetRefSorter;
  PatRefPtSorter<edm::Ptr<pat::Jet> > jetPtrSorter;
  PatRefPtSorter<reco::GenJetRef>  genJetRefSorter;
  PatRefPtSorter<reco::PFJetRef>  recoJetRefSorter;

  /// Distance between vertices
  template<typename T, typename A>
  static Measurement1D vertexDxy(const T &i, const A & j){
    VertexDistanceXY dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.distance(svtx,j);
  }


  template<typename T, typename A>
  static Measurement1D vertexDxySigned(const T &i, const A & j, const GlobalVector & ref){
    VertexDistanceXY dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.signedDistance(svtx,j,ref);
  }


  template<typename T, typename A>
  static Measurement1D vertexD3d(const T &i, const A & j){
    VertexDistance3D dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.distance(svtx,j);
  }

  template<typename T, typename A>
  static Measurement1D vertexD3dSigned(const T &i, const A & j, const GlobalVector & ref){
    VertexDistance3D dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.signedDistance(svtx,j,ref);
  }


  template<typename T, typename A>
  class VertexSorterDxy {
  public:
    
    VertexSorterDxy (const A & pv):
      pv_(pv){};

    // from https://cmssdt.cern.ch/lxr/source/RecoBTag/FeatureTools/src/deep_helpers.cc?v=CMSSW_11_3_X_2021-01-04-2300
    bool operator()(const T& i, const T& j) {      
      Measurement1D idxy = vertexDxy<T,A>(i,pv_);
      Measurement1D jdxy = vertexDxy<T,A>(j,pv_);
      float isig = abs(idxy.significance());
      float jsig = abs(jdxy.significance());
      if(std::isinf(isig) or std::isnan(isig)) isig = 0;
      if(std::isinf(jsig) or std::isnan(jsig)) jsig = 0;
      return isig > jsig;
    }

  private :
    A pv_;
  };
      
};

TrainingNtupleMakerAK8::TrainingNtupleMakerAK8(const edm::ParameterSet& iConfig): 
  pileupInfoToken          (mayConsume<std::vector<PileupSummaryInfo> >  (iConfig.getParameter<edm::InputTag>("pileUpInfo"))),
  gensToken                (mayConsume<reco::GenParticleCollection>      (iConfig.getParameter<edm::InputTag>("genParticles"))),
  genEvtInfoToken          (mayConsume<GenEventInfoProduct>              (iConfig.getParameter<edm::InputTag>("genEventInfo"))),
  lheInfoToken             (mayConsume<LHEEventProduct>                  (iConfig.getParameter<edm::InputTag>("lheInfo"))),
  filterResultsTag         (iConfig.getParameter<edm::InputTag>("filterResults")),
  filterResultsToken       (consumes<edm::TriggerResults>      (filterResultsTag)),
  triggerResultsTag        (iConfig.getParameter<edm::InputTag>("triggerResults")),
  triggerResultsToken      (consumes<edm::TriggerResults>      (triggerResultsTag)),
  l1AlgosToken             (consumes<GlobalAlgBlkBxCollection>         (iConfig.getParameter<edm::InputTag>("triggerL1Algos"))),
  l1JetToken               (consumes<l1t::JetBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1Jets"))),
  l1MuonToken              (consumes<l1t::MuonBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1Muons"))),
  l1EtSumToken             (consumes<l1t::EtSumBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1EtSum"))),
  l1GtMenuToken            (esConsumes<L1TUtmTriggerMenu, L1TUtmTriggerMenuRcd>()),
  primaryVerticesToken     (consumes<reco::VertexCollection>           (iConfig.getParameter<edm::InputTag>("pVertices"))),
  secondaryVerticesToken   (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("sVertices"))),
  rhoToken                 (consumes<double>(iConfig.getParameter<edm::InputTag>("rho"))),
  muonsToken               (consumes<pat::MuonCollection>             (iConfig.getParameter<edm::InputTag>("muons"))), 
  electronsToken           (consumes<pat::ElectronCollection>         (iConfig.getParameter<edm::InputTag>("electrons"))), 
  tausToken                (consumes<pat::TauCollection>              (iConfig.getParameter<edm::InputTag>("taus"))), 
  metToken                 (consumes<pat::METCollection >             (iConfig.getParameter<edm::InputTag>("met"))),
  jetsAK4Token             (consumes<pat::JetCollection >             (iConfig.getParameter<edm::InputTag>("jetsAK4"))),
  jetsAK8Token             (consumes<pat::JetCollection >             (iConfig.getParameter<edm::InputTag>("jetsAK8"))),
  subJetCollectionName     (iConfig.existsAs<std::string>("subJetCollectionName") ? iConfig.getParameter<std::string>("subJetCollectionName") : "SoftDropPuppi"),
  primaryVerticesHLTToken   (mayConsume<reco::VertexCollection >      (iConfig.getParameter<edm::InputTag>("hltPVertices"))),
  secondaryVerticesHLTToken (mayConsume<reco::VertexCompositePtrCandidateCollection> (iConfig.getParameter<edm::InputTag>("hltSVertices"))),
  rhoHLTToken              (mayConsume<double>(iConfig.getParameter<edm::InputTag>("hltRho"))),
  jetsAK4HLTToken          (mayConsume<reco::PFJetCollection >        (iConfig.getParameter<edm::InputTag>("hltJetsAK4"))),
  jetsAK8HLTToken          (mayConsume<pat::JetCollection >           (iConfig.getParameter<edm::InputTag>("hltJetsAK8"))),
  subJetCollectionNameHLT  (iConfig.existsAs<std::string>("subJetCollectionName") ? iConfig.getParameter<std::string>("subJetCollectionNameHLT") : "SoftDrop"),
  trackBuilderToken        (esConsumes<TransientTrackBuilder, TransientTrackRecord>(edm::ESInputTag("","TransientTrackBuilder"))), 
  genJetsAK8Token          (mayConsume<pat::JetCollection>  (iConfig.getParameter<edm::InputTag>("genJetsAK8"))),
  genJetsToken             (mayConsume<reco::GenJetCollection>  (iConfig.getParameter<edm::InputTag>("genJets"))),
  genSubJetCollectionName  (iConfig.existsAs<std::string>("genSubJetCollectionName") ? iConfig.getParameter<std::string>("genSubJetCollectionName") : "SoftDrop"),
  muonPtMin                (iConfig.existsAs<double>("muonPtMin")     ? iConfig.getParameter<double>("muonPtMin") : 10.),
  muonEtaMax               (iConfig.existsAs<double>("muonEtaMax")    ? iConfig.getParameter<double>("muonEtaMax") : 2.4),
  electronPtMin            (iConfig.existsAs<double>("electronPtMin")   ? iConfig.getParameter<double>("electronPtMin") : 15.),
  electronEtaMax           (iConfig.existsAs<double>("electronEtaMax")  ? iConfig.getParameter<double>("electronEtaMax") : 2.5),
  tauPtMin                 (iConfig.existsAs<double>("tauPtMin")        ? iConfig.getParameter<double>("tauPtMin") : 15.),
  tauEtaMax                (iConfig.existsAs<double>("tauEtaMax")       ? iConfig.getParameter<double>("tauEtaMax") : 2.5),
  jetPtAK4Min              (iConfig.existsAs<double>("jetPtAK4Min")     ? iConfig.getParameter<double>("jetPtAK4Min") : 20.),
  jetPtAK8Min              (iConfig.existsAs<double>("jetPtAK8Min")     ? iConfig.getParameter<double>("jetPtAK8Min") : 20.),
  jetEtaMax                (iConfig.existsAs<double>("jetEtaMax")       ? iConfig.getParameter<double>("jetEtaMax") : 2.5),
  jetEtaMin                (iConfig.existsAs<double>("jetEtaMin")       ? iConfig.getParameter<double>("jetEtaMin") : 0.0),
  jetPFCandidatePtMin      (iConfig.existsAs<double>("jetPFCandidatePtMin")    ? iConfig.getParameter<double>("jetPFCandidatePtMin") : 0.1),
  dRJetGenMatch            (iConfig.existsAs<double>("dRJetGenMatch")    ? iConfig.getParameter<double>("dRJetGenMatch") : 0.4),
  filterEvents             (iConfig.existsAs<bool>("filterEvents") ? iConfig.getParameter<bool>  ("filterEvents") : true),
  dumpOnlyJetMatchedToGen  (iConfig.existsAs<bool>("dumpOnlyJetMatchedToGen")  ? iConfig.getParameter<bool>  ("dumpOnlyJetMatchedToGen") : false),
  saveLHEObjects           (iConfig.existsAs<bool>("saveLHEObjects")    ? iConfig.getParameter<bool>  ("saveLHEObjects") : false),
  saveL1Objects            (iConfig.existsAs<bool>("saveL1Objects")  ? iConfig.getParameter<bool>  ("saveL1Objects") : true),
  isMC                     (iConfig.existsAs<bool>("isMC")    ? iConfig.getParameter<bool>  ("isMC") : true),
  xsec                     (iConfig.existsAs<double>("xsec")  ? iConfig.getParameter<double>("xsec") * 1000.0 : 1.){

  usesResource("TFileService");
  hltPrescaleProvider.reset(new HLTPrescaleProvider(iConfig,consumesCollector(),*this));

}

TrainingNtupleMakerAK8::~TrainingNtupleMakerAK8() {}

void TrainingNtupleMakerAK8::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  /// Trigger objects / filters
  edm::Handle<edm::TriggerResults> filterResultsH;
  iEvent.getByToken(filterResultsToken, filterResultsH); 
  edm::Handle<edm::TriggerResults> triggerResultsH;
  iEvent.getByToken(triggerResultsToken, triggerResultsH); 
  edm::Handle<GlobalAlgBlkBxCollection> l1AlgosH;
  iEvent.getByToken(l1AlgosToken,l1AlgosH);

  // Vertexes
  edm::Handle<reco::VertexCollection > primaryVerticesH;
  iEvent.getByToken(primaryVerticesToken, primaryVerticesH);
  edm::Handle<reco::VertexCompositePtrCandidateCollection> secondaryVerticesH;
  iEvent.getByToken(secondaryVerticesToken, secondaryVerticesH);
  reco::VertexCompositePtrCandidateCollection svColl = *secondaryVerticesH;
  edm::Handle<double> rho_val;
  iEvent.getByToken(rhoToken,rho_val);  

  edm::Handle<reco::VertexCollection > primaryVerticesHLTH;
  iEvent.getByToken(primaryVerticesHLTToken, primaryVerticesHLTH);
  edm::Handle<reco::VertexCompositePtrCandidateCollection> secondaryVerticesHLTH;
  reco::VertexCompositePtrCandidateCollection svColl_hlt;
  iEvent.getByToken(secondaryVerticesHLTToken, secondaryVerticesHLTH);
  svColl_hlt = *secondaryVerticesHLTH;
  edm::Handle<double> rho_hlt_val;
  iEvent.getByToken(rhoHLTToken,rho_hlt_val);  
  
  // Muons / Electrons / Photons
  edm::Handle<pat::MuonCollection> muonsH;
  iEvent.getByToken(muonsToken,muonsH);
  pat::MuonCollection muons = *muonsH;

  edm::Handle<pat::ElectronCollection> electronsH;
  iEvent.getByToken(electronsToken,electronsH);
  pat::ElectronCollection electrons = *electronsH;

  edm::Handle<pat::TauCollection> tausH;
  iEvent.getByToken(tausToken,tausH);
  pat::TauCollection taus = *tausH;

  edm::Handle<pat::METCollection> metH;
  iEvent.getByToken(metToken, metH);
  
  // L1 objects                                                                                                                                                                                       
  edm::Handle<l1t::MuonBxCollection>  l1MuonsH;
  edm::Handle<l1t::JetBxCollection>   l1JetsH;
  edm::Handle<l1t::EtSumBxCollection> l1EtSumH;
  if(saveL1Objects){
    iEvent.getByToken(l1JetToken,l1JetsH);
    iEvent.getByToken(l1MuonToken,l1MuonsH);
    iEvent.getByToken(l1EtSumToken,l1EtSumH);
  }
  
  // Jets 
  edm::Handle<pat::JetCollection> jetsAK8H;
  iEvent.getByToken(jetsAK8Token, jetsAK8H);
  edm::Handle<pat::JetCollection> jetsAK4H;
  iEvent.getByToken(jetsAK4Token, jetsAK4H);

  edm::Handle<pat::JetCollection> jetsAK8HLTH;
  edm::Handle<reco::PFJetCollection> jetsAK4HLTH;
  iEvent.getByToken(jetsAK8HLTToken, jetsAK8HLTH);
  iEvent.getByToken(jetsAK4HLTToken, jetsAK4HLTH);


  edm::ESHandle<TransientTrackBuilder> trackBuilderH;
  trackBuilderH = iSetup.getHandle(trackBuilderToken);

  // GEN Level info and LHE
  edm::Handle<vector<PileupSummaryInfo> > pileupInfoH;
  edm::Handle<GenEventInfoProduct> genEvtInfoH;
  edm::Handle<reco::GenParticleCollection> gensH;
  edm::Handle<pat::JetCollection> genJetsAK8H;
  edm::Handle<reco::GenJetCollection> genJetsH;
  if(isMC){
    iEvent.getByToken(pileupInfoToken, pileupInfoH);      
    iEvent.getByToken(genEvtInfoToken, genEvtInfoH);      
    iEvent.getByToken(gensToken, gensH);    
    iEvent.getByToken(genJetsAK8Token, genJetsAK8H);
    iEvent.getByToken(genJetsToken, genJetsH);
  }

  edm::Handle<LHEEventProduct> lheInfoH;
  if(isMC and saveLHEObjects) 
    iEvent.getByToken(lheInfoToken,lheInfoH);        
  
  edm::ESHandle<L1TUtmTriggerMenu> menu;
  menu = iSetup.getHandle(l1GtMenuToken);

  ///
  initializeBranches();

  if(filterEvents){
    bool acceptEvent = false;
    for (auto jets_iter = jetsAK8H->begin(); jets_iter != jetsAK8H->end(); ++jets_iter) { 
      if(jets_iter->pt() > jetPtAK8Min and fabs(jets_iter->eta()) < jetEtaMax and fabs(jets_iter->eta()) > jetEtaMin) 
	acceptEvent = true;
    }
    for (auto jets_iter = jetsAK8HLTH->begin(); jets_iter != jetsAK8HLTH->end(); ++jets_iter) { 
      if(jets_iter->pt() > jetPtAK8Min and fabs(jets_iter->eta()) < jetEtaMax and fabs(jets_iter->eta()) > jetEtaMin) 
	acceptEvent = true;
    }          
    if(not acceptEvent) return;
  }

  // Event information - MC weight, event ID (run, lumi, event) and so on
  event = iEvent.id().event();
  run   = iEvent.id().run();
  lumi  = iEvent.luminosityBlock();

  // MC weight
  wgt = 1.0;
  if(lheInfoH.isValid()){
    if(not lheInfoH->weights().empty())
      wgt = lheInfoH->weights()[0].wgt;
  }
  else if(genEvtInfoH.isValid())
    wgt = genEvtInfoH->weight(); 
  
  // Pileup information
  putrue = 0;
  if(pileupInfoH.isValid()){
    for (auto pileupInfo_iter = pileupInfoH->begin(); pileupInfo_iter != pileupInfoH->end(); ++pileupInfo_iter) {
      if (pileupInfo_iter->getBunchCrossing() == 0) 
	putrue = (unsigned int) pileupInfo_iter->getTrueNumInteractions();
    }
  }

  // Vertices RECO
  npv  = (unsigned int) primaryVerticesH->size();
  nsv  = (unsigned int) secondaryVerticesH->size();
  npv_hlt  = (unsigned int) primaryVerticesHLTH->size();
  nsv_hlt  = (unsigned int) secondaryVerticesHLTH->size();

  // Rho value
  rho = *rho_val;
  rho_hlt = *rho_hlt_val;

  // Missing energy
  met      = metH->front().corPt();
  met_phi  = metH->front().corPhi();

  // GEN particle informatio
  if(gensH.isValid()){
    unsigned int igen = 0;
    for (auto gens_iter = gensH->begin(); gens_iter != gensH->end(); ++gens_iter) {      

      if((abs(gens_iter->pdgId()) == 25 or abs(gens_iter->pdgId()) == 24 or abs(gens_iter->pdgId()) == 23 or abs(gens_iter->pdgId()) == 39) and
	 gens_iter->isLastCopy() and 
	 gens_iter->statusFlags().fromHardProcess()){ 
	
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

        for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
          gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
          gen_particle_daughters_igen.push_back(igen);
          gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
          gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
          gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
          gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
        }
        igen++;
      }

      // Final states Leptons (e,mu) and Neutrinos --> exclude taus. They need to be prompt or from Tau decay      
      if (abs(gens_iter->pdgId()) > 10 and abs(gens_iter->pdgId()) < 17 and abs(gens_iter->pdgId()) != 15  and 
	  (gens_iter->isPromptFinalState() or 
	   gens_iter->isDirectPromptTauDecayProductFinalState())) { 

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// No need to save daughters here
        igen++;
      }
      
      // Final state quarks or gluons from the hard process before the shower --> partons in which H/Z/W/top decay into
      if (((abs(gens_iter->pdgId()) >= 1 and abs(gens_iter->pdgId()) <= 5) or abs(gens_iter->pdgId()) == 21) and 
	  gens_iter->statusFlags().fromHardProcess() and 
	  gens_iter->statusFlags().isFirstCopy()){
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());
	igen++;
	// no need to save daughters
      }

      // Special case of taus: last-copy, from hard process and, prompt and decayed
      if(abs(gens_iter->pdgId()) == 15 and 
	 gens_iter->isLastCopy() and
	 gens_iter->statusFlags().fromHardProcess() and
	 gens_iter->isPromptDecayed()){ // hadronic taus

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// only store the final decay particles
	for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
	  if(not dynamic_cast<const reco::GenParticle*>(gens_iter->daughter(idau))->statusFlags().isPromptTauDecayProduct()) continue;
	  gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
	  gen_particle_daughters_igen.push_back(igen);
	  gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
	  gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
	  gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
	  gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());    
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
	}
	igen++;
      }  
    }
  }

  // LHE level information
  if(lheInfoH.isValid()){

    const auto& hepeup = lheInfoH->hepeup();
    const auto& pup    = hepeup.PUP;

    for (unsigned int i = 0, n = pup.size(); i < n; ++i) {
      int status = hepeup.ISTUP[i];
      int id     = hepeup.IDUP[i];
      TLorentzVector p4(pup[i][0], pup[i][1], pup[i][2], pup[i][3]);
      if(abs(id) == 39 or abs(id) == 25 or abs(id) == 23 or abs(id) == 24 or abs(id)==6){ // Higgs, X,  Z or W or top                                                                                 
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 10 && abs(id) <= 17 and status == 1){ // final state leptons                                                                                                             
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 1 && abs(id) <= 5 and status == 1){ // final state leptons                                                                                                                   
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
	lhe_particle_id.push_back(id);
      }
      else if(abs(id) == 21 and status == 1){ // final state leptons                                                                                                                              
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
    }
  }

  // MET filter info
  unsigned int flagvtx       = 1 * 1;
  unsigned int flaghalo      = 1 * 2;
  unsigned int flaghbhe      = 1 * 4;
  unsigned int flaghbheiso   = 1 * 8;
  unsigned int flagecaltp    = 1 * 16;
  unsigned int flagbadmuon   = 1 * 32; 
  unsigned int flagbadhad    = 1 * 64;
 
  // Which MET filters passed
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    if (filterPathsMap[filterPathsVector[i]] == -1) continue;
    if (i == 0  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagvtx       = 0; // goodVertices
    if (i == 1  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghalo      = 0; // CSCTightHaloFilter
    if (i == 2  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbhe      = 0; // HBHENoiseFilter
    if (i == 3  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbheiso   = 0; // HBHENoiseIsoFilter
    if (i == 4  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagecaltp    = 0; // EcalDeadCellTriggerPrimitiveFilter
    if (i == 5  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadmuon   = 0; // badmuon
    if (i == 6  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadhad    = 0; // badhadrons
  }

  flags = flagvtx + flaghalo + flaghbhe + flaghbheiso + flagecaltp + flagbadmuon + flagbadhad;

  // HLT triggers
  for(auto key : triggerPathsMap){
    trigger_hlt_path.push_back(key.first);
    trigger_hlt_pass.push_back(triggerResultsH->accept(key.second));
    auto result = hltPrescaleProvider->prescaleValuesInDetail<double>(iEvent, iSetup, key.first);
    trigger_hlt_prescale.push_back(result.second);
  }

  // L1 algos
  if(l1AlgosH.isValid()) {    
    // Access menu
    std::map<std::string,int> mapping_seed;
    for (auto keyval : menu->getAlgorithmMap()) { 
      std::string trigName  = keyval.second.getName();
      if(triggerL1SeedMap.find(trigName) != triggerL1SeedMap.end()){
	unsigned int index = keyval.second.getIndex();
	mapping_seed[trigName] = index;
      }
    }
    auto l1TriggerResult = l1AlgosH->at(0,0); // take BX 0
    for(auto trigger : mapping_seed){
      unsigned int passed = l1TriggerResult.getAlgoDecisionFinal(trigger.second) ; 
      trigger_l1_pass.push_back(passed);
      trigger_l1_seed.push_back(trigger.first);
      auto result = hltPrescaleProvider->prescaleValuesInDetail<double>(iEvent,iSetup,triggerL1SeedMap[trigger.first].front());
      for(auto iel: result.first){
	if(iel.first == trigger.first) {
	  trigger_l1_prescale.push_back(iel.second);
	  break;
	}
      }
    }    
  }

  if(l1JetsH.isValid()){
    for (auto l1Jet = l1JetsH->begin(0); l1Jet != l1JetsH->end(0); l1Jet++){
      if(l1Jet->pt() < jetPtAK4Min or fabs(l1Jet->eta()) > jetEtaMax or fabs(l1Jet->eta()) < jetEtaMin) continue;
      trigger_l1_jet_pt.push_back(l1Jet->pt());
      trigger_l1_jet_eta.push_back(l1Jet->eta());
      trigger_l1_jet_phi.push_back(l1Jet->phi());
      trigger_l1_jet_mass.push_back(l1Jet->mass());
    }
  }
  if(l1MuonsH.isValid()){
    for (auto l1Muon = l1MuonsH->begin(0); l1Muon != l1MuonsH->end(0); l1Muon++){
      if(l1Muon->pt() < 3 or fabs(l1Muon->eta()) > 2.4) continue;
      trigger_l1_muon_pt.push_back(l1Muon->pt());
      trigger_l1_muon_eta.push_back(l1Muon->eta());
      trigger_l1_muon_phi.push_back(l1Muon->phi());
      trigger_l1_muon_mass.push_back(l1Muon->mass());
      trigger_l1_muon_qual.push_back(l1Muon->hwQual());
      trigger_l1_muon_iso.push_back(l1Muon->hwIso());
    }
  }
  if(l1EtSumH.isValid()){
    for(auto l1EtSum = l1EtSumH->begin(0); l1EtSum != l1EtSumH->end(0); l1EtSum++){
      auto sumtype = static_cast<int>(l1EtSum->getType());
      if(sumtype == l1t::EtSum::kTotalHt)
        trigger_l1_ht = l1EtSum->et();
      if(sumtype == l1t::EtSum::kMissingHt)
        trigger_l1_mht = l1EtSum->et();
    }
  }

  // Sorting muons based on pT
  sort(muons.begin(),muons.end(),muonSorter);
  for (size_t i = 0; i < muons.size(); i++) {

    if(muons.at(i).pt() < muonPtMin) continue;
    if(fabs(muons.at(i).eta()) > muonEtaMax) continue;

    muon_pt.push_back(muons.at(i).pt());
    muon_eta.push_back(muons.at(i).eta());
    muon_phi.push_back(muons.at(i).phi());
    muon_mass.push_back(muons.at(i).mass());

    // Muon isolation
    int isoval = 0;
    if(muons.at(i).passed(reco::Muon::PFIsoLoose))
      isoval += 1;
    if(muons.at(i).passed(reco::Muon::PFIsoMedium))
      isoval += 2;
    if(muons.at(i).passed(reco::Muon::PFIsoTight))
      isoval += 4;
    if(muons.at(i).passed(reco::Muon::PFIsoVeryTight))
      isoval += 8;
    if(muons.at(i).passed(reco::Muon::MiniIsoLoose))
      isoval += 16;
    if(muons.at(i).passed(reco::Muon::MiniIsoMedium))
      isoval += 32;
    if(muons.at(i).passed(reco::Muon::MiniIsoTight))
      isoval += 64;    
    muon_iso.push_back(isoval);
    
    // Muon id
    int midval = 0;
    if(muons.at(i).passed(reco::Muon::CutBasedIdLoose))
      midval += 1;
    if(muons.at(i).passed(reco::Muon::CutBasedIdMedium))
      midval += 2;
    if(muons.at(i).passed(reco::Muon::CutBasedIdTight))
      midval += 4;
    if(muons.at(i).passed(reco::Muon::MvaLoose))
      midval += 8;
    if(muons.at(i).passed(reco::Muon::MvaMedium))
      midval += 16;
    if(muons.at(i).passed(reco::Muon::MvaTight))
      midval += 32;    
    muon_id.push_back(midval);

    muon_charge.push_back(muons.at(i).charge());
    muon_d0.push_back(muons.at(i).muonBestTrack()->dxy(primaryVerticesH->at(0).position()));
    muon_dz.push_back(muons.at(i).muonBestTrack()->dz(primaryVerticesH->at(0).position()));
  }
  
  // Sorting electrons based on pT
  sort(electrons.begin(),electrons.end(),electronSorter);

  for (size_t i = 0; i < electrons.size(); i++) {

    if(electrons.at(i).pt() < electronPtMin) continue;
    if(fabs(electrons.at(i).eta()) > electronEtaMax) continue;

    electron_pt.push_back(electrons.at(i).pt());
    electron_eta.push_back(electrons.at(i).eta());
    electron_phi.push_back(electrons.at(i).phi());
    electron_mass.push_back(electrons.at(i).mass());

    int eidval = 0;   
    if(electrons.at(i).electronID("cutBasedElectronID-Fall17-94X-V2-loose"))
      eidval += 1;
    if(electrons.at(i).electronID("cutBasedElectronID-Fall17-94X-V2-medium"))
      eidval += 2;
    if(electrons.at(i).electronID("cutBasedElectronID-Fall17-94X-V2-tight"))
      eidval += 4;
    if(electrons.at(i).electronID("mvaEleID-Fall17-iso-V2-wpLoose"))
      eidval += 8;
    if(electrons.at(i).electronID("mvaEleID-Fall17-iso-V2-wp90"))
      eidval += 16;
    if(electrons.at(i).electronID("mvaEleID-Fall17-iso-V2-wp80"))
      eidval += 32;

    electron_id.push_back(eidval);
    electron_charge.push_back(electrons.at(i).charge());
    electron_d0.push_back(electrons.at(i).gsfTrack()->dxy(primaryVerticesH->at(0).position()));
    electron_dz.push_back(electrons.at(i).gsfTrack()->dz(primaryVerticesH->at(0).position()));
  }

  // Sorting taus based on pT
  sort(taus.begin(),taus.end(),tauSorter);
  std::vector<math::XYZTLorentzVector> tau_pfcandidates;

  for (size_t i = 0; i < taus.size(); i++) {
    
    if(taus[i].pt() < tauPtMin) continue;
    if(fabs(taus[i].eta()) > tauEtaMax) continue;

    tau_pt.push_back(taus[i].pt());
    tau_eta.push_back(taus[i].eta());
    tau_phi.push_back(taus[i].phi());
    tau_mass.push_back(taus[i].mass());
    
    tau_decaymode.push_back(taus[i].decayMode());
    tau_idjet.push_back(taus[i].tauID("byDeepTau2017v2p1VSjetraw"));
    tau_idele.push_back(taus[i].tauID("byDeepTau2017v2p1VSeraw"));
    tau_idmu.push_back(taus[i].tauID("byDeepTau2017v2p1VSmuraw"));
    tau_charge.push_back(taus[i].charge());

    if(taus[i].isTauIDAvailable("byDeepTau2018v2p5VSjetraw"))
      tau_idjet.push_back(taus[i].tauID("byDeepTau2018v2p5VSjetraw"));
    else if(taus[i].isTauIDAvailable("byDeepTau2017v2p1VSjetraw"))
      tau_idjet.push_back(taus[i].tauID("byDeepTau2017v2p1VSjetraw"));

    if(taus[i].isTauIDAvailable("byDeepTau2018v2p5VSmuraw"))
      tau_idmu.push_back(taus[i].tauID("byDeepTau2018v2p5VSmuraw"));
    else if(taus[i].isTauIDAvailable("byDeepTau2017v2p1VSmuraw"))
      tau_idmu.push_back(taus[i].tauID("byDeepTau2017v2p1VSmuraw"));

    if(taus[i].isTauIDAvailable("byDeepTau2018v2p5VSeraw"))
      tau_idele.push_back(taus[i].tauID("byDeepTau2018v2p5VSeraw"));
    else if(taus[i].isTauIDAvailable("byDeepTau2017v2p1VSeraw"))
      tau_idele.push_back(taus[i].tauID("byDeepTau2017v2p1VSeraw"));

    int tauvsjetid = 0;
    if(taus[i].isTauIDAvailable("byVVVLooseDeepTau2017v2p1VSjet") and taus[i].tauID("byVVVLooseDeepTau2017v2p1VSjet")) 
      tauvsjetid += 1;
    if(taus[i].isTauIDAvailable("byVVLooseDeepTau2017v2p1VSjet") and taus[i].tauID("byVVLooseDeepTau2017v2p1VSjet")) 
      tauvsjetid += 2;
    if(taus[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSjet") and taus[i].tauID("byVLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 4;
    if(taus[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSjet") and taus[i].tauID("byLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 8;
    if(taus[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSjet") and taus[i].tauID("byMediumDeepTau2017v2p1VSjet"))
      tauvsjetid += 16;
    if(taus[i].isTauIDAvailable("byTightDeepTau2017v2p1VSjet") and taus[i].tauID("byTightDeepTau2017v2p1VSjet"))
      tauvsjetid += 32;

    tau_idjet_wp.push_back(tauvsjetid);

    int tauvsmuid = 0;
    if(taus[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSmu") and taus[i].tauID("byVLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 1;
    if(taus[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSmu") and taus[i].tauID("byLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 2;
    if(taus[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSmu") and taus[i].tauID("byMediumDeepTau2017v2p1VSmu"))
      tauvsmuid += 4;
    if(taus[i].isTauIDAvailable("byTightDeepTau2017v2p1VSmu") and taus[i].tauID("byTightDeepTau2017v2p1VSmu"))
      tauvsmuid += 8;

    tau_idmu_wp.push_back(tauvsmuid);

    int tauvselid = 0;
    if(taus[i].isTauIDAvailable("byVVVLooseDeepTau2017v2p1VSe") and taus[i].tauID("byVVVLooseDeepTau2017v2p1VSe")) 
      tauvselid += 1;
    if(taus[i].isTauIDAvailable("byVVLooseDeepTau2017v2p1VSe") and taus[i].tauID("byVVLooseDeepTau2017v2p1VSe")) 
      tauvselid += 2;
    if(taus[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSe") and taus[i].tauID("byVLooseDeepTau2017v2p1VSe"))
      tauvselid += 4;
    if(taus[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSe") and taus[i].tauID("byLooseDeepTau2017v2p1VSe"))
      tauvselid += 8;
    if(taus[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSe") and taus[i].tauID("byMediumDeepTau2017v2p1VSe"))
      tauvselid += 16;
    if(taus[i].isTauIDAvailable("byTightDeepTau2017v2p1VSe") and taus[i].tauID("byTightDeepTau2017v2p1VSe"))
      tauvselid += 32;

    tau_idele_wp.push_back(tauvselid);

    if(taus[i].genJet()){
      tau_genmatch_pt.push_back(taus[i].genJet()->pt());
      tau_genmatch_eta.push_back(taus[i].genJet()->eta());
      tau_genmatch_phi.push_back(taus[i].genJet()->phi());
      tau_genmatch_mass.push_back(taus[i].genJet()->mass());
      // reconstruct the decay mode of the gen-jet
      unsigned int tau_ch = 0;
      unsigned int tau_ph = 0;
      unsigned int tau_nh = 0;
      auto gen_constituents = taus[i].genJet()->getGenConstituents();
      for(size_t iconst = 0; iconst < gen_constituents.size(); iconst++){
	auto part = gen_constituents[iconst];
	if(part->status() != 1) continue;
	if(part->charge() == 0 and abs(part->pdgId()) == 22) tau_ph++;
	if(part->charge() == 0 and abs(part->pdgId()) != 22) tau_nh++;
	if(part->charge() != 0 and abs(part->pdgId()) != 11 and abs(part->pdgId()) != 13) tau_ch++;
      }
      tau_genmatch_decaymode.push_back(5*(tau_ch-1)+tau_ph/2+tau_nh);
    }
    else{
      tau_genmatch_pt.push_back(-1);
      tau_genmatch_eta.push_back(-1);
      tau_genmatch_phi.push_back(-1);
      tau_genmatch_mass.push_back(-1);
      tau_genmatch_decaymode.push_back(-1);
    }    
  }

  // Jets AK4                                                                                                                                                                                    
  if(jetsAK4H.isValid()){
    vector<pat::JetRef> jetv;
    for (auto jets_iter = jetsAK4H->begin(); jets_iter != jetsAK4H->end(); ++jets_iter) {
      pat::JetRef jref(jetsAK4H, jets_iter - jetsAK4H->begin());
      if (jref->pt() < jetPtAK4Min) continue;
      if (fabs(jref->eta()) > jetEtaMax) continue;
      if (fabs(jref->eta()) < jetEtaMin) continue;
      if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
      jetv.push_back(jref);
    }
    sort(jetv.begin(), jetv.end(), jetRefSorter);

    for (size_t i = 0; i < jetv.size(); i++) {
      jet_ak4_pt.push_back(jetv[i]->pt());
      jet_ak4_eta.push_back(jetv[i]->eta());
      jet_ak4_phi.push_back(jetv[i]->phi());
      jet_ak4_mass.push_back(jetv[i]->mass());

      if(jetv[i]->genJet()){
	jet_ak4_genmatch_pt.push_back(jetv[i]->genJet()->pt());
	jet_ak4_genmatch_eta.push_back(jetv[i]->genJet()->eta());
	jet_ak4_genmatch_phi.push_back(jetv[i]->genJet()->phi());
	jet_ak4_genmatch_mass.push_back(jetv[i]->genJet()->mass());
      }
      else{
	jet_ak4_genmatch_pt.push_back(0);
	jet_ak4_genmatch_eta.push_back(0);
	jet_ak4_genmatch_phi.push_back(0);
	jet_ak4_genmatch_mass.push_back(0);
      }
    }
  }

  // Jets HLT AK4                                                                                                                                                                                    
  if(jetsAK4HLTH.isValid()){
    // Standard gen-jets excluding the neutrinos
    vector<reco::GenJetRef> jetv_gen;   
    if(genJetsH.isValid()){
      for (auto jets_iter = genJetsH->begin(); jets_iter != genJetsH->end(); ++jets_iter) {                                                                                                   
	reco::GenJetRef jref (genJetsH, jets_iter - genJetsH->begin());                                                                                                                      
	jetv_gen.push_back(jref);                                                                                                                                                              
      }
      sort(jetv_gen.begin(), jetv_gen.end(), genJetRefSorter);
    }

    vector<reco::PFJetRef> jetv;
    for (auto jets_iter = jetsAK4HLTH->begin(); jets_iter != jetsAK4HLTH->end(); ++jets_iter) {
      reco::PFJetRef jref(jetsAK4HLTH, jets_iter - jetsAK4HLTH->begin());
      if (jref->pt() < jetPtAK4Min) continue;
      if (fabs(jref->eta()) > jetEtaMax) continue;
      if (fabs(jref->eta()) < jetEtaMin) continue;
      jetv.push_back(jref);
    }
    sort(jetv.begin(), jetv.end(), recoJetRefSorter);

    for (size_t i = 0; i < jetv.size(); i++) {
      jet_hlt_ak4_pt.push_back(jetv[i]->pt());
      jet_hlt_ak4_eta.push_back(jetv[i]->eta());
      jet_hlt_ak4_phi.push_back(jetv[i]->phi());
      jet_hlt_ak4_mass.push_back(jetv[i]->mass());

      // Matching with gen-jets
      int   pos_matched = -1;
      float minDR = dRJetGenMatch;
      for(size_t igen = 0; igen < jetv_gen.size(); igen++){
	if(reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4()) < minDR){
	  pos_matched = igen;
	  minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4());
	}
      }
      if(pos_matched){
	jet_hlt_ak4_genmatch_pt.push_back(jetv[pos_matched]->pt());
	jet_hlt_ak4_genmatch_eta.push_back(jetv[pos_matched]->eta());
	jet_hlt_ak4_genmatch_phi.push_back(jetv[pos_matched]->phi());
	jet_hlt_ak4_genmatch_mass.push_back(jetv[pos_matched]->mass());
      }
      else{
	jet_hlt_ak4_genmatch_pt.push_back(0);
	jet_hlt_ak4_genmatch_eta.push_back(0);
	jet_hlt_ak4_genmatch_phi.push_back(0);
	jet_hlt_ak4_genmatch_mass.push_back(0);
      }
    }
  }
  
  // GEN jets --> make a list and sort them
  std::vector<pat::JetRef> jetv_gen;  
  if(isMC and genJetsAK8H.isValid()){
    for (auto jets_iter = genJetsAK8H->begin(); jets_iter != genJetsAK8H->end(); ++jets_iter) {                                                                                                   
      pat::JetRef jref  (genJetsAK8H, jets_iter - genJetsAK8H->begin());                                                                                                                         
      jetv_gen.push_back(jref);                                                                                                                                                           
    }
    sort(jetv_gen.begin(), jetv_gen.end(), jetRefSorter);
  }

  // Offline jets
  std::vector<pat::JetRef> jetv; 
  for (auto jets_iter = jetsAK8H->begin(); jets_iter != jetsAK8H->end(); ++jets_iter) {                                                                                                      
    pat::JetRef jref(jetsAK8H, jets_iter - jetsAK8H->begin());                                                                                                                                 
    if (jref->pt() < jetPtAK8Min) continue; 
    if (fabs(jref->eta()) > jetEtaMax) continue;                 
    if (fabs(jref->eta()) < jetEtaMin) continue;                 
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
    jetv.push_back(jref);                                                                                                                                                              
  }    
  sort(jetv.begin(), jetv.end(), jetRefSorter);

  ///////////////////  
  for (size_t i = 0; i < jetv.size(); i++) {

    jet_pt.push_back(jetv[i]->pt());
    jet_eta.push_back(jetv[i]->eta());
    jet_phi.push_back(jetv[i]->phi());
    jet_mass.push_back(jetv[i]->mass());
    jet_pt_raw.push_back(jetv[i]->correctedJet("Uncorrected").pt());
    jet_mass_raw.push_back(jetv[i]->correctedJet("Uncorrected").mass());

    jet_pnet_probXbb.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probXbb"));
    jet_pnet_probXcc.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probXcc"));
    jet_pnet_probXqq.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probXqq"));
    jet_pnet_probQCDbb.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDbb"));
    jet_pnet_probQCDcc.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDcc"));
    jet_pnet_probQCDb.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDb"));
    jet_pnet_probQCDc.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDc"));
    jet_pnet_probQCDothers.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDothers"));
    jet_pnet_regression_mass.push_back(jetv[i]->bDiscriminator("pfParticleNetMassRegressionJetTags:mass"));

    if(jetv[i]->hasUserFloat("NjettinessAK8Puppi:tau1"))
      jet_tau1.push_back(jetv[i]->userFloat("NjettinessAK8Puppi:tau1"));
    if(jetv[i]->hasUserFloat("NjettinessAK8Puppi:tau2"))
      jet_tau2.push_back(jetv[i]->userFloat("NjettinessAK8Puppi:tau2"));
    if(jetv[i]->hasUserFloat("NjettinessAK8Puppi:tau3"))
      jet_tau3.push_back(jetv[i]->userFloat("NjettinessAK8Puppi:tau3"));
    if(jetv[i]->hasUserFloat("NjettinessAK8Puppi:tau4"))
      jet_tau4.push_back(jetv[i]->userFloat("NjettinessAK8Puppi:tau4"));

    int jetid = 0;
    if(applyJetID(*jetv[i],"tight",true)) jetid += 1;
    jet_id.push_back(jetid);

    // Energy fractions
    jet_chf.push_back(jetv[i]->chargedHadronEnergyFraction());
    jet_nhf.push_back(jetv[i]->neutralHadronEnergyFraction());
    jet_elf.push_back(jetv[i]->electronEnergyFraction());
    jet_phf.push_back(jetv[i]->photonEnergyFraction());
    jet_muf.push_back(jetv[i]->muonEnergyFraction());

    // PF components
    jet_ncand.push_back(jetv[i]->chargedHadronMultiplicity()+jetv[i]->neutralHadronMultiplicity()+jetv[i]->electronMultiplicity()+jetv[i]->photonMultiplicity()+jetv[i]->muonMultiplicity());
    jet_nch.push_back(jetv[i]->chargedHadronMultiplicity());
    jet_nnh.push_back(jetv[i]->neutralHadronMultiplicity());
    jet_nel.push_back(jetv[i]->electronMultiplicity());
    jet_nph.push_back(jetv[i]->photonMultiplicity());
    jet_nmu.push_back(jetv[i]->muonMultiplicity());
    jet_hflav.push_back(jetv[i]->hadronFlavour());
    jet_pflav.push_back(jetv[i]->partonFlavour());
    jet_nbhad.push_back(jetv[i]->jetFlavourInfo().getbHadrons().size());
    jet_nchad.push_back(jetv[i]->jetFlavourInfo().getcHadrons().size());

    // Matching with gen-jets
    int pos_gen_matched = -1;
    float minDR = dRJetGenMatch;    
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4()) < minDR){
	pos_gen_matched = igen;
	minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4());
      }
    }
    
    if(pos_gen_matched >= 0){       
      jet_genmatch_pt.push_back(jetv_gen[pos_gen_matched]->pt());
      jet_genmatch_eta.push_back(jetv_gen[pos_gen_matched]->eta());
      jet_genmatch_phi.push_back(jetv_gen[pos_gen_matched]->phi());
      jet_genmatch_mass.push_back(jetv_gen[pos_gen_matched]->mass());
    }
    else{
      jet_genmatch_pt.push_back(0);
      jet_genmatch_eta.push_back(0);
      jet_genmatch_phi.push_back(0);
      jet_genmatch_mass.push_back(0);
    }

    TLorentzVector jet_softdrop_4V;
    TLorentzVector jet_softdrop_4V_raw;
    TLorentzVector genjet_softdrop_4V;
    pat::JetPtrCollection subjets;

    if(jetv[i]->nSubjetCollections() > 0){
      subjets = jetv[i]->subjets(subJetCollectionName);
      std::sort(subjets.begin(), subjets.end(), jetPtrSorter);
      std::vector<unsigned int> subjetToSkip;
      for(size_t isub = 0; isub < subjets.size(); isub++){
	jet_softdrop_subjet_pt.push_back(subjets.at(isub)->pt());
	jet_softdrop_subjet_eta.push_back(subjets.at(isub)->eta());
	jet_softdrop_subjet_phi.push_back(subjets.at(isub)->phi());
	jet_softdrop_subjet_mass.push_back(subjets.at(isub)->mass());
	jet_softdrop_subjet_mass_raw.push_back(subjets.at(isub)->correctedJet("Uncorrected").mass());
	jet_softdrop_subjet_pt_raw.push_back(subjets.at(isub)->correctedJet("Uncorrected").pt());
	jet_softdrop_subjet_ijet.push_back(i);
	jet_softdrop_subjet_nbhad.push_back(subjets.at(isub)->jetFlavourInfo().getbHadrons().size());
	jet_softdrop_subjet_nchad.push_back(subjets.at(isub)->jetFlavourInfo().getcHadrons().size());
	jet_softdrop_subjet_hflav.push_back(subjets.at(isub)->hadronFlavour());
	jet_softdrop_subjet_pflav.push_back(subjets.at(isub)->partonFlavour());  
	TLorentzVector subjet_4v, subjet_raw_4v;
	subjet_4v.SetPtEtaPhiM(subjets.at(isub)->pt(),subjets.at(isub)->eta(),subjets.at(isub)->phi(),subjets.at(isub)->mass());
	subjet_raw_4v.SetPtEtaPhiM(subjets.at(isub)->correctedJet("Uncorrected").pt(),subjets.at(isub)->correctedJet("Uncorrected").eta(),
				   subjets.at(isub)->correctedJet("Uncorrected").phi(),subjets.at(isub)->correctedJet("Uncorrected").mass());
	jet_softdrop_4V += subjet_4v;
	jet_softdrop_4V_raw += subjet_raw_4v;
	minDR = 1000;
	int pos_gen_subjet_matched = -1;
	if(pos_gen_matched >= 0){
	  if(jetv_gen[pos_gen_matched]->nSubjetCollections() > 0){
	    int jsub = 0;
	    for(auto const & genSubJet: jetv_gen[pos_gen_matched]->subjets(genSubJetCollectionName)){
	      if(std::find(subjetToSkip.begin(),subjetToSkip.end(),jsub) != subjetToSkip.end()){
		jsub++;
		continue;
	      }
	      if(reco::deltaR(subjets.at(isub)->p4(),genSubJet->p4()) < minDR){
		pos_gen_subjet_matched = jsub;
		minDR = reco::deltaR(subjets.at(isub)->p4(),genSubJet->p4());
	      }
	      jsub++;
	    }  
	  }
	}
	if(pos_gen_matched >= 0 and pos_gen_subjet_matched >= 0){
	  auto const & genSubJet = jetv_gen[pos_gen_matched]->subjets(genSubJetCollectionName).at(pos_gen_subjet_matched);
	  subjetToSkip.push_back(pos_gen_subjet_matched);
	  TLorentzVector gen_subjet_4v;
	  gen_subjet_4v.SetPtEtaPhiM(genSubJet->pt(),genSubJet->eta(),genSubJet->phi(),genSubJet->mass());
	  genjet_softdrop_4V += gen_subjet_4v;  
	  jet_softdrop_subjet_genmatch_pt.push_back(genSubJet->pt());
	  jet_softdrop_subjet_genmatch_eta.push_back(genSubJet->eta());
	  jet_softdrop_subjet_genmatch_phi.push_back(genSubJet->phi());
	  jet_softdrop_subjet_genmatch_mass.push_back(genSubJet->mass());
	}
	else{
	  jet_softdrop_subjet_genmatch_pt.push_back(0);
	  jet_softdrop_subjet_genmatch_eta.push_back(0);
	  jet_softdrop_subjet_genmatch_phi.push_back(0);
	  jet_softdrop_subjet_genmatch_mass.push_back(0);
	}
      }
    }
    
    jet_softdrop_pt.push_back(jet_softdrop_4V.Pt());
    jet_softdrop_eta.push_back(jet_softdrop_4V.Eta());
    jet_softdrop_phi.push_back(jet_softdrop_4V.Phi());
    jet_softdrop_mass.push_back(jet_softdrop_4V.M());
    jet_softdrop_pt_raw.push_back(jet_softdrop_4V_raw.Pt());
    jet_softdrop_mass_raw.push_back(jet_softdrop_4V_raw.M());

    if(isMC){
      jet_softdrop_genmatch_pt.push_back(genjet_softdrop_4V.Pt());
      jet_softdrop_genmatch_eta.push_back(genjet_softdrop_4V.Eta());
      jet_softdrop_genmatch_phi.push_back(genjet_softdrop_4V.Phi());
      jet_softdrop_genmatch_mass.push_back(genjet_softdrop_4V.M());
    }    
  }

  // Jets  HLT
  if(jetsAK8HLTH.isValid()){
    
    std::vector<pat::JetRef> jetv_hlt; 
    for (auto jets_iter = jetsAK8HLTH->begin(); jets_iter != jetsAK8HLTH->end(); ++jets_iter) {                                                                                                    
      pat::JetRef jref(jetsAK8HLTH, jets_iter - jetsAK8HLTH->begin());                                                                                                                          
      if (jref->pt() < jetPtAK8Min) continue; 
      if (fabs(jref->eta()) > jetEtaMax) continue;                 
      if (fabs(jref->eta()) < jetEtaMin) continue;                 
      if (isMC and dumpOnlyJetMatchedToGen){
	bool isMatched  = false;
	for(auto const & genref : jetv_gen){
	  if(reco::deltaR(jref->p4(),genref->p4()) < dRJetGenMatch)
	    isMatched = true;
	}
	if(not isMatched) continue;
      }
      jetv_hlt.push_back(jref);                                                                                                                                                              
    }    
    sort(jetv_hlt.begin(), jetv_hlt.end(), jetRefSorter);

    for (size_t i = 0; i < jetv_hlt.size(); i++) {
      
      jet_hlt_pt.push_back(jetv_hlt[i]->pt());
      jet_hlt_eta.push_back(jetv_hlt[i]->eta());
      jet_hlt_phi.push_back(jetv_hlt[i]->phi());
      jet_hlt_mass.push_back(jetv_hlt[i]->mass());
      jet_hlt_pt_raw.push_back(jetv_hlt[i]->correctedJet("Uncorrected").pt());
      jet_hlt_mass_raw.push_back(jetv_hlt[i]->correctedJet("Uncorrected").mass());

      jet_hlt_pnethlt_probXtt.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probHtt"));
      jet_hlt_pnethlt_probXbb.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probHbb"));
      jet_hlt_pnethlt_probXcc.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probHcc"));
      jet_hlt_pnethlt_probXqq.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probHqq"));
      jet_hlt_pnethlt_probXgg.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probHgg"));
      jet_hlt_pnethlt_probQCD.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probQCD"));

      if(jetv_hlt[i]->hasUserFloat("hltNjettinessAK8PFJets:tau1"))
	jet_hlt_tau1.push_back(jetv_hlt[i]->userFloat("hltNjettinessAK8PFJets:tau1"));
      if(jetv_hlt[i]->hasUserFloat("hltNjettinessAK8PFJets:tau2"))
	jet_hlt_tau2.push_back(jetv_hlt[i]->userFloat("hltNjettinessAK8PFJets:tau2"));
      if(jetv_hlt[i]->hasUserFloat("hltNjettinessAK8PFJets:tau3"))
	jet_hlt_tau3.push_back(jetv_hlt[i]->userFloat("hltNjettinessAK8PFJets:tau3"));
      if(jetv_hlt[i]->hasUserFloat("hltNjettinessAK8PFJets:tau4"))
	jet_hlt_tau4.push_back(jetv_hlt[i]->userFloat("hltNjettinessAK8PFJets:tau4"));

      int jetid = 0;
      if(applyJetID(*jetv_hlt[i],"tight",true)) jetid += 1;
      jet_hlt_id.push_back(jetid);
      
      // Energy fractions
      jet_hlt_chf.push_back(jetv_hlt[i]->chargedHadronEnergyFraction());
      jet_hlt_nhf.push_back(jetv_hlt[i]->neutralHadronEnergyFraction());
      jet_hlt_elf.push_back(jetv_hlt[i]->electronEnergyFraction());
      jet_hlt_phf.push_back(jetv_hlt[i]->photonEnergyFraction());
      jet_hlt_muf.push_back(jetv_hlt[i]->muonEnergyFraction());
      
      // PF components
      jet_hlt_ncand.push_back(jetv_hlt[i]->chargedHadronMultiplicity()+jetv_hlt[i]->neutralHadronMultiplicity()+jetv_hlt[i]->electronMultiplicity()+jetv_hlt[i]->photonMultiplicity()+jetv_hlt[i]->muonMultiplicity());
      jet_hlt_nch.push_back(jetv_hlt[i]->chargedHadronMultiplicity());
      jet_hlt_nnh.push_back(jetv_hlt[i]->neutralHadronMultiplicity());
      jet_hlt_nel.push_back(jetv_hlt[i]->electronMultiplicity());
      jet_hlt_nph.push_back(jetv_hlt[i]->photonMultiplicity());
      jet_hlt_nmu.push_back(jetv_hlt[i]->muonMultiplicity());
      jet_hlt_hflav.push_back(jetv_hlt[i]->hadronFlavour());
      jet_hlt_pflav.push_back(jetv_hlt[i]->partonFlavour());
      jet_hlt_nbhad.push_back(jetv_hlt[i]->jetFlavourInfo().getbHadrons().size());
      jet_hlt_nchad.push_back(jetv_hlt[i]->jetFlavourInfo().getcHadrons().size());

      // Matching with gen-jets
      int pos_gen_matched = -1;
      float minDR = dRJetGenMatch;    
      for(size_t igen = 0; igen < jetv_gen.size(); igen++){
	if(reco::deltaR(jetv_gen[igen]->p4(),jetv_hlt[i]->p4()) < minDR){
	  pos_gen_matched = igen;
	  minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv_hlt[i]->p4());
	}
      }

      if(pos_gen_matched >= 0){      
	jet_hlt_genmatch_pt.push_back(jetv_gen[pos_gen_matched]->pt());
	jet_hlt_genmatch_eta.push_back(jetv_gen[pos_gen_matched]->eta());
	jet_hlt_genmatch_phi.push_back(jetv_gen[pos_gen_matched]->phi());
	jet_hlt_genmatch_mass.push_back(jetv_gen[pos_gen_matched]->mass());
      }
      else{
	jet_hlt_genmatch_pt.push_back(0);
	jet_hlt_genmatch_eta.push_back(0);
	jet_hlt_genmatch_phi.push_back(0);
	jet_hlt_genmatch_mass.push_back(0);
      }

      TLorentzVector jet_softdrop_4V;
      TLorentzVector jet_softdrop_4V_raw;
      TLorentzVector genjet_softdrop_4V;

      pat::JetPtrCollection subjets;
      if(jetv_hlt[i]->nSubjetCollections() > 0){
	subjets = jetv_hlt[i]->subjets(subJetCollectionNameHLT);
	std::sort(subjets.begin(), subjets.end(), jetPtrSorter);
	std::vector<unsigned int> subjetToSkip;
	for(size_t isub = 0; isub < subjets.size(); isub++){
	  jet_hlt_softdrop_subjet_pt.push_back(subjets.at(isub)->pt());
	  jet_hlt_softdrop_subjet_eta.push_back(subjets.at(isub)->eta());
	  jet_hlt_softdrop_subjet_phi.push_back(subjets.at(isub)->phi());
	  jet_hlt_softdrop_subjet_mass.push_back(subjets.at(isub)->mass());
	  jet_hlt_softdrop_subjet_mass_raw.push_back(subjets.at(isub)->correctedJet("Uncorrected").mass());
	  jet_hlt_softdrop_subjet_pt_raw.push_back(subjets.at(isub)->correctedJet("Uncorrected").pt());
	  jet_hlt_softdrop_subjet_ijet.push_back(i);
	  jet_hlt_softdrop_subjet_nbhad.push_back(subjets.at(isub)->jetFlavourInfo().getbHadrons().size());
	  jet_hlt_softdrop_subjet_nchad.push_back(subjets.at(isub)->jetFlavourInfo().getcHadrons().size());
	  jet_hlt_softdrop_subjet_hflav.push_back(subjets.at(isub)->hadronFlavour());
	  jet_hlt_softdrop_subjet_pflav.push_back(subjets.at(isub)->partonFlavour());  
	  TLorentzVector subjet_4v, subjet_raw_4v;
	  subjet_4v.SetPtEtaPhiM(subjets.at(isub)->pt(),subjets.at(isub)->eta(),subjets.at(isub)->phi(),subjets.at(isub)->mass());
	  subjet_raw_4v.SetPtEtaPhiM(subjets.at(isub)->correctedJet("Uncorrected").pt(),subjets.at(isub)->correctedJet("Uncorrected").eta(),
				     subjets.at(isub)->correctedJet("Uncorrected").phi(),subjets.at(isub)->correctedJet("Uncorrected").mass());
	  jet_softdrop_4V += subjet_4v;
	  jet_softdrop_4V_raw += subjet_raw_4v;
	  minDR = 1000;
	  int pos_gen_subjet_matched = -1;
	  if(pos_gen_matched >= 0){
	    if(jetv_gen[pos_gen_matched]->nSubjetCollections() > 0){
	      int jsub = 0;
	      for(auto const & genSubJet: jetv_gen[pos_gen_matched]->subjets(subJetCollectionNameHLT)){
		if(std::find(subjetToSkip.begin(),subjetToSkip.end(),jsub) != subjetToSkip.end()){
		  jsub++;
		  continue;
		}
		if(reco::deltaR(subjets.at(isub)->p4(),genSubJet->p4()) < minDR){
		  pos_gen_subjet_matched = jsub;
		  minDR = reco::deltaR(subjets.at(isub)->p4(),genSubJet->p4());
		}
		jsub++;
	      }  
	    }
	  }
	  if(pos_gen_matched >= 0 and pos_gen_subjet_matched >= 0){
	    auto const & genSubJet = jetv_gen[pos_gen_matched]->subjets(genSubJetCollectionName).at(pos_gen_subjet_matched);
	    subjetToSkip.push_back(pos_gen_subjet_matched);
	    TLorentzVector gen_subjet_4v;
	    gen_subjet_4v.SetPtEtaPhiM(genSubJet->pt(),genSubJet->eta(),genSubJet->phi(),genSubJet->mass());
	    genjet_softdrop_4V += gen_subjet_4v;  
	    jet_hlt_softdrop_subjet_genmatch_pt.push_back(genSubJet->pt());
	    jet_hlt_softdrop_subjet_genmatch_eta.push_back(genSubJet->eta());
	    jet_hlt_softdrop_subjet_genmatch_phi.push_back(genSubJet->phi());
	    jet_hlt_softdrop_subjet_genmatch_mass.push_back(genSubJet->mass());
	  }
	  else{
	    jet_hlt_softdrop_subjet_genmatch_pt.push_back(0);
	    jet_hlt_softdrop_subjet_genmatch_eta.push_back(0);
	    jet_hlt_softdrop_subjet_genmatch_phi.push_back(0);
	    jet_hlt_softdrop_subjet_genmatch_mass.push_back(0);
	  }
	}
      }
    
      jet_hlt_softdrop_pt.push_back(jet_softdrop_4V.Pt());
      jet_hlt_softdrop_eta.push_back(jet_softdrop_4V.Eta());
      jet_hlt_softdrop_phi.push_back(jet_softdrop_4V.Phi());
      jet_hlt_softdrop_mass.push_back(jet_softdrop_4V.M());
      jet_hlt_softdrop_pt_raw.push_back(jet_softdrop_4V_raw.Pt());
      jet_hlt_softdrop_mass_raw.push_back(jet_softdrop_4V_raw.M());
      
      if(isMC){
	jet_hlt_softdrop_genmatch_pt.push_back(genjet_softdrop_4V.Pt());
	jet_hlt_softdrop_genmatch_eta.push_back(genjet_softdrop_4V.Eta());
	jet_hlt_softdrop_genmatch_phi.push_back(genjet_softdrop_4V.Phi());
	jet_hlt_softdrop_genmatch_mass.push_back(genjet_softdrop_4V.M());
      }

      math::XYZVector jetDir = jetv_hlt[i]->momentum().Unit();
      TVector3 jet_direction (jetv_hlt[i]->momentum().Unit().x(),jetv_hlt[i]->momentum().Unit().y(),jetv_hlt[i]->momentum().Unit().z());
      GlobalVector jet_global_vec (jetv_hlt[i]->px(),jetv_hlt[i]->py(),jetv_hlt[i]->pz());
      
      // secondary vertex that are inside the jet cone + sorting in significance
      std::vector<reco::VertexCompositePtrCandidate> jetSVs;    
      for(size_t isv = 0; isv < secondaryVerticesHLTH->size(); isv++){
	if(reco::deltaR(secondaryVerticesHLTH->at(isv),*jetv_hlt[i]) < dRJetGenMatch){
	  jetSVs.push_back(secondaryVerticesHLTH->at(isv));
	}
      }
      
      VertexSorterDxy<reco::VertexCompositePtrCandidate,reco::Vertex> svSorter (primaryVerticesHLTH->front());
      std::sort(jetSVs.begin(),jetSVs.end(),svSorter);
      
      for(auto const & svcand : jetSVs){
	jet_hlt_sv_pt.push_back(svcand.pt());
	jet_hlt_sv_eta.push_back(svcand.eta());
	jet_hlt_sv_phi.push_back(svcand.phi());
	jet_hlt_sv_mass.push_back(svcand.mass());
	jet_hlt_sv_energy.push_back(svcand.energy());
	jet_hlt_sv_chi2.push_back(svcand.vertexNormalizedChi2());
	auto dxy = vertexDxySigned(svcand,primaryVerticesHLTH->front(),jet_global_vec);
	jet_hlt_sv_dxy.push_back(dxy.value());
	jet_hlt_sv_dxysig.push_back(fabs(dxy.significance()));
	auto d3d = vertexD3dSigned(svcand,primaryVerticesHLTH->front(),jet_global_vec);
	jet_hlt_sv_d3d.push_back(d3d.value());
	jet_hlt_sv_d3dsig.push_back(fabs(d3d.significance()));
	jet_hlt_sv_ntrack.push_back(svcand.numberOfDaughters());
	jet_hlt_sv_ijet.push_back(i);
      }    
      
      // pf candidates
      std::vector<pat::PackedCandidate> vectorOfConstituents;
      for(unsigned ipart = 0; ipart < jetv_hlt[i]->numberOfDaughters(); ipart++){
	const pat::PackedCandidate* pfPart = dynamic_cast<const pat::PackedCandidate*> (jetv_hlt[i]->daughter(ipart));      
	vectorOfConstituents.push_back(*pfPart);
      }    
      std::sort(vectorOfConstituents.begin(),vectorOfConstituents.end(),packedPFCandidateSorter);
      
      for(auto const & pfcand : vectorOfConstituents){      
	if(pfcand.pt() < jetPFCandidatePtMin) continue;
	
	jet_hlt_pfcandidate_pt.push_back(pfcand.pt());
	jet_hlt_pfcandidate_eta.push_back(pfcand.eta());
	jet_hlt_pfcandidate_phi.push_back(pfcand.phi());
	jet_hlt_pfcandidate_mass.push_back(pfcand.mass());
	jet_hlt_pfcandidate_energy.push_back(pfcand.energy());
	jet_hlt_pfcandidate_calofraction.push_back(pfcand.caloFraction());
	jet_hlt_pfcandidate_hcalfraction.push_back(pfcand.hcalFraction());
	
	jet_hlt_pfcandidate_id.push_back(abs(pfcand.pdgId()));
	jet_hlt_pfcandidate_charge.push_back(pfcand.charge());
	jet_hlt_pfcandidate_ijet.push_back(i);
	
	// PV association and distance from PV
	jet_hlt_pfcandidate_frompv.push_back(pfcand.fromPV());
	jet_hlt_pfcandidate_dz.push_back(pfcand.dz(primaryVerticesHLTH->front().position()));
	jet_hlt_pfcandidate_dxy.push_back(pfcand.dxy(primaryVerticesHLTH->front().position()));
	
	// Track related
	jet_hlt_pfcandidate_npixhits.push_back(pfcand.numberOfPixelHits());
	jet_hlt_pfcandidate_nstriphits.push_back(pfcand.stripLayersWithMeasurement());
	jet_hlt_pfcandidate_nlostinnerhits.push_back(pfcand.lostInnerHits());
	jet_hlt_pfcandidate_highpurity.push_back(pfcand.trackHighPurity());
	
	TVector3 pfcand_momentum (pfcand.momentum().x(),pfcand.momentum().y(),pfcand.momentum().z());
	jet_hlt_pfcandidate_candjet_pperp_ratio.push_back(jet_direction.Perp(pfcand_momentum)/pfcand_momentum.Mag());
	jet_hlt_pfcandidate_candjet_ppara_ratio.push_back(jet_direction.Dot(pfcand_momentum)/pfcand_momentum.Mag());
	jet_hlt_pfcandidate_candjet_dphi.push_back(jet_direction.DeltaPhi(pfcand_momentum));
	jet_hlt_pfcandidate_candjet_deta.push_back(jet_direction.Eta()-pfcand_momentum.Eta());
	jet_hlt_pfcandidate_candjet_etarel.push_back(reco::btau::etaRel(jetDir,pfcand.momentum()));
	
	const reco::Track* track = pfcand.bestTrack();      
	
	if(track){ // need valid track object
	  
	  jet_hlt_pfcandidate_dzsig.push_back(fabs(pfcand.dz(primaryVerticesHLTH->front().position()))/pfcand.dzError());
	  jet_hlt_pfcandidate_dxysig.push_back(fabs(pfcand.dxy(primaryVerticesHLTH->front().position()))/pfcand.dxyError());
	  jet_hlt_pfcandidate_track_chi2.push_back(track->normalizedChi2());
	  jet_hlt_pfcandidate_track_qual.push_back(track->qualityMask());
	  
	  reco::TransientTrack transientTrack = trackBuilderH->build(*track);
	  Measurement1D meas_ip2d    = IPTools::signedTransverseImpactParameter(transientTrack,jet_global_vec,primaryVerticesHLTH->front()).second;
	  Measurement1D meas_ip3d    = IPTools::signedImpactParameter3D(transientTrack,jet_global_vec,primaryVerticesHLTH->front()).second;
	  Measurement1D meas_jetdist = IPTools::jetTrackDistance(transientTrack,jet_global_vec,primaryVerticesHLTH->front()).second;
	  Measurement1D meas_decayl  = IPTools::signedDecayLength3D(transientTrack,jet_global_vec,primaryVerticesHLTH->front()).second;
	  
	  jet_hlt_pfcandidate_trackjet_dxy.push_back(meas_ip2d.value());
	  jet_hlt_pfcandidate_trackjet_dxysig.push_back(fabs(meas_ip2d.significance()));
	  jet_hlt_pfcandidate_trackjet_d3d.push_back(meas_ip3d.value());
	  jet_hlt_pfcandidate_trackjet_d3dsig.push_back(fabs(meas_ip3d.significance()));
	  jet_hlt_pfcandidate_trackjet_dist.push_back(-meas_jetdist.value());
	  jet_hlt_pfcandidate_trackjet_decayL.push_back(meas_decayl.value());
	  
	}
	else{
	  jet_hlt_pfcandidate_dzsig.push_back(0.);
	  jet_hlt_pfcandidate_dxysig.push_back(0.);
	  jet_hlt_pfcandidate_track_chi2.push_back(0);
	  jet_hlt_pfcandidate_track_qual.push_back(0);
	  jet_hlt_pfcandidate_trackjet_dxy.push_back(0.);
	  jet_hlt_pfcandidate_trackjet_dxysig.push_back(0.);
	  jet_hlt_pfcandidate_trackjet_d3d.push_back(0.);
	  jet_hlt_pfcandidate_trackjet_d3dsig.push_back(0.);
	  jet_hlt_pfcandidate_trackjet_dist.push_back(0.);
	  jet_hlt_pfcandidate_trackjet_decayL.push_back(0.);
	}	  
      }
    }
  }
  tree->Fill();
 }

void TrainingNtupleMakerAK8::beginJob() {

  // Access the TFileService
  edm::Service<TFileService> fs;
  
  // Create the TTree
  tree = fs->make<TTree>("tree","tree");

  tree->Branch("event", &event, "event/i");
  tree->Branch("run", &run, "run/i");
  tree->Branch("lumi", &lumi, "lumi/i");
  tree->Branch("flags", &flags, "flags/i");

  if(isMC){
    tree->Branch("xsec", &xsec, "xsec/F");
    tree->Branch("wgt" , &wgt , "wgt/F");
    tree->Branch("putrue", &putrue, "putrue/i");
  }

  tree->Branch("rho", &rho, "rho/F");
  tree->Branch("rho_hlt", &rho_hlt, "rho_hlt/F");

  tree->Branch("met",&met,"met/F");
  tree->Branch("met_phi", &met_phi, "met_phi/F");

  tree->Branch("npv", &npv, "npv/i");
  tree->Branch("nsv", &nsv, "nsv/i");

  tree->Branch("npv_hlt", &npv_hlt, "npv_hlt/i");
  tree->Branch("nsv_hlt", &nsv_hlt, "nsv_hlt/i");
  
  // GEN particles
  if(isMC){
    tree->Branch("gen_particle_pt","std::vector<float>", &gen_particle_pt);
    tree->Branch("gen_particle_eta","std::vector<float>", &gen_particle_eta);
    tree->Branch("gen_particle_phi","std::vector<float>", &gen_particle_phi);
    tree->Branch("gen_particle_mass","std::vector<float>", &gen_particle_mass);
    tree->Branch("gen_particle_id","std::vector<int>", &gen_particle_id);
    tree->Branch("gen_particle_status","std::vector<unsigned int>", &gen_particle_status);
    tree->Branch("gen_particle_daughters_id","std::vector<int>", &gen_particle_daughters_id);
    tree->Branch("gen_particle_daughters_igen","std::vector<unsigned int>", &gen_particle_daughters_igen);
    tree->Branch("gen_particle_daughters_pt","std::vector<float>", &gen_particle_daughters_pt);
    tree->Branch("gen_particle_daughters_eta","std::vector<float>", &gen_particle_daughters_eta);
    tree->Branch("gen_particle_daughters_phi","std::vector<float>", &gen_particle_daughters_phi);
    tree->Branch("gen_particle_daughters_mass","std::vector<float>", &gen_particle_daughters_mass);
    tree->Branch("gen_particle_daughters_status","std::vector<unsigned int>", &gen_particle_daughters_status);
    tree->Branch("gen_particle_daughters_charge","std::vector<int>", &gen_particle_daughters_charge);
  }

  if(isMC and saveLHEObjects){
    tree->Branch("lhe_particle_pt", "std::vector<float>", &lhe_particle_pt);
    tree->Branch("lhe_particle_eta", "std::vector<float>", &lhe_particle_eta);
    tree->Branch("lhe_particle_phi", "std::vector<float>", &lhe_particle_phi);
    tree->Branch("lhe_particle_mass", "std::vector<float>", &lhe_particle_mass);
    tree->Branch("lhe_particle_id", "std::vector<int>", &lhe_particle_id);
    tree->Branch("lhe_particle_status", "std::vector<unsigned int>", &lhe_particle_status);
  }

  tree->Branch("trigger_hlt_path","std::vector<std::string>",&trigger_hlt_path);
  tree->Branch("trigger_hlt_pass","std::vector<unsigned int>",&trigger_hlt_pass);
  tree->Branch("trigger_hlt_prescale","std::vector<int>",&trigger_hlt_prescale);

  tree->Branch("trigger_l1_seed","std::vector<std::string>",&trigger_l1_seed);
  tree->Branch("trigger_l1_pass","std::vector<unsigned int>",&trigger_l1_pass);
  tree->Branch("trigger_l1_prescale","std::vector<int>",&trigger_l1_prescale);

  if(saveL1Objects){
    tree->Branch("trigger_l1_jet_pt","std::vector<float>",&trigger_l1_jet_pt);
    tree->Branch("trigger_l1_jet_eta","std::vector<float>",&trigger_l1_jet_eta);
    tree->Branch("trigger_l1_jet_phi","std::vector<float>",&trigger_l1_jet_phi);
    tree->Branch("trigger_l1_jet_mass","std::vector<float>",&trigger_l1_jet_mass);
    tree->Branch("trigger_l1_muon_pt","std::vector<float>",&trigger_l1_muon_pt);
    tree->Branch("trigger_l1_muon_eta","std::vector<float>",&trigger_l1_muon_eta);
    tree->Branch("trigger_l1_muon_phi","std::vector<float>",&trigger_l1_muon_phi);
    tree->Branch("trigger_l1_muon_mass","std::vector<float>",&trigger_l1_muon_mass);
    tree->Branch("trigger_l1_muon_qual","std::vector<float>",&trigger_l1_muon_qual);
    tree->Branch("trigger_l1_muon_iso","std::vector<float>",&trigger_l1_muon_iso);
    tree->Branch("trigger_l1_ht",&trigger_l1_ht,"trigger_l1_ht/F");
    tree->Branch("trigger_l1_mht",&trigger_l1_mht,"trigger_l1_mht/F");
  }

  tree->Branch("muon_pt", "std::vector<float>", &muon_pt);
  tree->Branch("muon_eta", "std::vector<float>", &muon_eta);
  tree->Branch("muon_phi", "std::vector<float>", &muon_phi);
  tree->Branch("muon_mass", "std::vector<float>", &muon_mass);
  tree->Branch("muon_id", "std::vector<unsigned int>" , &muon_id);
  tree->Branch("muon_iso", "std::vector<unsigned int>" , &muon_iso);
  tree->Branch("muon_charge", "std::vector<int>" , &muon_charge);
  tree->Branch("muon_d0", "std::vector<float>" , &muon_d0);
  tree->Branch("muon_dz", "std::vector<float>" , &muon_dz);

  tree->Branch("electron_pt", "std::vector<float>", &electron_pt);
  tree->Branch("electron_eta", "std::vector<float>", &electron_eta);
  tree->Branch("electron_phi", "std::vector<float>", &electron_phi);
  tree->Branch("electron_mass", "std::vector<float>", &electron_mass);
  tree->Branch("electron_id", "std::vector<unsigned int>" , &electron_id);
  tree->Branch("electron_charge", "std::vector<int>" , &electron_charge);
  tree->Branch("electron_d0", "std::vector<float>" , &electron_d0);
  tree->Branch("electron_dz", "std::vector<float>" , &electron_dz);

  tree->Branch("tau_pt", "std::vector<float>" , &tau_pt);
  tree->Branch("tau_eta", "std::vector<float>" , &tau_eta);
  tree->Branch("tau_phi", "std::vector<float>" , &tau_phi);
  tree->Branch("tau_mass", "std::vector<float>" , &tau_mass);
  tree->Branch("tau_decaymode", "std::vector<unsigned int>" , &tau_decaymode);
  tree->Branch("tau_idjet", "std::vector<float>" , &tau_idjet);
  tree->Branch("tau_idele", "std::vector<float>" , &tau_idele);
  tree->Branch("tau_idmu", "std::vector<float>" , &tau_idmu);
  tree->Branch("tau_idjet_wp", "std::vector<unsigned int>" , &tau_idjet_wp);
  tree->Branch("tau_idmu_wp", "std::vector<unsigned int>" , &tau_idmu_wp);
  tree->Branch("tau_idele_wp", "std::vector<unsigned int>" , &tau_idele_wp);
  tree->Branch("tau_charge", "std::vector<int>" , &tau_charge);
  if(isMC){
    tree->Branch("tau_genmatch_pt", "std::vector<float>" , &tau_genmatch_pt);
    tree->Branch("tau_genmatch_eta", "std::vector<float>" , &tau_genmatch_eta);
    tree->Branch("tau_genmatch_phi", "std::vector<float>" , &tau_genmatch_phi);
    tree->Branch("tau_genmatch_mass", "std::vector<float>" , &tau_genmatch_mass);
    tree->Branch("tau_genmatch_decaymode", "std::vector<int>" , &tau_genmatch_decaymode);
  }

  tree->Branch("jet_ak4_pt", "std::vector<float>" , &jet_ak4_pt);
  tree->Branch("jet_ak4_eta", "std::vector<float>" , &jet_ak4_eta);
  tree->Branch("jet_ak4_phi", "std::vector<float>" , &jet_ak4_phi);
  tree->Branch("jet_ak4_mass", "std::vector<float>" , &jet_ak4_mass);
  if(isMC){
    tree->Branch("jet_ak4_genmatch_pt","std::vector<float>", &jet_ak4_genmatch_pt);
    tree->Branch("jet_ak4_genmatch_eta","std::vector<float>", &jet_ak4_genmatch_eta);
    tree->Branch("jet_ak4_genmatch_phi","std::vector<float>", &jet_ak4_genmatch_phi);
    tree->Branch("jet_ak4_genmatch_mass","std::vector<float>", &jet_ak4_genmatch_mass);
  }


  tree->Branch("jet_hlt_ak4_pt", "std::vector<float>" , &jet_hlt_ak4_pt);
  tree->Branch("jet_hlt_ak4_eta", "std::vector<float>" , &jet_hlt_ak4_eta);
  tree->Branch("jet_hlt_ak4_phi", "std::vector<float>" , &jet_hlt_ak4_phi);
  tree->Branch("jet_hlt_ak4_mass", "std::vector<float>" , &jet_hlt_ak4_mass);
  if(isMC){
    tree->Branch("jet_hlt_ak4_genmatch_pt","std::vector<float>", &jet_hlt_ak4_genmatch_pt);
    tree->Branch("jet_hlt_ak4_genmatch_eta","std::vector<float>", &jet_hlt_ak4_genmatch_eta);
    tree->Branch("jet_hlt_ak4_genmatch_phi","std::vector<float>", &jet_hlt_ak4_genmatch_phi);
    tree->Branch("jet_hlt_ak4_genmatch_mass","std::vector<float>", &jet_hlt_ak4_genmatch_mass);
  }


  tree->Branch("jet_pt", "std::vector<float>" , &jet_pt);
  tree->Branch("jet_eta", "std::vector<float>" , &jet_eta);
  tree->Branch("jet_phi", "std::vector<float>" , &jet_phi);
  tree->Branch("jet_mass", "std::vector<float>" , &jet_mass);
  tree->Branch("jet_mass_raw", "std::vector<float>" , &jet_mass_raw);
  tree->Branch("jet_pt_raw", "std::vector<float>" , &jet_pt_raw);
  tree->Branch("jet_chf", "std::vector<float>" , &jet_chf);
  tree->Branch("jet_nhf", "std::vector<float>" , &jet_nhf);
  tree->Branch("jet_elf", "std::vector<float>" , &jet_elf);
  tree->Branch("jet_phf", "std::vector<float>" , &jet_phf);
  tree->Branch("jet_muf", "std::vector<float>" , &jet_muf);
  tree->Branch("jet_tau1", "std::vector<float>" , &jet_tau1);
  tree->Branch("jet_tau2", "std::vector<float>" , &jet_tau2);
  tree->Branch("jet_tau3", "std::vector<float>" , &jet_tau3);
  tree->Branch("jet_tau4", "std::vector<float>" , &jet_tau4);
  tree->Branch("jet_pnet_probXbb","std::vector<float>",&jet_pnet_probXbb);
  tree->Branch("jet_pnet_probXcc","std::vector<float>",&jet_pnet_probXcc);
  tree->Branch("jet_pnet_probXqq","std::vector<float>",&jet_pnet_probXqq);
  tree->Branch("jet_pnet_probQCDbb","std::vector<float>",&jet_pnet_probQCDbb);
  tree->Branch("jet_pnet_probQCDcc","std::vector<float>",&jet_pnet_probQCDcc);
  tree->Branch("jet_pnet_probQCDb","std::vector<float>",&jet_pnet_probQCDb);
  tree->Branch("jet_pnet_probQCDc","std::vector<float>",&jet_pnet_probQCDc);
  tree->Branch("jet_pnet_probQCDothers","std::vector<float>",&jet_pnet_probQCDothers);
  tree->Branch("jet_pnet_regression_mass","std::vector<float>", &jet_pnet_regression_mass);
  tree->Branch("jet_id", "std::vector<unsigned int>" , &jet_id);
  tree->Branch("jet_ncand", "std::vector<unsigned int>" , &jet_ncand);
  tree->Branch("jet_nch", "std::vector<unsigned int>" , &jet_nch);
  tree->Branch("jet_nnh", "std::vector<unsigned int>" , &jet_nnh);
  tree->Branch("jet_nel", "std::vector<unsigned int>" , &jet_nel);
  tree->Branch("jet_nph", "std::vector<unsigned int>" , &jet_nph);
  tree->Branch("jet_nmu", "std::vector<unsigned int>" , &jet_nmu);
  tree->Branch("jet_hflav", "std::vector<unsigned int>" , &jet_hflav);
  tree->Branch("jet_pflav", "std::vector<int>" , &jet_pflav);
  tree->Branch("jet_nbhad", "std::vector<unsigned int>" , &jet_nbhad);
  tree->Branch("jet_nchad", "std::vector<unsigned int>" , &jet_nchad);
  if(isMC){
    tree->Branch("jet_genmatch_pt","std::vector<float>" , &jet_genmatch_pt);
    tree->Branch("jet_genmatch_eta","std::vector<float>" , &jet_genmatch_eta);
    tree->Branch("jet_genmatch_phi","std::vector<float>" , &jet_genmatch_phi);
    tree->Branch("jet_genmatch_mass","std::vector<float>" , &jet_genmatch_mass);
  }

  tree->Branch("jet_softdrop_pt","std::vector<float>", &jet_softdrop_pt);
  tree->Branch("jet_softdrop_eta","std::vector<float>", &jet_softdrop_eta);
  tree->Branch("jet_softdrop_phi","std::vector<float>", &jet_softdrop_phi);
  tree->Branch("jet_softdrop_mass","std::vector<float>", &jet_softdrop_mass);
  tree->Branch("jet_softdrop_pt_raw","std::vector<float>", &jet_softdrop_pt_raw);
  tree->Branch("jet_softdrop_mass_raw","std::vector<float>", &jet_softdrop_mass_raw);

  if(isMC){
    tree->Branch("jet_softdrop_genmatch_pt","std::vector<float>", &jet_softdrop_genmatch_pt);
    tree->Branch("jet_softdrop_genmatch_eta","std::vector<float>", &jet_softdrop_genmatch_eta);
    tree->Branch("jet_softdrop_genmatch_phi","std::vector<float>", &jet_softdrop_genmatch_phi);
    tree->Branch("jet_softdrop_genmatch_mass","std::vector<float>", &jet_softdrop_genmatch_mass);
  }
  
  tree->Branch("jet_softdrop_subjet_pt","std::vector<float>", &jet_softdrop_subjet_pt);
  tree->Branch("jet_softdrop_subjet_eta","std::vector<float>", &jet_softdrop_subjet_eta);
  tree->Branch("jet_softdrop_subjet_phi","std::vector<float>", &jet_softdrop_subjet_phi);
  tree->Branch("jet_softdrop_subjet_mass","std::vector<float>", &jet_softdrop_subjet_mass);
  tree->Branch("jet_softdrop_subjet_pt_raw","std::vector<float>", &jet_softdrop_subjet_pt_raw);
  tree->Branch("jet_softdrop_subjet_mass_raw","std::vector<float>", &jet_softdrop_subjet_mass_raw);  
  tree->Branch("jet_softdrop_subjet_ijet","std::vector<unsigned int>", &jet_softdrop_subjet_ijet);
  tree->Branch("jet_softdrop_subjet_nbhad","std::vector<unsigned int>", &jet_softdrop_subjet_nbhad);
  tree->Branch("jet_softdrop_subjet_nchad","std::vector<unsigned int>", &jet_softdrop_subjet_nchad);
  tree->Branch("jet_softdrop_subjet_hflav","std::vector<unsigned int>", &jet_softdrop_subjet_hflav);
  tree->Branch("jet_softdrop_subjet_pflav","std::vector<int>", &jet_softdrop_subjet_pflav);
  
  if(isMC){
    tree->Branch("jet_softdrop_subjet_genmatch_pt","std::vector<float>", &jet_softdrop_subjet_genmatch_pt);
    tree->Branch("jet_softdrop_subjet_genmatch_eta","std::vector<float>", &jet_softdrop_subjet_genmatch_eta);
    tree->Branch("jet_softdrop_subjet_genmatch_phi","std::vector<float>", &jet_softdrop_subjet_genmatch_phi);
    tree->Branch("jet_softdrop_subjet_genmatch_mass","std::vector<float>", &jet_softdrop_subjet_genmatch_mass);
  }


  tree->Branch("jet_hlt_pt", "std::vector<float>" , &jet_hlt_pt);
  tree->Branch("jet_hlt_eta", "std::vector<float>" , &jet_hlt_eta);
  tree->Branch("jet_hlt_phi", "std::vector<float>" , &jet_hlt_phi);
  tree->Branch("jet_hlt_mass", "std::vector<float>" , &jet_hlt_mass);
  tree->Branch("jet_hlt_mass_raw", "std::vector<float>" , &jet_hlt_mass_raw);
  tree->Branch("jet_hlt_pt_raw", "std::vector<float>" , &jet_hlt_pt_raw);
  tree->Branch("jet_hlt_chf", "std::vector<float>" , &jet_hlt_chf);
  tree->Branch("jet_hlt_nhf", "std::vector<float>" , &jet_hlt_nhf);
  tree->Branch("jet_hlt_elf", "std::vector<float>" , &jet_hlt_elf);
  tree->Branch("jet_hlt_phf", "std::vector<float>" , &jet_hlt_phf);
  tree->Branch("jet_hlt_muf", "std::vector<float>" , &jet_hlt_muf);
  tree->Branch("jet_hlt_tau1", "std::vector<float>" , &jet_hlt_tau1);
  tree->Branch("jet_hlt_tau2", "std::vector<float>" , &jet_hlt_tau2);
  tree->Branch("jet_hlt_tau3", "std::vector<float>" , &jet_hlt_tau3);
  tree->Branch("jet_hlt_tau4", "std::vector<float>" , &jet_hlt_tau4);
  tree->Branch("jet_hlt_pnethlt_probXtt","std::vector<float>",&jet_hlt_pnethlt_probXtt);
  tree->Branch("jet_hlt_pnethlt_probXbb","std::vector<float>",&jet_hlt_pnethlt_probXbb);
  tree->Branch("jet_hlt_pnethlt_probXcc","std::vector<float>",&jet_hlt_pnethlt_probXcc);
  tree->Branch("jet_hlt_pnethlt_probXqq","std::vector<float>",&jet_hlt_pnethlt_probXqq);
  tree->Branch("jet_hlt_pnethlt_probXgg","std::vector<float>",&jet_hlt_pnethlt_probXgg);
  tree->Branch("jet_hlt_pnethlt_probQCD","std::vector<float>",&jet_hlt_pnethlt_probQCD);
  tree->Branch("jet_hlt_id", "std::vector<unsigned int>" , &jet_hlt_id);
  tree->Branch("jet_hlt_ncand", "std::vector<unsigned int>" , &jet_hlt_ncand);
  tree->Branch("jet_hlt_nch", "std::vector<unsigned int>" , &jet_hlt_nch);
  tree->Branch("jet_hlt_nnh", "std::vector<unsigned int>" , &jet_hlt_nnh);
  tree->Branch("jet_hlt_nel", "std::vector<unsigned int>" , &jet_hlt_nel);
  tree->Branch("jet_hlt_nph", "std::vector<unsigned int>" , &jet_hlt_nph);
  tree->Branch("jet_hlt_nmu", "std::vector<unsigned int>" , &jet_hlt_nmu);
  tree->Branch("jet_hlt_hflav", "std::vector<unsigned int>" , &jet_hlt_hflav);
  tree->Branch("jet_hlt_pflav", "std::vector<int>" , &jet_hlt_pflav);
  tree->Branch("jet_hlt_nbhad", "std::vector<unsigned int>" , &jet_hlt_nbhad);
  tree->Branch("jet_hlt_nchad", "std::vector<unsigned int>" , &jet_hlt_nchad);
  if(isMC){
    tree->Branch("jet_hlt_genmatch_pt","std::vector<float>" , &jet_hlt_genmatch_pt);
    tree->Branch("jet_hlt_genmatch_eta","std::vector<float>" , &jet_hlt_genmatch_eta);
    tree->Branch("jet_hlt_genmatch_phi","std::vector<float>" , &jet_hlt_genmatch_phi);
    tree->Branch("jet_hlt_genmatch_mass","std::vector<float>" , &jet_hlt_genmatch_mass);
  }

  tree->Branch("jet_hlt_softdrop_pt","std::vector<float>", &jet_hlt_softdrop_pt);
  tree->Branch("jet_hlt_softdrop_eta","std::vector<float>", &jet_hlt_softdrop_eta);
  tree->Branch("jet_hlt_softdrop_phi","std::vector<float>", &jet_hlt_softdrop_phi);
  tree->Branch("jet_hlt_softdrop_mass","std::vector<float>", &jet_hlt_softdrop_mass);
  tree->Branch("jet_hlt_softdrop_pt_raw","std::vector<float>", &jet_hlt_softdrop_pt_raw);
  tree->Branch("jet_hlt_softdrop_mass_raw","std::vector<float>", &jet_hlt_softdrop_mass_raw);

  if(isMC){
    tree->Branch("jet_hlt_softdrop_genmatch_pt","std::vector<float>", &jet_hlt_softdrop_genmatch_pt);
    tree->Branch("jet_hlt_softdrop_genmatch_eta","std::vector<float>", &jet_hlt_softdrop_genmatch_eta);
    tree->Branch("jet_hlt_softdrop_genmatch_phi","std::vector<float>", &jet_hlt_softdrop_genmatch_phi);
    tree->Branch("jet_hlt_softdrop_genmatch_mass","std::vector<float>", &jet_hlt_softdrop_genmatch_mass);
  }
  
  tree->Branch("jet_hlt_softdrop_subjet_pt","std::vector<float>", &jet_hlt_softdrop_subjet_pt);
  tree->Branch("jet_hlt_softdrop_subjet_eta","std::vector<float>", &jet_hlt_softdrop_subjet_eta);
  tree->Branch("jet_hlt_softdrop_subjet_phi","std::vector<float>", &jet_hlt_softdrop_subjet_phi);
  tree->Branch("jet_hlt_softdrop_subjet_mass","std::vector<float>", &jet_hlt_softdrop_subjet_mass);
  tree->Branch("jet_hlt_softdrop_subjet_pt_raw","std::vector<float>", &jet_hlt_softdrop_subjet_pt_raw);
  tree->Branch("jet_hlt_softdrop_subjet_mass_raw","std::vector<float>", &jet_hlt_softdrop_subjet_mass_raw);  
  tree->Branch("jet_hlt_softdrop_subjet_ijet","std::vector<unsigned int>", &jet_hlt_softdrop_subjet_ijet);
  tree->Branch("jet_hlt_softdrop_subjet_nbhad","std::vector<unsigned int>", &jet_hlt_softdrop_subjet_nbhad);
  tree->Branch("jet_hlt_softdrop_subjet_nchad","std::vector<unsigned int>", &jet_hlt_softdrop_subjet_nchad);
  tree->Branch("jet_hlt_softdrop_subjet_hflav","std::vector<unsigned int>", &jet_hlt_softdrop_subjet_hflav);
  tree->Branch("jet_hlt_softdrop_subjet_pflav","std::vector<int>", &jet_hlt_softdrop_subjet_pflav);
  
  if(isMC){
    tree->Branch("jet_hlt_softdrop_subjet_genmatch_pt","std::vector<float>", &jet_hlt_softdrop_subjet_genmatch_pt);
    tree->Branch("jet_hlt_softdrop_subjet_genmatch_eta","std::vector<float>", &jet_hlt_softdrop_subjet_genmatch_eta);
    tree->Branch("jet_hlt_softdrop_subjet_genmatch_phi","std::vector<float>", &jet_hlt_softdrop_subjet_genmatch_phi);
    tree->Branch("jet_hlt_softdrop_subjet_genmatch_mass","std::vector<float>", &jet_hlt_softdrop_subjet_genmatch_mass);
  }
  
  tree->Branch("jet_hlt_sv_pt", "std::vector<float>" , &jet_hlt_sv_pt);
  tree->Branch("jet_hlt_sv_eta", "std::vector<float>" , &jet_hlt_sv_eta);
  tree->Branch("jet_hlt_sv_phi", "std::vector<float>" , &jet_hlt_sv_phi);
  tree->Branch("jet_hlt_sv_mass", "std::vector<float>" , &jet_hlt_sv_mass);
  tree->Branch("jet_hlt_sv_energy", "std::vector<float>" , &jet_hlt_sv_energy);
  tree->Branch("jet_hlt_sv_chi2", "std::vector<float>" , &jet_hlt_sv_chi2);
  tree->Branch("jet_hlt_sv_dxy", "std::vector<float>" , &jet_hlt_sv_dxy);
  tree->Branch("jet_hlt_sv_dxysig", "std::vector<float>" , &jet_hlt_sv_dxysig);
  tree->Branch("jet_hlt_sv_d3d", "std::vector<float>" , &jet_hlt_sv_d3d);
  tree->Branch("jet_hlt_sv_d3dsig", "std::vector<float>" , &jet_hlt_sv_d3dsig);
  tree->Branch("jet_hlt_sv_ntrack", "std::vector<unsigned int>" , &jet_hlt_sv_ntrack);
  tree->Branch("jet_hlt_sv_ijet", "std::vector<unsigned int>" , &jet_hlt_sv_ijet);
  
  tree->Branch("jet_hlt_pfcandidate_pt","std::vector<float>",&jet_hlt_pfcandidate_pt);
  tree->Branch("jet_hlt_pfcandidate_eta","std::vector<float>",&jet_hlt_pfcandidate_eta);
  tree->Branch("jet_hlt_pfcandidate_phi","std::vector<float>",&jet_hlt_pfcandidate_phi);
  tree->Branch("jet_hlt_pfcandidate_mass","std::vector<float>",&jet_hlt_pfcandidate_mass);
  tree->Branch("jet_hlt_pfcandidate_energy","std::vector<float>",&jet_hlt_pfcandidate_energy);
  tree->Branch("jet_hlt_pfcandidate_calofraction","std::vector<float>",&jet_hlt_pfcandidate_calofraction);
  tree->Branch("jet_hlt_pfcandidate_hcalfraction","std::vector<float>",&jet_hlt_pfcandidate_hcalfraction);
  tree->Branch("jet_hlt_pfcandidate_dz","std::vector<float>",&jet_hlt_pfcandidate_dz);
  tree->Branch("jet_hlt_pfcandidate_dzsig","std::vector<float>",&jet_hlt_pfcandidate_dzsig);
  tree->Branch("jet_hlt_pfcandidate_dxy","std::vector<float>",&jet_hlt_pfcandidate_dxy);
  tree->Branch("jet_hlt_pfcandidate_dxysig","std::vector<float>",&jet_hlt_pfcandidate_dxysig);
  tree->Branch("jet_hlt_pfcandidate_track_chi2","std::vector<unsigned int>",&jet_hlt_pfcandidate_track_chi2);
  tree->Branch("jet_hlt_pfcandidate_candjet_pperp_ratio","std::vector<float>",&jet_hlt_pfcandidate_candjet_pperp_ratio);
  tree->Branch("jet_hlt_pfcandidate_candjet_ppara_ratio","std::vector<float>",&jet_hlt_pfcandidate_candjet_ppara_ratio);
  tree->Branch("jet_hlt_pfcandidate_candjet_deta","std::vector<float>",&jet_hlt_pfcandidate_candjet_deta);
  tree->Branch("jet_hlt_pfcandidate_candjet_dphi","std::vector<float>",&jet_hlt_pfcandidate_candjet_dphi);
  tree->Branch("jet_hlt_pfcandidate_candjet_etarel","std::vector<float>",&jet_hlt_pfcandidate_candjet_etarel);
  tree->Branch("jet_hlt_pfcandidate_trackjet_dxy","std::vector<float>",&jet_hlt_pfcandidate_trackjet_dxy);
  tree->Branch("jet_hlt_pfcandidate_trackjet_dxysig","std::vector<float>",&jet_hlt_pfcandidate_trackjet_dxysig);
  tree->Branch("jet_hlt_pfcandidate_trackjet_d3d","std::vector<float>",&jet_hlt_pfcandidate_trackjet_d3d);
  tree->Branch("jet_hlt_pfcandidate_trackjet_d3dsig","std::vector<float>",&jet_hlt_pfcandidate_trackjet_d3dsig);
  tree->Branch("jet_hlt_pfcandidate_trackjet_d3d","std::vector<float>",&jet_hlt_pfcandidate_trackjet_d3d);
  tree->Branch("jet_hlt_pfcandidate_trackjet_dist","std::vector<float>",&jet_hlt_pfcandidate_trackjet_dist);
  tree->Branch("jet_hlt_pfcandidate_trackjet_decayL","std::vector<float>",&jet_hlt_pfcandidate_trackjet_decayL);
  tree->Branch("jet_hlt_pfcandidate_frompv","std::vector<unsigned int>",&jet_hlt_pfcandidate_frompv);
  tree->Branch("jet_hlt_pfcandidate_highpurity","std::vector<unsigned int>",&jet_hlt_pfcandidate_highpurity);
  tree->Branch("jet_hlt_pfcandidate_track_qual","std::vector<unsigned int>",&jet_hlt_pfcandidate_track_qual);
  tree->Branch("jet_hlt_pfcandidate_npixhits","std::vector<unsigned int>",&jet_hlt_pfcandidate_npixhits);
  tree->Branch("jet_hlt_pfcandidate_nstriphits","std::vector<unsigned int>",&jet_hlt_pfcandidate_nstriphits);
  tree->Branch("jet_hlt_pfcandidate_nlostinnerhits","std::vector<int>",&jet_hlt_pfcandidate_nlostinnerhits);
  tree->Branch("jet_hlt_pfcandidate_id","std::vector<unsigned int>",&jet_hlt_pfcandidate_id);
  tree->Branch("jet_hlt_pfcandidate_charge","std::vector<int>",&jet_hlt_pfcandidate_charge);
  tree->Branch("jet_hlt_pfcandidate_ijet","std::vector<unsigned int>",&jet_hlt_pfcandidate_ijet);    
}

void TrainingNtupleMakerAK8::endJob() {}

void TrainingNtupleMakerAK8::beginRun(edm::Run const& iRun, edm::EventSetup const& iSetup) {


  HLTConfigProvider fltrConfig;
  bool flag = false;
  fltrConfig.init(iRun, iSetup, filterResultsTag.process(), flag);
  
  // MET filter Paths
  filterPathsMap.clear();
  filterPathsVector.clear();
  filterPathsVector.push_back("Flag_goodVertices");
  filterPathsVector.push_back("Flag_globalSuperTightHalo2016Filter");
  filterPathsVector.push_back("Flag_HBHENoiseFilter");
  filterPathsVector.push_back("Flag_HBHENoiseIsoFilter");
  filterPathsVector.push_back("Flag_EcalDeadCellTriggerPrimitiveFilter");
  filterPathsVector.push_back("Flag_BadPFMuonFilter");
  filterPathsVector.push_back("Flag_BadChargedCandidateFilter");
  
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    filterPathsMap[filterPathsVector[i]] = -1;
  }
  
  for(size_t i = 0; i < filterPathsVector.size(); i++){
    TPRegexp pattern(filterPathsVector[i]);
    for(size_t j = 0; j < fltrConfig.triggerNames().size(); j++){
      std::string pathName = fltrConfig.triggerNames()[j];
      if(TString(pathName).Contains(pattern)){
	filterPathsMap[filterPathsVector[i]] = j;
      }
    }
  }

  // HLT + L1 Trigger
  triggerPathsMap.clear();
  triggerL1SeedMap.clear();
  HLTConfigProvider hltConfig;
  hltConfig.init(iRun, iSetup, triggerResultsTag.process(), flag);
  for(size_t i = 0; i < hltConfig.triggerNames().size(); i++){
    std::string pathName = hltConfig.triggerNames()[i];
    if(pathName == "HLTriggerFinalPath" or pathName=="HLTriggerFirstPath") continue;
    triggerPathsMap[pathName] = i;
    std::vector<std::string> tokens;
    for(auto module : hltConfig.hltL1TSeeds(i)){
      split(tokens,module,boost::algorithm::is_any_of("OR"));
      for(auto seed : tokens){
	if(seed == "") continue;
	seed.erase(std::remove(seed.begin(),seed.end(),' '),seed.end());
	triggerL1SeedMap[seed].push_back(pathName); // fix all prescales to 1
      }
    }
  }

  bool changedHLTPSP = false;
  hltPrescaleProvider->init(iRun, iSetup, triggerResultsTag.process(), changedHLTPSP); 

}


void TrainingNtupleMakerAK8::endRun(edm::Run const&, edm::EventSetup const&) {
}

void TrainingNtupleMakerAK8::initializeBranches(){

  event  = 0;
  run    = 0;
  lumi   = 0;
  putrue = 0;
  flags  = 0;
  wgt    = 0.;

  rho    = 0.;
  rho_hlt = 0.;
  
  gen_particle_pt.clear();
  gen_particle_eta.clear();
  gen_particle_phi.clear();
  gen_particle_mass.clear();
  gen_particle_id.clear();
  gen_particle_status.clear();
  gen_particle_daughters_id.clear();
  gen_particle_daughters_igen.clear();
  gen_particle_daughters_pt.clear();
  gen_particle_daughters_eta.clear();
  gen_particle_daughters_phi.clear();
  gen_particle_daughters_mass.clear();
  gen_particle_daughters_status.clear();
  gen_particle_daughters_charge.clear();

  lhe_particle_pt.clear();
  lhe_particle_eta.clear();
  lhe_particle_phi.clear();
  lhe_particle_mass.clear();
  lhe_particle_id.clear();
  lhe_particle_status.clear();

  trigger_hlt_pass.clear();
  trigger_hlt_path.clear();
  trigger_hlt_prescale.clear();

  trigger_l1_pass.clear();
  trigger_l1_seed.clear();
  trigger_l1_prescale.clear();

  trigger_l1_jet_pt.clear();
  trigger_l1_jet_eta.clear();
  trigger_l1_jet_phi.clear();
  trigger_l1_jet_mass.clear();
  trigger_l1_muon_pt.clear();
  trigger_l1_muon_eta.clear();
  trigger_l1_muon_phi.clear();
  trigger_l1_muon_mass.clear();
  trigger_l1_muon_qual.clear();
  trigger_l1_muon_iso.clear();
  trigger_l1_ht = 0;
  trigger_l1_mht = 0;

  muon_pt.clear();
  muon_eta.clear();
  muon_phi.clear();
  muon_mass.clear();
  muon_id.clear();
  muon_charge.clear();
  muon_iso.clear();
  muon_d0.clear();
  muon_dz.clear();

  electron_pt.clear();
  electron_eta.clear();
  electron_phi.clear();
  electron_mass.clear();
  electron_id.clear();
  electron_charge.clear();
  electron_d0.clear();
  electron_dz.clear();

  tau_pt.clear();
  tau_eta.clear();
  tau_phi.clear();
  tau_mass.clear();
  tau_decaymode.clear();
  tau_idjet.clear();
  tau_idele.clear();
  tau_idmu.clear();
  tau_idjet_wp.clear();
  tau_idmu_wp.clear();
  tau_idele_wp.clear();
  tau_charge.clear();
  tau_genmatch_pt.clear();
  tau_genmatch_eta.clear();
  tau_genmatch_phi.clear();
  tau_genmatch_mass.clear();
  tau_genmatch_decaymode.clear();

  met = 0.;
  met_phi = 0;

  npv = 0;
  nsv = 0;
  npv_hlt = 0;
  nsv_hlt = 0;

  jet_pt.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_mass.clear();
  jet_pt_raw.clear();
  jet_mass_raw.clear();
  jet_chf.clear();
  jet_nhf.clear();
  jet_elf.clear();
  jet_phf.clear();
  jet_muf.clear();
  jet_pnet_probXbb.clear();
  jet_pnet_probXcc.clear();
  jet_pnet_probXqq.clear();
  jet_pnet_probXgg.clear();
  jet_pnet_probQCD.clear();
  jet_pnet_probQCDbb.clear();
  jet_pnet_probQCDcc.clear();
  jet_pnet_probQCDb.clear();
  jet_pnet_probQCDc.clear();
  jet_pnet_probQCDothers.clear();
  jet_pnet_regression_mass.clear();
  jet_id.clear();
  jet_ncand.clear();
  jet_nch.clear();
  jet_nnh.clear();
  jet_nel.clear();
  jet_nph.clear();
  jet_nmu.clear();
  jet_hflav.clear();
  jet_pflav.clear();
  jet_nbhad.clear();
  jet_nchad.clear();
  jet_genmatch_pt.clear();
  jet_genmatch_eta.clear();
  jet_genmatch_phi.clear();
  jet_genmatch_mass.clear();
  jet_tau1.clear();
  jet_tau2.clear();
  jet_tau3.clear();
  jet_tau4.clear();
  jet_softdrop_pt.clear();
  jet_softdrop_eta.clear();
  jet_softdrop_phi.clear();
  jet_softdrop_mass.clear();
  jet_softdrop_pt_raw.clear();
  jet_softdrop_mass_raw.clear();
  jet_softdrop_genmatch_pt.clear();
  jet_softdrop_genmatch_eta.clear();
  jet_softdrop_genmatch_phi.clear();
  jet_softdrop_genmatch_mass.clear();  
  jet_softdrop_subjet_pt.clear();
  jet_softdrop_subjet_eta.clear();
  jet_softdrop_subjet_phi.clear();
  jet_softdrop_subjet_mass.clear();
  jet_softdrop_subjet_pt_raw.clear();
  jet_softdrop_subjet_mass_raw.clear();
  jet_softdrop_subjet_ijet.clear(); 
  jet_softdrop_subjet_nbhad.clear();
  jet_softdrop_subjet_nchad.clear();
  jet_softdrop_subjet_hflav.clear();
  jet_softdrop_subjet_pflav.clear();
  jet_softdrop_subjet_genmatch_pt.clear();
  jet_softdrop_subjet_genmatch_eta.clear();
  jet_softdrop_subjet_genmatch_phi.clear();
  jet_softdrop_subjet_genmatch_mass.clear(); 

  jet_hlt_pt.clear();
  jet_hlt_eta.clear();
  jet_hlt_phi.clear();
  jet_hlt_mass.clear();
  jet_hlt_pt_raw.clear();
  jet_hlt_mass_raw.clear();
  jet_hlt_chf.clear();
  jet_hlt_nhf.clear();
  jet_hlt_elf.clear();
  jet_hlt_phf.clear();
  jet_hlt_muf.clear();
  jet_hlt_pnethlt_probXtt.clear();
  jet_hlt_pnethlt_probXbb.clear();
  jet_hlt_pnethlt_probXcc.clear();
  jet_hlt_pnethlt_probXqq.clear();
  jet_hlt_pnethlt_probXgg.clear();
  jet_hlt_pnethlt_probQCD.clear();
  jet_hlt_id.clear();
  jet_hlt_ncand.clear();
  jet_hlt_nch.clear();
  jet_hlt_nnh.clear();
  jet_hlt_nel.clear();
  jet_hlt_nph.clear();
  jet_hlt_nmu.clear();
  jet_hlt_hflav.clear();
  jet_hlt_pflav.clear();
  jet_hlt_nbhad.clear();
  jet_hlt_nchad.clear();
  jet_hlt_genmatch_pt.clear();
  jet_hlt_genmatch_eta.clear();
  jet_hlt_genmatch_phi.clear();
  jet_hlt_genmatch_mass.clear();
  jet_hlt_tau1.clear();
  jet_hlt_tau2.clear();
  jet_hlt_tau3.clear();
  jet_hlt_tau4.clear();
  jet_hlt_softdrop_pt.clear();
  jet_hlt_softdrop_eta.clear();
  jet_hlt_softdrop_phi.clear();
  jet_hlt_softdrop_mass.clear();
  jet_hlt_softdrop_pt_raw.clear();
  jet_hlt_softdrop_mass_raw.clear();
  jet_hlt_softdrop_genmatch_pt.clear();
  jet_hlt_softdrop_genmatch_eta.clear();
  jet_hlt_softdrop_genmatch_phi.clear();
  jet_hlt_softdrop_genmatch_mass.clear();  
  jet_hlt_softdrop_subjet_pt.clear();
  jet_hlt_softdrop_subjet_eta.clear();
  jet_hlt_softdrop_subjet_phi.clear();
  jet_hlt_softdrop_subjet_mass.clear();
  jet_hlt_softdrop_subjet_pt_raw.clear();
  jet_hlt_softdrop_subjet_mass_raw.clear();
  jet_hlt_softdrop_subjet_ijet.clear(); 
  jet_hlt_softdrop_subjet_nbhad.clear();
  jet_hlt_softdrop_subjet_nchad.clear();
  jet_hlt_softdrop_subjet_hflav.clear();
  jet_hlt_softdrop_subjet_pflav.clear();
  jet_hlt_softdrop_subjet_genmatch_pt.clear();
  jet_hlt_softdrop_subjet_genmatch_eta.clear();
  jet_hlt_softdrop_subjet_genmatch_phi.clear();
  jet_hlt_softdrop_subjet_genmatch_mass.clear(); 

  // jet pf candidates
  jet_hlt_pfcandidate_pt.clear();
  jet_hlt_pfcandidate_eta.clear();
  jet_hlt_pfcandidate_phi.clear(); 
  jet_hlt_pfcandidate_mass.clear();
  jet_hlt_pfcandidate_energy.clear();
  jet_hlt_pfcandidate_calofraction.clear();
  jet_hlt_pfcandidate_hcalfraction.clear();
  jet_hlt_pfcandidate_dz.clear();
  jet_hlt_pfcandidate_dxy.clear();
  jet_hlt_pfcandidate_dzsig.clear();
  jet_hlt_pfcandidate_dxysig.clear();
  jet_hlt_pfcandidate_frompv.clear();
  jet_hlt_pfcandidate_npixhits.clear();
  jet_hlt_pfcandidate_nstriphits.clear();
  jet_hlt_pfcandidate_highpurity.clear();
  jet_hlt_pfcandidate_id.clear();
  jet_hlt_pfcandidate_ijet.clear();
  jet_hlt_pfcandidate_nlostinnerhits.clear();
  jet_hlt_pfcandidate_charge.clear();
  jet_hlt_pfcandidate_candjet_pperp_ratio.clear();
  jet_hlt_pfcandidate_candjet_ppara_ratio.clear();
  jet_hlt_pfcandidate_candjet_deta.clear();
  jet_hlt_pfcandidate_candjet_dphi.clear();
  jet_hlt_pfcandidate_candjet_etarel.clear();
  jet_hlt_pfcandidate_track_chi2.clear();
  jet_hlt_pfcandidate_track_qual.clear();
  jet_hlt_pfcandidate_trackjet_dxy.clear();
  jet_hlt_pfcandidate_trackjet_dxysig.clear();
  jet_hlt_pfcandidate_trackjet_d3d.clear();
  jet_hlt_pfcandidate_trackjet_d3dsig.clear();
  jet_hlt_pfcandidate_trackjet_dist.clear();
  jet_hlt_pfcandidate_trackjet_decayL.clear();
  // jet-to-secondary vertex
  jet_hlt_sv_pt.clear();
  jet_hlt_sv_eta.clear();
  jet_hlt_sv_phi.clear();
  jet_hlt_sv_mass.clear();
  jet_hlt_sv_energy.clear();
  jet_hlt_sv_chi2.clear();
  jet_hlt_sv_dxy.clear();
  jet_hlt_sv_dxysig.clear();
  jet_hlt_sv_d3d.clear();
  jet_hlt_sv_d3dsig.clear();
  jet_hlt_sv_ntrack.clear();
  jet_hlt_sv_ijet.clear();

  jet_hlt_ak4_pt.clear();
  jet_hlt_ak4_eta.clear();
  jet_hlt_ak4_phi.clear();
  jet_hlt_ak4_mass.clear();
  jet_hlt_ak4_genmatch_pt.clear();
  jet_hlt_ak4_genmatch_eta.clear();
  jet_hlt_ak4_genmatch_phi.clear();
  jet_hlt_ak4_genmatch_mass.clear();

  jet_ak4_pt.clear();
  jet_ak4_eta.clear();
  jet_ak4_phi.clear();
  jet_ak4_mass.clear();
  jet_ak4_genmatch_pt.clear();
  jet_ak4_genmatch_eta.clear();
  jet_ak4_genmatch_phi.clear();
  jet_ak4_genmatch_mass.clear();

}

void TrainingNtupleMakerAK8::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

// to apply jet ID: https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL
bool TrainingNtupleMakerAK8::applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi){
  
  if(level != "tight" and level != "tightLepVeto")
    return true;
  
  double eta  = jet.eta();                                                                                                                                                                           
  double nhf  = jet.neutralHadronEnergyFraction();                                                                                                                                                   
  double nemf = jet.neutralEmEnergyFraction();                                                                                                                                                       
  double chf  = jet.chargedHadronEnergyFraction();                                                                                                                                                   
  double muf  = jet.muonEnergyFraction();                                                                                                                                                            
  double cemf = jet.chargedEmEnergyFraction();                                                                                                                            
  
  int jetid  = 0;
  if(isPuppi){ // Puppi jets
    float np, nnp, nch;
    if (jet.hasUserFloat("patPuppiJetSpecificProducer:puppiMultiplicity") and jet.hasUserFloat("patPuppiJetSpecificProducer:neutralPuppiMultiplicity")){
      np   = jet.userFloat("patPuppiJetSpecificProducer:puppiMultiplicity");
      nnp  = jet.userFloat("patPuppiJetSpecificProducer:neutralPuppiMultiplicity");
      nch  = np-nnp;
    }
    else{
      np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();
      nnp  = jet.neutralMultiplicity();
      nch  = jet.chargedMultiplicity();
    }
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 1;                                                                                               
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 2;                                                                                               
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 1;                                                                                                                        
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 2;                                                                                                                    
  }
  else{ // CHS jets
    int np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();                                                                                                                              
    int nnp  = jet.neutralMultiplicity();                                                                                                                                                        
    int nch  = jet.chargedMultiplicity();                                                                                                                                                          
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;    
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 1;                                                                                               
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 2;                                        
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 1;                                                                                                                      
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 2;                                                          
  }
                    
  if(level == "tight" and jetid > 1) return true;
  else if(level == "tightLepVeto" and jetid > 2) return true;
  else return false;

}

DEFINE_FWK_MODULE(TrainingNtupleMakerAK8);
