// Standard C++ includes
#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>

// ROOT includes
#include <TTree.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

// CMSSW framework includes
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/ESGetToken.h"

// CMSSW data formats
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/METReco/interface/PFMET.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/Candidate/interface/VertexCompositePtrCandidate.h"
#include "DataFormats/L1Trigger/interface/Jet.h"    
#include "DataFormats/L1Trigger/interface/Muon.h"   
#include "DataFormats/L1Trigger/interface/EtSum.h"   
#include "DataFormats/JetMatching/interface/JetFlavourInfoMatching.h"
#include "DataFormats/L1TGlobal/interface/GlobalAlgBlk.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/GenLumiInfoHeader.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/GenLumiInfoHeader.h"

// Other relevant CMSSW includes
#include "CommonTools/UtilAlgos/interface/TFileService.h" 

#include "CondFormats/L1TObjects/interface/L1TUtmTriggerMenu.h"
#include "CondFormats/DataRecord/interface/L1TUtmTriggerMenuRcd.h"

#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "HLTrigger/HLTcore/interface/HLTPrescaleProvider.h"

// For dumping b-tagging info
#include "DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h"
#include "RecoBTag/FeatureTools/interface/TrackInfoBuilder.h"
#include "RecoVertex/VertexTools/interface/VertexDistanceXY.h"
#include "RecoVertex/VertexTools/interface/VertexDistance3D.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"

class ValidationNtupleMakerAK4 : public edm::one::EDAnalyzer<edm::one::SharedResources, edm::one::WatchRuns> {

public:
  explicit ValidationNtupleMakerAK4(const edm::ParameterSet&);
  ~ValidationNtupleMakerAK4();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  bool applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi);
  bool applyPileupJetID(const pat::Jet & jet, const std::string & level);
 
private:

  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;  
  virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
  virtual void endRun(edm::Run const&, edm::EventSetup const&) override;

  void initializeBranches();

  // MC information
  const edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupInfoToken;
  const edm::EDGetTokenT<reco::GenParticleCollection > gensToken;
  const edm::EDGetTokenT<GenEventInfoProduct> genEvtInfoToken;
  const edm::EDGetTokenT<LHEEventProduct> lheInfoToken;

  // Filter result
  const edm::InputTag filterResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> filterResultsToken;
  const edm::InputTag triggerResultsTag;
  const edm::EDGetTokenT<edm::TriggerResults> triggerResultsToken;

  // HLT result
  const edm::EDGetTokenT<GlobalAlgBlkBxCollection> l1AlgosToken;
  const edm::EDGetTokenT<l1t::JetBxCollection>   l1JetToken;
  const edm::EDGetTokenT<l1t::MuonBxCollection>  l1MuonToken;
  const edm::EDGetTokenT<l1t::EtSumBxCollection> l1EtSumToken;
  const edm::ESGetToken<L1TUtmTriggerMenu, L1TUtmTriggerMenuRcd> l1GtMenuToken;

  // Vertices from miniAOD
  const edm::EDGetTokenT<reco::VertexCollection > primaryVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> secondaryVerticesToken;
  edm::EDGetTokenT< double > rhoToken;

  // Pat objects from miniAOD
  const edm::EDGetTokenT<pat::MuonCollection> muonsToken;
  const edm::EDGetTokenT<pat::ElectronCollection> electronsToken;
  const edm::EDGetTokenT<pat::TauCollection> tausToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsToken;
  const edm::EDGetTokenT<pat::METCollection> metToken;

  // Vertices from HLT
  edm::EDGetTokenT< double > rhoHLTToken;

  // Trigger level objects
  const edm::EDGetTokenT<pat::JetCollection> jetsHLTToken;
  const edm::EDGetTokenT<reco::PFJetCollection> pixelJetsHLTToken;

  // GEN level jets
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsWnuToken;
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsToken;
  const edm::EDGetTokenT<reco::JetFlavourInfoMatchingCollection> genJetsFlavourToken;
  const edm::EDGetTokenT<reco::GenJetCollection > hltGenJetsToken;
  const edm::EDGetTokenT<reco::JetFlavourInfoMatchingCollection> hltGenJetsFlavourToken;

  // Transient track
  const edm::ESGetToken<TransientTrackBuilder, TransientTrackRecord> trackBuilderToken;

  // MET filters
  std::vector<std::string>   filterPathsVector;
  std::map<std::string, int> filterPathsMap;

  // HLT paths                                                                                                                                                                                       
  std::map<std::string,int> triggerPathsMap;
  
  // L1 triggers
  std::map<std::string,std::vector<std::string> > triggerL1SeedMap;
  std::unique_ptr<HLTPrescaleProvider> hltPrescaleProvider;

  // Flag for the analyzer
  float muonPtMin, muonEtaMax;
  float electronPtMin, electronEtaMax;
  float tauPtMin, tauEtaMax;
  float jetPtMin, jetEtaMin, jetEtaMax;
  float jetPFCandidatePtMin;
  float dRJetGenMatch;
  bool  dumpOnlyJetMatchedToGen;
  bool  saveLHEObjects;
  bool  saveL1Objects;
  bool  usePuppiJets;
  bool  isMC;
  std::vector<std::string> pnetDiscriminatorLabels;
  std::vector<std::string> pnetDiscriminatorNames;

  // Event coordinates
  unsigned event, run, lumi;
  // Pileup information
  unsigned int putrue;
  // Flags for various event filters
  unsigned int flags;
  // Cross-section and event weight information for MC events
  float xsec, wgt;
  // Rho
  float rho;
  float rho_hlt;
  // Generator-level information (GEN particles)
  std::vector<float>  gen_particle_pt;
  std::vector<float>  gen_particle_eta;
  std::vector<float>  gen_particle_phi;
  std::vector<float>  gen_particle_mass;
  std::vector<int>    gen_particle_id;
  std::vector<unsigned int>  gen_particle_status;
  std::vector<int>    gen_particle_daughters_id;
  std::vector<unsigned int> gen_particle_daughters_igen;
  std::vector<unsigned int> gen_particle_daughters_status;
  std::vector<float>  gen_particle_daughters_pt;
  std::vector<float>  gen_particle_daughters_eta;
  std::vector<float>  gen_particle_daughters_phi;
  std::vector<float>  gen_particle_daughters_mass;
  std::vector<int>    gen_particle_daughters_charge;
  // LHE particles
  std::vector<float>  lhe_particle_pt;
  std::vector<float>  lhe_particle_eta;
  std::vector<float>  lhe_particle_phi;
  std::vector<float>  lhe_particle_mass;
  std::vector<int>    lhe_particle_id;
  std::vector<unsigned int>  lhe_particle_status;
  // HLT
  std::vector<unsigned int> trigger_hlt_pass;
  std::vector<std::string>  trigger_hlt_path;
  std::vector<int>  trigger_hlt_prescale;
  // L1 
  std::vector<unsigned int> trigger_l1_pass;
  std::vector<std::string>  trigger_l1_seed;
  std::vector<int>  trigger_l1_prescale;
  std::vector<float> trigger_l1_jet_pt;
  std::vector<float> trigger_l1_jet_eta;
  std::vector<float> trigger_l1_jet_phi;
  std::vector<float> trigger_l1_jet_mass;
  std::vector<float> trigger_l1_muon_pt;
  std::vector<float> trigger_l1_muon_eta;
  std::vector<float> trigger_l1_muon_phi;
  std::vector<float> trigger_l1_muon_mass;
  std::vector<float> trigger_l1_muon_qual;
  std::vector<float> trigger_l1_muon_iso;
  float trigger_l1_ht;
  float trigger_l1_mht;
  // Vertex RECO
  unsigned int npv;
  unsigned int nsv;
  // Collection of muon 4-vectors, muon ID bytes, muon isolation values
  std::vector<float>  muon_pt;
  std::vector<float>  muon_eta;
  std::vector<float>  muon_phi;
  std::vector<float>  muon_mass;
  std::vector<unsigned int> muon_id;
  std::vector<unsigned int> muon_iso;
  std::vector<int>    muon_charge;
  std::vector<float>  muon_d0;
  std::vector<float>  muon_dz;
  // Collection of electron 4-vectors, electron ID bytes, electron isolation values
  std::vector<float>  electron_pt;
  std::vector<float>  electron_eta;
  std::vector<float>  electron_phi;
  std::vector<float>  electron_mass;
  std::vector<unsigned int> electron_id;
  std::vector<int>    electron_charge;
  std::vector<float>  electron_d0;
  std::vector<float>  electron_dz;  
  // Collection of taus 4-vectors, tau ID bytes, tau isolation values
  std::vector<float>  tau_pt;
  std::vector<float>  tau_eta;
  std::vector<float>  tau_phi;
  std::vector<float>  tau_mass;
  std::vector<unsigned int>  tau_decaymode;
  std::vector<float>  tau_idjet;
  std::vector<float>  tau_idele;
  std::vector<float>  tau_idmu;
  std::vector<unsigned int> tau_idjet_wp;
  std::vector<unsigned int> tau_idmu_wp;
  std::vector<unsigned int> tau_idele_wp;
  std::vector<int>    tau_charge;  
  std::vector<float>  tau_genmatch_pt;
  std::vector<float>  tau_genmatch_eta;
  std::vector<float>  tau_genmatch_phi;
  std::vector<float>  tau_genmatch_mass;
  std::vector<int> tau_genmatch_decaymode;
  // MET
  float met, met_phi;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_pt;
  std::vector<float> jet_pt_raw;
  std::vector<float> jet_eta;
  std::vector<float> jet_phi;
  std::vector<float> jet_mass;
  std::vector<float> jet_mass_raw;
  std::vector<float> jet_chf;
  std::vector<float> jet_nhf;
  std::vector<float> jet_elf;
  std::vector<float> jet_phf;
  std::vector<float> jet_muf;
  std::vector<float> jet_deepjet_probb;
  std::vector<float> jet_deepjet_probbb;
  std::vector<float> jet_deepjet_problepb;
  std::vector<float> jet_deepjet_probc;
  std::vector<float> jet_deepjet_probg;
  std::vector<float> jet_deepjet_probuds;
  std::vector<float> jet_pnet_probb;
  std::vector<float> jet_pnet_probbb;
  std::vector<float> jet_pnet_probc;
  std::vector<float> jet_pnet_probcc;
  std::vector<float> jet_pnet_probuds;
  std::vector<float> jet_pnet_probg;
  std::vector<float> jet_pnet_probpu;
  std::vector<float> jet_pnet_probundef;
  std::map<std::string,std::vector<float> > jet_pnetlast_score;
  std::vector<unsigned int> jet_id;
  std::vector<unsigned int> jet_puid;
  std::vector<unsigned int> jet_ncand;
  std::vector<unsigned int> jet_nch;
  std::vector<unsigned int> jet_nnh;
  std::vector<unsigned int> jet_nel;
  std::vector<unsigned int> jet_nph;
  std::vector<unsigned int> jet_nmu;
  std::vector<unsigned int> jet_hflav;
  std::vector<int> jet_pflav;
  std::vector<unsigned int> jet_nbhad;
  std::vector<unsigned int> jet_nchad;
  std::vector<float> jet_genmatch_pt;
  std::vector<float> jet_genmatch_eta;
  std::vector<float> jet_genmatch_phi;
  std::vector<float> jet_genmatch_mass;
  std::vector<unsigned int> jet_genmatch_hflav;
  std::vector<int> jet_genmatch_pflav;
  std::vector<unsigned int> jet_genmatch_nbhad;
  std::vector<unsigned int> jet_genmatch_nchad;
  std::vector<float> jet_genmatch_wnu_pt;
  std::vector<float> jet_genmatch_wnu_eta;
  std::vector<float> jet_genmatch_wnu_phi;
  std::vector<float> jet_genmatch_wnu_mass;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_hlt_pt;
  std::vector<float> jet_hlt_pt_raw;
  std::vector<float> jet_hlt_eta;
  std::vector<float> jet_hlt_phi;
  std::vector<float> jet_hlt_mass;
  std::vector<float> jet_hlt_mass_raw;
  std::vector<float> jet_hlt_chf;
  std::vector<float> jet_hlt_nhf;
  std::vector<float> jet_hlt_elf;
  std::vector<float> jet_hlt_phf;
  std::vector<float> jet_hlt_muf;
  std::vector<float> jet_hlt_pnethltlast_probtauhp;
  std::vector<float> jet_hlt_pnethltlast_probtauhm;
  std::vector<float> jet_hlt_pnethltlast_probb;
  std::vector<float> jet_hlt_pnethltlast_probc;
  std::vector<float> jet_hlt_pnethltlast_probuds;
  std::vector<float> jet_hlt_pnethltlast_probg;
  std::vector<float> jet_hlt_pnethltlast_ptcorr;
  std::vector<float> jet_hlt_pnethlt_probtauh;
  std::vector<float> jet_hlt_pnethlt_probb;
  std::vector<float> jet_hlt_pnethlt_probc;
  std::vector<float> jet_hlt_pnethlt_probuds;
  std::vector<float> jet_hlt_pnethlt_probg;
  std::vector<float> jet_hlt_deepcsv_probb;
  std::vector<float> jet_hlt_deepcsv_probc;
  std::vector<float> jet_hlt_deepcsv_probudsg;
  std::vector<float> jet_hlt_deepjet_probb;
  std::vector<float> jet_hlt_deepjet_problepb;
  std::vector<float> jet_hlt_deepjet_probc;
  std::vector<float> jet_hlt_deepjet_probg;
  std::vector<float> jet_hlt_deepjet_probuds;
  std::vector<unsigned int> jet_hlt_id;
  std::vector<unsigned int> jet_hlt_ncand;
  std::vector<unsigned int> jet_hlt_nch;
  std::vector<unsigned int> jet_hlt_nnh;
  std::vector<unsigned int> jet_hlt_nel;
  std::vector<unsigned int> jet_hlt_nph;
  std::vector<unsigned int> jet_hlt_nmu;
  std::vector<unsigned int> jet_hlt_hflav;
  std::vector<int> jet_hlt_pflav;
  std::vector<unsigned int> jet_hlt_nbhad;
  std::vector<unsigned int> jet_hlt_nchad;
  std::vector<float> jet_hlt_genmatch_pt;
  std::vector<float> jet_hlt_genmatch_eta;
  std::vector<float> jet_hlt_genmatch_phi;
  std::vector<float> jet_hlt_genmatch_mass;
  std::vector<unsigned int> jet_hlt_genmatch_hflav;
  std::vector<int> jet_hlt_genmatch_pflav;
  std::vector<unsigned int> jet_hlt_genmatch_nbhad;
  std::vector<unsigned int> jet_hlt_genmatch_nchad;
  std::vector<float> jet_hlt_genmatch_wnu_pt;
  std::vector<float> jet_hlt_genmatch_wnu_eta;
  std::vector<float> jet_hlt_genmatch_wnu_phi;
  std::vector<float> jet_hlt_genmatch_wnu_mass;
  // pixel jets
  std::vector<float> jet_hlt_pixel_pt;
  std::vector<float> jet_hlt_pixel_eta;
  std::vector<float> jet_hlt_pixel_phi;
  std::vector<float> jet_hlt_pixel_mass;
  std::vector<float> jet_hlt_pixel_genmatch_pt;
  std::vector<float> jet_hlt_pixel_genmatch_eta;
  std::vector<float> jet_hlt_pixel_genmatch_phi;
  std::vector<float> jet_hlt_pixel_genmatch_mass;
 

  // TTree carrying the event weight information
  TTree* tree;

  // Sorters to order object collections in decreasing order of pT
  template<typename T> 
  class PatPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i.pt() > j.pt());
    }
  };

  PatPtSorter<pat::Muon>     muonSorter;
  PatPtSorter<pat::Electron> electronSorter;
  PatPtSorter<pat::Tau>      tauSorter;
  PatPtSorter<pat::PackedCandidate>  packedPFCandidateSorter;
  PatPtSorter<reco::PFCandidate>  recoPFCandidateSorter;

  template<typename T> 
  class PatRefPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i->pt() > j->pt());
    }
  };

  PatRefPtSorter<pat::JetRef>      jetRefSorter;
  PatRefPtSorter<reco::PFJetRef>   recoJetRefSorter;
  PatRefPtSorter<reco::GenJetRef>  genJetRefSorter;

};


ValidationNtupleMakerAK4::ValidationNtupleMakerAK4(const edm::ParameterSet& iConfig): 
  pileupInfoToken          (mayConsume<std::vector<PileupSummaryInfo> >  (iConfig.getParameter<edm::InputTag>("pileUpInfo"))),
  gensToken                (mayConsume<reco::GenParticleCollection>   (iConfig.getParameter<edm::InputTag>("genParticles"))),
  genEvtInfoToken          (mayConsume<GenEventInfoProduct>           (iConfig.getParameter<edm::InputTag>("genEventInfo"))),
  lheInfoToken             (mayConsume<LHEEventProduct>               (iConfig.getParameter<edm::InputTag>("lheInfo"))),
  filterResultsTag         (iConfig.getParameter<edm::InputTag>("filterResults")),
  filterResultsToken       (consumes<edm::TriggerResults>      (filterResultsTag)),
  triggerResultsTag        (iConfig.getParameter<edm::InputTag>("triggerResults")),
  triggerResultsToken      (consumes<edm::TriggerResults>      (triggerResultsTag)),
  l1AlgosToken             (consumes<GlobalAlgBlkBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1Algos"))),
  l1JetToken               (consumes<l1t::JetBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1Jets"))),
  l1MuonToken              (consumes<l1t::MuonBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1Muons"))),
  l1EtSumToken             (consumes<l1t::EtSumBxCollection> (iConfig.getParameter<edm::InputTag>("triggerL1EtSum"))),
  l1GtMenuToken            (esConsumes<L1TUtmTriggerMenu, L1TUtmTriggerMenuRcd>()),
  primaryVerticesToken     (consumes<reco::VertexCollection>           (iConfig.getParameter<edm::InputTag>("pVertices"))),
  secondaryVerticesToken   (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("sVertices"))),
  rhoToken                 (consumes<double>(iConfig.getParameter<edm::InputTag>("rho"))),
  muonsToken               (consumes<pat::MuonCollection>             (iConfig.getParameter<edm::InputTag>("muons"))), 
  electronsToken           (consumes<pat::ElectronCollection>         (iConfig.getParameter<edm::InputTag>("electrons"))), 
  tausToken                (consumes<pat::TauCollection>              (iConfig.getParameter<edm::InputTag>("taus"))), 
  jetsToken                (consumes<pat::JetCollection >             (iConfig.getParameter<edm::InputTag>("jets"))),
  metToken                 (consumes<pat::METCollection >             (iConfig.getParameter<edm::InputTag>("met"))),
  rhoHLTToken              (mayConsume<double>(iConfig.getParameter<edm::InputTag>("hltRho"))),
  jetsHLTToken             (mayConsume<pat::JetCollection >           (iConfig.getParameter<edm::InputTag>("hltJets"))),
  pixelJetsHLTToken        (mayConsume<reco::PFJetCollection >        (iConfig.getParameter<edm::InputTag>("hltPixelJets"))),
  genJetsWnuToken          (mayConsume<reco::GenJetCollection >       (iConfig.getParameter<edm::InputTag>("genJetsWnu"))),
  genJetsToken             (mayConsume<reco::GenJetCollection >      (iConfig.getParameter<edm::InputTag>("genJets"))),
  genJetsFlavourToken      (mayConsume<reco::JetFlavourInfoMatchingCollection >  (iConfig.getParameter<edm::InputTag>("genJetsFlavour"))),
  hltGenJetsToken          (mayConsume<reco::GenJetCollection >       (iConfig.getParameter<edm::InputTag>("hltGenJets"))),
  hltGenJetsFlavourToken   (mayConsume<reco::JetFlavourInfoMatchingCollection >  (iConfig.getParameter<edm::InputTag>("hltGenJetsFlavour"))),
  trackBuilderToken        (esConsumes<TransientTrackBuilder, TransientTrackRecord>(edm::ESInputTag("","TransientTrackBuilder"))), 
  muonPtMin                (iConfig.existsAs<double>("muonPtMin")    ? iConfig.getParameter<double>("muonPtMin") : 5.),
  muonEtaMax               (iConfig.existsAs<double>("muonEtaMax")   ? iConfig.getParameter<double>("muonEtaMax") : 2.5),
  electronPtMin            (iConfig.existsAs<double>("electronPtMin")   ? iConfig.getParameter<double>("electronPtMin") : 5.),
  electronEtaMax           (iConfig.existsAs<double>("electronEtaMax")  ? iConfig.getParameter<double>("electronEtaMax") : 2.5),
  tauPtMin                 (iConfig.existsAs<double>("tauPtMin")   ? iConfig.getParameter<double>("tauPtMin") : 15.),
  tauEtaMax                (iConfig.existsAs<double>("tauEtaMax")  ? iConfig.getParameter<double>("tauEtaMax") : 2.5),
  jetPtMin                 (iConfig.existsAs<double>("jetPtMin")       ? iConfig.getParameter<double>("jetPtMin") : 20.),
  jetEtaMin                (iConfig.existsAs<double>("jetEtaMin")      ? iConfig.getParameter<double>("jetEtaMin") : 0.0),
  jetEtaMax                (iConfig.existsAs<double>("jetEtaMax")      ? iConfig.getParameter<double>("jetEtaMax") : 2.5),
  jetPFCandidatePtMin      (iConfig.existsAs<double>("jetPFCandidatePtMin")    ? iConfig.getParameter<double>("jetPFCandidatePtMin") : 0.1),
  dRJetGenMatch            (iConfig.existsAs<double>("dRJetGenMatch")    ? iConfig.getParameter<double>("dRJetGenMatch") : 0.4),
  dumpOnlyJetMatchedToGen  (iConfig.existsAs<bool>("dumpOnlyJetMatchedToGen")  ? iConfig.getParameter<bool>  ("dumpOnlyJetMatchedToGen") : false),
  saveLHEObjects           (iConfig.existsAs<bool>("saveLHEObjects")  ? iConfig.getParameter<bool>  ("saveLHEObjects") : false),
  saveL1Objects            (iConfig.existsAs<bool>("saveL1Objects")  ? iConfig.getParameter<bool>  ("saveL1Objects") : true),
  usePuppiJets             (iConfig.existsAs<bool>("usePuppiJets")  ? iConfig.getParameter<bool>  ("usePuppiJets") : false),
  isMC                     (iConfig.existsAs<bool>("isMC")   ? iConfig.getParameter<bool>  ("isMC") : true),
  pnetDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorLabels") : std::vector<std::string> ()),
  pnetDiscriminatorNames   (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorNames") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorNames") : std::vector<std::string> ()),
  xsec                     (iConfig.existsAs<double>("xsec") ? iConfig.getParameter<double>("xsec") * 1000.0 : 1.){

  usesResource("TFileService");
  hltPrescaleProvider.reset(new HLTPrescaleProvider(iConfig,consumesCollector(),*this));

}

ValidationNtupleMakerAK4::~ValidationNtupleMakerAK4() {}

void ValidationNtupleMakerAK4::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  // Trigger objects / filters
  edm::Handle<edm::TriggerResults> filterResultsH;
  iEvent.getByToken(filterResultsToken, filterResultsH); 
  edm::Handle<edm::TriggerResults> triggerResultsH;
  iEvent.getByToken(triggerResultsToken, triggerResultsH);
  edm::Handle<GlobalAlgBlkBxCollection> l1AlgosH;
  iEvent.getByToken(l1AlgosToken,l1AlgosH);

  // Vertexes
  edm::Handle<reco::VertexCollection > primaryVerticesH;
  iEvent.getByToken(primaryVerticesToken, primaryVerticesH);
  edm::Handle<reco::VertexCompositePtrCandidateCollection> secondaryVerticesH;
  iEvent.getByToken(secondaryVerticesToken, secondaryVerticesH);
  reco::VertexCompositePtrCandidateCollection svColl = *secondaryVerticesH;

  edm::Handle<double> rho_val;
  iEvent.getByToken(rhoToken,rho_val);  
  edm::Handle<double> rho_hlt_val;
  iEvent.getByToken(rhoHLTToken,rho_hlt_val);  

  // Muons / Electrons / Photons
  edm::Handle<pat::MuonCollection> muonsH;
  iEvent.getByToken(muonsToken,muonsH);
  pat::MuonCollection muonsColl = *muonsH;

  edm::Handle<pat::ElectronCollection> electronsH;
  iEvent.getByToken(electronsToken,electronsH);
  pat::ElectronCollection electronsColl = *electronsH;

  edm::Handle<pat::TauCollection> tausH;
  iEvent.getByToken(tausToken,tausH);
  pat::TauCollection tausColl = *tausH;

  // L1 objects
  edm::Handle<l1t::MuonBxCollection>  l1MuonsH;
  edm::Handle<l1t::JetBxCollection>   l1JetsH;
  edm::Handle<l1t::EtSumBxCollection> l1EtSumH;
  if(saveL1Objects){
    iEvent.getByToken(l1JetToken,l1JetsH);
    iEvent.getByToken(l1MuonToken,l1MuonsH);
    iEvent.getByToken(l1EtSumToken,l1EtSumH);
  }
    
  // Jets / CaloJets
  edm::Handle<pat::JetCollection> jetsH;
  iEvent.getByToken(jetsToken, jetsH);
  edm::Handle<pat::JetCollection> jetsHLTH;
  iEvent.getByToken(jetsHLTToken, jetsHLTH);
  edm::Handle<reco::PFJetCollection> pixelJetsHLT;
  iEvent.getByToken(pixelJetsHLTToken, pixelJetsHLT);

  // MET / HT / MHT
  edm::Handle<pat::METCollection> metH;
  iEvent.getByToken(metToken, metH);

  // GEN Level info and LHE
  edm::Handle<vector<PileupSummaryInfo> > pileupInfoH;
  edm::Handle<GenEventInfoProduct> genEvtInfoH;
  edm::Handle<reco::GenParticleCollection> gensH;
  if(isMC){
    iEvent.getByToken(pileupInfoToken, pileupInfoH);  
    iEvent.getByToken(genEvtInfoToken, genEvtInfoH);  
    iEvent.getByToken(gensToken, gensH);
  }

  edm::Handle<LHEEventProduct> lheInfoH;
  if(saveLHEObjects and isMC) 
    iEvent.getByToken(lheInfoToken,lheInfoH);

  edm::Handle<reco::GenJetCollection> genJetsWnuH;
  edm::Handle<reco::GenJetCollection> genJetsH;
  edm::Handle<reco::JetFlavourInfoMatchingCollection> genJetsFlavourH;
  edm::Handle<reco::GenJetCollection> hltGenJetsH;
  edm::Handle<reco::JetFlavourInfoMatchingCollection> hltGenJetsFlavourH;
  if(isMC){
    iEvent.getByToken(genJetsWnuToken, genJetsWnuH);
    iEvent.getByToken(genJetsToken, genJetsH);
    iEvent.getByToken(genJetsFlavourToken, genJetsFlavourH);
    iEvent.getByToken(hltGenJetsToken, hltGenJetsH);
    iEvent.getByToken(hltGenJetsFlavourToken, hltGenJetsFlavourH);
  }

  edm::ESHandle<L1TUtmTriggerMenu> menu;
  menu = iSetup.getHandle(l1GtMenuToken);

  initializeBranches();
     
  // Event information - MC weight, event ID (run, lumi, event) and so on
  event = iEvent.id().event();
  run   = iEvent.id().run();
  lumi  = iEvent.luminosityBlock();

  // MC weight
  wgt = 1.0;
  if(lheInfoH.isValid()){
    if(not lheInfoH->weights().empty())
      wgt = lheInfoH->weights()[0].wgt;
  }
  else if(genEvtInfoH.isValid())
    wgt = genEvtInfoH->weight(); 

  // Pileup information
  putrue = 0;
  if(pileupInfoH.isValid()){
    for (auto pileupInfo_iter = pileupInfoH->begin(); pileupInfo_iter != pileupInfoH->end(); ++pileupInfo_iter) {
      if (pileupInfo_iter->getBunchCrossing() == 0) 
	putrue = (unsigned int) pileupInfo_iter->getTrueNumInteractions();
    }
  }
    
  // Vertices RECO
  npv  = (unsigned int) primaryVerticesH->size();
  nsv  = (unsigned int) secondaryVerticesH->size();

  // Rho value
  rho = *rho_val;
  rho_hlt = *rho_hlt_val;

  // Missing energy
  met      = metH->front().corPt();
  met_phi  = metH->front().corPhi();

  // GEN particle informatio
  if(gensH.isValid()){
    unsigned int igen = 0;
    for (auto gens_iter = gensH->begin(); gens_iter != gensH->end(); ++gens_iter) {      

      if((abs(gens_iter->pdgId()) == 25 or abs(gens_iter->pdgId()) == 24 or abs(gens_iter->pdgId()) == 23) and
	 gens_iter->isLastCopy() and 
	 gens_iter->statusFlags().fromHardProcess()){ 
	
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

        for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
          gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
          gen_particle_daughters_igen.push_back(igen);
          gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
          gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
          gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
          gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
        }
        igen++;
      }
      
      // Final states Leptons (e,mu) and Neutrinos --> exclude taus. They need to be prompt or from Tau decay      
      if (abs(gens_iter->pdgId()) > 10 and abs(gens_iter->pdgId()) < 17 and abs(gens_iter->pdgId()) != 15  and 
	  (gens_iter->isPromptFinalState() or 
	   gens_iter->isDirectPromptTauDecayProductFinalState())) { 

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// No need to save daughters here
        igen++;
      }
      
      // Final state quarks or gluons from the hard process before the shower --> partons in which H/Z/W/top decay into
      if (((abs(gens_iter->pdgId()) >= 1 and abs(gens_iter->pdgId()) <= 5) or abs(gens_iter->pdgId()) == 21) and 
	  gens_iter->statusFlags().fromHardProcess() and 
	  gens_iter->statusFlags().isFirstCopy()){
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());
	igen++;
	// no need to save daughters
      }

      // Special case of taus: last-copy, from hard process and, prompt and decayed
      if(abs(gens_iter->pdgId()) == 15 and 
	 gens_iter->isLastCopy() and
	 gens_iter->statusFlags().fromHardProcess() and
	 gens_iter->isPromptDecayed()){ // hadronic taus

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// only store the final decay particles
	for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
	  if(not dynamic_cast<const reco::GenParticle*>(gens_iter->daughter(idau))->statusFlags().isPromptTauDecayProduct()) continue;
	  gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
	  gen_particle_daughters_igen.push_back(igen);
	  gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
	  gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
	  gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
	  gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());    
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
	}
	igen++;
      }  
    }
  }
   
  // LHE level information
  if(lheInfoH.isValid() and saveLHEObjects){
    
    const auto& hepeup = lheInfoH->hepeup();
    const auto& pup    = hepeup.PUP;

    for (unsigned int i = 0, n = pup.size(); i < n; ++i) {
      int status = hepeup.ISTUP[i];
      int id     = hepeup.IDUP[i];
      TLorentzVector p4(pup[i][0], pup[i][1], pup[i][2], pup[i][3]);
      if(abs(id) == 25 or abs(id) == 23 or abs(id) == 24 or abs(id)==6){ // Higgs, Z or W or top                                                                                             
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 10 && abs(id) <= 17 and status == 1){ // final state leptons                                                                                                             
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 1 && abs(id) <= 5 and status == 1){ // final state leptons                                                                                                                   
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
	lhe_particle_id.push_back(id);
      }
      else if(abs(id) == 21 and status == 1){ // final state leptons                                                                                                                              
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
    }
  }

  // MET filter info
  unsigned int flagvtx       = 1 * 1;
  unsigned int flaghalo      = 1 * 2;
  unsigned int flaghbhe      = 1 * 4;
  unsigned int flaghbheiso   = 1 * 8;
  unsigned int flagecaltp    = 1 * 16;
  unsigned int flagbadmuon   = 1 * 32; 
  unsigned int flagbadhad    = 1 * 64;
 
  // Which MET filters passed
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    if (filterPathsMap[filterPathsVector[i]] == -1) continue;
    if (i == 0  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagvtx       = 0; // goodVertices
    if (i == 1  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghalo      = 0; // CSCTightHaloFilter
    if (i == 2  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbhe      = 0; // HBHENoiseFilter
    if (i == 3  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbheiso   = 0; // HBHENoiseIsoFilter
    if (i == 4  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagecaltp    = 0; // EcalDeadCellTriggerPrimitiveFilter
    if (i == 5  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadmuon   = 0; // badmuon
    if (i == 6  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadhad    = 0; // badhadrons
  }

  flags = flagvtx + flaghalo + flaghbhe + flaghbheiso + flagecaltp + flagbadmuon + flagbadhad;

  // HLT triggers                                                                                                                                                                                     
  for(auto key : triggerPathsMap){
    trigger_hlt_path.push_back(key.first);
    trigger_hlt_pass.push_back(triggerResultsH->accept(key.second));
    auto result = hltPrescaleProvider->prescaleValuesInDetail<double>(iEvent, iSetup, key.first);
    trigger_hlt_prescale.push_back(result.second);
  }

  // L1 algos
  if(l1AlgosH.isValid()) {
    // Access menu
    std::map<std::string,int> mapping_seed;
    for (auto keyval : menu->getAlgorithmMap()) { 
      std::string trigName  = keyval.second.getName();
       if(triggerL1SeedMap.find(trigName) != triggerL1SeedMap.end()){
	unsigned int index = keyval.second.getIndex();
	mapping_seed[trigName] = index;
      }      
    }

    auto l1TriggerResult = l1AlgosH->at(0,0); // take BX 0
    for(auto trigger : mapping_seed){
      unsigned int passed = l1TriggerResult.getAlgoDecisionFinal(trigger.second) ; 
      trigger_l1_pass.push_back(passed);
      trigger_l1_seed.push_back(trigger.first);
      auto result = hltPrescaleProvider->prescaleValuesInDetail<double>(iEvent,iSetup,triggerL1SeedMap[trigger.first].front());
      for(auto iel: result.first){
	if(iel.first == trigger.first) {
	  trigger_l1_prescale.push_back(iel.second);
	  break;
	}
      }
    }
  }    
  
  if(l1JetsH.isValid()){
    for (auto l1Jet = l1JetsH->begin(0); l1Jet != l1JetsH->end(0); l1Jet++){
      if(l1Jet->pt() < jetPtMin or fabs(l1Jet->eta()) > jetEtaMax or fabs(l1Jet->eta()) < jetEtaMin) continue;      
      trigger_l1_jet_pt.push_back(l1Jet->pt());
      trigger_l1_jet_eta.push_back(l1Jet->eta());
      trigger_l1_jet_phi.push_back(l1Jet->phi());
      trigger_l1_jet_mass.push_back(l1Jet->mass());
    }
  }

  if(l1MuonsH.isValid()){
    for (auto l1Muon = l1MuonsH->begin(0); l1Muon != l1MuonsH->end(0); l1Muon++){
      if(l1Muon->pt() < 3 or fabs(l1Muon->eta()) > 2.4) continue;      
      trigger_l1_muon_pt.push_back(l1Muon->pt());
      trigger_l1_muon_eta.push_back(l1Muon->eta());
      trigger_l1_muon_phi.push_back(l1Muon->phi());
      trigger_l1_muon_mass.push_back(l1Muon->mass());
      trigger_l1_muon_qual.push_back(l1Muon->hwQual());
      trigger_l1_muon_iso.push_back(l1Muon->hwIso());
    }
  }
  
  if(l1EtSumH.isValid()){
    for(auto l1EtSum = l1EtSumH->begin(0); l1EtSum != l1EtSumH->end(0); l1EtSum++){
      auto sumtype = static_cast<int>(l1EtSum->getType());
      if(sumtype == l1t::EtSum::kTotalHt)
	trigger_l1_ht = l1EtSum->et();
      if(sumtype == l1t::EtSum::kMissingHt)
	trigger_l1_mht = l1EtSum->et();
    }
  }

  // Sorting muons based on pT
  sort(muonsColl.begin(),muonsColl.end(),muonSorter);
  for (size_t i = 0; i < muonsColl.size(); i++) {

    if(muonsColl[i].pt() < muonPtMin) continue;
    if(fabs(muonsColl[i].eta()) > muonEtaMax) continue;

    muon_pt.push_back(muonsColl[i].pt());
    muon_eta.push_back(muonsColl[i].eta());
    muon_phi.push_back(muonsColl[i].phi());
    muon_mass.push_back(muonsColl[i].mass());

    // Muon isolation
    int isoval = 0;
    if(muonsColl[i].passed(reco::Muon::PFIsoLoose))
      isoval += 1;
    if(muonsColl[i].passed(reco::Muon::PFIsoMedium))
      isoval += 2;
    if(muonsColl[i].passed(reco::Muon::PFIsoTight))
      isoval += 4;
    if(muonsColl[i].passed(reco::Muon::PFIsoVeryTight))
      isoval += 8;
    if(muonsColl[i].passed(reco::Muon::MiniIsoLoose))
      isoval += 16;
    if(muonsColl[i].passed(reco::Muon::MiniIsoMedium))
      isoval += 32;
    if(muonsColl[i].passed(reco::Muon::MiniIsoTight))
      isoval += 64;
    
    muon_iso.push_back(isoval);
    
    // Muon id
    int midval = 0;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdLoose))
      midval += 1;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdMedium))
      midval += 2;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdTight))
      midval += 4;
    if(muonsColl[i].passed(reco::Muon::MvaLoose))
      midval += 8;
    if(muonsColl[i].passed(reco::Muon::MvaMedium))
      midval += 16;
    if(muonsColl[i].passed(reco::Muon::MvaTight))
      midval += 32;
    
    muon_id.push_back(midval);
    muon_charge.push_back(muonsColl[i].charge());
    muon_d0.push_back(muonsColl[i].muonBestTrack()->dxy(primaryVerticesH->at(0).position()));
    muon_dz.push_back(muonsColl[i].muonBestTrack()->dz(primaryVerticesH->at(0).position()));
  }
  
  // Sorting electrons based on pT
  sort(electronsColl.begin(),electronsColl.end(),electronSorter);

  for (size_t i = 0; i < electronsColl.size(); i++) {

    if(electronsColl[i].pt() < electronPtMin) continue;
    if(fabs(electronsColl[i].eta()) > electronEtaMax) continue;

    electron_pt.push_back(electronsColl[i].pt());
    electron_eta.push_back(electronsColl[i].eta());
    electron_phi.push_back(electronsColl[i].phi());
    electron_mass.push_back(electronsColl[i].mass());

    int eidval = 0;   
    if(electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-loose"))
      eidval += 1;
    if(electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-medium"))
      eidval += 2;
    if(electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-tight"))
      eidval += 4;
    if(electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wpLoose"))
      eidval += 8;
    if(electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp90"))
      eidval += 16;
    if(electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp80"))
      eidval += 32;

    electron_id.push_back(eidval);
    electron_charge.push_back(electronsColl[i].charge());
    electron_d0.push_back(electronsColl[i].gsfTrack()->dxy(primaryVerticesH->at(0).position()));
    electron_dz.push_back(electronsColl[i].gsfTrack()->dz(primaryVerticesH->at(0).position()));
  }

  // Sorting taus based on pT
  sort(tausColl.begin(),tausColl.end(),tauSorter);

  for (size_t i = 0; i < tausColl.size(); i++) {
    
    if(tausColl[i].pt() < tauPtMin) continue;
    if(fabs(tausColl[i].eta()) > tauEtaMax) continue;

    tau_pt.push_back(tausColl[i].pt());
    tau_eta.push_back(tausColl[i].eta());
    tau_phi.push_back(tausColl[i].phi());
    tau_mass.push_back(tausColl[i].mass());

    tau_decaymode.push_back(tausColl[i].decayMode());
    tau_charge.push_back(tausColl[i].charge());

    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSjetraw"))
      tau_idjet.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSjetraw"));
    else if(tausColl[i].isTauIDAvailable("byDeepTau2017v2p1VSjetraw"))
      tau_idjet.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSjetraw"));

    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSmuraw"))
      tau_idmu.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSmuraw"));
    else if(tausColl[i].isTauIDAvailable("byDeepTau2017v2p1VSmuraw"))
      tau_idmu.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSmuraw"));

    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSeraw"))
      tau_idele.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSeraw"));
    else if(tausColl[i].isTauIDAvailable("byDeepTau2017v2p1VSeraw"))
      tau_idele.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSeraw"));

    int tauvsjetid = 0;
    if(tausColl[i].isTauIDAvailable("byVVVLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byVVVLooseDeepTau2017v2p1VSjet")) 
      tauvsjetid += 1;
    if(tausColl[i].isTauIDAvailable("byVVLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byVVLooseDeepTau2017v2p1VSjet")) 
      tauvsjetid += 2;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byVLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 4;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 8;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSjet") and tausColl[i].tauID("byMediumDeepTau2017v2p1VSjet"))
      tauvsjetid += 16;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2017v2p1VSjet") and tausColl[i].tauID("byTightDeepTau2017v2p1VSjet"))
      tauvsjetid += 32;

    tau_idjet_wp.push_back(tauvsjetid);

    int tauvsmuid = 0;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSmu") and tausColl[i].tauID("byVLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 1;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSmu") and tausColl[i].tauID("byLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 2;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSmu") and tausColl[i].tauID("byMediumDeepTau2017v2p1VSmu"))
      tauvsmuid += 4;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2017v2p1VSmu") and tausColl[i].tauID("byTightDeepTau2017v2p1VSmu"))
      tauvsmuid += 8;

    tau_idmu_wp.push_back(tauvsmuid);

    int tauvselid = 0;
    if(tausColl[i].isTauIDAvailable("byVVVLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byVVVLooseDeepTau2017v2p1VSe")) 
      tauvselid += 1;
    if(tausColl[i].isTauIDAvailable("byVVLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byVVLooseDeepTau2017v2p1VSe")) 
      tauvselid += 2;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byVLooseDeepTau2017v2p1VSe"))
      tauvselid += 4;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byLooseDeepTau2017v2p1VSe"))
      tauvselid += 8;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSe") and tausColl[i].tauID("byMediumDeepTau2017v2p1VSe"))
      tauvselid += 16;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2017v2p1VSe") and tausColl[i].tauID("byTightDeepTau2017v2p1VSe"))
      tauvselid += 32;

    tau_idele_wp.push_back(tauvselid);

    if(tausColl[i].genJet()){
      tau_genmatch_pt.push_back(tausColl[i].genJet()->pt());
      tau_genmatch_eta.push_back(tausColl[i].genJet()->eta());
      tau_genmatch_phi.push_back(tausColl[i].genJet()->phi());
      tau_genmatch_mass.push_back(tausColl[i].genJet()->mass());
      // reconstruct the decay mode of the gen-jet
      unsigned int tau_ch = 0;
      unsigned int tau_ph = 0;
      unsigned int tau_nh = 0;
      auto gen_constituents = tausColl[i].genJet()->getGenConstituents();
      for(size_t iconst = 0; iconst < gen_constituents.size(); iconst++){
	auto part = gen_constituents[iconst];
	if(part->status() != 1) continue;
	if(part->charge() == 0 and abs(part->pdgId()) == 22) tau_ph++;
	if(part->charge() == 0 and abs(part->pdgId()) != 22) tau_nh++;
	if(part->charge() != 0 and abs(part->pdgId()) != 11 and abs(part->pdgId()) != 13) tau_ch++;
      }
      tau_genmatch_decaymode.push_back(5*(tau_ch-1)+tau_ph/2+tau_nh);
    }
    else{
      tau_genmatch_pt.push_back(-1);
      tau_genmatch_eta.push_back(-1);
      tau_genmatch_phi.push_back(-1);
      tau_genmatch_mass.push_back(-1);
      tau_genmatch_decaymode.push_back(-1);
    }    
  }
  
  // Standard gen-jets excluding the neutrinos
  vector<reco::GenJetRef> jetv_gen;   
  if(genJetsH.isValid()){
    for (auto jets_iter = genJetsH->begin(); jets_iter != genJetsH->end(); ++jets_iter) {                                                                                                   
      reco::GenJetRef jref (genJetsH, jets_iter - genJetsH->begin());                                                                                                                      
      jetv_gen.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen.begin(), jetv_gen.end(), genJetRefSorter);
  }

  // GEN jets with neutrinos
  vector<reco::GenJetRef> jetv_gen_wnu;   
  if(genJetsWnuH.isValid()){
    for (auto jets_iter = genJetsWnuH->begin(); jets_iter != genJetsWnuH->end(); ++jets_iter) {                                                                                           
      reco::GenJetRef jref  (genJetsWnuH, jets_iter - genJetsWnuH->begin());                                                                                                                 
      jetv_gen_wnu.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen_wnu.begin(), jetv_gen_wnu.end(), genJetRefSorter);
  }
 
  // Offline jets
  vector<pat::JetRef> jetv;   
  for (auto jets_iter = jetsH->begin(); jets_iter != jetsH->end(); ++jets_iter) {                                                                                                                     
    pat::JetRef jref(jetsH, jets_iter - jetsH->begin());                                                                                                                                            
    if (jref->pt() < jetPtMin) continue; 
    if (fabs(jref->eta()) > jetEtaMax) continue;                 
    if (fabs(jref->eta()) < jetEtaMin) continue;                 
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
    jetv.push_back(jref);                                                                                                                                                                           
  }    
  sort(jetv.begin(), jetv.end(), jetRefSorter);

  for (size_t i = 0; i < jetv.size(); i++) {

    jet_pt.push_back(jetv[i]->pt());
    jet_eta.push_back(jetv[i]->eta());
    jet_phi.push_back(jetv[i]->phi());
    jet_mass.push_back(jetv[i]->mass());
    jet_pt_raw.push_back(jetv[i]->correctedJet("Uncorrected").pt());
    jet_mass_raw.push_back(jetv[i]->correctedJet("Uncorrected").mass());

    jet_deepjet_probb.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probb"));
    jet_deepjet_probbb.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probbb"));
    jet_deepjet_problepb.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:problepb"));
    jet_deepjet_probc.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probc"));
    jet_deepjet_probg.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probg"));
    jet_deepjet_probuds.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probuds"));

    jet_pnet_probb.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probb"));
    jet_pnet_probbb.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probbb"));
    jet_pnet_probc.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probc"));
    jet_pnet_probcc.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probcc"));
    jet_pnet_probuds.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probuds"));
    jet_pnet_probg.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probg"));
    jet_pnet_probpu.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probpu"));
    jet_pnet_probundef.push_back(jetv[i]->bDiscriminator("pfParticleNetAK4JetTags:probundef"));
   
    for (size_t ilabel = 0; ilabel < pnetDiscriminatorLabels.size(); ilabel++)
      jet_pnetlast_score[pnetDiscriminatorLabels[ilabel]].push_back(jetv[i]->bDiscriminator(pnetDiscriminatorNames[ilabel].c_str()));
   
    int jetid = 0;    
    if(applyJetID(*jetv[i],"tight",usePuppiJets)) jetid += 1;
    jet_id.push_back(jetid);

    int jetpuid = 0;
    if(applyPileupJetID(*jetv[i],"loose"))  jetpuid += 1;
    if(applyPileupJetID(*jetv[i],"medium")) jetpuid += 2;
    if(applyPileupJetID(*jetv[i],"tight"))  jetpuid += 4;
    jet_puid.push_back(jetpuid);

    // Energy fractions
    jet_chf.push_back(jetv[i]->chargedHadronEnergyFraction());
    jet_nhf.push_back(jetv[i]->neutralHadronEnergyFraction());
    jet_elf.push_back(jetv[i]->electronEnergyFraction());
    jet_phf.push_back(jetv[i]->photonEnergyFraction());
    jet_muf.push_back(jetv[i]->muonEnergyFraction());

    // PF components
    jet_ncand.push_back(jetv[i]->chargedHadronMultiplicity()+jetv[i]->neutralHadronMultiplicity()+jetv[i]->electronMultiplicity()+jetv[i]->photonMultiplicity()+jetv[i]->muonMultiplicity());
    jet_nch.push_back(jetv[i]->chargedHadronMultiplicity());
    jet_nnh.push_back(jetv[i]->neutralHadronMultiplicity());
    jet_nel.push_back(jetv[i]->electronMultiplicity());
    jet_nph.push_back(jetv[i]->photonMultiplicity());
    jet_nmu.push_back(jetv[i]->muonMultiplicity());    
    jet_hflav.push_back(jetv[i]->hadronFlavour());
    jet_pflav.push_back(jetv[i]->partonFlavour());
    jet_nbhad.push_back(jetv[i]->jetFlavourInfo().getbHadrons().size());
    jet_nchad.push_back(jetv[i]->jetFlavourInfo().getcHadrons().size());
   
    // Matching with gen-jets
    int   pos_matched = -1;
    float minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4());
      }
    }
      
    if(pos_matched >= 0){      
      jet_genmatch_pt.push_back(jetv_gen[pos_matched]->pt());
      jet_genmatch_eta.push_back(jetv_gen[pos_matched]->eta());
      jet_genmatch_phi.push_back(jetv_gen[pos_matched]->phi());
      jet_genmatch_mass.push_back(jetv_gen[pos_matched]->mass());
      jet_genmatch_hflav.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getHadronFlavour());
      jet_genmatch_pflav.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getPartonFlavour());      
      jet_genmatch_nbhad.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getbHadrons().size());      
      jet_genmatch_nchad.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getcHadrons().size());      
    }
    else{
      jet_genmatch_pt.push_back(0);
      jet_genmatch_eta.push_back(0);
      jet_genmatch_phi.push_back(0);
      jet_genmatch_mass.push_back(0);
      jet_genmatch_hflav.push_back(0);
      jet_genmatch_pflav.push_back(0);
      jet_genmatch_nbhad.push_back(0);
      jet_genmatch_nchad.push_back(0);
    }
  
    
    pos_matched = -1;
    minDR = dRJetGenMatch;

    //////////
    for(size_t igen = 0; igen < jetv_gen_wnu.size(); igen++){
      if(reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[i]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[i]->p4());
      }
    }
    
    if(pos_matched >= 0){      
      jet_genmatch_wnu_pt.push_back(jetv_gen_wnu[pos_matched]->pt());
      jet_genmatch_wnu_eta.push_back(jetv_gen_wnu[pos_matched]->eta());
      jet_genmatch_wnu_phi.push_back(jetv_gen_wnu[pos_matched]->phi());
      jet_genmatch_wnu_mass.push_back(jetv_gen_wnu[pos_matched]->mass());
    }
    else{
      jet_genmatch_wnu_pt.push_back(0);
      jet_genmatch_wnu_eta.push_back(0);
      jet_genmatch_wnu_phi.push_back(0);
      jet_genmatch_wnu_mass.push_back(0);
    }    
  }

  // Gen jets and HLT jets
  jetv_gen.clear();
  if(hltGenJetsH.isValid()){
    for (auto jets_iter = hltGenJetsH->begin(); jets_iter != hltGenJetsH->end(); ++jets_iter) {                                                                                                   
      reco::GenJetRef jref (hltGenJetsH, jets_iter - hltGenJetsH->begin());                                                                                                                      
      jetv_gen.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen.begin(), jetv_gen.end(), genJetRefSorter);
  }
  
  vector<pat::JetRef> jetv_hlt;  
  for (auto jets_iter = jetsHLTH->begin(); jets_iter != jetsHLTH->end(); ++jets_iter) {                                                                                                   
    pat::JetRef jref(jetsHLTH, jets_iter - jetsHLTH->begin());                                                                                                                                 
    if (jref->pt() < jetPtMin) continue;
    if (fabs(jref->eta()) > jetEtaMax) continue;                 
    if (fabs(jref->eta()) < jetEtaMin) continue;                 
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue; // matching performed correctly in the job
    jetv_hlt.push_back(jref);                                                                                                                                                              
  }
  sort(jetv_hlt.begin(), jetv_hlt.end(), jetRefSorter);

  for (size_t i = 0; i < jetv_hlt.size(); i++) {
      
    jet_hlt_pt.push_back(jetv_hlt[i]->pt());
    jet_hlt_eta.push_back(jetv_hlt[i]->eta());
    jet_hlt_phi.push_back(jetv_hlt[i]->phi());
    jet_hlt_mass.push_back(jetv_hlt[i]->mass());
    jet_hlt_pt_raw.push_back(jetv_hlt[i]->correctedJet("Uncorrected").pt());
    jet_hlt_mass_raw.push_back(jetv_hlt[i]->correctedJet("Uncorrected").mass());

    jet_hlt_deepcsv_probb.push_back(jetv_hlt[i]->bDiscriminator("hltDeepCSVJetTags:probb"));
    jet_hlt_deepcsv_probc.push_back(jetv_hlt[i]->bDiscriminator("hltDeepCSVJetTags:probc"));
    jet_hlt_deepcsv_probudsg.push_back(jetv_hlt[i]->bDiscriminator("hltDeepCSVJetTags:probudsg"));

    jet_hlt_deepjet_probb.push_back(jetv_hlt[i]->bDiscriminator("hltDeepFlavourJetTags:probb"));
    jet_hlt_deepjet_problepb.push_back(jetv_hlt[i]->bDiscriminator("hltDeepFlavourJetTags:problepb"));
    jet_hlt_deepjet_probc.push_back(jetv_hlt[i]->bDiscriminator("hltDeepFlavourJetTags:probc"));
    jet_hlt_deepjet_probg.push_back(jetv_hlt[i]->bDiscriminator("hltDeepFlavourJetTags:probg"));
    jet_hlt_deepjet_probuds.push_back(jetv_hlt[i]->bDiscriminator("hltDeepFlavourJetTags:probuds"));
   
    jet_hlt_pnethltlast_probtauhp.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:probtauhp"));
    jet_hlt_pnethltlast_probtauhm.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:probtauhm"));
    jet_hlt_pnethltlast_probb.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:probb"));
    jet_hlt_pnethltlast_probc.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:probc"));
    jet_hlt_pnethltlast_probuds.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:probuds"));
    jet_hlt_pnethltlast_probg.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:probg"));
    jet_hlt_pnethltlast_ptcorr.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXLastJetTags:ptcorr"));

    jet_hlt_pnethlt_probtauh.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probtauh"));
    jet_hlt_pnethlt_probb.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probb"));
    jet_hlt_pnethlt_probc.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probc"));
    jet_hlt_pnethlt_probuds.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probuds"));
    jet_hlt_pnethlt_probg.push_back(jetv_hlt[i]->bDiscriminator("hltParticleNetONNXJetTags:probg"));

    int jetid = 0;
    if(applyJetID(*jetv_hlt[i],"tight",false)) jetid += 1;
    jet_hlt_id.push_back(jetid);
   
    // Energy fractions
    jet_hlt_chf.push_back(jetv_hlt[i]->chargedHadronEnergyFraction());
    jet_hlt_nhf.push_back(jetv_hlt[i]->neutralHadronEnergyFraction());
    jet_hlt_elf.push_back(jetv_hlt[i]->electronEnergyFraction());
    jet_hlt_phf.push_back(jetv_hlt[i]->photonEnergyFraction());
    jet_hlt_muf.push_back(jetv_hlt[i]->muonEnergyFraction());
      
    // PF components
    jet_hlt_ncand.push_back(jetv_hlt[i]->chargedMultiplicity()+jetv_hlt[i]->neutralMultiplicity());
    jet_hlt_nch.push_back(jetv_hlt[i]->chargedHadronMultiplicity());
    jet_hlt_nnh.push_back(jetv_hlt[i]->neutralHadronMultiplicity());
    jet_hlt_nel.push_back(jetv_hlt[i]->electronMultiplicity());
    jet_hlt_nph.push_back(jetv_hlt[i]->photonMultiplicity());
    jet_hlt_nmu.push_back(jetv_hlt[i]->muonMultiplicity());
      
    // flavour
    jet_hlt_hflav.push_back(jetv_hlt[i]->hadronFlavour());
    jet_hlt_pflav.push_back(jetv_hlt[i]->partonFlavour());
    jet_hlt_nbhad.push_back(jetv_hlt[i]->jetFlavourInfo().getbHadrons().size());
    jet_hlt_nchad.push_back(jetv_hlt[i]->jetFlavourInfo().getcHadrons().size());

    int   pos_matched = -1;
    float minDR = dRJetGenMatch;

    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv_hlt[i]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv_hlt[i]->p4());
      }
    }
      
    if(pos_matched >= 0){      
      jet_hlt_genmatch_pt.push_back(jetv_gen[pos_matched]->pt());
      jet_hlt_genmatch_eta.push_back(jetv_gen[pos_matched]->eta());
      jet_hlt_genmatch_phi.push_back(jetv_gen[pos_matched]->phi());
      jet_hlt_genmatch_mass.push_back(jetv_gen[pos_matched]->mass());
      jet_hlt_genmatch_hflav.push_back((*hltGenJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getHadronFlavour());
      jet_hlt_genmatch_pflav.push_back((*hltGenJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getPartonFlavour());      
      jet_hlt_genmatch_nbhad.push_back((*hltGenJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getbHadrons().size());      
      jet_hlt_genmatch_nchad.push_back((*hltGenJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getcHadrons().size());      
    }
    else{
      jet_hlt_genmatch_pt.push_back(0);
      jet_hlt_genmatch_eta.push_back(0);
      jet_hlt_genmatch_phi.push_back(0);
      jet_hlt_genmatch_mass.push_back(0);
      jet_hlt_genmatch_hflav.push_back(0);
      jet_hlt_genmatch_pflav.push_back(0);
      jet_hlt_genmatch_nbhad.push_back(0);
      jet_hlt_genmatch_nchad.push_back(0);
    }
    
    pos_matched = -1;
    minDR = dRJetGenMatch;      
    for(size_t igen = 0; igen < jetv_gen_wnu.size(); igen++){
      if(reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv_hlt[i]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv_hlt[i]->p4());
      }
    }
    
    if(pos_matched!=-1){      
      jet_hlt_genmatch_wnu_pt.push_back(jetv_gen_wnu[pos_matched]->pt());
      jet_hlt_genmatch_wnu_eta.push_back(jetv_gen_wnu[pos_matched]->eta());
      jet_hlt_genmatch_wnu_phi.push_back(jetv_gen_wnu[pos_matched]->phi());
      jet_hlt_genmatch_wnu_mass.push_back(jetv_gen_wnu[pos_matched]->mass());
    }
    else{
      jet_hlt_genmatch_wnu_pt.push_back(0);
      jet_hlt_genmatch_wnu_eta.push_back(0);
      jet_hlt_genmatch_wnu_phi.push_back(0);
      jet_hlt_genmatch_wnu_mass.push_back(0);
    }
  }

  // pixel
  vector<reco::PFJetRef> jetv_pixel_hlt;
  if(pixelJetsHLT.isValid()){
    for (auto jets_iter = pixelJetsHLT->begin(); jets_iter != pixelJetsHLT->end(); ++jets_iter) {                                                                                                   
      reco::PFJetRef jref (pixelJetsHLT, jets_iter - pixelJetsHLT->begin());                                                                                                                       
      if (jref->pt() < jetPtMin) continue;
      if (fabs(jref->eta()) > jetEtaMax) continue;                 
      if (fabs(jref->eta()) < jetEtaMin) continue;                 
      jetv_pixel_hlt.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_pixel_hlt.begin(), jetv_pixel_hlt.end(), recoJetRefSorter);    
  }

  for (size_t i = 0; i < jetv_pixel_hlt.size(); i++) {     

    jet_hlt_pixel_pt.push_back(jetv_pixel_hlt[i]->pt());
    jet_hlt_pixel_eta.push_back(jetv_pixel_hlt[i]->eta());
    jet_hlt_pixel_phi.push_back(jetv_pixel_hlt[i]->phi());
    jet_hlt_pixel_mass.push_back(jetv_pixel_hlt[i]->mass());

    int   pos_matched = -1;
    float minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv_pixel_hlt[i]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv_pixel_hlt[i]->p4());
      }
    }
      
    if(pos_matched >= 0){      
      jet_hlt_pixel_genmatch_pt.push_back(jetv_gen.at(pos_matched)->pt());
      jet_hlt_pixel_genmatch_eta.push_back(jetv_gen.at(pos_matched)->eta());
      jet_hlt_pixel_genmatch_phi.push_back(jetv_gen.at(pos_matched)->phi());
      jet_hlt_pixel_genmatch_mass.push_back(jetv_gen.at(pos_matched)->mass());
    }
    else{
      jet_hlt_pixel_genmatch_pt.push_back(0);
      jet_hlt_pixel_genmatch_eta.push_back(0);
      jet_hlt_pixel_genmatch_phi.push_back(0);
      jet_hlt_pixel_genmatch_mass.push_back(0);
    }
  }
 
  tree->Fill();
}


void ValidationNtupleMakerAK4::beginJob() {

  edm::Service<TFileService> fs;
  tree = fs->make<TTree>("tree","tree");

  tree->Branch("event", &event, "event/i");
  tree->Branch("run", &run, "run/i");
  tree->Branch("lumi", &lumi, "lumi/i");
  tree->Branch("flags", &flags, "flags/i");

  if(isMC){
    tree->Branch("xsec", &xsec, "xsec/F");
    tree->Branch("wgt" , &wgt , "wgt/F");
    tree->Branch("putrue", &putrue, "putrue/i");
  }

  tree->Branch("rho", &rho, "rho/F");
  tree->Branch("rho_hlt", &rho_hlt, "rho_hlt/F");
  
  tree->Branch("met",&met,"met/F");
  tree->Branch("met_phi", &met_phi, "met_phi/F");

  tree->Branch("npv", &npv, "npv/i");
  tree->Branch("nsv", &nsv, "nsv/i");

  if(isMC){
    tree->Branch("gen_particle_pt","std::vector<float>", &gen_particle_pt);
    tree->Branch("gen_particle_eta","std::vector<float>", &gen_particle_eta);
    tree->Branch("gen_particle_phi","std::vector<float>", &gen_particle_phi);
    tree->Branch("gen_particle_mass","std::vector<float>", &gen_particle_mass);
    tree->Branch("gen_particle_id","std::vector<int>", &gen_particle_id);
    tree->Branch("gen_particle_status","std::vector<unsigned int>", &gen_particle_status);
    tree->Branch("gen_particle_daughters_id","std::vector<int>", &gen_particle_daughters_id);
    tree->Branch("gen_particle_daughters_igen","std::vector<unsigned int>", &gen_particle_daughters_igen);
    tree->Branch("gen_particle_daughters_pt","std::vector<float>", &gen_particle_daughters_pt);
    tree->Branch("gen_particle_daughters_eta","std::vector<float>", &gen_particle_daughters_eta);
    tree->Branch("gen_particle_daughters_phi","std::vector<float>", &gen_particle_daughters_phi);
    tree->Branch("gen_particle_daughters_mass","std::vector<float>", &gen_particle_daughters_mass);
    tree->Branch("gen_particle_daughters_status","std::vector<unsigned int>", &gen_particle_daughters_status);
    tree->Branch("gen_particle_daughters_charge","std::vector<int>", &gen_particle_daughters_charge);

    if(saveLHEObjects){
      tree->Branch("lhe_particle_pt", "std::vector<float>", &lhe_particle_pt);
      tree->Branch("lhe_particle_eta", "std::vector<float>", &lhe_particle_eta);
      tree->Branch("lhe_particle_phi", "std::vector<float>", &lhe_particle_phi);
      tree->Branch("lhe_particle_mass", "std::vector<float>", &lhe_particle_mass);
      tree->Branch("lhe_particle_id", "std::vector<int>", &lhe_particle_id);
      tree->Branch("lhe_particle_status", "std::vector<unsigned int>", &lhe_particle_status);
    }
  }

  tree->Branch("trigger_hlt_path","std::vector<std::string>",&trigger_hlt_path);
  tree->Branch("trigger_hlt_pass","std::vector<unsigned int>",&trigger_hlt_pass);
  tree->Branch("trigger_hlt_prescale","std::vector<int>",&trigger_hlt_prescale);

  tree->Branch("trigger_l1_seed","std::vector<std::string>",&trigger_l1_seed);
  tree->Branch("trigger_l1_pass","std::vector<unsigned int>",&trigger_l1_pass);
  tree->Branch("trigger_l1_prescale","std::vector<int>",&trigger_l1_prescale);

  if(saveL1Objects){
    tree->Branch("trigger_l1_jet_pt","std::vector<float>",&trigger_l1_jet_pt);
    tree->Branch("trigger_l1_jet_eta","std::vector<float>",&trigger_l1_jet_eta);
    tree->Branch("trigger_l1_jet_phi","std::vector<float>",&trigger_l1_jet_phi);
    tree->Branch("trigger_l1_jet_mass","std::vector<float>",&trigger_l1_jet_mass);
    tree->Branch("trigger_l1_muon_pt","std::vector<float>",&trigger_l1_muon_pt);
    tree->Branch("trigger_l1_muon_eta","std::vector<float>",&trigger_l1_muon_eta);
    tree->Branch("trigger_l1_muon_phi","std::vector<float>",&trigger_l1_muon_phi);
    tree->Branch("trigger_l1_muon_mass","std::vector<float>",&trigger_l1_muon_mass);
    tree->Branch("trigger_l1_muon_qual","std::vector<float>",&trigger_l1_muon_qual);
    tree->Branch("trigger_l1_muon_iso","std::vector<float>",&trigger_l1_muon_iso);
    tree->Branch("trigger_l1_ht",&trigger_l1_ht,"trigger_l1_ht/F");
    tree->Branch("trigger_l1_mht",&trigger_l1_mht,"trigger_l1_mht/F");
  }

  tree->Branch("muon_pt", "std::vector<float>", &muon_pt);
  tree->Branch("muon_eta", "std::vector<float>", &muon_eta);
  tree->Branch("muon_phi", "std::vector<float>", &muon_phi);
  tree->Branch("muon_mass", "std::vector<float>", &muon_mass);
  tree->Branch("muon_id", "std::vector<unsigned int>" , &muon_id);
  tree->Branch("muon_iso", "std::vector<unsigned int>" , &muon_iso);
  tree->Branch("muon_charge", "std::vector<int>" , &muon_charge);
  tree->Branch("muon_d0", "std::vector<float>" , &muon_d0);
  tree->Branch("muon_dz", "std::vector<float>" , &muon_dz);

  tree->Branch("electron_pt", "std::vector<float>", &electron_pt);
  tree->Branch("electron_eta", "std::vector<float>", &electron_eta);
  tree->Branch("electron_phi", "std::vector<float>", &electron_phi);
  tree->Branch("electron_mass", "std::vector<float>", &electron_mass);
  tree->Branch("electron_id", "std::vector<unsigned int>" , &electron_id);
  tree->Branch("electron_charge", "std::vector<int>" , &electron_charge);
  tree->Branch("electron_d0", "std::vector<float>" , &electron_d0);
  tree->Branch("electron_dz", "std::vector<float>" , &electron_dz);

  tree->Branch("tau_pt", "std::vector<float>" , &tau_pt);
  tree->Branch("tau_eta", "std::vector<float>" , &tau_eta);
  tree->Branch("tau_phi", "std::vector<float>" , &tau_phi);
  tree->Branch("tau_mass", "std::vector<float>" , &tau_mass);
  tree->Branch("tau_decaymode", "std::vector<unsigned int>" , &tau_decaymode);
  tree->Branch("tau_idjet", "std::vector<float>" , &tau_idjet);
  tree->Branch("tau_idele", "std::vector<float>" , &tau_idele);
  tree->Branch("tau_idmu", "std::vector<float>" , &tau_idmu);
  tree->Branch("tau_idjet_wp", "std::vector<unsigned int>" , &tau_idjet_wp);
  tree->Branch("tau_idmu_wp", "std::vector<unsigned int>" , &tau_idmu_wp);
  tree->Branch("tau_idele_wp", "std::vector<unsigned int>" , &tau_idele_wp);
  tree->Branch("tau_charge", "std::vector<int>" , &tau_charge);

  if(isMC){
    tree->Branch("tau_genmatch_pt", "std::vector<float>" , &tau_genmatch_pt);
    tree->Branch("tau_genmatch_eta", "std::vector<float>" , &tau_genmatch_eta);
    tree->Branch("tau_genmatch_phi", "std::vector<float>" , &tau_genmatch_phi);
    tree->Branch("tau_genmatch_mass", "std::vector<float>" , &tau_genmatch_mass);
    tree->Branch("tau_genmatch_decaymode", "std::vector<int>" , &tau_genmatch_decaymode);
  }

  tree->Branch("jet_pt", "std::vector<float>" , &jet_pt);
  tree->Branch("jet_eta", "std::vector<float>" , &jet_eta);
  tree->Branch("jet_phi", "std::vector<float>" , &jet_phi);
  tree->Branch("jet_mass", "std::vector<float>" , &jet_mass);
  tree->Branch("jet_pt_raw", "std::vector<float>" , &jet_pt_raw);
  tree->Branch("jet_mass_raw", "std::vector<float>" , &jet_mass_raw);
  tree->Branch("jet_chf", "std::vector<float>" , &jet_chf);
  tree->Branch("jet_nhf", "std::vector<float>" , &jet_nhf);
  tree->Branch("jet_elf", "std::vector<float>" , &jet_elf);
  tree->Branch("jet_phf", "std::vector<float>" , &jet_phf);
  tree->Branch("jet_muf", "std::vector<float>" , &jet_muf);
  tree->Branch("jet_deepjet_probb", "std::vector<float>" , &jet_deepjet_probb);
  tree->Branch("jet_deepjet_probbb", "std::vector<float>" , &jet_deepjet_probbb);
  tree->Branch("jet_deepjet_probc", "std::vector<float>" , &jet_deepjet_probc);
  tree->Branch("jet_deepjet_problepb", "std::vector<float>" , &jet_deepjet_problepb);
  tree->Branch("jet_deepjet_probg", "std::vector<float>" , &jet_deepjet_probg);
  tree->Branch("jet_deepjet_probuds", "std::vector<float>" , &jet_deepjet_probuds);
  tree->Branch("jet_pnet_probb", "std::vector<float>" , &jet_pnet_probb);
  tree->Branch("jet_pnet_probbb", "std::vector<float>" , &jet_pnet_probbb);
  tree->Branch("jet_pnet_probc", "std::vector<float>" , &jet_pnet_probc);
  tree->Branch("jet_pnet_probcc", "std::vector<float>" , &jet_pnet_probcc);
  tree->Branch("jet_pnet_probuds", "std::vector<float>" , &jet_pnet_probuds);
  tree->Branch("jet_pnet_probg", "std::vector<float>" , &jet_pnet_probg);
  tree->Branch("jet_pnet_probpu", "std::vector<float>" , &jet_pnet_probpu);
  tree->Branch("jet_pnet_probundef", "std::vector<float>" , &jet_pnet_probundef);

  for(const auto & label : pnetDiscriminatorLabels){
    jet_pnetlast_score[label] = std::vector<float>();
    tree->Branch(("jet_pnetlast_"+label).c_str(),"std::vector<float>", &jet_pnetlast_score[label]);
  }

  tree->Branch("jet_id", "std::vector<unsigned int>" , &jet_id);
  tree->Branch("jet_puid", "std::vector<unsigned int>" , &jet_puid);
  tree->Branch("jet_ncand", "std::vector<unsigned int>" , &jet_ncand);
  tree->Branch("jet_nch", "std::vector<unsigned int>" , &jet_nch);
  tree->Branch("jet_nnh", "std::vector<unsigned int>" , &jet_nnh);
  tree->Branch("jet_nel", "std::vector<unsigned int>" , &jet_nel);
  tree->Branch("jet_nph", "std::vector<unsigned int>" , &jet_nph);
  tree->Branch("jet_nmu", "std::vector<unsigned int>" , &jet_nmu);
  tree->Branch("jet_hflav", "std::vector<unsigned int>" , &jet_hflav);
  tree->Branch("jet_pflav", "std::vector<int>" , &jet_pflav);
  tree->Branch("jet_nbhad", "std::vector<unsigned int>" , &jet_nbhad);
  tree->Branch("jet_nchad", "std::vector<unsigned int>" , &jet_nchad);

  if(isMC){
    tree->Branch("jet_genmatch_pt","std::vector<float>" , &jet_genmatch_pt);
    tree->Branch("jet_genmatch_eta","std::vector<float>" , &jet_genmatch_eta);
    tree->Branch("jet_genmatch_phi","std::vector<float>" , &jet_genmatch_phi);
    tree->Branch("jet_genmatch_mass","std::vector<float>" , &jet_genmatch_mass);
    tree->Branch("jet_genmatch_hflav","std::vector<unsigned int>" , &jet_genmatch_hflav);
    tree->Branch("jet_genmatch_pflav","std::vector<int>" , &jet_genmatch_pflav);
    tree->Branch("jet_genmatch_nbhad","std::vector<unsigned int>" , &jet_genmatch_nbhad);
    tree->Branch("jet_genmatch_nchad","std::vector<unsigned int>" , &jet_genmatch_nchad);
    tree->Branch("jet_genmatch_wnu_pt","std::vector<float>" , &jet_genmatch_wnu_pt);
    tree->Branch("jet_genmatch_wnu_eta","std::vector<float>" , &jet_genmatch_wnu_eta);
    tree->Branch("jet_genmatch_wnu_phi","std::vector<float>" , &jet_genmatch_wnu_phi);
    tree->Branch("jet_genmatch_wnu_mass","std::vector<float>" , &jet_genmatch_wnu_mass);
  }
  
  tree->Branch("jet_hlt_pt", "std::vector<float>" , &jet_hlt_pt);
  tree->Branch("jet_hlt_eta", "std::vector<float>" , &jet_hlt_eta);
  tree->Branch("jet_hlt_phi", "std::vector<float>" , &jet_hlt_phi);
  tree->Branch("jet_hlt_mass", "std::vector<float>" , &jet_hlt_mass);
  tree->Branch("jet_hlt_pt_raw", "std::vector<float>" , &jet_hlt_pt_raw);
  tree->Branch("jet_hlt_mass_raw", "std::vector<float>" , &jet_hlt_mass_raw);
  tree->Branch("jet_hlt_chf", "std::vector<float>" , &jet_hlt_chf);
  tree->Branch("jet_hlt_nhf", "std::vector<float>" , &jet_hlt_nhf);
  tree->Branch("jet_hlt_elf", "std::vector<float>" , &jet_hlt_elf);
  tree->Branch("jet_hlt_phf", "std::vector<float>" , &jet_hlt_phf);
  tree->Branch("jet_hlt_muf", "std::vector<float>" , &jet_hlt_muf);
  tree->Branch("jet_hlt_pnethlt_probtauh","std::vector<float>", &jet_hlt_pnethlt_probtauh);
  tree->Branch("jet_hlt_pnethlt_probb","std::vector<float>", &jet_hlt_pnethlt_probb);
  tree->Branch("jet_hlt_pnethlt_probc","std::vector<float>", &jet_hlt_pnethlt_probc);
  tree->Branch("jet_hlt_pnethlt_probuds","std::vector<float>", &jet_hlt_pnethlt_probuds);
  tree->Branch("jet_hlt_pnethlt_probg","std::vector<float>", &jet_hlt_pnethlt_probg);
  tree->Branch("jet_hlt_pnethltlast_probtauhp","std::vector<float>", &jet_hlt_pnethltlast_probtauhp);
  tree->Branch("jet_hlt_pnethltlast_probtauhm","std::vector<float>", &jet_hlt_pnethltlast_probtauhm);
  tree->Branch("jet_hlt_pnethltlast_probb","std::vector<float>", &jet_hlt_pnethltlast_probb);
  tree->Branch("jet_hlt_pnethltlast_probc","std::vector<float>", &jet_hlt_pnethltlast_probc);
  tree->Branch("jet_hlt_pnethltlast_probuds","std::vector<float>", &jet_hlt_pnethltlast_probuds);
  tree->Branch("jet_hlt_pnethltlast_probg","std::vector<float>", &jet_hlt_pnethltlast_probg);
  tree->Branch("jet_hlt_pnethltlast_ptcorr","std::vector<float>", &jet_hlt_pnethltlast_ptcorr);
  tree->Branch("jet_hlt_deepjet_probb", "std::vector<float>" , &jet_hlt_deepjet_probb);
  tree->Branch("jet_hlt_deepjet_probc", "std::vector<float>" , &jet_hlt_deepjet_probc);
  tree->Branch("jet_hlt_deepjet_problepb", "std::vector<float>" , &jet_hlt_deepjet_problepb);
  tree->Branch("jet_hlt_deepjet_probg", "std::vector<float>" , &jet_hlt_deepjet_probg);
  tree->Branch("jet_hlt_deepjet_probuds", "std::vector<float>" , &jet_hlt_deepjet_probuds);
  tree->Branch("jet_hlt_deepcsv_probb", "std::vector<float>" , &jet_hlt_deepcsv_probb);
  tree->Branch("jet_hlt_deepcsv_probc", "std::vector<float>" , &jet_hlt_deepcsv_probc);
  tree->Branch("jet_hlt_deepcsv_probudsg", "std::vector<float>" , &jet_hlt_deepcsv_probudsg);
  tree->Branch("jet_hlt_id", "std::vector<unsigned int>" , &jet_hlt_id);
  tree->Branch("jet_hlt_ncand", "std::vector<unsigned int>" , &jet_hlt_ncand);
  tree->Branch("jet_hlt_nch", "std::vector<unsigned int>" , &jet_hlt_nch);
  tree->Branch("jet_hlt_nnh", "std::vector<unsigned int>" , &jet_hlt_nnh);
  tree->Branch("jet_hlt_nel", "std::vector<unsigned int>" , &jet_hlt_nel);
  tree->Branch("jet_hlt_nph", "std::vector<unsigned int>" , &jet_hlt_nph);
  tree->Branch("jet_hlt_nmu", "std::vector<unsigned int>" , &jet_hlt_nmu);
  tree->Branch("jet_hlt_hflav", "std::vector<unsigned int>" , &jet_hlt_hflav);
  tree->Branch("jet_hlt_pflav", "std::vector<int>" , &jet_hlt_pflav);
  tree->Branch("jet_hlt_nbhad", "std::vector<unsigned int>" , &jet_hlt_nbhad);
  tree->Branch("jet_hlt_nchad", "std::vector<unsigned int>" , &jet_hlt_nchad);

  if(isMC){
    tree->Branch("jet_hlt_genmatch_pt","std::vector<float>" , &jet_hlt_genmatch_pt);
    tree->Branch("jet_hlt_genmatch_eta","std::vector<float>" , &jet_hlt_genmatch_eta);
    tree->Branch("jet_hlt_genmatch_phi","std::vector<float>" , &jet_hlt_genmatch_phi);
    tree->Branch("jet_hlt_genmatch_mass","std::vector<float>" , &jet_hlt_genmatch_mass);
    tree->Branch("jet_hlt_genmatch_hflav","std::vector<unsigned int>" , &jet_hlt_genmatch_hflav);
    tree->Branch("jet_hlt_genmatch_pflav","std::vector<int>" , &jet_hlt_genmatch_pflav);
    tree->Branch("jet_hlt_genmatch_nbhad","std::vector<unsigned int>" , &jet_hlt_genmatch_nbhad);
    tree->Branch("jet_hlt_genmatch_nchad","std::vector<unsigned int>" , &jet_hlt_genmatch_nchad);
    tree->Branch("jet_hlt_genmatch_wnu_pt","std::vector<float>" , &jet_hlt_genmatch_wnu_pt);
    tree->Branch("jet_hlt_genmatch_wnu_eta","std::vector<float>" , &jet_hlt_genmatch_wnu_eta);
    tree->Branch("jet_hlt_genmatch_wnu_phi","std::vector<float>" , &jet_hlt_genmatch_wnu_phi);
    tree->Branch("jet_hlt_genmatch_wnu_mass","std::vector<float>" , &jet_hlt_genmatch_wnu_mass);
  }

  tree->Branch("jet_hlt_pixel_pt","std::vector<float>",&jet_hlt_pixel_pt);
  tree->Branch("jet_hlt_pixel_eta","std::vector<float>",&jet_hlt_pixel_eta);
  tree->Branch("jet_hlt_pixel_phi","std::vector<float>",&jet_hlt_pixel_phi);
  tree->Branch("jet_hlt_pixel_mass","std::vector<float>",&jet_hlt_pixel_mass);

  if(isMC){
    tree->Branch("jet_hlt_pixel_genmatch_pt","std::vector<float>",&jet_hlt_pixel_genmatch_pt);
    tree->Branch("jet_hlt_pixel_genmatch_eta","std::vector<float>",&jet_hlt_pixel_genmatch_eta);
    tree->Branch("jet_hlt_pixel_genmatch_phi","std::vector<float>",&jet_hlt_pixel_genmatch_phi);
    tree->Branch("jet_hlt_pixel_genmatch_mass","std::vector<float>",&jet_hlt_pixel_genmatch_mass);
  }  
}

void ValidationNtupleMakerAK4::endJob() {
}

void ValidationNtupleMakerAK4::beginRun(edm::Run const& iRun, edm::EventSetup const& iSetup) {

  HLTConfigProvider fltrConfig;
  bool flag = false;
  fltrConfig.init(iRun, iSetup, filterResultsTag.process(), flag);
  
  // MET filter Paths
  filterPathsMap.clear();
  filterPathsVector.clear();
  filterPathsVector.push_back("Flag_goodVertices");
  filterPathsVector.push_back("Flag_globalSuperTightHalo2016Filter");
  filterPathsVector.push_back("Flag_HBHENoiseFilter");
  filterPathsVector.push_back("Flag_HBHENoiseIsoFilter");
  filterPathsVector.push_back("Flag_EcalDeadCellTriggerPrimitiveFilter");
  filterPathsVector.push_back("Flag_BadPFMuonFilter");
  filterPathsVector.push_back("Flag_BadChargedCandidateFilter");
  
  for (size_t i = 0; i < filterPathsVector.size(); i++)
    filterPathsMap[filterPathsVector[i]] = -1;
  
  for(size_t i = 0; i < filterPathsVector.size(); i++){
    TPRegexp pattern(filterPathsVector[i]);
    for(size_t j = 0; j < fltrConfig.triggerNames().size(); j++){
      std::string pathName = fltrConfig.triggerNames()[j];
      if(TString(pathName).Contains(pattern)){
	filterPathsMap[filterPathsVector[i]] = j;
      }
    }
  }

  // HLT + L1 Trigger                                                                                                                                                                                
  triggerPathsMap.clear();
  triggerL1SeedMap.clear();
  HLTConfigProvider hltConfig;
  hltConfig.init(iRun, iSetup, triggerResultsTag.process(), flag);
  for(size_t i = 0; i < hltConfig.triggerNames().size(); i++){
    std::string pathName = hltConfig.triggerNames()[i];
    if(pathName == "HLTriggerFinalPath" or pathName=="HLTriggerFirstPath" or TString(pathName).Contains("MC_")) continue;
    triggerPathsMap[pathName] = i;
    std::vector<std::string> tokens;
    for(auto module : hltConfig.hltL1TSeeds(i)){
      split(tokens,module,boost::algorithm::is_any_of("OR"));
      for(auto seed : tokens){
        if(seed == "") continue;
        seed.erase(std::remove(seed.begin(),seed.end(),' '),seed.end());
        triggerL1SeedMap[seed].push_back(pathName); // fix all prescales to 1                                                                                                                         
      }
    }
  }
  
  bool changedHLTPSP = false;
  hltPrescaleProvider->init(iRun, iSetup, triggerResultsTag.process(), changedHLTPSP); 

}

void ValidationNtupleMakerAK4::endRun(edm::Run const&, edm::EventSetup const&) {
}

void ValidationNtupleMakerAK4::initializeBranches(){

  event  = 0;
  run    = 0;
  lumi   = 0;
  putrue = 0;
  flags  = 0;
  wgt    = 0.;

  rho    = 0.;
  rho_hlt = 0.;
  
  gen_particle_pt.clear();
  gen_particle_eta.clear();
  gen_particle_phi.clear();
  gen_particle_mass.clear();
  gen_particle_id.clear();
  gen_particle_status.clear();
  gen_particle_daughters_id.clear();
  gen_particle_daughters_igen.clear();
  gen_particle_daughters_pt.clear();
  gen_particle_daughters_eta.clear();
  gen_particle_daughters_phi.clear();
  gen_particle_daughters_mass.clear();
  gen_particle_daughters_status.clear();
  gen_particle_daughters_charge.clear();

  lhe_particle_pt.clear();
  lhe_particle_eta.clear();
  lhe_particle_phi.clear();
  lhe_particle_mass.clear();
  lhe_particle_id.clear();
  lhe_particle_status.clear();

  trigger_hlt_path.clear();
  trigger_hlt_pass.clear();
  trigger_hlt_prescale.clear();

  trigger_l1_pass.clear();
  trigger_l1_seed.clear();
  trigger_l1_prescale.clear();

  trigger_l1_jet_pt.clear();
  trigger_l1_jet_eta.clear();
  trigger_l1_jet_phi.clear();
  trigger_l1_jet_mass.clear();

  trigger_l1_muon_pt.clear();
  trigger_l1_muon_eta.clear();
  trigger_l1_muon_phi.clear();
  trigger_l1_muon_mass.clear();
  trigger_l1_muon_qual.clear();
  trigger_l1_muon_iso.clear();

  trigger_l1_ht = 0;
  trigger_l1_mht = 0;

  muon_pt.clear();
  muon_eta.clear();
  muon_phi.clear();
  muon_mass.clear();
  muon_id.clear();
  muon_charge.clear();
  muon_iso.clear();
  muon_d0.clear();
  muon_dz.clear();

  electron_pt.clear();
  electron_eta.clear();
  electron_phi.clear();
  electron_mass.clear();
  electron_id.clear();
  electron_charge.clear();
  electron_d0.clear();
  electron_dz.clear();

  tau_pt.clear();
  tau_eta.clear();
  tau_phi.clear();
  tau_mass.clear();
  tau_decaymode.clear();
  tau_idjet.clear();
  tau_idele.clear();
  tau_idmu.clear();
  tau_idjet_wp.clear();
  tau_idmu_wp.clear();
  tau_idele_wp.clear();
  tau_charge.clear();
  tau_genmatch_pt.clear();
  tau_genmatch_eta.clear();
  tau_genmatch_phi.clear();
  tau_genmatch_mass.clear();
  tau_genmatch_decaymode.clear();

  met = 0.;
  met_phi = 0;

  npv = 0;
  nsv = 0;

  jet_pt.clear();
  jet_pt_raw.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_mass.clear();
  jet_mass_raw.clear();
  jet_chf.clear();
  jet_nhf.clear();
  jet_elf.clear();
  jet_phf.clear();
  jet_muf.clear();
  jet_deepjet_probb.clear();
  jet_deepjet_probbb.clear();
  jet_deepjet_problepb.clear();
  jet_deepjet_probc.clear();
  jet_deepjet_probg.clear();
  jet_deepjet_probuds.clear();
  jet_pnet_probb.clear();
  jet_pnet_probbb.clear();
  jet_pnet_probc.clear();
  jet_pnet_probcc.clear();
  jet_pnet_probg.clear();
  jet_pnet_probuds.clear();
  jet_pnet_probpu.clear();
  jet_pnet_probundef.clear();

  for(auto & imap : jet_pnetlast_score)
    imap.second.clear();

  jet_id.clear();
  jet_puid.clear();
  jet_ncand.clear();
  jet_nch.clear();
  jet_nnh.clear();
  jet_nel.clear();
  jet_nph.clear();
  jet_nmu.clear();
  jet_hflav.clear();
  jet_pflav.clear();
  jet_nbhad.clear();
  jet_nchad.clear();
  jet_genmatch_pt.clear();
  jet_genmatch_eta.clear();
  jet_genmatch_phi.clear();
  jet_genmatch_mass.clear();
  jet_genmatch_hflav.clear();
  jet_genmatch_pflav.clear();
  jet_genmatch_nbhad.clear();
  jet_genmatch_nchad.clear();
  jet_genmatch_wnu_pt.clear();
  jet_genmatch_wnu_eta.clear();
  jet_genmatch_wnu_phi.clear();
  jet_genmatch_wnu_mass.clear();

  jet_hlt_pt.clear();
  jet_hlt_pt_raw.clear();
  jet_hlt_eta.clear();
  jet_hlt_phi.clear();
  jet_hlt_mass.clear();
  jet_hlt_mass_raw.clear();
  jet_hlt_chf.clear();
  jet_hlt_nhf.clear();
  jet_hlt_elf.clear();
  jet_hlt_phf.clear();
  jet_hlt_muf.clear();
  jet_hlt_pnethltlast_probtauhp.clear();
  jet_hlt_pnethltlast_probtauhm.clear();
  jet_hlt_pnethltlast_probb.clear();
  jet_hlt_pnethltlast_probc.clear();
  jet_hlt_pnethltlast_probuds.clear();
  jet_hlt_pnethltlast_probg.clear();
  jet_hlt_pnethltlast_ptcorr.clear();
  jet_hlt_pnethlt_probtauh.clear();
  jet_hlt_pnethlt_probb.clear();
  jet_hlt_pnethlt_probc.clear();
  jet_hlt_pnethlt_probuds.clear();
  jet_hlt_pnethlt_probg.clear();
  jet_hlt_deepjet_probb.clear();
  jet_hlt_deepjet_problepb.clear();
  jet_hlt_deepjet_probc.clear();
  jet_hlt_deepjet_probg.clear();
  jet_hlt_deepjet_probuds.clear();
  jet_hlt_deepcsv_probb.clear();
  jet_hlt_deepcsv_probc.clear();
  jet_hlt_deepcsv_probudsg.clear();
  jet_hlt_id.clear();
  jet_hlt_ncand.clear();
  jet_hlt_nch.clear();
  jet_hlt_nnh.clear();
  jet_hlt_nel.clear();
  jet_hlt_nph.clear();
  jet_hlt_nmu.clear();
  jet_hlt_hflav.clear();
  jet_hlt_pflav.clear();
  jet_hlt_nbhad.clear();
  jet_hlt_nchad.clear();
  jet_hlt_genmatch_pt.clear();
  jet_hlt_genmatch_eta.clear();
  jet_hlt_genmatch_phi.clear();
  jet_hlt_genmatch_mass.clear();
  jet_hlt_genmatch_hflav.clear();
  jet_hlt_genmatch_pflav.clear();
  jet_hlt_genmatch_nbhad.clear();
  jet_hlt_genmatch_nchad.clear();
  jet_hlt_genmatch_wnu_pt.clear();
  jet_hlt_genmatch_wnu_eta.clear();
  jet_hlt_genmatch_wnu_phi.clear();
  jet_hlt_genmatch_wnu_mass.clear();

  jet_hlt_pixel_pt.clear();
  jet_hlt_pixel_eta.clear();
  jet_hlt_pixel_phi.clear();
  jet_hlt_pixel_mass.clear();
  jet_hlt_pixel_genmatch_pt.clear();
  jet_hlt_pixel_genmatch_eta.clear();
  jet_hlt_pixel_genmatch_phi.clear();
  jet_hlt_pixel_genmatch_mass.clear();

}

void ValidationNtupleMakerAK4::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}


// to apply jet ID: https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL
bool ValidationNtupleMakerAK4::applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi){
  
  if(level != "tight" and level != "tightLepVeto")
    return true;
  
  double eta  = jet.eta();                                                                                                                                                                           
  double nhf  = jet.neutralHadronEnergyFraction();                                                                                                                                                   
  double nemf = jet.neutralEmEnergyFraction();                                                                                                                                                       
  double chf  = jet.chargedHadronEnergyFraction();                                                                                                                                                   
  double muf  = jet.muonEnergyFraction();                                                                                                                                                            
  double cemf = jet.chargedEmEnergyFraction();                                                                                                                            
  
  int jetid  = 0;
  if(isPuppi){ // Puppi jets
    float np, nnp, nch;
    if (jet.hasUserFloat("patPuppiJetSpecificProducer:puppiMultiplicity") and jet.hasUserFloat("patPuppiJetSpecificProducer:neutralPuppiMultiplicity")){
      np   = jet.userFloat("patPuppiJetSpecificProducer:puppiMultiplicity");
      nnp  = jet.userFloat("patPuppiJetSpecificProducer:neutralPuppiMultiplicity");
      nch  = np-nnp;
    }
    else{
      np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();
      nnp  = jet.neutralMultiplicity();
      nch  = jet.chargedMultiplicity();
    }
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 1;                                                                                               
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 2;                                                                                               
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 1;                                                                                                                        
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 2;                                                                                                                    
  }
  else{ // CHS jets
    int np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();                                                                                                                              
    int nnp  = jet.neutralMultiplicity();                                                                                                                                                        
    int nch  = jet.chargedMultiplicity();                                                                                                                                                          
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;    
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 1;                                                                                               
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 2;                                        
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 1;                                                                                                                      
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 2;                                                          
  }
                    
  if(level == "tight" and jetid > 1) return true;
  else if(level == "tightLepVeto" and jetid > 2) return true;
  else return false;

}


bool ValidationNtupleMakerAK4::applyPileupJetID(const pat::Jet & jet, const std::string & level){
  bool passpuid = false;  
  if(jet.hasUserInt("pileupJetIdUpdated:fullId")){
    if(level == "loose"  and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 0)) or jet.pt() > 50)) passpuid = true;
    if(level == "medium" and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 1)) or jet.pt() > 50)) passpuid = true;
    if(level == "tight"  and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 2)) or jet.pt() > 50)) passpuid = true;
  }
  else if (jet.hasUserInt("pileupJetId:fullId")){
    if(level == "loose"  and (bool(jet.userInt("pileupJetId:fullId") & (1 << 0)) or jet.pt() > 50)) passpuid = true;
    if(level == "medium" and (bool(jet.userInt("pileupJetId:fullId") & (1 << 1)) or jet.pt() > 50)) passpuid = true;
    if(level == "tight"  and (bool(jet.userInt("pileupJetId:fullId") & (1 << 2)) or jet.pt() > 50)) passpuid = true;
  }
  return passpuid;
}


DEFINE_FWK_MODULE(ValidationNtupleMakerAK4);

