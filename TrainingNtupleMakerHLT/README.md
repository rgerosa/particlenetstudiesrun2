# Training Ntuples for ParticleNet at HLT

## HLT step and L1-emulation 

First step is to generate a recent HLT trigger menu that can be used in order to re-emulate the trigger in simulated or data events from a GEN-SIM-RAW or RAW data-tier, respectively. In this respect please monitor the recipies provided in the following twikis:

* Global HLT instructions $`\to`$ [SWGuideGlobalHLT](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideGlobalHLT)
* Timing studies instructions  $`\to`$ [TriggerStudiesTiming](https://twiki.cern.ch/twiki/bin/view/CMS/TriggerStudiesTiming)
* Reference era is `Run3` with `126X_mcRun3_2023_forPU65_v4` conditions
* Customizations are needed because the PF rechit thresholds will be raised in 2023, therefore the training should be performed after HCAL update of PF-hadron calibration and using the following customization $`\to`$ [customizeHLTFor2023.py](https://github.com/silviodonato/cmssw/blob/customizeHLTFor2023/HLTrigger/Configuration/python/customizeHLTFor2023.py):

  ```sh 
  cd python;
  wget https://raw.githubusercontent.com/silviodonato/cmssw/customizeHLTFor2023/HLTrigger/Configuration/python/customizeHLTFor2023.py 
  cd -;
  ```

* If you want to take-out L1 prescales when running on ZeroBias data [L1-recipe](https://github.com/cms-l1-dpg/L1MenuRun3/tree/master/development/L1Menu_Collisions2023_v1_0_0/PrescaleTable):

  ```sh 
  git cms-addpkg L1Trigger/L1TGlobal
  mkdir -p L1Trigger/L1TGlobal/data/Luminosity/startup
  cd L1Trigger/L1TGlobal/data/Luminosity/startup
  wget https://raw.githubusercontent.com/cms-l1-dpg/L1MenuRun3/master/development/L1Menu_Collisions2023_v1_0_0/L1Menu_Collisions2023_v1_0_0.xml
  wget https://raw.githubusercontent.com/cms-l1-dpg/L1MenuRun3/master/development/L1Menu_Collisions2023_v1_0_0/PrescaleTable/UGT_BASE_RS_FINOR_MASK_L1MenuCollisions2023_v1_0_0.xml
  wget https://raw.githubusercontent.com/cms-l1-dpg/L1MenuRun3/master/development/L1Menu_Collisions2023_v1_0_0/PrescaleTable/UGT_BASE_RS_PRESCALES_L1MenuCollisions2023_v1_0_0.xml
  cd -
  ```

* Generate a menu with only ParticleNet path for MC:

  ```sh	    
  hltGetConfiguration /dev/CMSSW_13_0_0/GRun --globaltag auto:126X_mcRun3_2023_forPU65_v4 --mc --unprescale --offline --output full --max-events -1 --eras Run3 --l1-emulator FullMC --l1 L1Menu_Collisions2023_v1_0_0_xml --input /store/mc/Run3Winter23Digi/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/GEN-SIM-RAW/126X_mcRun3_2023_forPU65_v1-v2/40000/00204dcc-fb01-415f-8ef4-9617fc1565c5.root --paths HLTriggerFirstPath,*HLT_QuadPFJet70_50_40_35_PNet2BTagMean0p65*,*MC_PFBTagDeepCSV*,*MC_PFBTagDeepJet*,HLTriggerFinalPath --cff > python/hlt_step_mc_cff.py
  hltGetConfiguration /dev/CMSSW_13_0_0/GRun --globaltag auto:126X_mcRun3_2023_forPU65_v4 --mc --unprescale --offline --output full --max-events -1 --eras Run3 --l1-emulator FullMC --l1 L1Menu_Collisions2023_v1_0_0_xml --input /store/mc/Run3Winter23Digi/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/GEN-SIM-RAW/126X_mcRun3_2023_forPU65_v1-v2/40000/00204dcc-fb01-415f-8ef4-9617fc1565c5.root --paths HLTriggerFirstPath,*HLT_AK8PFJet250_SoftDropMass40_PFAK8ParticleNetBB0p35*,*HLT_QuadPFJet70_50_40_35_PNet2BTagMean0p65*,HLTriggerFinalPath --cff > python/hlt_step_mc_ak8_cff.py
  ```

* Generate a menu with only ParticleNet path for Data 2022:

```sh
  hltGetConfiguration /dev/CMSSW_13_0_0/GRun --globaltag 130X_dataRun3_HLT_v2 --data --unprescale --offline --output full --max-events -1 --eras Run3 --l1-emulator uGT --l1 L1Menu_Collisions2023_v1_0_0_xml --input /store/data/Run2022G/EphemeralHLTPhysics3/RAW/v1/000/362/720/00000/850a6b3c-6eef-424c-9dad-da1e678188f3.root --paths HLTriggerFirstPath,*HLT_QuadPFJet70_50_40_35_PNet2BTagMean0p65*,*MC_PFBTagDeepCSV*,*MC_PFBTagDeepJet*,HLTriggerFinalPath --cff > python/hlt_step_data_2022_cff.py
  hltGetConfiguration /dev/CMSSW_13_0_0/GRun --globaltag 130X_dataRun3_HLT_v2 --data --unprescale --offline --output full --max-events -1 --eras Run3 --l1-emulator uGT --l1 L1Menu_Collisions2023_v1_0_0_xml --input /store/data/Run2022G/EphemeralHLTPhysics3/RAW/v1/000/362/720/00000/850a6b3c-6eef-424c-9dad-da1e678188f3.root --paths HLTriggerFirstPath,*HLT_AK8PFJet250_SoftDropMass40_PFAK8ParticleNetBB0p35*,*HLT_QuadPFJet70_50_40_35_PNet2BTagMean0p65*,HLTriggerFinalPath --cff > python/hlt_step_data_2022_ak8_cff.py
  ```

The trigger menu generated above, from a given ConfDb and CMSSW version, should be then edited in order to suppress the selections imposed in the ParticleNet trigger paths, potentially replace or modify the L1 trigger seeds, etc .. The goal is to just run PNET AK4 or PNET AK8 on all events, on all jets above a certain threshold and dump these events for the subsequent analysis.

The customization of the trigger menu happens via the `python/customizedHLTMenu.py` or `python/customizedHLTMenu_ak8.py` that supports the following inputs parameters:

* `nThreads`: number of threads to be used in the process
* `outputName`: name of the output file produced with the HLT collections converted into PAT format
* `hltProcessName`: name of the HLT process that has to be different from "HLT"
* `inputFileListFile`: provide input files as a text file. If provided, files will be read from there
* `jsonFile`: json file containing the LS to be analyzed (only for data)
* `useLocalInputFile`: if True append ***file:*** to the file name to access it locally
* `datasetType`: type of dataset (MC126X, MC124X, 2022 data, etc) used to select GT.
* `l1Menu`: name of the L1 menu xml to be access in the DB for the emulation
* `filterOnL1Seeds`: filter events on the basis of a list of L1 seeds contained in the file PNET paths (by default is OFF but should be turned-ON when studying rates in data). Caveat: the jet reconstruction, SV fit and PNET inference is done in for all events, events are filtered at the level of the outputModule
* `additionalL1Seeds`: if you want to add more seeds not included in the original PNET L1 pre-selection
* `addHLTCustomization2023`: add the customization for HLT menu of 2023 not yet in release
* `useSQLiteFile`: use SQL file to pick up HCAL corrections that is committed under `ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/PFCalibration.db`
* `jetPtMin` and `jetEtaMax`: selection on ak4 jets to be stored in the output collections

The step above runs the HLT and produces an output file that contains only a subset of useful information. The ParticleNet path are modified by removing all filters, as the goal is to store jets/PF-candidates/PNET-scores for all jets above threshold in all events. The removal of the trigger filters is implemented in `python/modifyPNETPaths_cff.py`. In addition, it seeds the b-tagging sequence with uncorrected jets (for both AK4 and AK8) as JECs will be applied in the PAT step. Moreover, the collections stored in the output are PAT-objects build on top of the HLT PF-candidates, HLT AK4/AK8 PF and GEN jets. The logic is expressed in `python/patAK4JetsForHLT.py` and `python/patAK8JetsForHLT.py` that takes the following inputs:

* `process`: HLT process contained in the `python/customizedHLTMenu.py` or `python/customizedHLTMenu_ak8.py`
* `options`: parameters used in the `python/customizedHLTMenu.py` or python/customizedHLTMenu_ak8.py`
* `jetCollection`: jet collection used in the ParticleNet path
* `pathName`: name of the trigger Path of the job to which the PAT collection producers will be appended
* `outputModuleName`: output module used to create the output file and express which collections should be kept
* `isMC`: flag to express whether run on data or MC
* `minPtGenJets` and `maxEtaGenJets`: used for AK4/AK8 GEN jets clustering

Example on how to run the HLT emulation step:

```sh
cmsRun customizedHLTMenu.py useLocalInputFile=False inputFiles=/store/mc/Run3Winter23Digi/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/GEN-SIM-RAW/126X_mcRun3_2023_forPU65_v1-v2/40000/00204dcc-fb01-415f-8ef4-9617fc1565c5.root maxEvents=50 hltProcessName=HLTX filterOnL1Seeds=False nThreads=4
cmsRun customizedHLTMenu.py useLocalInputFile=False inputFiles=/store/data/Run2022G/EphemeralHLTPhysics3/RAW/v1/000/362/720/00000/f2b72761-399f-4fad-811a-e6e32171ce67.root maxEvents=50 hltProcessName=HLTX filterOnL1Seeds=True datasetType=Data2022 nThreads=4
cmsRun customizedHLTMenu_ak8.py useLocalInputFile=False inputFiles=/store/mc/Run3Winter23Digi/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/GEN-SIM-RAW/126X_mcRun3_2023_forPU65_v1-v2/40000/00204dcc-fb01-415f-8ef4-9617fc1565c5.root maxEvents=50 hltProcessName=HLTX filterOnL1Seeds=False nThreads=4
cmsRun customizedHLTMenu_ak8.py useLocalInputFile=False inputFiles=/store/data/Run2022G/EphemeralHLTPhysics3/RAW/v1/000/362/720/00000/f2b72761-399f-4fad-811a-e6e32171ce67.root maxEvents=50 hltProcessName=HLTX filterOnL1Seeds=True datasetType=Data2022 nThreads=4
```

## Run RECO+MINIAOD step

Configuration files are generated by searching in MCM the various production chains of the `/QCD_Pt-120To170_TuneCP5_13p6TeV-pythia8/*Run3Summer22EEMiniAODv3*/MINIAODSIM` dataset. The data configurations can be similarly obained. Below commands are reported for the RECO and miniAOD steps

* **Configuration generation** for data and MC using the following using the following `cmsDriver` commands
    ```sh
    cmsDriver.py  --python_filename miniaod_step_mc_cfg.py --eventcontent MINIAODSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier MINIAODSIM --fileout file:output.root --conditions 124X_mcRun3_2022_realistic_postEE_v1 --step RAW2DIGI,L1Reco,RECO,RECOSIM,PAT --geometry DB:Extended --filein file:input.root --era Run3 --no_exec --mc -n -1
    cmsDriver.py  --python_filename miniaod_step_data_cfg.py --eventcontent MINIAOD --customise Configuration/DataProcessing/Utils.addMonitoring --datatier MINIAOD --fileout file:output.root --conditions auto:run3_data --step RAW2DIGI,L1Reco,RECO,PAT --geometry DB:Extended --filein file:input.root --era Run3 --no_exec --data -n -1
    ```
* The **configuration needs to be modified** in order to add input-parameters needed to run the step as well as a keep of the collections created by the HLT step based on the `hltProcessName`. The list of parameters that can be given to run the RECO+MiniAOD steps are:
  * `hltProcessName`: HLT process contained in the `python/customizedHLTMenu.py`
  * `nThreads`: number of threads to be used in the process
  * `inputFiles`: input files, as a list, from the HLT emulation step that saves in the file all info needed for the RECO to RUN
  * `outputName`: name of the output file that needs to become input to the ntuplizer
  * `useLocalInputFile`: if True append `file:` to the file names to access them locally

Example on how to run the RECO+MiniAOD emulation step:

```sh
cmsRun miniaod_step_mc_cfg.py nThreads=4 inputFiles=hltMenu_step.root hltProcessName=HLTX useLocalInputFile=True maxEvents=-1
cmsRun miniaod_step_data_cfg.py nThreads=4 inputFiles=hltMenu_step.root hltProcessName=HLTX useLocalInputFile=True maxEvents=-1
```

## Run Training Ntuplizer for AK4

Code that produces ntuples from the modified miniAOD described/produced with the step above. The ntuple creation happens by running `test/makeTrainingNtuple_cfg.py` that uses the EDAnalyzer called `plugins/TrainingNtupleMakerAK4`. The ntuple-step can be run by expressing the following options:

* `nThreads`: number of threads to be used in the process
* `outputName`: name of the output file produced with the HLT collections converted into PAT format
* `hltProcessName`: name of the HLT process that has to be different from "HLT"
* `useLocalInputFile`: if True append `file:` to the file name to access it locally
* `datasetType`: type of dataset (MC126X, MC124X, 2022 data, etc) used to select GT.
* `saveL1Objects`: save or not the L1 trigger objects in the ntuples
* `saveLHEObjects`: save or not the LHE particles in the ntuples
* `useOfflinePuppiJets`: use offline puppi jets instead of CHS (default is True as Puppi is baseline for Run3)
* `muonPtMin`, `electronPtMin`, `tauPtMin`: minimum $`p_{T}`$ on the offline leptons
* `jetPtMin`, `jetEtaMax`, `jetEtaMin`: jet AK4 selections applied on both online and offline jets
* `dumpOnlyJetMatchedToGen` and `dRJetGenMatch`: $`\Delta R`$ matching between online/offline jets and gen jets (only for MC)

Example on how to run the ntuples from the miniAOD produced:

```sh
cmsRun ../test/makeTrainingNtuple_cfg.py datasetType=MC126X nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
cmsRun ../test/makeTrainingNtuple_cfg.py datasetType=Data2022 nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
```

## Run Validation Ntuplizer for AK4

Code that produces ntuples from the modified miniAOD described/produced with the step above. The ntuple creation happens by running `test/makeValidationNtuple_cfg.py` that uses the EDAnalyzer called `plugins/ValidationNtupleMakerAK4`. The ntuple-step can be run by expressing the following options:

* `nThreads`: number of threads to be used in the process
* `outputName`: name of the output file produced with the HLT collections converted into PAT format
* `hltProcessName`: name of the HLT process that has to be different from "HLT"
* `useLocalInputFile`: if True append `file:` to the file name to access it locally
* `datasetType`: type of dataset (MC126X, MC124X, 2022 data, etc) used to select GT.
* `saveL1Objects`: save or not the L1 trigger objects in the ntuples
* `saveLHEObjects`: save or not the LHE particles in the ntuples
* `useOfflinePuppiJets`: use offline puppi jets instead of CHS (default is True as Puppi is baseline for Run3)
* `muonPtMin`, `electronPtMin`, `tauPtMin`: minimum $`p_{T}`$ on the offline leptons
* `jetPtMin`, `jetEtaMax`, `jetEtaMin`: jet AK4 selections applied on both online and offline jets
* `dumpOnlyJetMatchedToGen` and `dRJetGenMatch`: $`\Delta R`$ matching between online/offline jets and gen jets (only for MC)

Example on how to run the ntuples from the miniAOD produced:

```sh
cmsRun ../test/makeValidationNtuple_cfg.py datasetType=MC126X nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
cmsRun ../test/makeValidationNtuple_cfg.py datasetType=Data2022 nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
```

## Run Training Ntuplizer for AK8

Code that produces ntuples from the modified miniAOD described/produced with the step above. The ntuple creation happens by running `test/makeTrainingNtuple_ak8_cfg.py` that uses the EDAnalyzer called `plugins/TrainingNtupleMakerAK8`. The ntuple-step can be run by expressing the following options:

* `nThreads`: number of threads to be used in the process
* `outputName`: name of the output file produced with the HLT collections converted into PAT format
* `hltProcessName`: name of the HLT process that has to be different from "HLT"
* `useLocalInputFile`: if True append `file:` to the file name to access it locally
* `datasetType`: type of dataset (MC126X, MC124X, 2022 data, etc) used to select GT.
* `saveL1Objects`: save or not the L1 trigger objects in the ntuples
* `saveLHEObjects`: save or not the LHE particles in the ntuples
* `useOfflinePuppiJets`: use offline puppi jets instead of CHS (default is True as Puppi is baseline for Run3)
* `muonPtMin`, `electronPtMin`, `tauPtMin`: minimum $`p_{T}`$ on the offline leptons
* `jetPtAK4Min`, `jetPtAK8Min`: minimum $`p_{T}`$ on the online and offline AK4 and AK8 jets
* `jetEtaMax`, `jetEtaMin`: jet $|\eta|`$ selection applied on AK4 and AK8 jets
* `dumpOnlyJetMatchedToGen` and `dRJetGenMatch`: $`\Delta R`$ matching between online/offline jets and gen jets (only for MC)
* `filterEventsInTheDumper`: filter out events that don't contain at least one AK8 jet fullfilling the selection

Example on how to run the ntuples from the miniAOD produced:

```sh
cmsRun ../test/makeTrainingNtuple_ak8_cfg.py datasetType=MC126X nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
cmsRun ../test/makeTrainingNtuple_ak8_cfg.py datasetType=Data2022 nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True

## Run Validation Ntuplizer for AK8

Code that produces ntuples from the modified miniAOD described/produced with the step above. The ntuple creation happens by running `test/makeValidationNtuple_cfg.py` that uses the EDAnalyzer called `plugins/ValidationNtupleMakerAK4`. The ntuple-step can be run by expressing the following options:

* `nThreads`: number of threads to be used in the process
* `outputName`: name of the output file produced with the HLT collections converted into PAT format
* `hltProcessName`: name of the HLT process that has to be different from "HLT"
* `useLocalInputFile`: if True append `file:` to the file name to access it locally
* `datasetType`: type of dataset (MC126X, MC124X, 2022 data, etc) used to select GT.
* `saveL1Objects`: save or not the L1 trigger objects in the ntuples
* `saveLHEObjects`: save or not the LHE particles in the ntuples
* `useOfflinePuppiJets`: use offline puppi jets instead of CHS (default is True as Puppi is baseline for Run3)
* `muonPtMin`, `electronPtMin`, `tauPtMin`: minimum $`p_{T}`$ on the offline leptons
* `jetPtAK4Min`, `jetPtAK8Min`: minimum $`p_{T}`$ on the online and offline AK4 and AK8 jets
* `jetEtaMax`, `jetEtaMin`: jet $|\eta|`$ selection applied on AK4 and AK8 jets
* `dumpOnlyJetMatchedToGen` and `dRJetGenMatch`: $`\Delta R`$ matching between online/offline jets and gen jets (only for MC)
* `filterEventsInTheDumper`: filter out events that don't contain at least one AK8 jet fullfilling the selection

Example on how to run the ntuples from the miniAOD produced:

```sh
cmsRun ../test/makeValidationNtuple_ak8_cfg.py datasetType=MC126X nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
cmsRun ../test/makeValidationNtuple_ak8_cfg.py datasetType=Data2022 nThreads=4 saveL1Objects=True inputFiles=miniAOD_step.root useLocalInputFile=True
```
```

## Crab processing of MiniAOD and Ntuples from GEN-SIM-RAW

In order to submit crab jobs on existing `GEN-SIM-RAW` or `RAW` datasets you can use the script called `python/createCrabJob.py` that takes the following input parameters:
* `--sample-config`: python file containing the list of samples to be processed along with crab job splitting and process options information to run the jobs.
* `--sample-type`: string that identifies the class/type of samples/dataset to be considered in the crab-job creation (blocks in the sample training files).
* `--crab-config`: generic crab config describing output directory, crab job options, etc. that needs to be used in submission
* `--crab-script`: indicate the bash script that needs to be executed by crab
* `--crab-job-dir`: directory in which crab jobs will be created
* `--command`: crab command to be executed
* `--options`: specific options of the crab command that needs to be run
* `--force-copy-python-dir`: temporary solution for a crab problem .. creates sym link in the `$CMSSW_BASE/python` directory for all python files under ``$CMSSW_BASE/src`. A fix has been already implemented in crab-dev.

Example on how to submit crab jobs is reported below:
```sh
python3 createCrabJob.py --sample-config samples_training_raw.py  --sample-type DY --crab-config ../crab/crabConfig_training_raw.py  --crab-job-dir ../crab/crab_training_ak4 --crab-script ../crab/scriptExe_training_raw.sh --command submit 
```

In a similar manner the AK8 ntuples crab workflow can be created by using the same script but substituting the crab/bash/sample cfgs with the proper ones. Example is:

Example on how to check crab jobs is reported below:
```sh
python3 createCrabJob.py --sample-config samples_training_ak8_raw.py  --sample-type DY --crab-config ../crab/crabConfig_training_ak8_raw.py  --crab-job-dir ../crab/crab_training_ak8 --crab-script ../crab/scriptExe_training_ak8_raw.sh --command status 
```

## Crab processing of MiniAOD and Ntuples from PrivateMC generation

The crab workflow needs in this case not to take `GEN-SIM-RAW` produced dataset as inputs but produce a certain set of events from either `LHE` or from `GEN` (Pythia or Herwig direct production).

A concrete example is the production of X to HH events with random X and H masses in a flat range. The Higgs bosons can then undergo to decays in $`bb/cc/\tau\tau/ss/uu/dd/gg`$ with equal probabilities. This is generation is performed by running, within the same crab job, the following steps: [`GEN-SIM`,`DIGI-RAW`,`HLT`,`RECO+MINIAOD`,`Ntuplization`]. The last three steps are identical to those described above, the first two are instead customized for every process one wants to generate starting from the basic configuration files obtained by the following CMSSW driver commands:

* `GEN-SIM` step in CMSSW 126X Winter23 production:
```sh
cmsDriver.py Configuration/GenProduction/python/TSG-Run3Winter23GS-00019-fragment.py --python_filename TSG-Run3Winter23GS-00019_1_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM --fileout file:TSG-Run3Winter23GS-00019.root --conditions 126X_mcRun3_2023_forPU65_v1 --beamspot Realistic25ns13p6TeVEarly2022Collision --step GEN,SIM --geometry DB:Extended --era Run3 --no_exec --mc -n -1 ;
```
* `DIGI-RAW` step CMSSW 126X Winter23 production relying on MiniBias events for PU:

```sh
cmsDriver.py  --python_filename TSG-Run3Winter23Digi-00025_1_cfg.py --eventcontent RAWSIM --pileup 2023_LHC_Simulation_12p5h_9h_hybrid2p23 --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM-RAW --fileout file:TSG-Run3Winter23Digi-00025.root --pileup_input "dbs:/MinBias_TuneCP5_13p6TeV-pythia8/Run3Winter23GS-126X_mcRun3_2023_forPU65_v1-v1/GEN-SIM" --conditions 126X_mcRun3_2023_forPU65_v4 --customise_commands 'process.hltParticleFlowClusterHBHE.seedFinder.thresholdsByDetector[0].seedingThreshold=[0.6,0.5,0.5,0.5]\n process.hltParticleFlowClusterHBHE.initialClusteringStep.thresholdsByDetector[0].gatheringThreshold=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHE.pfClusterBuilder.recHitEnergyNorms[0].recHitEnergyNorm=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHE.pfClusterBuilder.positionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHE.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHCAL.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowRecHitHBHE.producers[0].qualityTests[0].cuts[0].threshold=[0.4,0.3,0.3,0.3]\n process.hltEgammaHoverE.eThresHB=[0.4,0.3,0.3,0.3]\n process.hltEgammaHoverEUnseeded.eThresHB=[0.4,0.3,0.3,0.3]\n process.hltEgammaHToverET.eThresHB=[0.4,0.3,0.3,0.3]\n process.hltFixedGridRhoFastjetECALMFForMuons.eThresHB=[0.4,0.3,0.3,0.3]\n process.hltFixedGridRhoFastjetAllCaloForMuons.eThresHB=[0.4,0.3,0.3,0.3]\n process.hltFixedGridRhoFastjetHCAL.eThresHB=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHECPUOnly.seedFinder.thresholdsByDetector[0].seedingThreshold=[0.6,0.5,0.5,0.5]\n process.hltParticleFlowClusterHBHECPUOnly.initialClusteringStep.thresholdsByDetector[0].gatheringThreshold=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHECPUOnly.pfClusterBuilder.recHitEnergyNorms[0].recHitEnergyNorm=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHECPUOnly.pfClusterBuilder.positionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHBHECPUOnly.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowClusterHCALCPUOnly.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=[0.4,0.3,0.3,0.3]\n process.hltParticleFlowRecHitHBHECPUOnly.producers[0].qualityTests[0].cuts[0].threshold=[0.4,0.3,0.3,0.3]' --step DIGI,L1,DIGI2RAW,HLT:2022v15 --geometry DB:Extended --filein "dbs:/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23wmLHEGS-126X_mcRun3_2023_forPU65_v1-v1/GEN-SIM" --era Run3 --no_exec --mc -n
```

Let's consider one specific example, i.e. as mentioned above the the production of X to HH events with random X and H masses in a flat range. The customized configuration files can be found in `python/mc_generation_126X` folder. 

In particular:
* `gen_sim_step_xtohh.py`: file used for the GEN-SIM production of such events with the following paramaters
   * `jobNum`: identifier of the job number in the crab production, used also to set the run number.
   * `jobEvents`: number of events that are produced overall in the job from crab-config
   * `nEvents`: events that are generated by this cmsRun execution. The logic is the following: in order to sample as much granular as possible the mX and mH mass spectrum, for each crab job producing jobEvents events, N-sub gen-sim jobs are run by changing randomly the mass values. Each sub job contains `nEvents`.
   * `nThreads`: number of threads used in the production
   * `outputName`: name of the output file
   * `resonanceMassMin`: minimum mass of the Graviton (X)
   * `resonanceMassMax`: maximum mass of the Graviton (X)
   * `resonanceMassStepSize`: step size of mX in order to sample the graviton mass
   * `higgsMassMin`: minimum mass of the Higgs boson (H)
   * `higgsMassMax`: maximum mass of the Higgs boson (H)
   * `higgsMassStepSize`: step size of mH in order to sample the Higgs mass
   * `generationStep`: identifier of the sub-job in the current crab job. It is used to set the luminosity block.
* `digi_raw_step.py`: file used for the DIGI-RAW production (generic, it does not depend of the type of process produced before) with the following parameters:
   * `nEvents`:	events that are processed by this cmsRun execution
   * `inputFiles`: name of the input locals file produced by the `GEN-SIM` step ... the prefix `file:` needs to be added
   * `outputName`: name of the output file
   * `pileupName`: file containing a list of the pileup events used for the Mixing/Pre-mixing
   * `maxEvents`: number of max-events to be processed

Let's consider a second specific example for QCD $`p_{T}`$ binned production:
* `gen_sim_step_qcd.py`: file used for the GEN-SIM production of such events with the following paramaters
   * `jobNum`: identifier of the job number in the crab production, used also to set the run number.
   * `nEvents`: number of events that are produced overall in the job from crab-config
   * `nThreads`: number of threads used in the production
   * `outputName`: name of the output file
   * `pTHatMin`: minimum pTHat
   * `pTHatMax`: maximum pTHat

In order to submit crab jobs you can use the script called `python/createCrabJob.py` that takes the following input parameters:
* `--sample-config`: python file containing the list of samples to be processed along with crab job splitting and process options information to run the jobs.
* `--sample-type`: string that identifies the class/type of samples/dataset to be considered in the crab-job creation (blocks in the sample training files).
* `--crab-config`: generic crab config describing output directory, crab job options, etc. that needs to be used in submission
* `--crab-script`: indicate the bash script that needs to be executed by crab
* `--crab-job-dir`: directory in which crab jobs will be created
* `--command`: crab command to be executed
* `--options`: specific options of the crab command that needs to be run

Example on how to submit crab jobs is reported below:
```sh
python3 createCrabJob.py --sample-config samples_training_ak8_gen.py  --sample-type XtoHH --crab-config ../crab/crabConfig_training_ak8_gen_xtohh.py --crab-job-dir ../crab/crab_training_ak8 --crab-script ../crab/scriptExe_training_ak8_gen_xtohh.sh --command submit 
```


## Crab processing of MiniAOD and Ntuples from RAW DATA


