#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <thread>
#include <atomic>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "TFile.h"
#include "TChain.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TString.h"
#include "TTreeReader.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "ROOT/TSeq.hxx"
#include "Compression.h"

/// Simple object to store resonance and daughter GEN kinematics
class Resonance {
public:
  Resonance(){};
  ~Resonance(){};
  TLorentzVector p4;
  std::vector<int> daugid;
  std::vector<TLorentzVector> daugp4;
};

/// thresholds for matching
float dRCone = 0.8;
float dRMatching = 0.8;
float dRMatchingDaughterJet = 0.8;
float dRMatchingDaughters   = 0.8;
float ptGenLeptonMin  = 8;
float ptGenTauVisibleMin = 15;

/// atomic counters
std::atomic_uint nJetsTotal = 0;
std::atomic_uint nJetsRejectedEvents = 0;
std::atomic_uint nJetsRejectedBaseCuts = 0;
std::atomic_uint nJetsRejectedSignalResCuts = 0;
std::atomic_uint nJetsRejectedJetId = 0;
std::atomic_uint nJetsRejectedLeptonContent = 0;
std::atomic_uint nJetsRejectedMassCut = 0;
std::atomic_uint nJetsTraining = 0;

/// sample type definition
enum class sample_type {undefined=-1,qcd=0,qcdb=1,XtoHH=2,Hjet=3,Zjet=4,ggHH=5,bulkG=6};

sample_type convertSampleType (const std::string & sample_name){
  if(sample_name == "sample_type::undefined") return sample_type::undefined;
  else if(sample_name == "sample_type::qcd") return sample_type::qcd;
  else if(sample_name == "sample_type::qcdb") return sample_type::qcdb;
  else if(sample_name == "sample_type::XtoHH") return sample_type::XtoHH;
  else if(sample_name == "sample_type::Hjet") return sample_type::Hjet;
  else if(sample_name == "sample_type::Zjet") return sample_type::Zjet;
  else if(sample_name == "sample_type::ggHH") return sample_type::ggHH;
  else if(sample_name == "sample_type::bulkG") return sample_type::bulkG;
  else return sample_type::undefined;
}


/// Global parameters that are parsed from command line
std::string inputFileList;
std::string inputFileDIR;
std::string outputDIR;
std::string outputFileName;
unsigned int nThreads;
unsigned int maxNumberOfFilesToBeProcessed;
bool mergeThreadOutputFiles;
float jetPtMin;
float jetEtaMax;
float jetEtaMin;
float jetMassTruthMin;
float pfCandPtMin;
sample_type sample;
std::string sample_name ;
bool saveOnlyGenMatchedJets;
bool saveOnlyResonanceMatchedJets;
bool saveLeptonOppositeFlavor;
bool compressOutputFile;

/// creation of output tree
void ntupleCreation (const int & workerID, 
		     const std::vector<std::string> & fileList, 
		     const std::vector<std::shared_ptr<TTree> > & trees_out, 
		     const int & nevents, 
		     const std::atomic_int & nthreads){

  std::unique_ptr<TChain> tree_in  (new TChain("dnntree/tree","dnntree/tree"));
  for(size_t ifile = 0; ifile < fileList.size(); ifile++)
    tree_in->Add(fileList.at(ifile).c_str());
  auto tree_out = trees_out.at(workerID);

  TTreeReader reader;
  reader.SetTree(tree_in.get());
  auto beginEntry = (Long64_t) nevents*workerID/nthreads;
  auto endEntry   = (Long64_t) nevents*std::min(nthreads.load(std::memory_order_relaxed),workerID+1)/nthreads-1;
  reader.SetEntriesRange(beginEntry,endEntry);
  std::cout<<"ntupleCreation --> thread "<<workerID<<" starting from entry "<<beginEntry<<" to entry "<<endEntry<<std::endl;

  TTreeReaderValue<unsigned int> run   (reader,"run");
  TTreeReaderValue<unsigned int> lumi  (reader,"lumi");
  TTreeReaderValue<unsigned int> event (reader,"event");
  TTreeReaderValue<unsigned int> putrue (reader,"putrue");
  TTreeReaderValue<float>        wgt   (reader,"wgt");
  TTreeReaderValue<float>        rho (reader,"rho");
  TTreeReaderValue<unsigned int> npv (reader,"npv");
  TTreeReaderValue<unsigned int> nsv (reader,"nsv");
  TTreeReaderValue<float>        met (reader,"met");
  
  TTreeReaderValue<std::vector<float> >  gen_particle_pt  (reader,"gen_particle_pt");
  TTreeReaderValue<std::vector<float> >  gen_particle_eta  (reader,"gen_particle_eta");
  TTreeReaderValue<std::vector<float> >  gen_particle_phi  (reader,"gen_particle_phi");
  TTreeReaderValue<std::vector<float> >  gen_particle_mass  (reader,"gen_particle_mass");
  TTreeReaderValue<std::vector<int> >    gen_particle_id  (reader,"gen_particle_id");
  TTreeReaderValue<std::vector<unsigned int> > gen_particle_daughters_igen  (reader,"gen_particle_daughters_igen");
  TTreeReaderValue<std::vector<unsigned int> > gen_particle_daughters_status  (reader,"gen_particle_daughters_status");
  TTreeReaderValue<std::vector<int> >    gen_particle_daughters_charge  (reader,"gen_particle_daughters_charge");
  TTreeReaderValue<std::vector<int> >    gen_particle_daughters_id  (reader,"gen_particle_daughters_id");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_pt  (reader,"gen_particle_daughters_pt");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_eta  (reader,"gen_particle_daughters_eta");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_phi  (reader,"gen_particle_daughters_phi");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_mass  (reader,"gen_particle_daughters_mass");

  TTreeReaderValue<std::vector<float> > jet_reco_pt (reader,"jet_pt");
  TTreeReaderValue<std::vector<float> > jet_reco_eta (reader,"jet_eta");
  TTreeReaderValue<std::vector<float> > jet_reco_phi (reader,"jet_phi");
  TTreeReaderValue<std::vector<float> > jet_reco_mass (reader,"jet_mass");
  TTreeReaderValue<std::vector<float> > jet_reco_pt_raw (reader,"jet_pt_raw");
  TTreeReaderValue<std::vector<float> > jet_reco_mass_raw (reader,"jet_mass_raw");
  
  TTreeReaderValue<std::vector<float> > jet_hlt_pt (reader,"jet_hlt_pt");
  TTreeReaderValue<std::vector<float> > jet_hlt_eta (reader,"jet_hlt_eta");
  TTreeReaderValue<std::vector<float> > jet_hlt_phi (reader,"jet_hlt_phi");
  TTreeReaderValue<std::vector<float> > jet_hlt_mass (reader,"jet_hlt_mass");
  TTreeReaderValue<std::vector<float> > jet_hlt_pt_raw (reader,"jet_hlt_pt_raw");
  TTreeReaderValue<std::vector<float> > jet_hlt_mass_raw (reader,"jet_hlt_mass_raw");

  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probXbb (reader,"jet_pnet_probXbb");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probXcc (reader,"jet_pnet_probXcc");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probXqq (reader,"jet_pnet_probXqq");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probQCDbb (reader,"jet_pnet_probQCDbb");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probQCDb (reader,"jet_pnet_probQCDb");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probQCDcc (reader,"jet_pnet_probQCDcc");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probQCDc (reader,"jet_pnet_probQCDc");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_probQCDothers (reader,"jet_pnet_probQCDothers");
  TTreeReaderValue<std::vector<float> > jet_reco_pnet_regmass (reader,"jet_pnet_regression_mass");

  TTreeReaderValue<std::vector<float> > jet_hlt_pnethlt_probXtt (reader,"jet_hlt_pnethlt_probXtt");
  TTreeReaderValue<std::vector<float> > jet_hlt_pnethlt_probXbb (reader,"jet_hlt_pnethlt_probXbb");
  TTreeReaderValue<std::vector<float> > jet_hlt_pnethlt_probXcc (reader,"jet_hlt_pnethlt_probXcc");
  TTreeReaderValue<std::vector<float> > jet_hlt_pnethlt_probXqq (reader,"jet_hlt_pnethlt_probXqq");
  TTreeReaderValue<std::vector<float> > jet_hlt_pnethlt_probXgg (reader,"jet_hlt_pnethlt_probXgg");
  TTreeReaderValue<std::vector<float> > jet_hlt_pnethlt_probQCD (reader,"jet_hlt_pnethlt_probQCD");

  TTreeReaderValue<std::vector<unsigned int> > jet_reco_id (reader,"jet_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_reco_hflav (reader,"jet_hflav");
  TTreeReaderValue<std::vector<int> > jet_reco_pflav (reader,"jet_pflav");
  TTreeReaderValue<std::vector<unsigned int> > jet_reco_nbhad (reader,"jet_nbhad");
  TTreeReaderValue<std::vector<unsigned int> > jet_reco_nchad (reader,"jet_nchad");    
  TTreeReaderValue<std::vector<unsigned int> > jet_reco_ncand (reader,"jet_ncand");
  TTreeReaderValue<std::vector<float> > jet_reco_chf (reader,"jet_chf");
  TTreeReaderValue<std::vector<float> > jet_reco_nhf (reader,"jet_nhf");
  TTreeReaderValue<std::vector<float> > jet_reco_elf (reader,"jet_elf");
  TTreeReaderValue<std::vector<float> > jet_reco_phf (reader,"jet_phf");
  TTreeReaderValue<std::vector<float> > jet_reco_muf (reader,"jet_muf");
  
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_id (reader,"jet_hlt_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_hflav (reader,"jet_hlt_hflav");
  TTreeReaderValue<std::vector<int> > jet_hlt_pflav (reader,"jet_hlt_pflav");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_nbhad (reader,"jet_hlt_nbhad");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_nchad (reader,"jet_hlt_nchad");    
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_ncand (reader,"jet_hlt_ncand");
  TTreeReaderValue<std::vector<float> > jet_hlt_chf (reader,"jet_hlt_chf");
  TTreeReaderValue<std::vector<float> > jet_hlt_nhf (reader,"jet_hlt_nhf");
  TTreeReaderValue<std::vector<float> > jet_hlt_elf (reader,"jet_hlt_elf");
  TTreeReaderValue<std::vector<float> > jet_hlt_phf (reader,"jet_hlt_phf");
  TTreeReaderValue<std::vector<float> > jet_hlt_muf (reader,"jet_hlt_muf");
 
  TTreeReaderValue<std::vector<float> > jet_hlt_genmatch_pt (reader,"jet_hlt_genmatch_pt");
  TTreeReaderValue<std::vector<float> > jet_hlt_genmatch_eta (reader,"jet_hlt_genmatch_eta");
  TTreeReaderValue<std::vector<float> > jet_hlt_genmatch_phi (reader,"jet_hlt_genmatch_phi");
  TTreeReaderValue<std::vector<float> > jet_hlt_genmatch_mass (reader,"jet_hlt_genmatch_mass");

  TTreeReaderValue<std::vector<float> > jet_reco_softdrop_pt (reader,"jet_softdrop_pt");
  TTreeReaderValue<std::vector<float> > jet_reco_softdrop_eta (reader,"jet_softdrop_eta");
  TTreeReaderValue<std::vector<float> > jet_reco_softdrop_phi (reader,"jet_softdrop_phi");
  TTreeReaderValue<std::vector<float> > jet_reco_softdrop_mass (reader,"jet_softdrop_mass");
  
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_pt (reader,"jet_hlt_softdrop_pt");
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_eta (reader,"jet_hlt_softdrop_eta");
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_phi (reader,"jet_hlt_softdrop_phi");
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_mass (reader,"jet_hlt_softdrop_mass");
  
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_genmatch_pt (reader,"jet_hlt_softdrop_genmatch_pt");
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_genmatch_eta (reader,"jet_hlt_softdrop_genmatch_eta");
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_genmatch_phi (reader,"jet_hlt_softdrop_genmatch_phi");
  TTreeReaderValue<std::vector<float> > jet_hlt_softdrop_genmatch_mass (reader,"jet_hlt_softdrop_genmatch_mass");

  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_pt (reader,"jet_hlt_pfcandidate_pt");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_eta (reader,"jet_hlt_pfcandidate_eta");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_phi (reader,"jet_hlt_pfcandidate_phi");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_mass (reader,"jet_hlt_pfcandidate_mass");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_energy (reader,"jet_hlt_pfcandidate_energy");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_calofraction (reader,"jet_hlt_pfcandidate_calofraction");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_hcalfraction (reader,"jet_hlt_pfcandidate_hcalfraction");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_dxy (reader,"jet_hlt_pfcandidate_dxy");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_dz (reader,"jet_hlt_pfcandidate_dz");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_dxysig (reader,"jet_hlt_pfcandidate_dxysig");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_dzsig (reader,"jet_hlt_pfcandidate_dzsig");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_pperp_ratio (reader,"jet_hlt_pfcandidate_candjet_pperp_ratio");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_ppara_ratio (reader,"jet_hlt_pfcandidate_candjet_ppara_ratio");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_deta (reader,"jet_hlt_pfcandidate_candjet_deta");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_dphi (reader,"jet_hlt_pfcandidate_candjet_dphi");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_etarel (reader,"jet_hlt_pfcandidate_candjet_etarel");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_npixhits (reader,"jet_hlt_pfcandidate_npixhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_nstriphits (reader,"jet_hlt_pfcandidate_nstriphits");
  TTreeReaderValue<std::vector<int > > jet_hlt_pfcand_nlostinnerhits (reader,"jet_hlt_pfcandidate_nlostinnerhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_frompv (reader,"jet_hlt_pfcandidate_frompv");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_id (reader,"jet_hlt_pfcandidate_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_ijet (reader,"jet_hlt_pfcandidate_ijet");
  TTreeReaderValue<std::vector<int> > jet_hlt_pfcand_charge (reader,"jet_hlt_pfcandidate_charge");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_track_qual (reader,"jet_hlt_pfcandidate_track_qual");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_highpurity (reader,"jet_hlt_pfcandidate_highpurity");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_pfcand_track_chi2 (reader,"jet_hlt_pfcandidate_track_chi2");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_trackjet_d3d (reader,"jet_hlt_pfcandidate_trackjet_d3d");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_trackjet_d3dsig (reader,"jet_hlt_pfcandidate_trackjet_d3dsig");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_trackjet_dist (reader,"jet_hlt_pfcandidate_trackjet_dist");
  TTreeReaderValue<std::vector<float> > jet_hlt_pfcand_trackjet_decayL (reader,"jet_hlt_pfcandidate_trackjet_decayL");

  TTreeReaderValue<std::vector<float> > jet_hlt_sv_pt (reader,"jet_hlt_sv_pt");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_eta (reader,"jet_hlt_sv_eta");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_phi (reader,"jet_hlt_sv_phi");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_mass (reader,"jet_hlt_sv_mass");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_energy (reader,"jet_hlt_sv_energy");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_chi2 (reader,"jet_hlt_sv_chi2");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_dxy (reader,"jet_hlt_sv_dxy");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_dxysig (reader,"jet_hlt_sv_dxysig");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_d3d (reader,"jet_hlt_sv_d3d");
  TTreeReaderValue<std::vector<float> > jet_hlt_sv_d3dsig (reader,"jet_hlt_sv_d3dsig");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_sv_ntrack (reader,"jet_hlt_sv_ntrack");
  TTreeReaderValue<std::vector<unsigned int> > jet_hlt_sv_ijet (reader,"jet_hlt_sv_ijet");
  
      // output ntupla
  unsigned int run_b, lumi_b, event_b, npv_b, nsv_b, npu_b;
  float wgt_b,rho_b,met_b;
  int sample_b;
  unsigned int ijet_b;

  tree_out->Branch("run",&run_b,"run/i"); 
  tree_out->Branch("lumi",&lumi_b,"lumi/i"); 
  tree_out->Branch("event",&event_b,"event/i"); 
  tree_out->Branch("npv",&npv_b,"npv/i"); 
  tree_out->Branch("nsv",&nsv_b,"nsv/i"); 
  tree_out->Branch("npu",&npu_b,"npu/i"); 
  tree_out->Branch("wgt",&wgt_b,"wgt/F"); 
  tree_out->Branch("rho",&rho_b,"rho/F"); 
  tree_out->Branch("met",&met_b,"met/F"); 
  tree_out->Branch("sample",&sample_b,"sample/I");
  tree_out->Branch("ijet",&ijet_b,"ijet/i");

  float jet_recomatch_pt_b, jet_recomatch_eta_b,jet_recomatch_phi_b,jet_recomatch_mass_b,jet_recomatch_pt_raw_b,jet_recomatch_mass_raw_b;
  float jet_hlt_pt_b, jet_hlt_eta_b,jet_hlt_phi_b,jet_hlt_mass_b,jet_hlt_pt_raw_b,jet_hlt_mass_raw_b;

  tree_out->Branch("jet_recomatch_pt",&jet_recomatch_pt_b,"jet_recomatch_pt/F");
  tree_out->Branch("jet_recomatch_eta",&jet_recomatch_eta_b,"jet_recomatch_eta/F");
  tree_out->Branch("jet_recomatch_phi",&jet_recomatch_phi_b,"jet_recomatch_phi/F");
  tree_out->Branch("jet_recomatch_mass",&jet_recomatch_mass_b,"jet_recomatch_mass/F");
  tree_out->Branch("jet_recomatch_pt_raw",&jet_recomatch_pt_raw_b,"jet_recomatch_pt_raw/F");
  tree_out->Branch("jet_recomatch_mass_raw",&jet_recomatch_mass_raw_b,"jet_recomatch_mass_raw/F");
  
  tree_out->Branch("jet_hlt_pt",&jet_hlt_pt_b,"jet_hlt_pt/F");
  tree_out->Branch("jet_hlt_eta",&jet_hlt_eta_b,"jet_hlt_eta/F");
  tree_out->Branch("jet_hlt_phi",&jet_hlt_phi_b,"jet_hlt_phi/F");
  tree_out->Branch("jet_hlt_mass",&jet_hlt_mass_b,"jet_hlt_mass/F");
  tree_out->Branch("jet_hlt_pt_raw",&jet_hlt_pt_raw_b,"jet_hlt_pt_raw/F");
  tree_out->Branch("jet_hlt_mass_raw",&jet_hlt_mass_raw_b,"jet_hlt_mass_raw/F");

  float jet_recomatch_pnet_probXbb_b,jet_recomatch_pnet_probXcc_b,jet_recomatch_pnet_probXqq_b, jet_recomatch_pnet_probQCDbb_b, jet_recomatch_pnet_probQCDb_b, jet_recomatch_pnet_probQCDcc_b;
  float jet_recomatch_pnet_probQCDc_b, jet_recomatch_pnet_probQCDothers_b, jet_recomatch_pnet_regmass_b;  

  tree_out->Branch("jet_recomatch_pnet_probXbb",&jet_recomatch_pnet_probXbb_b,"jet_recomatch_pnet_probXbb/F");
  tree_out->Branch("jet_recomatch_pnet_probXcc",&jet_recomatch_pnet_probXcc_b,"jet_recomatch_pnet_probXcc/F");
  tree_out->Branch("jet_recomatch_pnet_probXqq",&jet_recomatch_pnet_probXqq_b,"jet_recomatch_pnet_probXqq/F");
  tree_out->Branch("jet_recomatch_pnet_probQCDbb",&jet_recomatch_pnet_probQCDbb_b,"jet_recomatch_pnet_probQCDbb/F");
  tree_out->Branch("jet_recomatch_pnet_probQCDb",&jet_recomatch_pnet_probQCDb_b,"jet_recomatch_pnet_probQCDb/F");
  tree_out->Branch("jet_recomatch_pnet_probQCDcc",&jet_recomatch_pnet_probQCDcc_b,"jet_recomatch_pnet_probQCDcc/F");
  tree_out->Branch("jet_recomatch_pnet_probQCDc",&jet_recomatch_pnet_probQCDc_b,"jet_recomatch_pnet_probQCDc/F");
  tree_out->Branch("jet_recomatch_pnet_probQCDothers",&jet_recomatch_pnet_probQCDothers_b,"jet_recomatch_pnet_probQCDothers/F");
  tree_out->Branch("jet_recomatch_pnet_regmass",&jet_recomatch_pnet_regmass_b,"jet_recomatch_pnet_regmass/F");

  float jet_hlt_pnethlt_probXbb_b,jet_hlt_pnethlt_probXcc_b,jet_hlt_pnethlt_probXqq_b,jet_hlt_pnethlt_probXgg_b,jet_hlt_pnethlt_probXtt_b,jet_hlt_pnethlt_probQCD_b;
  tree_out->Branch("jet_hlt_pnethlt_probXbb",&jet_hlt_pnethlt_probXbb_b,"jet_hlt_pnethlt_probXbb/F");
  tree_out->Branch("jet_hlt_pnethlt_probXcc",&jet_hlt_pnethlt_probXcc_b,"jet_hlt_pnethlt_probXcc/F");
  tree_out->Branch("jet_hlt_pnethlt_probXqq",&jet_hlt_pnethlt_probXqq_b,"jet_hlt_pnethlt_probXqq/F");
  tree_out->Branch("jet_hlt_pnethlt_probXgg",&jet_hlt_pnethlt_probXgg_b,"jet_hlt_pnethlt_probXgg/F");
  tree_out->Branch("jet_hlt_pnethlt_probXtt",&jet_hlt_pnethlt_probXtt_b,"jet_hlt_pnethlt_probXtt/F");
  tree_out->Branch("jet_hlt_pnethlt_probQCD",&jet_hlt_pnethlt_probQCD_b,"jet_hlt_pnethlt_probQCD/F");

  unsigned int jet_hlt_ncand_b, jet_hlt_nbhad_b, jet_hlt_nchad_b, jet_hlt_hflav_b;
  float jet_hlt_chf_b, jet_hlt_nhf_b, jet_hlt_phf_b, jet_hlt_elf_b, jet_hlt_muf_b;
  int jet_hlt_pflav_b, jet_hlt_tauflav_b, jet_hlt_muflav_b, jet_hlt_elflav_b, jet_hlt_taudecaymode_b, jet_hlt_lepflav_b;
  int jet_hlt_flav_2prong_partonjet_match_b, jet_hlt_flav_2prong_parton_match_b;

  tree_out->Branch("jet_hlt_chf",&jet_hlt_chf_b,"jet_hlt_chf/F");
  tree_out->Branch("jet_hlt_nhf",&jet_hlt_nhf_b,"jet_hlt_nhf/F");
  tree_out->Branch("jet_hlt_phf",&jet_hlt_phf_b,"jet_hlt_phf/F");
  tree_out->Branch("jet_hlt_elf",&jet_hlt_elf_b,"jet_hlt_elf/F");
  tree_out->Branch("jet_hlt_muf",&jet_hlt_muf_b,"jet_hlt_muf/F");
  tree_out->Branch("jet_hlt_ncand",&jet_hlt_ncand_b,"jet_hlt_ncand/i");
  tree_out->Branch("jet_hlt_nbhad",&jet_hlt_nbhad_b,"jet_hlt_nbhad/i");
  tree_out->Branch("jet_hlt_nchad",&jet_hlt_nchad_b,"jet_hlt_nchad/i");
  tree_out->Branch("jet_hlt_hflav",&jet_hlt_hflav_b,"jet_hlt_hflav/i");
  tree_out->Branch("jet_hlt_pflav",&jet_hlt_pflav_b,"jet_hlt_pflav/I");
  tree_out->Branch("jet_hlt_muflav",&jet_hlt_muflav_b,"jet_hlt_muflav/I");
  tree_out->Branch("jet_hlt_elflav",&jet_hlt_elflav_b,"jet_hlt_elflav/I");
  tree_out->Branch("jet_hlt_lepflav",&jet_hlt_lepflav_b,"jet_hlt_lepflav/I");
  tree_out->Branch("jet_hlt_tauflav",&jet_hlt_tauflav_b,"jet_hlt_tauflav/I");
  tree_out->Branch("jet_hlt_taudecaymode",&jet_hlt_taudecaymode_b,"jet_hlt_taudecaymode/I");
  tree_out->Branch("jet_hlt_flav_2prong_partonjet_match",&jet_hlt_flav_2prong_partonjet_match_b,"jet_hlt_flav_2prong_partonjet_match/I");
  tree_out->Branch("jet_hlt_flav_2prong_parton_match",&jet_hlt_flav_2prong_parton_match_b,"jet_hlt_flav_2prong_parton_match/I");
  
  unsigned int jet_recomatch_ncand_b, jet_recomatch_nbhad_b, jet_recomatch_nchad_b;
  float jet_recomatch_chf_b, jet_recomatch_nhf_b, jet_recomatch_phf_b, jet_recomatch_elf_b, jet_recomatch_muf_b;
  
  tree_out->Branch("jet_recomatch_chf",&jet_recomatch_chf_b,"jet_recomatch_chf/F");
  tree_out->Branch("jet_recomatch_nhf",&jet_recomatch_nhf_b,"jet_recomatch_nhf/F");
  tree_out->Branch("jet_recomatch_phf",&jet_recomatch_phf_b,"jet_recomatch_phf/F");
  tree_out->Branch("jet_recomatch_elf",&jet_recomatch_elf_b,"jet_recomatch_elf/F");
  tree_out->Branch("jet_recomatch_muf",&jet_recomatch_muf_b,"jet_recomatch_muf/F");
  tree_out->Branch("jet_recomatch_ncand",&jet_recomatch_ncand_b,"jet_recomatch_ncand/i");
  tree_out->Branch("jet_recomatch_nbhad",&jet_recomatch_nbhad_b,"jet_recomatch_nbhad/i");
  tree_out->Branch("jet_recomatch_nchad",&jet_recomatch_nchad_b,"jet_recomatch_nchad/i");

  float jet_hlt_genmatch_pt_b,jet_hlt_genmatch_eta_b,jet_hlt_genmatch_phi_b,jet_hlt_genmatch_mass_b;

  tree_out->Branch("jet_hlt_genmatch_pt",&jet_hlt_genmatch_pt_b,"jet_hlt_genmatch_pt/F");
  tree_out->Branch("jet_hlt_genmatch_eta",&jet_hlt_genmatch_eta_b,"jet_hlt_genmatch_eta/F");
  tree_out->Branch("jet_hlt_genmatch_phi",&jet_hlt_genmatch_phi_b,"jet_hlt_genmatch_phi/F");
  tree_out->Branch("jet_hlt_genmatch_mass",&jet_hlt_genmatch_mass_b,"jet_hlt_genmatch_mass/F");

  float jet_hlt_softdrop_pt_b, jet_hlt_softdrop_eta_b, jet_hlt_softdrop_phi_b, jet_hlt_softdrop_mass_b;

  tree_out->Branch("jet_hlt_softdrop_pt",&jet_hlt_softdrop_pt_b,"jet_hlt_softdrop_pt/F");
  tree_out->Branch("jet_hlt_softdrop_eta",&jet_hlt_softdrop_eta_b,"jet_hlt_softdrop_eta/F");
  tree_out->Branch("jet_hlt_softdrop_phi",&jet_hlt_softdrop_phi_b,"jet_hlt_softdrop_phi/F");
  tree_out->Branch("jet_hlt_softdrop_mass",&jet_hlt_softdrop_mass_b,"jet_hlt_softdrop_mass/F");
  
  float jet_recomatch_softdrop_pt_b, jet_recomatch_softdrop_eta_b, jet_recomatch_softdrop_phi_b, jet_recomatch_softdrop_mass_b;

  tree_out->Branch("jet_recomatch_softdrop_pt",&jet_recomatch_softdrop_pt_b,"jet_recomatch_softdrop_pt/F");
  tree_out->Branch("jet_recomatch_softdrop_eta",&jet_recomatch_softdrop_eta_b,"jet_recomatch_softdrop_eta/F");
  tree_out->Branch("jet_recomatch_softdrop_phi",&jet_recomatch_softdrop_phi_b,"jet_recomatch_softdrop_phi/F");
  tree_out->Branch("jet_recomatch_softdrop_mass",&jet_recomatch_softdrop_mass_b,"jet_recomatch_softdrop_mass/F");
  
  float jet_hlt_softdrop_genmatch_pt_b, jet_hlt_softdrop_genmatch_eta_b, jet_hlt_softdrop_genmatch_phi_b, jet_hlt_softdrop_genmatch_mass_b;

  tree_out->Branch("jet_hlt_softdrop_genmatch_pt",&jet_hlt_softdrop_genmatch_pt_b,"jet_hlt_softdrop_genmatch_pt/F");
  tree_out->Branch("jet_hlt_softdrop_genmatch_eta",&jet_hlt_softdrop_genmatch_eta_b,"jet_hlt_softdrop_genmatch_eta/F");
  tree_out->Branch("jet_hlt_softdrop_genmatch_phi",&jet_hlt_softdrop_genmatch_phi_b,"jet_hlt_softdrop_genmatch_phi/F");
  tree_out->Branch("jet_hlt_softdrop_genmatch_mass",&jet_hlt_softdrop_genmatch_mass_b,"jet_hlt_softdrop_genmatch_mass/F");

  float jet_hlt_pt_truth_b,jet_hlt_eta_truth_b,jet_hlt_phi_truth_b,jet_hlt_mass_truth_b;

  tree_out->Branch("jet_hlt_pt_truth",&jet_hlt_pt_truth_b,"jet_hlt_pt_truth/F");
  tree_out->Branch("jet_hlt_eta_truth",&jet_hlt_eta_truth_b,"jet_hlt_eta_truth/F");
  tree_out->Branch("jet_hlt_phi_truth",&jet_hlt_phi_truth_b,"jet_hlt_phi_truth/F");
  tree_out->Branch("jet_hlt_mass_truth",&jet_hlt_mass_truth_b,"jet_hlt_mass_truth/F");

  std::vector<float> jet_hlt_pfcand_pt_b, jet_hlt_pfcand_eta_b, jet_hlt_pfcand_phi_b, jet_hlt_pfcand_mass_b, jet_hlt_pfcand_energy_b, jet_hlt_pfcand_pt_log_b, jet_hlt_pfcand_energy_log_b, jet_hlt_pfcand_calofraction_b;
  std::vector<float> jet_hlt_pfcand_hcalfraction_b, jet_hlt_pfcand_dxy_b, jet_hlt_pfcand_dz_b, jet_hlt_pfcand_dxysig_b, jet_hlt_pfcand_dzsig_b, jet_hlt_pfcand_pperp_ratio_b, jet_hlt_pfcand_ppara_ratio_b;
  std::vector<float> jet_hlt_pfcand_deta_b, jet_hlt_pfcand_dphi_b, jet_hlt_pfcand_etarel_b;

  tree_out->Branch("jet_hlt_pfcand_pt","std::vector<float>",&jet_hlt_pfcand_pt_b);
  tree_out->Branch("jet_hlt_pfcand_eta","std::vector<float>",&jet_hlt_pfcand_eta_b);
  tree_out->Branch("jet_hlt_pfcand_phi","std::vector<float>",&jet_hlt_pfcand_phi_b);
  tree_out->Branch("jet_hlt_pfcand_mass","std::vector<float>",&jet_hlt_pfcand_mass_b);
  tree_out->Branch("jet_hlt_pfcand_energy","std::vector<float>",&jet_hlt_pfcand_energy_b);
  tree_out->Branch("jet_hlt_pfcand_pt_log","std::vector<float>",&jet_hlt_pfcand_pt_log_b);
  tree_out->Branch("jet_hlt_pfcand_energy_log","std::vector<float>",&jet_hlt_pfcand_energy_log_b);
  tree_out->Branch("jet_hlt_pfcand_calofraction","std::vector<float>",&jet_hlt_pfcand_calofraction_b);
  tree_out->Branch("jet_hlt_pfcand_hcalfraction","std::vector<float>",&jet_hlt_pfcand_hcalfraction_b);
  tree_out->Branch("jet_hlt_pfcand_dxy","std::vector<float>",&jet_hlt_pfcand_dxy_b);
  tree_out->Branch("jet_hlt_pfcand_dxysig","std::vector<float>",&jet_hlt_pfcand_dxysig_b);
  tree_out->Branch("jet_hlt_pfcand_dz","std::vector<float>",&jet_hlt_pfcand_dz_b);
  tree_out->Branch("jet_hlt_pfcand_dzsig","std::vector<float>",&jet_hlt_pfcand_dzsig_b);
  tree_out->Branch("jet_hlt_pfcand_pperp_ratio","std::vector<float>",&jet_hlt_pfcand_pperp_ratio_b);
  tree_out->Branch("jet_hlt_pfcand_ppara_ratio","std::vector<float>",&jet_hlt_pfcand_ppara_ratio_b);
  tree_out->Branch("jet_hlt_pfcand_deta","std::vector<float>",&jet_hlt_pfcand_deta_b);
  tree_out->Branch("jet_hlt_pfcand_dphi","std::vector<float>",&jet_hlt_pfcand_dphi_b);
  tree_out->Branch("jet_hlt_pfcand_etarel","std::vector<float>",&jet_hlt_pfcand_etarel_b);
      
  std::vector<float> jet_hlt_pfcand_npixhits_b, jet_hlt_pfcand_nstriphits_b;
  std::vector<float> jet_hlt_pfcand_nlostinnerhits_b, jet_hlt_pfcand_charge_b;
  std::vector<float> jet_hlt_pfcand_frompv_b, jet_hlt_pfcand_id_b, jet_hlt_pfcand_highpurity_b, jet_hlt_pfcand_track_qual_b, jet_hlt_pfcand_track_chi2_b;
  std::vector<float> jet_hlt_pfcand_muon_id_b, jet_hlt_pfcand_muon_chi2_b, jet_hlt_pfcand_muon_segcomp_b, jet_hlt_pfcand_muon_isglobal_b, jet_hlt_pfcand_muon_nvalidhit_b, jet_hlt_pfcand_muon_nstation_b;
  std::vector<float> jet_hlt_pfcand_electron_eOverP_b, jet_hlt_pfcand_electron_detaIn_b, jet_hlt_pfcand_electron_dphiIn_b, jet_hlt_pfcand_electron_r9_b, jet_hlt_pfcand_electron_sigIetaIeta_b;
  std::vector<float> jet_hlt_pfcand_electron_convProb_b, jet_hlt_pfcand_electron_sigIphiIphi_b;
  std::vector<float> jet_hlt_pfcand_photon_sigIetaIeta_b, jet_hlt_pfcand_photon_r9_b, jet_hlt_pfcand_photon_eVeto_b;
  std::vector<float> jet_hlt_pfcand_trackjet_d3d_b,  jet_hlt_pfcand_trackjet_d3dsig_b;
  std::vector<float> jet_hlt_pfcand_trackjet_dist_b, jet_hlt_pfcand_trackjet_decayL_b;      

  tree_out->Branch("jet_hlt_pfcand_npixhits","std::vector<float>",&jet_hlt_pfcand_npixhits_b);
  tree_out->Branch("jet_hlt_pfcand_nstriphits","std::vector<float>",&jet_hlt_pfcand_nstriphits_b);
  tree_out->Branch("jet_hlt_pfcand_frompv","std::vector<float>",&jet_hlt_pfcand_frompv_b);
  tree_out->Branch("jet_hlt_pfcand_id","std::vector<float>",&jet_hlt_pfcand_id_b);
  tree_out->Branch("jet_hlt_pfcand_track_qual","std::vector<float>",&jet_hlt_pfcand_track_qual_b);
  tree_out->Branch("jet_hlt_pfcand_track_chi2","std::vector<float>",&jet_hlt_pfcand_track_chi2_b);    
  tree_out->Branch("jet_hlt_pfcand_highpurity","std::vector<float>",&jet_hlt_pfcand_highpurity_b);
  tree_out->Branch("jet_hlt_pfcand_nlostinnerhits","std::vector<float>",&jet_hlt_pfcand_nlostinnerhits_b);
  tree_out->Branch("jet_hlt_pfcand_charge","std::vector<float>",&jet_hlt_pfcand_charge_b);
  tree_out->Branch("jet_hlt_pfcand_trackjet_d3d","std::vector<float>",&jet_hlt_pfcand_trackjet_d3d_b);
  tree_out->Branch("jet_hlt_pfcand_trackjet_d3dsig","std::vector<float>",&jet_hlt_pfcand_trackjet_d3dsig_b);
  tree_out->Branch("jet_hlt_pfcand_trackjet_dist","std::vector<float>",&jet_hlt_pfcand_trackjet_dist_b);
  tree_out->Branch("jet_hlt_pfcand_trackjet_decayL","std::vector<float>",&jet_hlt_pfcand_trackjet_decayL_b);
  
  std::vector<float> jet_hlt_sv_pt_b, jet_hlt_sv_pt_log_b, jet_hlt_sv_eta_b, jet_hlt_sv_mass_b, jet_hlt_sv_energy_b, jet_hlt_sv_energy_log_b;
  std::vector<float> jet_hlt_sv_deta_b, jet_hlt_sv_dphi_b, jet_hlt_sv_chi2_b, jet_hlt_sv_dxy_b, jet_hlt_sv_dxysig_b;
  std::vector<float> jet_hlt_sv_d3d_b, jet_hlt_sv_d3dsig_b;
  std::vector<float> jet_hlt_sv_ntrack_b;  

  tree_out->Branch("jet_hlt_sv_pt","std::vector<float>",&jet_hlt_sv_pt_b);
  tree_out->Branch("jet_hlt_sv_pt_log","std::vector<float>",&jet_hlt_sv_pt_log_b);
  tree_out->Branch("jet_hlt_sv_eta","std::vector<float>",&jet_hlt_sv_eta_b);
  tree_out->Branch("jet_hlt_sv_mass","std::vector<float>",&jet_hlt_sv_mass_b);
  tree_out->Branch("jet_hlt_sv_energy","std::vector<float>",&jet_hlt_sv_energy_b);
  tree_out->Branch("jet_hlt_sv_energy_log","std::vector<float>",&jet_hlt_sv_energy_log_b);
  tree_out->Branch("jet_hlt_sv_deta","std::vector<float>",&jet_hlt_sv_deta_b);
  tree_out->Branch("jet_hlt_sv_dphi","std::vector<float>",&jet_hlt_sv_dphi_b);
  tree_out->Branch("jet_hlt_sv_chi2","std::vector<float>",&jet_hlt_sv_chi2_b);
  tree_out->Branch("jet_hlt_sv_dxy","std::vector<float>",&jet_hlt_sv_dxy_b);
  tree_out->Branch("jet_hlt_sv_dxysig","std::vector<float>",&jet_hlt_sv_dxysig_b);
  tree_out->Branch("jet_hlt_sv_d3d","std::vector<float>",&jet_hlt_sv_d3d_b);
  tree_out->Branch("jet_hlt_sv_d3dsig","std::vector<float>",&jet_hlt_sv_d3dsig_b);
  tree_out->Branch("jet_hlt_sv_ntrack","std::vector<float>",&jet_hlt_sv_ntrack_b);      

  while(reader.Next()){

    // Gen leptons from resonance decay as well as neutrinos
    std::vector<TLorentzVector> genLepFromResonance4V;
    std::vector<TLorentzVector> genMuonsFromResonance4V;
    std::vector<TLorentzVector> genElectronsFromResonance4V;

    for(size_t igen = 0; igen < gen_particle_pt->size(); igen++){
      // select resonances like Higgs, W, Z, taus
      if(abs(gen_particle_id->at(igen)) == 25 or
	 abs(gen_particle_id->at(igen)) == 23 or
	 abs(gen_particle_id->at(igen)) == 24 or
	 abs(gen_particle_id->at(igen)) == 15){	
	for(size_t idau = 0; idau < gen_particle_daughters_id->size(); idau++){
	  // select electrons or muons from the resonance / tau decay
	  if(gen_particle_daughters_igen->at(idau) == igen and
	     (abs(gen_particle_daughters_id->at(idau)) == 11 or
	      abs(gen_particle_daughters_id->at(idau)) == 13)){
	    TLorentzVector gen4V;
	    gen4V.SetPtEtaPhiM(gen_particle_daughters_pt->at(idau),gen_particle_daughters_eta->at(idau),gen_particle_daughters_phi->at(idau),gen_particle_daughters_mass->at(idau));
	    if(std::find(genLepFromResonance4V.begin(),genLepFromResonance4V.end(),gen4V) == genLepFromResonance4V.end())
	      genLepFromResonance4V.push_back(gen4V);
	    if(abs(gen_particle_daughters_id->at(idau)) == 13 and 
	       std::find(genMuonsFromResonance4V.begin(),genMuonsFromResonance4V.end(),gen4V) == genMuonsFromResonance4V.end())
	      genMuonsFromResonance4V.push_back(gen4V);
	    if(abs(gen_particle_daughters_id->at(idau)) == 11 and 
	       std::find(genElectronsFromResonance4V.begin(),genElectronsFromResonance4V.end(),gen4V) == genElectronsFromResonance4V.end())
	      genElectronsFromResonance4V.push_back(gen4V);		
	  }
	}
      }
    }

    // Gen hadronic taus	
    std::vector<TLorentzVector> tau_gen_visible;
    std::vector<TLorentzVector> tau_gen;
    std::vector<unsigned int> tau_gen_nch;
    std::vector<unsigned int> tau_gen_np0;
    std::vector<unsigned int> tau_gen_nnh;

    for(size_t igen = 0; igen < gen_particle_pt->size(); igen++){
      if(abs(gen_particle_id->at(igen)) == 15){ // hadronic or leptonic tau
	TLorentzVector tau_gen_tmp;
	unsigned int tau_gen_nch_tmp = 0;
	unsigned int tau_gen_np0_tmp = 0;
	unsigned int tau_gen_nnh_tmp = 0;
	for(size_t idau = 0; idau < gen_particle_daughters_pt->size(); idau++){
	  if(gen_particle_daughters_igen->at(idau) == igen and
	     abs(gen_particle_daughters_id->at(idau)) != 11 and // no mu
	     abs(gen_particle_daughters_id->at(idau)) != 13 and // no el
	     abs(gen_particle_daughters_id->at(idau)) != 12 and // no neutrinos
	     abs(gen_particle_daughters_id->at(idau)) != 14 and
	     abs(gen_particle_daughters_id->at(idau)) != 16){
	    TLorentzVector tmp4V; 
	    tmp4V.SetPtEtaPhiM(gen_particle_daughters_pt->at(idau),gen_particle_daughters_eta->at(idau),gen_particle_daughters_phi->at(idau),gen_particle_daughters_mass->at(idau));
	    tau_gen_tmp += tmp4V;
	    if (gen_particle_daughters_charge->at(idau) != 0 and gen_particle_daughters_status->at(idau) == 1) tau_gen_nch_tmp ++; // charged particles
	    else if(gen_particle_daughters_charge->at(idau) == 0 and gen_particle_daughters_id->at(idau) == 111) tau_gen_np0_tmp++;
	    else if(gen_particle_daughters_charge->at(idau) == 0 and gen_particle_daughters_id->at(idau) != 111) tau_gen_nnh_tmp++;
	  }
	}
	if(tau_gen_tmp.Pt() > 0){ // good hadronic tau
	  tau_gen_visible.push_back(tau_gen_tmp);
	  tau_gen_tmp.SetPtEtaPhiM(gen_particle_pt->at(igen),gen_particle_eta->at(igen),gen_particle_phi->at(igen),gen_particle_mass->at(igen));
	  tau_gen.push_back(tau_gen_tmp);
	  tau_gen_nch.push_back(tau_gen_nch_tmp);
	  tau_gen_np0.push_back(tau_gen_np0_tmp);
	  tau_gen_nnh.push_back(tau_gen_nnh_tmp);
	}
      }
    }

    // Build 2-prong resonances matching Higgs/W/Z pdgId                                                                                                                                           
    bool badGenEvent = false;
    std::vector<Resonance> resonances;
    for(size_t igen = 0; igen < gen_particle_pt->size(); igen++){
      if(abs(gen_particle_id->at(igen)) == 25 or
	 abs(gen_particle_id->at(igen)) == 23 or
	 abs(gen_particle_id->at(igen)) == 24){
	TLorentzVector resonace4V;
	resonace4V.SetPtEtaPhiM(gen_particle_pt->at(igen),gen_particle_eta->at(igen),gen_particle_phi->at(igen),gen_particle_mass->at(igen));
	resonances.push_back(Resonance());
	resonances.back().p4 = resonace4V;
	for(size_t idau = 0; idau < gen_particle_daughters_id->size(); idau++){
	  if(gen_particle_daughters_igen->at(idau) == igen){
	    resonances.back().daugid.push_back(gen_particle_daughters_id->at(idau));
	    TLorentzVector daughter4V;
	    daughter4V.SetPtEtaPhiM(gen_particle_daughters_pt->at(idau),gen_particle_daughters_eta->at(idau),gen_particle_daughters_phi->at(idau),gen_particle_daughters_mass->at(idau));
	    resonances.back().daugp4.push_back(daughter4V);
	  }
	}
	if(resonances.back().daugp4.size() != 2){
	  std::cerr<<"Found more than two decay particles from Higgs or W or Z --> please check!!! --> rejecting the event"<<std::endl;
	  badGenEvent = true;
	}
      }
    }
    if(badGenEvent){
      nJetsRejectedEvents++;
      continue;
    }

    // list used later in the jet-event loop
    std::vector<unsigned int> vetoResonancePosition;
    std::vector<unsigned int> vetoMuonPosition;
    std::vector<unsigned int> vetoElectronPosition;
    std::vector<unsigned int> vetoTauPosition;
    
    /// Loop over the main jet collection
    for(size_t ijet = 0; ijet < jet_hlt_pt->size(); ijet++){
      
      nJetsTotal++;

      ///// Selection
      if((jet_hlt_pt->at(ijet) < jetPtMin and jet_hlt_pt_raw->at(ijet) < jetPtMin) or
	 fabs(jet_hlt_eta->at(ijet)) > jetEtaMax or 
	 fabs(jet_hlt_eta->at(ijet)) < jetEtaMin or
	 (saveOnlyGenMatchedJets and jet_hlt_genmatch_pt->at(ijet) <= 0)){
	nJetsRejectedBaseCuts++;
	continue;
      }

      //// Jet 4V
      TLorentzVector jet4V;
      jet4V.SetPtEtaPhiM(jet_hlt_pt->at(ijet),jet_hlt_eta->at(ijet),jet_hlt_phi->at(ijet),jet_hlt_mass->at(ijet));

      // Matching with the resonance for signal jets using the kinematics of the resonance                                                                                                         
      int match_index_dR_parton_jet = -1;
      int match_index_dR_partons = -1;
      float mindR_jetres = 1000;
      for(size_t ipair = 0; ipair < resonances.size(); ipair++){
	float dRjres = jet4V.DeltaR(resonances.at(ipair).p4);
	if(dRjres < dRCone and  dRjres < mindR_jetres){
	  mindR_jetres = dRjres;
	  float dR1 = resonances.at(ipair).daugp4.front().DeltaR(jet4V);
	  float dR2 = resonances.at(ipair).daugp4.back().DeltaR(jet4V);
	  float dR  = resonances.at(ipair).daugp4.front().DeltaR(resonances.at(ipair).daugp4.back());
	  if(dR1 < dRMatchingDaughterJet and dR2 < dRMatchingDaughterJet)
	    match_index_dR_parton_jet = ipair;
	  if(dR < dRMatchingDaughters)
	    match_index_dR_partons = ipair;
	}
      }

      // matching based on dR parton-jet
      if(match_index_dR_parton_jet != -1 and resonances.at(match_index_dR_parton_jet).daugid.size() == 2){
	if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 15 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 15) // 2-taus H(tt), Z(tt)                           
	  jet_hlt_flav_2prong_partonjet_match_b = 7;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 5 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 5) // 2-b H(bb), Z(bb)                           
	  jet_hlt_flav_2prong_partonjet_match_b = 6;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 4 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 4) // 2-c H(cc), Z(cc)                           
	  jet_hlt_flav_2prong_partonjet_match_b = 5;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) <= 3 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) <= 3) // 2-q H(qq), Z(qq)                           
	  jet_hlt_flav_2prong_partonjet_match_b = 4;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 21 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 21) // 2-g H(gg)                                
	  jet_hlt_flav_2prong_partonjet_match_b = 3;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 13 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 13) // 2-mu H(mm), Z(mm)                        
	  jet_hlt_flav_2prong_partonjet_match_b = 2;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 11 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 11) // 2-el H(ee), Z(ee)                        
	  jet_hlt_flav_2prong_partonjet_match_b = 1;
	else
	  jet_hlt_flav_2prong_partonjet_match_b = 0; // whatever remains                                                                                                                              
      }
      else
	jet_hlt_flav_2prong_partonjet_match_b = -1; // only real QCD jets / not-matched resonances

      // matching based on dR parton-parton
      if(match_index_dR_partons != -1 and resonances.at(match_index_dR_partons).daugid.size() == 2){
	if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 15 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 15) // 2-taus H(tt), Z(tt)                                 
	  jet_hlt_flav_2prong_parton_match_b = 7;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 5 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 5) // 2-b H(bb), Z(bb)                                 
	  jet_hlt_flav_2prong_parton_match_b = 6;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 4 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 4) // 2-c H(cc), Z(cc)                                 
	  jet_hlt_flav_2prong_parton_match_b = 5;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) <= 3 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) <= 3) // 2-q H(qq), Z(qq)                                 
	  jet_hlt_flav_2prong_parton_match_b = 4;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 21 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 21) // 2-g H(gg)                                      
	  jet_hlt_flav_2prong_parton_match_b = 3;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 13 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 13) // 2-g H(mm)                                      
	  jet_hlt_flav_2prong_parton_match_b = 2;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 11 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 11) // 2-g H(ee)                                      
	  jet_hlt_flav_2prong_parton_match_b = 1;
	else
	  jet_hlt_flav_2prong_parton_match_b = 0; // whatever remains                                                                                                                                 
      }
      else 
	jet_hlt_flav_2prong_parton_match_b = -1;  // only real QCD jets / not-matched resonances

      // for signal samples exclude all jets unmatched (if enabled) to avoid labelling as QCD signal jets that are not matched to a resonance
      if(saveOnlyResonanceMatchedJets and 
	 jet_hlt_flav_2prong_partonjet_match_b == -1 and 
	 jet_hlt_flav_2prong_parton_match_b == -1) 
	continue;

      // Matching with gen muons/electrons/hadronic taus                                                                                                                                           
      std::vector<TLorentzVector> genMuonsMatched;
      std::vector<TLorentzVector> genElectronsMatched;
      std::vector<TLorentzVector> genTausHMatched;
      std::vector<TLorentzVector> genLepton4V;
      std::vector<TLorentzVector> genLeptonVis4V;
      TLorentzVector genLepton4V_total;
      TLorentzVector genLeptonVis4V_total;

      int gentau_decaymode = -1;

      // pt ordering priviledge in the assignement
      for(size_t igen = 0; igen < genMuonsFromResonance4V.size(); igen++){
	if(std::find(vetoMuonPosition.begin(),vetoMuonPosition.end(),igen) != vetoMuonPosition.end()) continue;
	float dR = jet4V.DeltaR(genMuonsFromResonance4V.at(igen));
	if(dR < dRCone){
	  vetoMuonPosition.push_back(igen);
	  genMuonsMatched.push_back(genMuonsFromResonance4V.at(igen));
	  genLepton4V.push_back(genMuonsFromResonance4V.at(igen));
	  genLeptonVis4V.push_back(genMuonsFromResonance4V.at(igen));
	  genLepton4V_total += genMuonsFromResonance4V.at(igen);
	  genLeptonVis4V_total += genMuonsFromResonance4V.at(igen);
	}
      }
      for(size_t igen = 0; igen < genElectronsFromResonance4V.size(); igen++){
	if(std::find(vetoElectronPosition.begin(),vetoElectronPosition.end(),igen) != vetoElectronPosition.end()) continue;
	float dR = jet4V.DeltaR(genElectronsFromResonance4V.at(igen));
	if(dR < dRCone){
	  genElectronsMatched.push_back(genElectronsFromResonance4V.at(igen));
	  genLepton4V.push_back(genElectronsFromResonance4V.at(igen));
	  genLeptonVis4V.push_back(genElectronsFromResonance4V.at(igen));
	  genLepton4V_total += genElectronsFromResonance4V.at(igen);
	  genLeptonVis4V_total += genElectronsFromResonance4V.at(igen);
	}
      }    
      for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){
	if(std::find(vetoTauPosition.begin(),vetoTauPosition.end(),itau) != vetoTauPosition.end()) continue;
	float dR = tau_gen_visible.at(itau).DeltaR(jet4V);
	if(dR < dRCone){
	  vetoTauPosition.push_back(itau);
	  genTausHMatched.push_back(tau_gen_visible.at(itau));
	  genLepton4V.push_back(tau_gen.at(itau));
	  genLeptonVis4V.push_back(tau_gen_visible.at(itau));
	  if(gentau_decaymode == -1) 
	    gentau_decaymode = 5*(tau_gen_nch.at(itau)-1)+tau_gen_nnh.at(itau);
	  else 
	    gentau_decaymode += 5*(tau_gen_nch.at(itau)-1)+tau_gen_nnh.at(itau);
	  genLepton4V_total += tau_gen.at(itau);
	  genLeptonVis4V_total += tau_gen_visible.at(itau);
	}
      }

      // Jet id applied only to jets not overlapping with gen-leptons                                                                                                                             
      if(genMuonsMatched.size() == 0 and
	 genElectronsMatched.size() == 0 and
	 genTausHMatched.size() == 0 and
	 jet_hlt_id->at(ijet) == 0){
	nJetsRejectedJetId++;
	continue;
      }

      // Reject jets on the basis of lepton content                                                                                                                                                
      if(not saveLeptonOppositeFlavor  and
	 ((genTausHMatched.size() != 0 and genMuonsMatched.size() != 0) or
	  (genTausHMatched.size() != 0 and genElectronsMatched.size() != 0) or
	  (genElectronsMatched.size() != 0 and genMuonsMatched.size() != 0))){
	nJetsRejectedLeptonContent++;
	continue;
      }
      
      // reject on the basis of the GEN lepton 
      bool reject = false;
      for(size_t igen = 0; igen < genMuonsMatched.size(); igen++){
	if(genMuonsMatched.at(igen).Pt() < ptGenLeptonMin) 
	  reject = true;
      }
      for(size_t igen = 0; igen < genElectronsMatched.size(); igen++){
	if(genElectronsMatched.at(igen).Pt() < ptGenLeptonMin) 
	  reject = true;
      }
      for(size_t igen = 0; igen < genTausHMatched.size(); igen++){
	if(genTausHMatched.at(igen).Pt() < ptGenTauVisibleMin) 
	  reject = true;
      }
      if(reject){
	nJetsRejectedLeptonContent++;
	continue;
      }
          
      /// apply neutrino contribution to GEN softdrop (status 1 neutrinos)
      TLorentzVector jet_gen_4V;
      TLorentzVector jet_gen_softdrop_4V;
      jet_gen_4V.SetPtEtaPhiM(jet_hlt_genmatch_pt->at(ijet),jet_hlt_genmatch_eta->at(ijet),jet_hlt_genmatch_phi->at(ijet),jet_hlt_genmatch_mass->at(ijet));
      jet_gen_softdrop_4V.SetPtEtaPhiM(jet_hlt_softdrop_genmatch_pt->at(ijet),jet_hlt_softdrop_genmatch_eta->at(ijet),jet_hlt_softdrop_genmatch_phi->at(ijet),jet_hlt_softdrop_genmatch_mass->at(ijet));
      
      // take the mass of the closest resonance                                                                                                                                                  
      float minDR = 1000;
      int closest_resonance = -1;
      for(size_t ires = 0; ires < resonances.size(); ires++){
	// skip resonances that decay directly with a neutrino                                                                                                                                   
	if(fabs(resonances.at(ires).daugid.at(0)) == 12 or fabs(resonances.at(ires).daugid.at(0)) == 14 or fabs(resonances.at(ires).daugid.at(0)) == 16) continue;
	if(fabs(resonances.at(ires).daugid.at(1)) == 12 or fabs(resonances.at(ires).daugid.at(1)) == 14 or fabs(resonances.at(ires).daugid.at(1)) == 16) continue;
	if(find(vetoResonancePosition.begin(),vetoResonancePosition.end(),ires) != vetoResonancePosition.end()) continue;
	if(jet4V.DeltaR(resonances.at(ires).p4) < minDR and jet4V.DeltaR(resonances.at(ires).p4) < dRCone){
	  minDR = jet4V.DeltaR(resonances.at(ires).p4);
	  closest_resonance = ires;
	}
      }

      // if found take the truth kinematic of the resonance from its GEN level (neutrinos included) and blacklist the resonance                                                                  
      if(closest_resonance != -1){
	vetoResonancePosition.push_back(closest_resonance);
	jet_hlt_pt_truth_b   = resonances.at(closest_resonance).p4.Pt();
	jet_hlt_eta_truth_b  = resonances.at(closest_resonance).p4.Eta();
	jet_hlt_phi_truth_b  = resonances.at(closest_resonance).p4.Phi();
	jet_hlt_mass_truth_b = resonances.at(closest_resonance).p4.M();
      }
      else{
	jet_hlt_pt_truth_b = jet_gen_4V.Pt();
	jet_hlt_eta_truth_b = jet_gen_4V.Eta();
	jet_hlt_phi_truth_b = jet_gen_4V.Phi();
	jet_hlt_mass_truth_b = jet_gen_softdrop_4V.M();
      }
      
       // selection on the truth mass
      if(jet_hlt_mass_truth_b < jetMassTruthMin){
	nJetsRejectedMassCut++;
	continue;
      }

      // good jets for the analysis	  
      nJetsTraining++;	  
      
      /// Fill event branches                                                                                                                                                                      
      run_b = *run;
      lumi_b = *lumi;
      event_b = *event;
      npu_b = *putrue;
      npv_b = *npv;
      nsv_b = *nsv;
      wgt_b = *wgt;
      rho_b = *rho;
      met_b = *met;
      sample_b = static_cast<int>(sample);
      ijet_b = ijet;

      // fill jet branches                                                                                                                                                                         
      jet_hlt_pt_b = jet_hlt_pt->at(ijet);
      jet_hlt_eta_b = jet_hlt_eta->at(ijet);
      jet_hlt_phi_b = jet_hlt_phi->at(ijet);
      jet_hlt_mass_b = jet_hlt_mass->at(ijet);      
      jet_hlt_pt_raw_b = jet_hlt_pt_raw->at(ijet);      
      jet_hlt_mass_raw_b = jet_hlt_mass_raw->at(ijet);      
      
      jet_hlt_chf_b = jet_hlt_chf->at(ijet);
      jet_hlt_nhf_b = jet_hlt_nhf->at(ijet);
      jet_hlt_phf_b = jet_hlt_phf->at(ijet);
      jet_hlt_elf_b = jet_hlt_elf->at(ijet);
      jet_hlt_muf_b = jet_hlt_muf->at(ijet);

      jet_hlt_ncand_b = jet_hlt_ncand->at(ijet);
      jet_hlt_nbhad_b = jet_hlt_nbhad->at(ijet);
      jet_hlt_nchad_b = jet_hlt_nchad->at(ijet);	  
      jet_hlt_hflav_b = jet_hlt_hflav->at(ijet);
      jet_hlt_pflav_b = jet_hlt_pflav->at(ijet);

      
      // truth corresponds to the pole resonance kinematic properties
      jet_hlt_pnethlt_probXqq_b = jet_hlt_pnethlt_probXqq->at(ijet);
      jet_hlt_pnethlt_probXgg_b = jet_hlt_pnethlt_probXgg->at(ijet);
      jet_hlt_pnethlt_probXtt_b = jet_hlt_pnethlt_probXtt->at(ijet);
      jet_hlt_pnethlt_probQCD_b = jet_hlt_pnethlt_probQCD->at(ijet);
      
      jet_hlt_genmatch_pt_b = jet_hlt_genmatch_pt->at(ijet);
      jet_hlt_genmatch_eta_b = jet_hlt_genmatch_eta->at(ijet);
      jet_hlt_genmatch_phi_b = jet_hlt_genmatch_phi->at(ijet);
      jet_hlt_genmatch_mass_b = jet_hlt_genmatch_mass->at(ijet);
      
      jet_hlt_muflav_b = genMuonsMatched.size();
      jet_hlt_elflav_b = genElectronsMatched.size();
      jet_hlt_tauflav_b = genTausHMatched.size();
      jet_hlt_taudecaymode_b = gentau_decaymode;
      jet_hlt_lepflav_b = genMuonsMatched.size()+genElectronsMatched.size()+genTausHMatched.size();
      
      // soft drop                                                                                                                                                                                 
      jet_hlt_softdrop_pt_b = jet_hlt_softdrop_pt->at(ijet);
      jet_hlt_softdrop_eta_b = jet_hlt_softdrop_eta->at(ijet);
      jet_hlt_softdrop_phi_b = jet_hlt_softdrop_phi->at(ijet);
      jet_hlt_softdrop_mass_b = jet_hlt_softdrop_mass->at(ijet);
      
      jet_hlt_softdrop_genmatch_pt_b = jet_hlt_softdrop_genmatch_pt->at(ijet);
      jet_hlt_softdrop_genmatch_eta_b = jet_hlt_softdrop_genmatch_eta->at(ijet);
      jet_hlt_softdrop_genmatch_phi_b = jet_hlt_softdrop_genmatch_phi->at(ijet);
      jet_hlt_softdrop_genmatch_mass_b = jet_hlt_softdrop_genmatch_mass->at(ijet);
      
      // SV	  
      jet_hlt_sv_pt_b.clear(); jet_hlt_sv_eta_b.clear(); jet_hlt_sv_mass_b.clear(); jet_hlt_sv_energy_b.clear();
      jet_hlt_sv_deta_b.clear(); jet_hlt_sv_dphi_b.clear();
      jet_hlt_sv_chi2_b.clear(); jet_hlt_sv_dxy_b.clear(); jet_hlt_sv_dxysig_b.clear();
      jet_hlt_sv_d3d_b.clear(); jet_hlt_sv_d3dsig_b.clear(); jet_hlt_sv_ntrack_b.clear();
      jet_hlt_sv_pt_log_b.clear(); jet_hlt_sv_energy_log_b.clear();
      
      for(size_t isv = 0; isv < jet_hlt_sv_pt->size(); isv++){
	
	if(ijet != jet_hlt_sv_ijet->at(isv)) continue;
	
	jet_hlt_sv_pt_b.push_back(jet_hlt_sv_pt->at(isv));
	jet_hlt_sv_pt_log_b.push_back(std::isnan(std::log(jet_hlt_sv_pt->at(isv))) ? 0 : std::log(jet_hlt_sv_pt->at(isv)));
	jet_hlt_sv_eta_b.push_back(jet_hlt_sv_eta->at(isv));	  
	jet_hlt_sv_energy_b.push_back(jet_hlt_sv_energy->at(isv));
	jet_hlt_sv_energy_log_b.push_back(std::isnan(std::log(jet_hlt_sv_energy->at(isv))) ? 0 : std::log(jet_hlt_sv_energy->at(isv)));
	jet_hlt_sv_mass_b.push_back(jet_hlt_sv_mass->at(isv));
	jet_hlt_sv_deta_b.push_back(jet_hlt_sv_eta->at(isv)-jet_hlt_eta->at(ijet));
	jet_hlt_sv_dphi_b.push_back(jet_hlt_sv_phi->at(isv)-jet_hlt_phi->at(ijet));
	jet_hlt_sv_chi2_b.push_back(jet_hlt_sv_chi2->at(isv));
        jet_hlt_sv_dxy_b.push_back(std::isnan(jet_hlt_sv_dxy->at(isv)) ? 0 : jet_hlt_sv_dxy->at(isv));
        jet_hlt_sv_dxysig_b.push_back(std::isnan(jet_hlt_sv_dxysig->at(isv)) ? 0 : jet_hlt_sv_dxysig->at(isv));
	jet_hlt_sv_d3d_b.push_back(std::isnan(jet_hlt_sv_d3d->at(isv)) ? 0 : jet_hlt_sv_d3d->at(isv));
        jet_hlt_sv_d3dsig_b.push_back(std::isnan(jet_hlt_sv_d3dsig->at(isv)) ? 0 : jet_hlt_sv_d3dsig->at(isv));
	jet_hlt_sv_ntrack_b.push_back(jet_hlt_sv_ntrack->at(isv));
      }
      
      // PF candidates	
      jet_hlt_pfcand_pt_b.clear(); jet_hlt_pfcand_eta_b.clear(); jet_hlt_pfcand_phi_b.clear(); jet_hlt_pfcand_mass_b.clear(); jet_hlt_pfcand_energy_b.clear(); jet_hlt_pfcand_calofraction_b.clear();
      jet_hlt_pfcand_hcalfraction_b.clear(); jet_hlt_pfcand_dxy_b.clear(); jet_hlt_pfcand_dz_b.clear(); jet_hlt_pfcand_dxysig_b.clear(); jet_hlt_pfcand_dzsig_b.clear(); 
      jet_hlt_pfcand_pperp_ratio_b.clear(); jet_hlt_pfcand_ppara_ratio_b.clear(); jet_hlt_pfcand_deta_b.clear(); jet_hlt_pfcand_dphi_b.clear(); 
      jet_hlt_pfcand_pt_log_b.clear(); jet_hlt_pfcand_energy_log_b.clear(); 
      jet_hlt_pfcand_etarel_b.clear(); 
      jet_hlt_pfcand_npixhits_b.clear(); jet_hlt_pfcand_nstriphits_b.clear();
      jet_hlt_pfcand_nlostinnerhits_b.clear(); jet_hlt_pfcand_charge_b.clear();
      jet_hlt_pfcand_frompv_b.clear(); jet_hlt_pfcand_id_b.clear(); jet_hlt_pfcand_track_qual_b.clear(); jet_hlt_pfcand_track_chi2_b.clear(); jet_hlt_pfcand_highpurity_b.clear();  
      jet_hlt_pfcand_trackjet_d3d_b.clear(); jet_hlt_pfcand_trackjet_d3dsig_b.clear();
      jet_hlt_pfcand_trackjet_dist_b.clear(); jet_hlt_pfcand_trackjet_decayL_b.clear();  

      for(size_t icand = 0; icand < jet_hlt_pfcand_pt->size(); icand++){
	
	if(ijet != jet_hlt_pfcand_ijet->at(icand)) continue;
	if(jet_hlt_pfcand_pt->at(icand) < pfCandPtMin) continue;

	jet_hlt_pfcand_pt_b.push_back(jet_hlt_pfcand_pt->at(icand));
        jet_hlt_pfcand_pt_log_b.push_back(std::isnan(std::log(jet_hlt_pfcand_pt->at(icand))) ? 0 : std::log(jet_hlt_pfcand_pt->at(icand)));
	jet_hlt_pfcand_eta_b.push_back(jet_hlt_pfcand_eta->at(icand));
	jet_hlt_pfcand_phi_b.push_back(jet_hlt_pfcand_phi->at(icand));
	jet_hlt_pfcand_mass_b.push_back(jet_hlt_pfcand_mass->at(icand));
	jet_hlt_pfcand_energy_b.push_back(jet_hlt_pfcand_energy->at(icand));
	jet_hlt_pfcand_energy_log_b.push_back(std::isnan(std::log(jet_hlt_pfcand_energy->at(icand))) ? 0 : std::log(jet_hlt_pfcand_energy->at(icand)));
        jet_hlt_pfcand_energy_log_b.push_back(std::isnan(std::log(jet_hlt_pfcand_energy->at(icand))) ? 0 : std::log(jet_hlt_pfcand_energy->at(icand)));
        jet_hlt_pfcand_calofraction_b.push_back(std::isnan(jet_hlt_pfcand_calofraction->at(icand)) ? 0 : jet_hlt_pfcand_calofraction->at(icand));
        jet_hlt_pfcand_hcalfraction_b.push_back(std::isnan(jet_hlt_pfcand_hcalfraction->at(icand)) ? 0 : jet_hlt_pfcand_hcalfraction->at(icand));
        jet_hlt_pfcand_dxy_b.push_back(std::isnan(jet_hlt_pfcand_dxy->at(icand)) ? 0 : jet_hlt_pfcand_dxy->at(icand));
        jet_hlt_pfcand_dz_b.push_back(std::isnan(jet_hlt_pfcand_dz->at(icand)) ? 0 : jet_hlt_pfcand_dz->at(icand));
        jet_hlt_pfcand_dzsig_b.push_back(std::isnan(jet_hlt_pfcand_dzsig->at(icand)) ? 0 : jet_hlt_pfcand_dzsig->at(icand));
        jet_hlt_pfcand_dxysig_b.push_back(std::isnan(jet_hlt_pfcand_dxysig->at(icand)) ? 0 : jet_hlt_pfcand_dxysig->at(icand));
        jet_hlt_pfcand_pperp_ratio_b.push_back(std::isnan(jet_hlt_pfcand_pperp_ratio->at(icand)) ? 0 : jet_hlt_pfcand_pperp_ratio->at(icand));
        jet_hlt_pfcand_ppara_ratio_b.push_back(std::isnan(jet_hlt_pfcand_ppara_ratio->at(icand)) ? 0 : jet_hlt_pfcand_ppara_ratio->at(icand));	
	jet_hlt_pfcand_deta_b.push_back(jet_hlt_pfcand_deta->at(icand));
	jet_hlt_pfcand_dphi_b.push_back(jet_hlt_pfcand_dphi->at(icand));
        jet_hlt_pfcand_etarel_b.push_back(std::isnan(jet_hlt_pfcand_etarel->at(icand)) ? 0 : jet_hlt_pfcand_etarel->at(icand));
	jet_hlt_pfcand_npixhits_b.push_back(jet_hlt_pfcand_npixhits->at(icand));
	jet_hlt_pfcand_nstriphits_b.push_back(jet_hlt_pfcand_nstriphits->at(icand));
	jet_hlt_pfcand_frompv_b.push_back(jet_hlt_pfcand_frompv->at(icand));
	jet_hlt_pfcand_nlostinnerhits_b.push_back(jet_hlt_pfcand_nlostinnerhits->at(icand));
	jet_hlt_pfcand_charge_b.push_back(jet_hlt_pfcand_charge->at(icand));
	jet_hlt_pfcand_track_qual_b.push_back(jet_hlt_pfcand_track_qual->at(icand));
	jet_hlt_pfcand_track_chi2_b.push_back(jet_hlt_pfcand_track_chi2->at(icand));
	jet_hlt_pfcand_highpurity_b.push_back(jet_hlt_pfcand_highpurity->at(icand));	    

	if(jet_hlt_pfcand_id->at(icand) == 11 and jet_hlt_pfcand_charge->at(icand) != 0) 
	  jet_hlt_pfcand_id_b.push_back(0);
	else if(jet_hlt_pfcand_id->at(icand) == 13 and jet_hlt_pfcand_charge->at(icand) != 0)
	  jet_hlt_pfcand_id_b.push_back(1);
	else if(jet_hlt_pfcand_id->at(icand) == 22 and jet_hlt_pfcand_charge->at(icand) == 0)
	  jet_hlt_pfcand_id_b.push_back(2);
	else if(jet_hlt_pfcand_id->at(icand) != 22 and jet_hlt_pfcand_charge->at(icand) == 0 and jet_hlt_pfcand_id->at(icand) != 1 and jet_hlt_pfcand_id->at(icand) != 2)
	  jet_hlt_pfcand_id_b.push_back(3);
	else if(jet_hlt_pfcand_id->at(icand) != 11 and jet_hlt_pfcand_id->at(icand) != 13 and jet_hlt_pfcand_charge->at(icand) != 0)
	  jet_hlt_pfcand_id_b.push_back(4);
	else if(jet_hlt_pfcand_charge->at(icand) == 0  and jet_hlt_pfcand_id->at(icand) == 1)
	  jet_hlt_pfcand_id_b.push_back(5);
	else if(jet_hlt_pfcand_charge->at(icand) == 0  and jet_hlt_pfcand_id->at(icand) == 2)
	  jet_hlt_pfcand_id_b.push_back(6);
	else
	  jet_hlt_pfcand_id_b.push_back(-1);    

	jet_hlt_pfcand_trackjet_d3d_b.push_back(std::isnan(jet_hlt_pfcand_trackjet_d3d->at(icand)) ? 0 : jet_hlt_pfcand_trackjet_d3d->at(icand));
	jet_hlt_pfcand_trackjet_d3dsig_b.push_back(std::isnan(jet_hlt_pfcand_trackjet_d3dsig->at(icand)) ? 0 : jet_hlt_pfcand_trackjet_d3dsig->at(icand));
	jet_hlt_pfcand_trackjet_dist_b.push_back(std::isnan(jet_hlt_pfcand_trackjet_dist->at(icand)) ? 0 : jet_hlt_pfcand_trackjet_dist->at(icand));
	jet_hlt_pfcand_trackjet_decayL_b.push_back(std::isnan(jet_hlt_pfcand_trackjet_decayL->at(icand)) ? 0 : jet_hlt_pfcand_trackjet_decayL->at(icand));
	
      }
      
      // Matching with offline jet
      minDR = 1000;
      int recomatch_pos = -1;
      for(size_t jjet = 0; jjet < jet_reco_pt->size(); jjet++){
          if(jet_reco_pt->at(jjet) < jetPtMin) continue;
          if(fabs(jet_reco_eta->at(jjet)) > jetEtaMax) continue;
          if(jet_reco_id->at(jjet) == 0) continue;

          TLorentzVector recojet_4V_tmp;
          recojet_4V_tmp.SetPtEtaPhiM(jet_reco_pt->at(jjet),jet_reco_eta->at(jjet),jet_reco_phi->at(jjet),jet_reco_mass->at(jjet));
          float dR = jet4V.DeltaR(recojet_4V_tmp);
          if(dR < minDR and dR < dRMatching){
              minDR = dR;
              recomatch_pos = jjet;
          }
      }

      if (recomatch_pos >= 0) {
	// fill jet branches                                                                                                                                                                         
	jet_recomatch_pt_b = jet_reco_pt->at(recomatch_pos);
	jet_recomatch_eta_b = jet_reco_eta->at(recomatch_pos);
	jet_recomatch_phi_b = jet_reco_phi->at(recomatch_pos);
	jet_recomatch_mass_b = jet_reco_mass->at(recomatch_pos);      
	jet_recomatch_pt_raw_b = jet_reco_pt_raw->at(recomatch_pos);      
	jet_recomatch_mass_raw_b = jet_reco_mass_raw->at(recomatch_pos);      
        
	jet_recomatch_chf_b = jet_reco_chf->at(recomatch_pos);
	jet_recomatch_nhf_b = jet_reco_nhf->at(recomatch_pos);
	jet_recomatch_phf_b = jet_reco_phf->at(recomatch_pos);
	jet_recomatch_elf_b = jet_reco_elf->at(recomatch_pos);
	jet_recomatch_muf_b = jet_reco_muf->at(recomatch_pos);
	
	jet_recomatch_ncand_b = jet_reco_ncand->at(recomatch_pos);
	jet_recomatch_nbhad_b = jet_reco_nbhad->at(recomatch_pos);
	jet_recomatch_nchad_b = jet_reco_nchad->at(recomatch_pos);	  
        
	jet_recomatch_pnet_probXbb_b = jet_reco_pnet_probXbb->at(recomatch_pos);
	jet_recomatch_pnet_probXcc_b = jet_reco_pnet_probXcc->at(recomatch_pos);
	jet_recomatch_pnet_probXqq_b = jet_reco_pnet_probXqq->at(recomatch_pos);
	jet_recomatch_pnet_probQCDbb_b = jet_reco_pnet_probQCDbb->at(recomatch_pos);
	jet_recomatch_pnet_probQCDb_b = jet_reco_pnet_probQCDb->at(recomatch_pos);
	jet_recomatch_pnet_probQCDcc_b = jet_reco_pnet_probQCDcc->at(recomatch_pos);
	jet_recomatch_pnet_probQCDc_b = jet_reco_pnet_probQCDc->at(recomatch_pos);
	jet_recomatch_pnet_probQCDothers_b = jet_reco_pnet_probQCDothers->at(recomatch_pos);
	jet_recomatch_pnet_regmass_b = jet_reco_pnet_regmass->at(recomatch_pos);
	
	jet_recomatch_softdrop_pt_b = jet_reco_softdrop_pt->at(recomatch_pos);
	jet_recomatch_softdrop_eta_b = jet_reco_softdrop_eta->at(recomatch_pos);
	jet_recomatch_softdrop_phi_b = jet_reco_softdrop_phi->at(recomatch_pos);
	jet_recomatch_softdrop_mass_b = jet_reco_softdrop_mass->at(recomatch_pos);
      }
      else {
	jet_recomatch_pt_b = -1; 
	jet_recomatch_eta_b = -1; 
	jet_recomatch_phi_b = -1; 
	jet_recomatch_mass_b = -1; 
	jet_recomatch_pt_raw_b = -1; 
	jet_recomatch_mass_raw_b = -1; 
        
	jet_recomatch_chf_b = -1; 
	jet_recomatch_nhf_b = -1; 
	jet_recomatch_phf_b = -1; 
	jet_recomatch_elf_b = -1; 
	jet_recomatch_muf_b = -1; 
	
	jet_recomatch_ncand_b = -1; 
	jet_recomatch_nbhad_b = -1; 
	jet_recomatch_nchad_b = -1; 
	   
	jet_recomatch_pnet_probXbb_b = -1;
	jet_recomatch_pnet_probXcc_b = -1;
	jet_recomatch_pnet_probXqq_b = -1;
	jet_recomatch_pnet_probQCDbb_b = -1;
	jet_recomatch_pnet_probQCDb_b = -1;
	jet_recomatch_pnet_probQCDcc_b = -1;
	jet_recomatch_pnet_probQCDc_b = -1;
	jet_recomatch_pnet_probQCDothers_b = -1;
	jet_recomatch_pnet_regmass_b = -1;
	
	jet_recomatch_softdrop_pt_b = -1;
	jet_recomatch_softdrop_eta_b = -1;
	jet_recomatch_softdrop_phi_b = -1;
	jet_recomatch_softdrop_mass_b = -1;
      }
      

      tree_out->Fill();
    }
  }
  std::cout<<"ntupleCreation --> thread "<<workerID<<" stopping "<<std::endl;
};
    
    
// Main function
int main(int argc, char **argv){
  
  boost::program_options::options_description desc("Main options");
  desc.add_options()
    ("inputFileList,i", boost::program_options::value<std::string>(&inputFileList)->default_value(""), "File that contains a list of ROOT files to be processed")
    ("inputFileDIR,d", boost::program_options::value<std::string>(&inputFileDIR)->default_value(""), "Directory that contained files to process")
    ("outputDIR,o", boost::program_options::value<std::string>(&outputDIR)->default_value(""), "Output directory where files need to be created")
    ("outputFileName,f", boost::program_options::value<std::string>(&outputFileName)->default_value(""), "Base name for the output file")
    ("nThreads,n", boost::program_options::value<unsigned int>(&nThreads)->default_value(1), "Number of threads to be used")
    ("maxNumberOfFilesToBeProcessed,max", boost::program_options::value<unsigned int>(&maxNumberOfFilesToBeProcessed)->default_value(1), "When running giving an input directory, split execution in chunks of Nfiles")
    ("mergeThreadOutputFiles,merge", boost::program_options::value<bool>(&mergeThreadOutputFiles)->default_value(false), "Merge the output files of the job into a single one")
    ("jetPtMin,jptmin", boost::program_options::value<float>(&jetPtMin)->default_value(180), "Min jet pT to apply")
    ("jetEtaMax,jetamax", boost::program_options::value<float>(&jetEtaMax)->default_value(2.5), "Max jet eta to apply")
    ("jetEtaMin,jetamin", boost::program_options::value<float>(&jetEtaMin)->default_value(0.), "Min jet eta to apply")
    ("jetMassTruthMin,jmass", boost::program_options::value<float>(&jetMassTruthMin)->default_value(20), "Min mass selection to apply based on the truth definition given in the code")
    ("pfCandPtMin,pfptmin", boost::program_options::value<float>(&pfCandPtMin)->default_value(0.), "Minimum pf candidate pt")
    ("sample-type,s", boost::program_options::value<std::string>(&sample_name)->default_value(""), "Type of physics sample")
    ("saveOnlyGenMatchedJets", boost::program_options::value<bool>(&saveOnlyGenMatchedJets)->default_value(true), "save or not jets matched to GEN jets")
    ("saveOnlyResonanceMatchedJets", boost::program_options::value<bool>(&saveOnlyResonanceMatchedJets)->default_value(false), "In case of signal type of sample, save only jets matched to a resonance")
    ("saveLeptonOppositeFlavor", boost::program_options::value<bool>(&saveLeptonOppositeFlavor)->default_value(true), "save or not jets matched to two GEN leptons of opposite flavour")
    ("compressOutputFile,c", boost::program_options::value<bool>(&compressOutputFile)->default_value(false), "Compress output file to save space")
    ("help,h", "Produce help message interface");

  boost::program_options::variables_map options;
  
  try{
    boost::program_options::store(boost::program_options::command_line_parser(argc,argv).options(desc).run(),options);
    boost::program_options::notify(options);
  }
  catch(std::exception &ex) {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cerr << "Use makeSkimmedNtuplesForTrainingAK8 --help to get a list of all the allowed options"  << std::endl;
    return 999;
  } 
  catch(...) {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if(options.count("help")) {
    std::cout << "Usage: makeSkimmedNtuplesForTrainingAK8 [options]\n";
    std::cout << desc;
    return 0;
  }

  // Conversion from string to sample type                                                                                                                                                            
  sample = convertSampleType(sample_name);

  std::cout<<"makeSkimmedNtuplesForTrainingAK8.cpp Parameter Summary"<<std::endl;
  std::cout<<"inputFileList --> "<<inputFileList<<std::endl;
  std::cout<<"inputFileDIR --> "<<inputFileDIR<<std::endl;
  std::cout<<"outputDIR --> "<<outputDIR<<std::endl;
  std::cout<<"outputFileName --> "<<outputFileName<<std::endl;
  std::cout<<"nThreads --> "<<nThreads<<std::endl;
  std::cout<<"maxNumberOfFilesToBeProcessed --> "<<maxNumberOfFilesToBeProcessed<<std::endl;
  std::cout<<"mergeThreadOutputFiles --> "<<mergeThreadOutputFiles<<std::endl;
  std::cout<<"jetPtMin --> "<<jetPtMin<<std::endl;
  std::cout<<"jetEtaMax --> "<<jetEtaMax<<std::endl;
  std::cout<<"jetEtaMin --> "<<jetEtaMin<<std::endl; 
  std::cout<<"jetMassTruthMin --> "<<jetMassTruthMin<<std::endl;
  std::cout<<"pfCandPtMin --> "<<pfCandPtMin<<std::endl;
  std::cout<<"sample_type --> "<<static_cast<int>(sample)<<std::endl;
  std::cout<<"saveOnlyGenMatchedJets --> "<<saveOnlyGenMatchedJets<<std::endl;
  std::cout<<"saveOnlyResonanceMatchedJets --> "<<saveOnlyResonanceMatchedJets<<std::endl;
  std::cout<<"saveLeptonOppositeFlavor --> "<<saveLeptonOppositeFlavor<<std::endl;
  std::cout<<"compressOutputFile --> "<<compressOutputFile<<std::endl;

  if(not inputFileList.empty() and not inputFileDIR.empty()){
    std::cerr<<"You cannot set inputFileList and inputFileDIR to be simultaneously non empty --> please either provide a directory or a file list"<<std::endl;
    return 1001;
  }

  gSystem->Exec(("mkdir -p "+outputDIR).c_str());

  // Prepare for multi-threading
  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();

  // Prepare the workflow
  if(not inputFileList.empty() and boost::filesystem::is_regular_file(inputFileList)){ // the input string is a file containing the list of ROOT files to process --> for batch mode
    
    std::vector<std::string> fileList;
    std::vector<std::shared_ptr<TFile> >  files_out;
    std::vector<std::shared_ptr<TTree> >  trees_out;      

    std::ifstream inputFile (inputFileList);
    if(inputFile.is_open()){
      std::string line;
      while (getline(inputFile,line)) {
	fileList.push_back(line);
      }
    }
    inputFile.close();

    for(size_t ithread = 0; ithread < nThreads; ithread++){
      files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_thread%zu.root",ithread)).Data()),"RECREATE"));
      trees_out.emplace_back(new TTree("tree","tree"));
      if(not compressOutputFile){
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	files_out.back()->SetCompressionLevel(4);
      }
      else{
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	files_out.back()->SetCompressionLevel(6);	
      }
    }      
    
    // Count number of events to split in threads
    std::cout<<"Count total number of events to process .."<<std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::unique_ptr<TChain> tree_in  (new TChain("dnntree/tree","dnntree/tree"));
    for(size_t ifile = 0; ifile < fileList.size(); ifile++)
      tree_in->Add(fileList.at(ifile).c_str());
    long int nevents = tree_in->GetEntries();
    auto stop  = std::chrono::high_resolution_clock::now(); 
    std::chrono::duration<float> duration = stop-start;
    std::cout<<"Duration of counting loop is "<<duration.count()<<" nevets = "<<nevents<<std::endl;

    // operation/function that will be executed in parallel  
    std::cout<<"Event loop to build output tree .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::vector<std::thread> threads;
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(ntupleCreation, workerID, fileList, trees_out, nevents, nThreads);
    for (auto && worker : threads) worker.join();
    stop  = std::chrono::high_resolution_clock::now(); 
    duration = stop-start;
    std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
    std::cout<<"Total jets in input "<<nJetsTotal<<std::endl;
    std::cout<<"Jets rejected by events propertie: "<<nJetsRejectedEvents<<" fraction "<<float(nJetsRejectedEvents)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by base cuts: "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by signal resonance only: "<<nJetsRejectedSignalResCuts<<" fraction "<<float(nJetsRejectedSignalResCuts)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by jet id: "<<nJetsRejectedJetId<<" fraction "<<float(nJetsRejectedJetId)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by mu/ele/tau content: "<<nJetsRejectedLeptonContent<<" fraction "<<float(nJetsRejectedLeptonContent)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by mass cut: "<<nJetsRejectedMassCut<<" fraction "<<float(nJetsRejectedMassCut)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
    threads.clear();
    
    std::cout<<"Writing the outout files .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::string name_list = "";
    for(size_t ifile = 0; ifile < files_out.size(); ifile++){
      files_out.at(ifile)->cd();
      name_list += files_out.at(ifile)->GetName();
      name_list += " ";
      trees_out.at(ifile)->Write("",TObject::kOverwrite);
    }
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;

    if(mergeThreadOutputFiles and files_out.size() > 1){
      std::cout<<"Merging outout files .."<<std::endl;
      std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
      gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
      std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
      std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
      unsigned int total_entries = tree->GetEntries();
      gSystem->Exec(("rm "+name_list).c_str());
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
    }   
  }
  else if(not inputFileDIR.empty() and boost::filesystem::is_directory(inputFileDIR)){ // pathInput is a real path to a directory

    std::cout<<"Build list of files in "<<inputFileDIR<<std::endl;

    std::vector<std::string> fileList;

    for(auto const & entry :  boost::filesystem::recursive_directory_iterator(inputFileDIR)){
      TString filePath (entry.path().string());
      if(filePath.Contains(".root"))
	fileList.push_back(entry.path().string()); 
    }      

    // split the job into n-steps (blocks) each containing n-files 
    unsigned int numberOfFiles = fileList.size();
    unsigned int numberOfBlocks = 1;  
    if(numberOfFiles > maxNumberOfFilesToBeProcessed){
      if(numberOfFiles%maxNumberOfFilesToBeProcessed == 0)
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed;
      else
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed+1;
    }

    std::cout<<"In iterative mode --> Number of files "<<numberOfFiles<<" numberOfBlocks "<<numberOfBlocks<<std::endl;

    // Loop on the blocks
    for(unsigned int iblock = 0; iblock < numberOfBlocks; iblock++){          
      std::vector<std::string> fileList_block;
      if(maxNumberOfFilesToBeProcessed == 1){
	for(size_t ifile = 0; ifile < fileList.size(); ifile++)
	  fileList_block.push_back(fileList.at(ifile));      
      }
      else{
	for(size_t ifile = iblock*maxNumberOfFilesToBeProcessed; ifile < std::min(fileList.size(),size_t((iblock+1)*maxNumberOfFilesToBeProcessed)); ifile++)
	  fileList_block.push_back(fileList.at(ifile));
      }    
      std::cout<<"Block number "<<iblock<<" number of files "<<fileList_block.size()<<std::endl;
      
      // Build job list and output files --> minimum compression to speed-up the training hjob
      std::vector<std::shared_ptr<TFile> >  files_out;
      std::vector<std::shared_ptr<TTree> >  trees_out;      
      for(unsigned int ithread = 0; ithread < nThreads; ithread++){
	files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_block%d_thread%d.root",iblock,ithread)).Data()),"RECREATE"));
	trees_out.emplace_back(new TTree("tree","tree"));
	if(not compressOutputFile){
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	  files_out.back()->SetCompressionLevel(4);
	}
	else{
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	  files_out.back()->SetCompressionLevel(6);	  
	}
      }      

      // Count number of events to split in threads
      std::cout<<"Count total number of events to process .."<<std::endl;
      auto start = std::chrono::high_resolution_clock::now();
      std::unique_ptr<TChain> tree_in  (new TChain("dnntree/tree","dnntree/tree"));
      for(size_t ifile = 0; ifile < fileList_block.size(); ifile++)
	tree_in->Add(fileList_block.at(ifile).c_str());
      long int nevents = tree_in->GetEntries();
      auto stop  = std::chrono::high_resolution_clock::now(); 
      std::chrono::duration<float> duration = stop-start;
      std::cout<<"Duration of counting loop is "<<duration.count()<<" nevents = "<<nevents<<std::endl;

      nJetsTotal = 0;
      nJetsRejectedEvents = 0;
      nJetsRejectedBaseCuts = 0;
      nJetsRejectedSignalResCuts = 0;
      nJetsRejectedJetId = 0;
      nJetsRejectedLeptonContent = 0;
      nJetsRejectedMassCut = 0;
      nJetsTraining = 0;
      
      // operation/function that will be executed in parallel  
      start = std::chrono::high_resolution_clock::now();
      std::vector<std::thread> threads;
      for (auto workerID : ROOT::TSeqI(nThreads))
	threads.emplace_back(ntupleCreation, workerID, fileList_block, trees_out, nevents, nThreads);
      for (auto && worker : threads) 
	worker.join();
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
      std::cout<<"Total jets in input "<<nJetsTotal<<std::endl;
      std::cout<<"Jets rejected by events propertie: "<<nJetsRejectedEvents<<" fraction "<<float(nJetsRejectedEvents)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by base cuts: "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by signal resonance only: "<<nJetsRejectedSignalResCuts<<" fraction "<<float(nJetsRejectedSignalResCuts)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by jet id: "<<nJetsRejectedJetId<<" fraction "<<float(nJetsRejectedJetId)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by mu/ele/tau content: "<<nJetsRejectedLeptonContent<<" fraction "<<float(nJetsRejectedLeptonContent)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by mass cut: "<<nJetsRejectedMassCut<<" fraction "<<float(nJetsRejectedMassCut)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
      threads.clear();      

      std::cout<<"Writing the outout files .."<<std::endl;
      start = std::chrono::high_resolution_clock::now();
      std::string name_list = "";
      for(size_t ifile = 0; ifile < files_out.size(); ifile++){
	files_out.at(ifile)->cd();
	name_list += files_out.at(ifile)->GetName();
	name_list += " ";
	trees_out.at(ifile)->Write("",TObject::kOverwrite);
      }
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;
      
      if(mergeThreadOutputFiles and files_out.size() > 1){
	std::cout<<"Merging outout files .."<<std::endl;
	std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
	gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
	std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
	std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
	unsigned int total_entries = tree->GetEntries();
	gSystem->Exec(("rm "+name_list).c_str());
	stop  = std::chrono::high_resolution_clock::now(); 
	duration = stop-start;
	std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
      } 
    }
  }
  std::cout<<"Exiting from the code"<<std::endl;  
}
