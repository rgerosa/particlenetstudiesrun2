## aggregator api endpoints https://cmsoms.cern.ch/agg/api/v1/version/endpoints
from __future__ import print_function
import sys, os, argparse, re
import json
from omsapi import OMSAPI

parser = argparse.ArgumentParser()
parser.add_argument('--input_json', metavar='input_json', type=str, help='input JSON file with runs and lumi to be query')
parser.add_argument('--output_directory', metavar='output_directory', type=str, help='path for the output directory')
parser.add_argument('--restartFromRun',metavar="restartFromRun", type=int, help='restart from this run number', default=-1)
parser.add_argument('--dtype', metavar="dtype", type=str, help="possible commands are: EphemeralHLTPhysics, EphemeralZeroBias", choices=['EphemeralHLTPhysics','EphemeralZeroBias']);

args = parser.parse_args()

## query for ephemeral dataset
def queryEphemeralDataset(name="",run_number=0,lumisections=[],start_time=0,end_time=0,query=None):
  query.clear_filter().filter("run_number",run_number).filter("dataset_name",name);
  response = query.data();
  data = response.json()["data"];
  result = [];
  ## dataset rate
  rejected_lumi = 0;
  for entry in data:
    info = entry["attributes"]
    if info["rate"] == 0 and info["events"] == 0: 
      rejected_lumi = rejected_lumi + 1;
      continue; ## skip lumi without events
    ## check if is a good lumi-number
    for lumi in lumisections:
      if info["first_lumisection_number"] >= lumi[0] and info["first_lumisection_number"] <= lumi[1]:
        found = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result))
        if len(found) > 0: continue;
        if "EphemeralHLTPhysics" in name:
          elem = { "runnumber" : run_number,
                   "start_time": start_time,
                   "end_time": end_time,
                   "lumisection" : info["first_lumisection_number"],
                   "pileup" : 0,
                   "events_l1a" : 0,                 
                   "rate" : info["rate"],
                   "events" : info["events"],
                   "prescale" : 0,
          }
        elif "EphemeralZeroBias" in name:
          elem = { "runnumber" : run_number,
                   "start_time": start_time,
                   "end_time": end_time,
                   "lumisection" : info["first_lumisection_number"],
                   "pileup" : 0,
                   "rate" : info["rate"],
                   "events" : info["events"],
                   "prescale" : 0,
                   "prescale_l1" : 0,                   
                   "prescale_hlt" : 0,
                   "events_l1_postdt": 0,
          }
          
        result.append(elem);    
  return (result,rejected_lumi);

## open input json
input_json = open(args.input_json,"r");
json_data  = json.load(input_json)

os.system("mkdir -p "+args.output_directory);
os.chdir(args.output_directory);

omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=True)
omsapi.auth_krb()

for runs in json_data:

  print(runs);
  run_number = runs["runnumber"];
  if run_number != args.restartFromRun and args.restartFromRun != -1: continue;
  else:
    args.restartFromRun = -1;
  print("Create OMS api object")
  omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api","v1")
  omsapi.auth_krb("./ssocookies.txt")

  ######## Retrive basic run info
  print("Query Run table")
  query_runs = omsapi.query("runs");  
  query_runs.per_page = 10000;
  query_runs.attrs(["run_number","start_time","end_time","duration"])
  query_runs.clear_filter().filter("run_number",run_number)
  response = query_runs.data();
  run_info = response.json()["data"];

  ######## Retrive information of ephemeral data HLT for each run and valid lumi
  print("Query datasets table")
  query = omsapi.query("datasetrates");  
  query.per_page = 10000;
  query.attrs(["first_lumisection_number","rate","events"])
  
  result_ephemeral0, rejected_lumi_ephemeral0 = queryEphemeralDataset(name=args.dtype+"0",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral1, rejected_lumi_ephemeral1 = queryEphemeralDataset(name=args.dtype+"1",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral2, rejected_lumi_ephemeral2 = queryEphemeralDataset(name=args.dtype+"2",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral3, rejected_lumi_ephemeral3 = queryEphemeralDataset(name=args.dtype+"3",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral4, rejected_lumi_ephemeral4 = queryEphemeralDataset(name=args.dtype+"4",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral5, rejected_lumi_ephemeral5 = queryEphemeralDataset(name=args.dtype+"5",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral6, rejected_lumi_ephemeral6 = queryEphemeralDataset(name=args.dtype+"6",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral7, rejected_lumi_ephemeral7 = queryEphemeralDataset(name=args.dtype+"7",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)
  result_ephemeral8, rejected_lumi_ephemeral8 = queryEphemeralDataset(name=args.dtype+"8",run_number=run_number,lumisections=runs["lumisections"],start_time=run_info[0]["attributes"]["start_time"],end_time=run_info[0]["attributes"]["end_time"],query=query)

  if (not result_ephemeral1 or not result_ephemeral2 or not result_ephemeral3 or not result_ephemeral4 or
      not result_ephemeral5 or not result_ephemeral6 or not result_ephemeral7 or not result_ephemeral8): continue;
  
  merged_result = [];
  ######## retrive total prescale info
  if args.dtype == "EphemeralHLTPhysics":

    print("Query L1 trigger rates for EphemeralHLTPhysics")
    query_l1rates = omsapi.query("l1triggerrates");  
    query_l1rates.per_page = 10000;
    query_l1rates.attrs(["run_number","first_lumisection_number","l1a_total"])
    query_l1rates.clear_filter().filter("run_number",run_number).custom("group[granularity]","lumisection");
    response = query_l1rates.data();
    data_json = response.json()["data"];

    print("Merge information for EphemeralHLTPhysics");
    for data in data_json:
      info = data["attributes"];
      found0 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral0))
      found1 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral1))
      found2 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral2))
      found3 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral3))
      found4 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral4))
      found5 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral5))
      found6 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral6))
      found7 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral7))
      found8 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral8))

      if found0:      
        found0[0]["events_l1a"] = info["l1a_total"]["counter"];
        found0[0]["prescale"] = int(found0[0]["events_l1a"]/found0[0]["events"]);
      if found1:      
        found1[0]["events_l1a"] = info["l1a_total"]["counter"];
        found1[0]["prescale"] = int(found1[0]["events_l1a"]/found1[0]["events"]);
      if found2:      
        found2[0]["events_l1a"] = info["l1a_total"]["counter"];
        found2[0]["prescale"] = int(found2[0]["events_l1a"]/found2[0]["events"]);
      if found3:      
        found3[0]["events_l1a"] = info["l1a_total"]["counter"];
        found3[0]["prescale"] = int(found3[0]["events_l1a"]/found3[0]["events"]);
      if found4:      
        found4[0]["events_l1a"] = info["l1a_total"]["counter"];
        found4[0]["prescale"] = int(found4[0]["events_l1a"]/found4[0]["events"]);
      if found5:      
        found5[0]["events_l1a"] = info["l1a_total"]["counter"];
        found5[0]["prescale"] = int(found5[0]["events_l1a"]/found5[0]["events"]);
      if found6:      
        found6[0]["events_l1a"] = info["l1a_total"]["counter"];
        found6[0]["prescale"] = int(found6[0]["events_l1a"]/found6[0]["events"]);
      if found7:      
        found7[0]["events_l1a"] = info["l1a_total"]["counter"];
        found7[0]["prescale"] = int(found7[0]["events_l1a"]/found7[0]["events"]);
      if found8:      
        found8[0]["events_l1a"] = info["l1a_total"]["counter"];
        found8[0]["prescale"] = int(found8[0]["events_l1a"]/found8[0]["events"]);

    print("Prepare final result for EphemeralHLTPhysics")
    for entry in range(0,len(result_ephemeral0)):
      elem = result_ephemeral0[entry].copy();
      elem["rate"] = elem["rate"] + result_ephemeral1[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral2[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral3[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral4[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral5[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral6[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral7[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral8[entry]["rate"];
      elem["events"] = elem["events"] + result_ephemeral1[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral2[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral3[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral4[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral5[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral6[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral7[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral8[entry]["events"];
      ## this means that there is no matching in a lumi between datasetrate and l1triggerrates
      if elem["events"] == 0 or elem["events_l1a"] == 0:
        print("Information for run = ",elem["runnumber"]," lumisection = ",elem["lumisection"]," badly formed --> skipped");
        continue;
      elem["prescale"] = round(elem["events_l1a"]/elem["events"]);
      merged_result.append(elem);
        
  elif args.dtype == "EphemeralZeroBias":
    print("Query L1 trigger rates for EphemeralZeroBias")
    query_l1rates = omsapi.query("l1algorithmtriggers");  
    query_l1rates.per_page = 10000;
    query_l1rates.attrs(["name","run_number","first_lumisection_number","initial_prescale","final_prescale","post_dt_rate","post_dt_counter"])
    query_l1rates.clear_filter().filter("run_number",run_number).filter("name","L1_ZeroBias_copy").custom("group[granularity]","lumisection");
    response = query_l1rates.data();
    data_json = response.json()["data"];
    for data in data_json:
      info = data["attributes"];
      found0 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral0))
      found1 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral1))
      found2 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral2))
      found3 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral3))
      found4 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral4))
      found5 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral5))
      found6 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral6))
      found7 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral7))
      found8 = list(filter(lambda item: item["lumisection"] == info["first_lumisection_number"], result_ephemeral8))

      if found0:
        found0[0]["events_l1_postdt"] = info["post_dt_counter"];
        found0[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found1:
        found1[0]["events_l1_postdt"] = info["post_dt_counter"];
        found1[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found2:
        found2[0]["events_l1_postdt"] = info["post_dt_counter"];
        found2[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found3:
        found3[0]["events_l1_postdt"] = info["post_dt_counter"];
        found3[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found4:
        found4[0]["events_l1_postdt"] = info["post_dt_counter"];
        found4[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found5:
        found5[0]["events_l1_postdt"] = info["post_dt_counter"];
        found5[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found6:
        found6[0]["events_l1_postdt"] = info["post_dt_counter"];
        found6[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found7:
        found7[0]["events_l1_postdt"] = info["post_dt_counter"];
        found7[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);
      if found8:
        found8[0]["events_l1_postdt"] = info["post_dt_counter"];
        found8[0]["prescale_l1"] = int((info["initial_prescale"]["prescale"]+info["final_prescale"]["prescale"])/2);

    print("Prepare final result for EphemeralZeroBias")
    for entry in range(0,len(result_ephemeral0)):
      elem = result_ephemeral0[entry].copy();
      elem["rate"] = elem["rate"] + result_ephemeral1[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral2[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral3[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral4[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral5[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral6[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral7[entry]["rate"];
      elem["rate"] = elem["rate"] + result_ephemeral8[entry]["rate"];
      elem["events"] = elem["events"] + result_ephemeral1[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral2[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral3[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral4[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral5[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral6[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral7[entry]["events"];
      elem["events"] = elem["events"] + result_ephemeral8[entry]["events"];

      if elem["events"] == 0 or elem["events_l1_postdt"] == 0:
        print("Information for run = ",elem["runnumber"]," lumisection = ",elem["lumisection"]," badly formed --> skipped");
        continue;
      elem["prescale_hlt"] = round(elem["events_l1_postdt"]/elem["events"]);
      elem["prescale"] = elem["prescale_hlt"]*elem["prescale_l1"]
      merged_result.append(elem);
      
  ### extract pileup max per lumi-section  
  print("Query lumisection table")
  query_luminosity = omsapi.query("lumisections");
  query_luminosity.per_page = 10000;
  query_luminosity.set_validation(False);
  query_luminosity.attrs(["run_number","lumisection_number","pileup"])
  query_luminosity.clear_filter().filter("run_number",run_info[0]["attributes"]["run_number"]);  
  response = query_luminosity.data();
  data_json = response.json()["data"];

  for info in data_json:
    attr = info["attributes"];
    found = list(filter(lambda item: item["lumisection"] == attr["lumisection_number"], merged_result))    
    if found:
      found[0]["pileup"] = attr["pileup"]

  #### Save final result in json format
  print("Save final json file")
  with open("run_"+str(run_number)+"_summary.json","w") as f:
    f.write("[\n");
    for element in range(0,len(merged_result)):
      if element == 0:
        f.write(json.dumps(merged_result[element]));
      elif element == len(merged_result)-1:
        f.write(",\n"+json.dumps(merged_result[element])+"\n");
      else:
        f.write(",\n"+json.dumps(merged_result[element]));
    f.write("]\n");
