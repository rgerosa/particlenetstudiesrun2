import sys,os,array
import glob
import math
import time
import subprocess
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('--input_json', metavar='input_json', type=str, help='input JSON file')
parser.add_argument('--output_json', metavar='output_json', type=str, help='output JSON file')
parser.add_argument('--input_run_list',metavar='input_run_list', type=str, help='input run list') 
args = parser.parse_args()
                                                                                  
##################                                                                                                                                                                                    
if __name__ == '__main__':

    print("Input JSON file: ",args.input_json);
    print("Output JSON file: ",args.output_json);
    print("Input Run List: ",args.input_run_list);

    ## open input json
    json_input = open(args.input_json,"r");
    json_data  = json.load(json_input);
    
    ## take run list to filter
    run_list_input = open(args.input_run_list,"r");
    run_list_filter = [];
    for line in run_list_input:
        line = line.replace("\n","");
        run_list_filter.append(line);
    print("Runs to be considered in ephemeral = ",len(run_list_filter));
    print("Runs to be considered in Golden json = ",len(json_data));
    
    ## filter json content
    accepted_entries = 0;
    rejected_entries = 0;
    json_output_string = "[";
    for data in json_data:
        if data in run_list_filter:
            accepted_entries = accepted_entries+1;            
            json_output_string = json_output_string+"{ \"runnumber\" : "+str(data)+", \"lumisections\" : "+str(json_data[data])+"},\n"
        else:
            rejected_entries = rejected_entries+1;

    json_output_string = json_output_string+"]"
    json_output_string = json_output_string.replace(" ","");
    json_output_string = json_output_string.replace(",\n]","]");

    print("Accepted entries = ",accepted_entries);
    print("Rejected entries = ",rejected_entries);

    with open(args.output_json,"w") as output_file:
        output_file.write(json_output_string);
    output_file.close();
