Install the OMS python API following the recipe at the following link [[https://gitlab.cern.ch/cmsoms/oms-api-client]]{OMS API Python).

Install and complie on an lxplus machine and be sure that: no cms software has been loaded via ```cmsenv``` and no other python-paths are set. OMS API need to use the python3 of the machine with kerberos credentials.

```sh
git clone ssh://git@gitlab.cern.ch:7999/cmsoms/oms-api-client.git
cd oms-api-client
python3 -m pip install -r requirements.txt
python3 setup.py install --user
```

Query in order to find complete list of runs for EphemeralHLT RAW dataset:

```sh
dasgoclient --query "run dataset=/EphemeralHLTPhysics0/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics1/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics2/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics3/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics4/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics5/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics6/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics7/Run2022G-v1/RAW dataset=/EphemeralHLTPhysics8/Run2022G-v1/RAW" > run_list_ephemeral_hlt.txt
dasgoclient --query "run dataset=/EphemeralZeroBias0/Run2022G-v1/RAW dataset=/EphemeralZeroBias1/Run2022G-v1/RAW dataset=/EphemeralZeroBias2/Run2022G-v1/RAW dataset=/EphemeralZeroBias3/Run2022G-v1/RAW dataset=/EphemeralZeroBias4/Run2022G-v1/RAW dataset=/EphemeralZeroBias5/Run2022G-v1/RAW dataset=/EphemeralZeroBias6/Run2022G-v1/RAW dataset=/EphemeralZeroBias7/Run2022G-v1/RAW dataset=/EphemeralZeroBias8/Run2022G-v1/RAW" > run_list_ephemeral_zerobias.txt
```

Golden JSON file of reference:

```sh
scp /eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json ./
```

Simple script that keeps only interesting runs from the json file (all ephemeral run list verified to be the same i.e. they contain the same runs):

```sh
python3 filterCertJSONFile.py --input_json Cert_Collisions2022_eraG_362433_362760_Golden.json --input_run_list run_list_ephemeral_hlt.txt --output_json Cert_filtered_JSON_EphemeralHLTPhysics_2022G.json
python3 filterCertJSONFile.py --input_json Cert_Collisions2022_eraG_362433_362760_Golden.json --input_run_list run_list_ephemeral_zerobias.txt --output_json Cert_filtered_JSON_EphemeralZeroBias_2022G.json
```

Simple script to query OMS and download for each run and each lumi of the certified json in the ephemeral dataset info like rate, events and prescale:

```sh
python3 query_oms_info.py --input_json Cert_filtered_JSON_EphemeralHLTPhysics_2022G.json --output_directory download_oms_ephemeral_hlt_2022G --dtype EphemeralHLTPhysics
python3 query_oms_info.py --input_json Cert_filtered_JSON_EphemeralZeroBias_2022G.json --output_directory download_oms_ephemeral_zerobias_2022G --dtype EphemeralZeroBias
```