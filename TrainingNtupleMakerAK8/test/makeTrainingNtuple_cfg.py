### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register (
    'filterEventsInTheDumper',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True events without an ak8 jet passing selection are rejected/filtered out');

options.register (
    'saveLHEObjects',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True, LHE objects are saved --> turn-on only for simulated samples coming from ME');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'muonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'photonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for photons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'boostedTauPtMin',100.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for boosted taus');

options.register (
    'jetPtMin',200.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetSoftDropMassMin',0.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for jets');

options.register (
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'minimum pt for reco PF candidates');

options.register (
    'lostTrackPtMin',1.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for lost tracks');

options.register (
    'dRLostTrackJet',0.4,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum dR between lost track and jet axis');

options.register (
    'dRJetGenMatch',0.8,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets');

options.register (
    'dumpOnlyJetMatchedToGen', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'applyJECs', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply updated JECs on the fly via pat modules');

options.register (
    'evaluatePNETTraining', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'evaluate the last PNET training performed and exported in ONNX format. It has to be placed in the external directory and shipped to the job via crab');

options.register (
    'evaluateParTTraining', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'evaluate the last ParT training performed and exported in ONNX format. It has to be placed in the external directory and shipped to the job via crab');

options.parseArguments()

# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(0)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
else:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')


if options.applyJECs:
    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    process.patJetCorrFactorsProducer = JetCorrFactorsProducer.clone();
    process.patJetCorrFactorsProducer.extraJPTOffset = cms.string('L1FastJet');
    if options.isMC:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L2Relative','L3Absolute');
    else:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L2Relative','L3Absolute','L2L3Residual');
    process.patJetCorrFactorsProducer.payload = cms.string('AK8PFPuppi');
    process.patJetCorrFactorsProducer.primaryVertices = cms.InputTag("offlineSlimmedPrimaryVertices");
    process.patJetCorrFactorsProducer.rho  = cms.InputTag("fixedGridRhoFastjetAll");
    process.patJetCorrFactorsProducer.useNPV = cms.bool(True);
    process.patJetCorrFactorsProducer.useRho = cms.bool(True);
    process.patJetCorrFactorsProducer.src = cms.InputTag("slimmedJetsAK8");

    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    process.slimmedJetsAK8Calibrated = updatedPatJets.clone(
            jetSource = 'slimmedJetsAK8Calibrated',
            addJetCorrFactors = ( True if options.applyJECs else False),
            jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsProducer"))
        )


### Evaluate particle-net regression + classification                                                                                                                                                 
pnetDiscriminatorNames = [];
pnetDiscriminatorLabels = [];

if options.evaluatePNETTraining:
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
    process.pfParticleNetAK8LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsAK8Calibrated" if options.applyJECs else "slimmedJetsAK8"),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.8),
        min_jet_pt = cms.double(options.jetPtMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin),
        min_pt_for_track_properties = cms.double(-1),
        min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
        max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
        min_pt_for_taus = cms.double(options.tauPtMin),
        max_eta_for_taus = cms.double(2.5),
        dump_feature_tree = cms.bool(True)
    )

    from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
    process.pfParticleNetAK8LastJetTags = boostedJetONNXJetTagsProducer.clone();
    process.pfParticleNetAK8LastJetTags.src = cms.InputTag("pfParticleNetAK8LastJetTagInfos");
    process.pfParticleNetAK8LastJetTags.flav_names = cms.vstring(
        'probHtt',
        'probHtm',
        'probHte',
        'probHbb',
        'probHcc',
        'probHqq',
        'probHgg',
        'probQCD2hf',
        'probQCD1hf',
        'probQCD0hf',
        'masscorr'
    );
    process.pfParticleNetAK8LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParticleNetAK8/Puppi/PNETUL/ClassReg/preprocess.json');
    process.pfParticleNetAK8LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParticleNetAK8/Puppi/PNETUL/ClassReg/particle-net.onnx');

    pnetDiscriminatorNames.extend([
        "pfParticleNetAK8LastJetTags:probHtt",
        "pfParticleNetAK8LastJetTags:probHtm",
        "pfParticleNetAK8LastJetTags:probHte",
        "pfParticleNetAK8LastJetTags:probHbb",
        "pfParticleNetAK8LastJetTags:probHcc",
        "pfParticleNetAK8LastJetTags:probHqq",
        "pfParticleNetAK8LastJetTags:probHgg",
        "pfParticleNetAK8LastJetTags:probQCD2hf",
        "pfParticleNetAK8LastJetTags:probQCD1hf",
        "pfParticleNetAK8LastJetTags:probQCD0hf",
        "pfParticleNetAK8LastJetTags:masscorr"
    ])
    
    pnetDiscriminatorLabels = [name.replace("pfParticleNetAK8LastJetTags:","") for name in pnetDiscriminatorNames]

## ParT training
parTDiscriminatorNames = [];
parTDiscriminatorLabels = [];

if options.evaluateParTTraining:
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParTFeatureEvaluator_cfi import ParTFeatureEvaluator
    process.pfParTAK8LastJetTagInfos = ParTFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsAK8Calibrated" if options.applyJECs else "slimmedJetsAK8"),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.8),
        min_jet_pt = cms.double(options.jetPtMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin),
        min_pt_for_track_properties = cms.double(-1),
        min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
        max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
        min_pt_for_taus = cms.double(options.tauPtMin),
        max_eta_for_taus = cms.double(2.5),
        dump_feature_tree = cms.bool(False)
    )

    from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
    process.pfParTAK8LastJetTags = boostedJetONNXJetTagsProducer.clone();
    process.pfParTAK8LastJetTags.src = cms.InputTag("pfParTAK8LastJetTagInfos");
    process.pfParTAK8LastJetTags.flav_names = cms.vstring(
        'probHtt',
        'probHtm',
        'probHte',
        'probHbb',
        'probHcc',
        'probHqq',        
        'probHgg',
        'probQCD2hf',
        'probQCD1hf',
        'probQCD0hf',
        'masscorr'
    );
    process.pfParTAK8LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParTAK8/Puppi/ParTUL/ClassReg/preprocess.json');
    process.pfParTAK8LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParTAK8/Puppi/ParTUL/ClassReg/particle-transformer.onnx');

    parTDiscriminatorNames.extend([
        "pfParTAK8LastJetTags:probHtt",
        "pfParTAK8LastJetTags:probHtm",
        "pfParTAK8LastJetTags:probHte",
        "pfParTAK8LastJetTags:probHbb",
        "pfParTAK8LastJetTags:probHcc",
        "pfParTAK8LastJetTags:probHqq",
        "pfParTAK8LastJetTags:probHgg",
        "pfParTAK8LastJetTags:probQCD2hf",
        "pfParTAK8LastJetTags:probQCD1hf",
        "pfParTAK8LastJetTags:probQCD0hf",
        "pfParTAK8LastJetTags:masscorr"
    ])
    
    parTDiscriminatorLabels = [name.replace("pfParTAK8LastJetTags:","") for name in parTDiscriminatorNames]
 
    
## Update final jet collection                                                                                                                                                    
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsAK8Updated = updatedPatJets.clone(
    jetSource = "slimmedJetsAK8Calibrated" if options.applyJECs else "slimmedJetsAK8",
    addJetCorrFactors = False,
)
process.slimmedJetsAK8Updated.discriminatorSources += pnetDiscriminatorNames        
process.slimmedJetsAK8Updated.discriminatorSources += parTDiscriminatorNames        

### produce GEN jets with neutrinos
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
process.genParticlesForJets = genParticlesForJets.clone(
    src = cms.InputTag("packedGenParticles")
)

from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
process.ak8GenJetsWithNu = ak4GenJets.clone(
    src = "genParticlesForJets",
    rParam = cms.double(0.8)
)

## deep tau evaluation
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in paralle
tauIdEmbedder.runTauID()

tauBoostedIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTausBoosted",
    updatedTauName  = "slimmedTausBoostedUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p7"]) #other tauIDs can be added in paralle
tauBoostedIdEmbedder.runTauID()

#### Final Dumper
process.dnntree = cms.EDAnalyzer('TrainingTreeMakerAK8',
    #### General flags
    xsec = cms.double(options.xsec),
    isMC = cms.bool(options.isMC),
    saveLHEObjects = cms.bool(options.saveLHEObjects),
    dumpOnlyJetMatchedToGen  = cms.bool(options.dumpOnlyJetMatchedToGen),
    filterEvents = cms.bool(options.filterEventsInTheDumper),
    pnetDiscriminatorLabels = cms.vstring(),
    pnetDiscriminatorNames  = cms.vstring(),
    parTDiscriminatorLabels = cms.vstring(),
    parTDiscriminatorNames  = cms.vstring(),
    ### Object selection (applied on miniAOD, HLT and Gen objects for jets)
    muonPtMin         = cms.double(options.muonPtMin),
    muonEtaMax        = cms.double(2.4),
    electronPtMin     = cms.double(options.electronPtMin),
    electronEtaMax    = cms.double(2.5),
    photonPtMin       = cms.double(options.photonPtMin),
    phootonEtaMax     = cms.double(2.5),
    tauPtMin          = cms.double(options.tauPtMin),
    tauEtaMax         = cms.double(2.5),
    boostedTauPtMin   = cms.double(options.boostedTauPtMin),
    boostedTauEtaMax  = cms.double(2.5),
    jetPtMin          = cms.double(options.jetPtMin),
    jetSoftDropMassMin  = cms.double(options.jetSoftDropMassMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    jetEtaMin         = cms.double(options.jetEtaMin),
    jetPFCandidatePtMin = cms.double(options.jetPFCandidatePtMin),
    lostTrackPtMin    = cms.double(options.lostTrackPtMin),
    dRLostTrackJet    = cms.double(options.dRLostTrackJet),
    dRJetGenMatch     = cms.double(options.dRJetGenMatch),
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    genParticles      = cms.InputTag("prunedGenParticles"),
    lheInfo           = cms.InputTag("externalLHEProducer"),
    ### miniAOD objects
    triggerResults    = cms.InputTag("TriggerResults","", "HLT"),                                 
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    photons           = cms.InputTag("slimmedPhotons"),
    taus              = cms.InputTag("slimmedTausUpdated"),
    boostedTaus       = cms.InputTag("slimmedTausBoostedUpdated"),
    jets              = cms.InputTag("slimmedJetsAK8Updated"),
    subJetCollectionName = cms.string("SoftDropPuppi"),                                 
    met               = cms.InputTag("slimmedMETs"),
    lostTracks        = cms.InputTag("lostTracks"),
    ### Gen jets  
    genJetsWnu        = cms.InputTag("ak8GenJetsWithNu"),
    genJets           = cms.InputTag("slimmedGenJetsAK8"),
    genSoftDropSubJets = cms.InputTag("slimmedGenJetsAK8SoftDropSubJets")
)

for element in pnetDiscriminatorNames:
    process.dnntree.pnetDiscriminatorNames.append(element);
for element in pnetDiscriminatorLabels:
    process.dnntree.pnetDiscriminatorLabels.append(element);
for element in parTDiscriminatorNames:
    process.dnntree.parTDiscriminatorNames.append(element);
for element in parTDiscriminatorLabels:
    process.dnntree.parTDiscriminatorLabels.append(element);

process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

process.dnnTreePath = cms.Path(process.dnntree,process.edTask)

