import os
from CRABClient.UserUtilities import config

### General options
config = config()
config.General.transferOutputs = True
config.General.transferLogs = False
### Job type setting
basepath = os.getenv('CMSSW_BASE')+'/src/ParticleNetStudiesRun2/TrainingNtupleMakerAK8'
config.JobType.pluginName  = 'PrivateMC'
config.JobType.psetName    = basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/gen_step.py'
config.JobType.allowUndistributedCMSSW = True
config.JobType.disableAutomaticOutputCollection = True
config.JobType.maxMemoryMB = 5500
### custom script to be executed
config.JobType.scriptExe   = 'scriptExe.sh'
config.JobType.inputFiles  = [basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/scriptExe.sh',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/gen_step.py',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/sim_step.py',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/digi_raw_step.py',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/hlt_step.py',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/reco_step.py',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/miniaod_step.py',basepath+'/python/mcgeneration/ggXtoHH_mXscan_mHscan/pileup.txt']
## Data
config.Data.splitting     = 'EventBased'
config.Data.outLFNDirBase = '/store/user/rgerosa/PrivateMC/RunIISummer20UL18MiniAODv2/'
config.Data.outputDatasetTag = 'RunIISummer20UL18MiniAODv2_106X_upgrade2018_realistic-MINIAODSIM'
config.Site.storageSite   = 'T2_US_UCSD'
config.Data.publication   = True

from ParticleNetStudiesRun2.TrainingNtupleMakerAK8.mcgeneration.ggXtoHH_mXscan_mHscan.samples_training_generation import AddAllSamples
samples = {};
AddAllSamples(samples);
dset = os.getcwd().replace(os.path.dirname(os.getcwd())+'/','')
config.Data.outputPrimaryDataset = samples[dset][0]
config.JobType.scriptArgs = samples[dset][1]
config.Data.unitsPerJob   = samples[dset][2] 
config.Data.totalUnits    = samples[dset][3]
config.JobType.numCores   = samples[dset][4]
config.JobType.outputFiles = [samples[dset][5]]
config.JobType.scriptArgs.append('nEvents='+str(config.Data.unitsPerJob))
config.JobType.scriptArgs.append('nThreads='+str(samples[dset][4]))
config.JobType.scriptArgs.append('outputName='+str(samples[dset][5]))
config.JobType.pyCfgParams = ['nEvents='+str(config.Data.unitsPerJob),'nThreads='+str(samples[dset][4]),'outputName='+str(samples[dset][5])]

print ("Submitting jobs with splitting :"+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' totalUnits: '+str(config.Data.totalUnits),' primary dataset ',str(config.Data.outputPrimaryDataset))
print ('Submitting jobs with script args: '+' '.join(config.JobType.scriptArgs))
print ('Submitting pyCfgParams options: '+' '.join(config.JobType.pyCfgParams))
