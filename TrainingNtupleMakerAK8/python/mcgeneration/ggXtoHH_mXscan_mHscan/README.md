# KK-Graviton to HH production with variable mX and mH masses

## cmsDriver based steps for UL-18

Base `cmsRun` configurations for the various steps are taken from [McM](https://cms-pdmv.cern.ch/mcm/) for the `RunIISummer20UL18MiniAODv2` production chain. Each job will execute the following steps sequentially in order to obtain an output `miniAOD` file.
* **GEN STEP**: [gen_step.py](./gen_step.py) is used to produce with Pythia KK-Graviton production via ggX and qqX modes. Configurable parameters are
  * `jobEvents`: number of events produced within a certain run-id
  * `nEvents`: number of events that produced within the same lumi block
  * `jobNum`: crab job ID used to set the run id.
  * `generationStep`: step number used to set the lumi block id.
  * `nThreads`: number of parallel threads
  * `outputName`: name of the output GEN to be produced
  * `resonanceMassMin`: minimum mass of the Graviton (mX) that can be set
  * `resonanceMassMax`: minimum mass of the Graviton (mX) that can be set
  * `resonanceMassStepSize`: granularity of random sampling of mX in the Max-Min range
  * `higgsMassMin`: minimum mass of the Higgs boson (mH) that can be set
  * `higgsMassMax`: minimum mass of the Higgs boson (mH) that can be set
  * `higgsMassStepSize`: granularity of random sampling of mH in the Max-Min range
  * For every `cmsRun` execution one random mH and mX values are set
  * Higgs boson decay branching ratios are re-defined to have all the same probability. Possible BRs are: Hbb, Hcc, Hqq, Hgg, Hmumu, Hee, Htautau

* **SIM STEP**: [sim_step.py](./sim_step.py) is used to run SIM on the output of the GEN step. Configurable parameters are
  * `nThreads`: number of parallel threads
  * `inputFiles`: name of the input files containing GEN events
  * `outputName`: name of the output GEN-SIM file to be produced

* **DIG-RAW STEP**: [digi_raw_step.py](./digi_raw_step.py) is order to run DIGI-RAW on the output of the SIM step. Configurable parameters are
  * `nThreads`: number of parallel threads
  * `inputFiles`: name of the input files containing GEN-SIM events
  * `outputName`: name of the output GEN-SIM-RAW-DIGI file to be produced
  * `pileupName`: name of the pileup file that by default is `pileup.txt` obtained from
     ```sh
     dasgoclient --query "file dataset=/Neutrino_E-10_gun/RunIISummer20ULPrePremix-UL18_106X_upgrade2018_realistic_v11_L1v1-v2/PREMIX" > pileup.txt
    ```

* **HLT STEP**: [hlt_step.py](./hlt_step.py) is used to run HLT on the output of the DIGI-RAW step. Configurable parameters are
  * `nThreads`: number of parallel threads
  * `inputFiles`: name of the input files containing GEN-SIM-DIGI-RAW events
  * `outputName`: name of the output GEN-SIM-RAW-DIGI file to be produced

* **RECO STEP**: [reco_step.py](./reco_step.py) is used to run RECO on the output of the HLT step. Configurable parameters are
  * `nThreads`: number of parallel threads
  * `inputFiles`: name of the input files containing GEN-SIM-DIGI-RAW events
  * `outputName`: name of the output AODSIM file to be produced

* **MINIAOD STEP**: [miniaod_step.py](./miniaod_step.py) is used to run MINIAOD on the output of the RECO step. Configurable parameters are
  * `nThreads`: number of parallel threads
  * `inputFiles`: name of the input files containing RECO events
  * `outputName`: name of the output MINIAOD file to be produced

## CRAB submission and configuration files

Files needed to submit crab jobs for the private production:

* **CRAB configuration**: [crabConfig_training_gen_xtohh.py](../../../crab/crabConfig_training_gen_xtohh.py) is the crab configuration file needed for the production containing the following informations
  * Bash script that needs to be executed by every job [scriptExe.sh](./scriptExe.sh)
  * Configuration descriptor indicating base parameters for the crab production for each dataset [samples_training_generation.py](./samples_training_generation.py)
  * Files that needs to be transfer and included in the crab sandbox
  * Output storage element and path needed for the publication of miniAOD

* **Bash execution**: [scriptExe.sh](./scriptExe.sh) is the bash script that contain the `cmsRun` commands that each crab job needs to execute
  * Input parameters provided by crab from its configuration file and passed as command line argument to the bash script
  * Internally, based on the number of events per job and number of steps per job, the GEN step is divided in sub-tasks and, in each of them, different sets of mX and mH are considered

* **CRAB production descriptor**: [samples_training_generation.py](./samples_training_generation.py) is a descriptor (python dictionary) expressing for each samples to be produced
  * Name of the dataset
  * Arguments needed for the execution of the `cmsRun` steps previously descibed
  * Number of events per job
  * Total number of events
  * Number of threads per job
  * Name of the output file.

* **CRAB production submission**: eventually the submission of the CRAB production is performed by using [createCrabJob.py](../../createCrabJob.py) as follows
  ```sh
  cd $CMSSW_BASE/src/ParticleNetStudiesRun2/TrainingNtupleMakerAK8/python
  cmsenv
  python3 createCrabJob.py -s mcgeneration/ggXtoHH_mXscan_mHscan/samples_training_generation.py -t All -c  ../crab/crabConfig_training_gen_xtohh.py -e mcgeneration/ggXtoHH_mXscan_mHscan/scriptExe.sh -j ../crab/crab_generation_training -m submit
  ```