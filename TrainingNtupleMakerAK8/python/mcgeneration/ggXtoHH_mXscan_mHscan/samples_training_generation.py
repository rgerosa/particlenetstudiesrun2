def AddXtoHHSamples(samples):

    samples['XtoHH_pythia8_13TeV_part1'] = [
        'XtoHH_pythia8_13TeV_part1',
        ['resonanceMassMin=300','resonanceMassMax=900','resonanceMassStepSize=25','higgsMassMin=30','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=30'],
        1500, ## events per job
        3000000, ## total events
        4, ## number of cores
        'miniAOD.root' ## output file name
    ]

    samples['XtoHH_pythia8_13TeV_part2'] = [
        'XtoHH_pythia8_13TeV_part2',
        ['resonanceMassMin=900','resonanceMassMax=1400','resonanceMassStepSize=25','higgsMassMin=30','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=30'],
        1500,
        3000000,
        4, ## number of cores
        'miniAOD.root'
    ]

    samples['XtoHH_pythia8_13TeV_part3'] = [
        'XtoHH_pythia8_13TeV_part3',
        ['resonanceMassMin=1400','resonanceMassMax=2000','resonanceMassStepSize=25','higgsMassMin=30','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=30'],
        1500,
        3000000,
        4, ## number of cores
        'miniAOD.root'
    ]

    samples['XtoHH_pythia8_13TeV_part4'] = [
        'XtoHH_pythia8_13TeV_part4',
        ['resonanceMassMin=2000','resonanceMassMax=3000','resonanceMassStepSize=50','higgsMassMin=30','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=30'],
        1500,
        3000000,
        4, ## number of cores
        'miniAOD.root'
    ]

    samples['XtoHH_pythia8_13TeV_part5'] = [
        'XtoHH_pythia8_13TeV_part5',
        ['resonanceMassMin=3000','resonanceMassMax=6000','resonanceMassStepSize=50','higgsMassMin=30','higgsMassMax=220','higgsMassStepSize=1','nMassesPerJob=30'],
        1500,
        3000000,
        4, ## number of cores
        'miniAOD.root'
    ]

def AddAllSamples(samples):
    AddXtoHHSamples(samples)
