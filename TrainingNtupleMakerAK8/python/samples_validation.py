def AddDYSamples(samples):

    samples['DYJetsToLL_LHEFilterPtZ-100To250'] = [
        '/DYJetsToLL_LHEFilterPtZ-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=9.430e+01','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['DYJetsToLL_LHEFilterPtZ-250To400'] = [
        '/DYJetsToLL_LHEFilterPtZ-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.849e+00','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['DYJetsToLL_LHEFilterPtZ-400To650'] = [
        '/DYJetsToLL_LHEFilterPtZ-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=5.151e-01','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['DYJetsToLL_LHEFilterPtZ-650ToInf'] = [
        '/DYJetsToLL_LHEFilterPtZ-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.768e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]


def AddHiggsSamples(samples):

    samples['GluGluHToBB_M-125'] = [
        '/GluGluHToBB_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=28.29','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['VBFHToBB_M-125'] = [
        '/VBFHToBB_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.203','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['WminusH_HToBB_WToLNu_M-125'] = [
        '/WminusH_HToBB_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.104','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['WplusH_HToBB_WToLNu_M-125'] = [
        '/WplusH_HToBB_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.165','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['ZH_HToBB_ZToLL_M-125'] = [
        '/ZH_HToBB_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.0522','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples ['ZH_HToBB_ZToBB_M-125'] = [
        '/ZH_HToBB_ZToBB_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.0787','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluHToTauTau_M-125'] = [
        '/GluGluHToTauTau_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.047','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]
    
    samples['VBFHToTauTau_M125'] = [
        '/VBFHToTauTau_M125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.237','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['VBFHToCC_M-125'] = [     
        '/VBFHToCC_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.109','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]
        
    samples['WminusH_HToCC_WToLNu_M-125'] = [
        '/WminusH_HToCC_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.00518','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['WplusH_HToCC_WToLNu_M-125'] = [
        '/WplusH_HToCC_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.00816','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['ggZH_HToCC_ZToLL_M-125'] = [
        '/ggZH_HToCC_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.00107','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

def AddGravitonHHSamples (samples):
    
    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-1000'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-1000_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.169e-01','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-1250'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-1250_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=8.201e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-1500'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-1500_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.394e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-1750'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-1750_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.505e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-2000'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-2000_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=7.042e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-2500'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-2500_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.734e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo2B2Tau_M-3000'] = [
        '/GluGluToBulkGravitonToHHTo2B2Tau_M-3000_TuneCP5_PSWeights_narrow_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.743e-04','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo4B_M-1000'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-1000_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.172e-01','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo4B_M-1500'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-1500_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=8.201e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo4B_M-2000'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-2000_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=7.042e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo4B_M-2500'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-2500_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.733e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo4B_M-3000'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-3000_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.739e-04','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]


    samples['GluGluToBulkGravitonToHHTo4B_M-4000'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-4000_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.171e-05','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['GluGluToBulkGravitonToHHTo4B_M-5000'] = [
        '/GluGluToBulkGravitonToHHTo4B_M-5000_narrow_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.887e-06','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

def AddGravitonVVSamples(samples):

    samples['BulkGravToWWToWhadWhad_narrow_M-1000'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-1000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.226e-01','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]
    
    samples['BulkGravToWWToWhadWhad_narrow_M-1200'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-1200_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=9.447e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-1400'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-1400_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.389e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-1600'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-1600_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.166e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]
    
    samples['BulkGravToWWToWhadWhad_narrow_M-1800'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-1800_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.126e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-2000'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-2000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.078e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-2500'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-2500_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.468e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-3000'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-3000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.957e-04','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-3500'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-3500_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.145e-04','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-4000'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-4000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.455e-05','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-4500'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-4500_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.055e-05','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToWWToWhadWhad_narrow_M-5000'] = [
        '/BulkGravToWWToWhadWhad_narrow_M-5000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.186e-06','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-1000'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-1000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.245e-01','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]
    
    samples['BulkGravToZZToZhadZhad_narrow_M-1200'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-1200_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=5.238e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-1400'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-1400_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.421e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-1600'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-1600_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.195e-02','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-1800'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-1800_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.202e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-2000'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-2000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.338e-03','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-2500'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-2500_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=8.041e-04','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-3000'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-3000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.170e-04','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]
    
    samples['BulkGravToZZToZhadZhad_narrow_M-3500'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-3500_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.275e-05','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-4000'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-4000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.885e-05','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-4500'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-4500_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=5.776e-06','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]

    samples['BulkGravToZZToZhadZhad_narrow_M-5000'] = [
        '/BulkGravToZZToZhadZhad_narrow_M-5000_TuneCP5_13TeV-madgraph-pythia/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.749e-06','isMC=True','jetPtMin=180','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        ''
    ]


def AddAllSamples(samples):
    AddDYSamples(samples)
    AddHiggsSamples(samples)
    AddGravitonHHSamples(samples)
    AddGravitonVVSamples(samples)
