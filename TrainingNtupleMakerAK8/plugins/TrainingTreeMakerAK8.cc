// Standard C++ includes
#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>

#include <TPRegexp.h>

// ROOT includes
#include <TTree.h>
#include <TLorentzVector.h>

// CMSSW framework includes
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/ESGetToken.h"

// CMSSW data formats
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/METReco/interface/PFMET.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/Candidate/interface/VertexCompositePtrCandidate.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"

// Other relevant CMSSW includes
#include "CommonTools/UtilAlgos/interface/TFileService.h" 
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

// For dumping b-tagging info
#include "DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h"
#include "RecoBTag/FeatureTools/interface/TrackInfoBuilder.h"
#include "RecoVertex/VertexTools/interface/VertexDistanceXY.h"
#include "RecoVertex/VertexTools/interface/VertexDistance3D.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"

class TrainingTreeMakerAK8 : public edm::one::EDAnalyzer<edm::one::SharedResources, edm::one::WatchRuns> {

public:
  explicit TrainingTreeMakerAK8(const edm::ParameterSet&);
  ~TrainingTreeMakerAK8();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  bool applyJetID(const pat::Jet & jet, const std::string & level);

private:

  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;  
  virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
  virtual void endRun(edm::Run const&, edm::EventSetup const&) override;

  void initializeBranches();

  // MC information
  const edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupInfoToken;
  const edm::EDGetTokenT<reco::GenParticleCollection > gensToken;
  const edm::EDGetTokenT<GenEventInfoProduct> genEvtInfoToken;
  const edm::EDGetTokenT<LHEEventProduct> lheInfoToken;

  // Filter result
  const edm::InputTag filterResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> filterResultsToken;

  // Vertices from miniAOD
  const edm::EDGetTokenT<reco::VertexCollection > primaryVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> secondaryVerticesToken;
  edm::EDGetTokenT< double > rhoToken;

  // Pat objects from miniAOD
  const edm::EDGetTokenT<pat::MuonCollection> muonsToken;
  const edm::EDGetTokenT<pat::ElectronCollection> electronsToken;
  const edm::EDGetTokenT<pat::PhotonCollection> photonsToken;
  const edm::EDGetTokenT<pat::TauCollection> tausToken;
  const edm::EDGetTokenT<pat::TauCollection> boostedTausToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsToken;
  const std::string subJetCollectionName;
  const edm::EDGetTokenT<pat::METCollection> metToken;
  const edm::EDGetTokenT<pat::PackedCandidateCollection> lostTracksToken;

  // GEN level jets
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsWnuToken;
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsToken;
  const edm::EDGetTokenT<reco::GenJetCollection > genSoftDropSubJetsToken;

  // Transient track
  const edm::ESGetToken<TransientTrackBuilder, TransientTrackRecord> trackBuilderToken;

  // MET filters
  std::vector<std::string>  filterPathsVector;
  std::map<std::string,int> filterPathsMap;

  // Flag for the analyzer
  float muonPtMin;
  float muonEtaMax;
  float electronPtMin;
  float electronEtaMax;
  float photonPtMin;
  float photonEtaMax;
  float tauPtMin;
  float tauEtaMax;
  float boostedTauPtMin;
  float boostedTauEtaMax;
  float jetPtMin;
  float jetSoftDropMassMin;
  float jetEtaMax;
  float jetEtaMin;
  float jetPFCandidatePtMin;
  float dRJetGenMatch;
  bool  filterEvents;
  float lostTracksPtMin;
  float dRLostTrackJet;
  bool  dumpOnlyJetMatchedToGen;
  bool  saveLHEObjects;
  bool  isMC;

  std::vector<std::string> pnetDiscriminatorLabels;
  std::vector<std::string> pnetDiscriminatorNames;
  std::vector<std::string> parTDiscriminatorLabels;
  std::vector<std::string> parTDiscriminatorNames;
  
  // Event coordinates
  unsigned event, run, lumi;
  // Pileup information
  unsigned int putrue;
  // Flags for various event filters
  unsigned int flags;
  // Cross-section and event weight information for MC events
  float xsec, wgt;
  // Rho
  float rho;
  // LHE particles
  std::vector<float>  lhe_particle_pt;
  std::vector<float>  lhe_particle_eta;
  std::vector<float>  lhe_particle_phi;
  std::vector<float>  lhe_particle_mass;
  std::vector<int>    lhe_particle_id;
  std::vector<unsigned int>  lhe_particle_status;
  // Generator-level information (Gen and LHE particles)
  std::vector<float>  gen_particle_pt;
  std::vector<float>  gen_particle_eta;
  std::vector<float>  gen_particle_phi;
  std::vector<float>  gen_particle_mass;
  std::vector<int>    gen_particle_id;
  std::vector<unsigned int>  gen_particle_status;
  std::vector<int>    gen_particle_daughters_id;
  std::vector<unsigned int> gen_particle_daughters_igen;
  std::vector<unsigned int> gen_particle_daughters_status;
  std::vector<float>  gen_particle_daughters_pt;
  std::vector<float>  gen_particle_daughters_eta;
  std::vector<float>  gen_particle_daughters_phi;
  std::vector<float>  gen_particle_daughters_mass;
  std::vector<int>    gen_particle_daughters_charge;
  // Vertex RECO
  unsigned int npv;
  unsigned int nsv;
  // Collection of muon 4-vectors, muon ID bytes, muon isolation values
  std::vector<float>  muon_pt;
  std::vector<float>  muon_eta;
  std::vector<float>  muon_phi;
  std::vector<float>  muon_mass;
  std::vector<float>  muon_energy;
  std::vector<unsigned int> muon_id;
  std::vector<int>    muon_charge;
  std::vector<unsigned int> muon_iso;
  std::vector<float>  muon_d0;
  std::vector<float>  muon_dz;
  // Collection of electron 4-vectors, electron ID bytes, electron isolation values
  std::vector<float>  electron_pt;
  std::vector<float>  electron_pt_corr;
  std::vector<float>  electron_eta;
  std::vector<float>  electron_phi;
  std::vector<float>  electron_mass;
  std::vector<float>  electron_energy;
  std::vector<unsigned int> electron_id;
  std::vector<float>  electron_idscore;
  std::vector<int>    electron_charge;
  std::vector<float>  electron_d0;
  std::vector<float>  electron_dz;  
  // Collection of photon 4-vectors, photon ID bytes, photon isolation values                                                                                                                        
  std::vector<float>  photon_pt;
  std::vector<float>  photon_pt_corr;
  std::vector<float>  photon_eta;
  std::vector<float>  photon_phi;
  std::vector<float>  photon_mass;
  std::vector<float>  photon_energy;
  std::vector<unsigned int> photon_id;
  std::vector<float>  photon_idscore;
  // Collection of taus 4-vectors, tau ID bytes, tau isolation values
  std::vector<float>  tau_pt;
  std::vector<float>  tau_eta;
  std::vector<float>  tau_phi;
  std::vector<float>  tau_mass;
  std::vector<float>  tau_energy;
  std::vector<float>  tau_dxy;
  std::vector<float>  tau_dz;
  std::vector<unsigned int> tau_decaymode;
  std::vector<unsigned int> tau_idjet_wp;
  std::vector<unsigned int> tau_idmu_wp;
  std::vector<unsigned int> tau_idele_wp;
  std::vector<float>  tau_idjet;
  std::vector<float>  tau_idele;
  std::vector<float>  tau_idmu;
  std::vector<int>    tau_charge;  
  std::vector<float>  tau_genmatch_pt;
  std::vector<float>  tau_genmatch_eta;
  std::vector<float>  tau_genmatch_phi;
  std::vector<float>  tau_genmatch_mass;
  std::vector<int> tau_genmatch_decaymode;
  // Collection of boosted taus 4-vectors, tau ID bytes, tau isolation values
  std::vector<float>  tau_boosted_pt;
  std::vector<float>  tau_boosted_eta;
  std::vector<float>  tau_boosted_phi;
  std::vector<float>  tau_boosted_mass;
  std::vector<float>  tau_boosted_energy;
  std::vector<float>  tau_boosted_dxy;
  std::vector<float>  tau_boosted_dz;
  std::vector<unsigned int> tau_boosted_decaymode;
  std::vector<unsigned int> tau_boosted_idjet_wp;
  std::vector<unsigned int> tau_boosted_idmu_wp;
  std::vector<unsigned int> tau_boosted_idele_wp;
  std::vector<float>  tau_boosted_idjet;
  std::vector<float>  tau_boosted_idele;
  std::vector<float>  tau_boosted_idmu;
  std::vector<int>    tau_boosted_charge;  
  std::vector<int>    tau_boosted_ncand;  
  std::vector<float>  tau_boosted_genmatch_pt;
  std::vector<float>  tau_boosted_genmatch_eta;
  std::vector<float>  tau_boosted_genmatch_phi;
  std::vector<float>  tau_boosted_genmatch_mass;
  std::vector<int> tau_boosted_genmatch_decaymode;
  // MET
  float met, met_phi;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_pt;
  std::vector<float> jet_eta;
  std::vector<float> jet_phi;
  std::vector<float> jet_mass;
  std::vector<float> jet_pt_raw;
  std::vector<float> jet_mass_raw;
  std::vector<float> jet_chf;
  std::vector<float> jet_nhf;
  std::vector<float> jet_elf;
  std::vector<float> jet_phf;
  std::vector<float> jet_muf;
  std::vector<float> jet_pnet_probHbb;
  std::vector<float> jet_pnet_probHcc;
  std::vector<float> jet_pnet_probHqq;
  std::vector<float> jet_pnet_probQCDbb;
  std::vector<float> jet_pnet_probQCDcc;
  std::vector<float> jet_pnet_probQCDb;
  std::vector<float> jet_pnet_probQCDc;
  std::vector<float> jet_pnet_probQCDothers;
  std::vector<float> jet_pnet_regmass;
  std::map<std::string,std::vector<float> > jet_pnetlast_score;
  std::map<std::string,std::vector<float> > jet_parTlast_score;
  std::vector<unsigned int> jet_id;
  std::vector<unsigned int> jet_ncand;
  std::vector<unsigned int> jet_nch;
  std::vector<unsigned int> jet_nnh;
  std::vector<unsigned int> jet_nel;
  std::vector<unsigned int> jet_nph;
  std::vector<unsigned int> jet_nmu;
  std::vector<unsigned int> jet_hflav;
  std::vector<int> jet_pflav;
  std::vector<unsigned int> jet_nbhad;
  std::vector<unsigned int> jet_nchad;
  std::vector<float> jet_genmatch_pt;
  std::vector<float> jet_genmatch_eta;
  std::vector<float> jet_genmatch_phi;
  std::vector<float> jet_genmatch_mass;
  std::vector<float> jet_genmatch_wnu_pt;
  std::vector<float> jet_genmatch_wnu_eta;
  std::vector<float> jet_genmatch_wnu_phi;
  std::vector<float> jet_genmatch_wnu_mass;
  // substructure
  std::vector<float> jet_softdrop_pt;
  std::vector<float> jet_softdrop_eta;
  std::vector<float> jet_softdrop_phi;
  std::vector<float> jet_softdrop_mass;
  std::vector<float> jet_softdrop_pt_raw;
  std::vector<float> jet_softdrop_mass_raw;
  std::vector<float> jet_softdrop_genmatch_pt;
  std::vector<float> jet_softdrop_genmatch_eta;
  std::vector<float> jet_softdrop_genmatch_phi;
  std::vector<float> jet_softdrop_genmatch_mass;  
  std::vector<float> jet_softdrop_subjet_pt;
  std::vector<float> jet_softdrop_subjet_eta;
  std::vector<float> jet_softdrop_subjet_phi;
  std::vector<float> jet_softdrop_subjet_mass;
  std::vector<float> jet_softdrop_subjet_pt_raw;
  std::vector<float> jet_softdrop_subjet_mass_raw;
  std::vector<unsigned int> jet_softdrop_subjet_nbhad;
  std::vector<unsigned int> jet_softdrop_subjet_nchad;
  std::vector<unsigned int> jet_softdrop_subjet_hflav;
  std::vector<int> jet_softdrop_subjet_pflav;
  std::vector<unsigned int> jet_softdrop_subjet_ijet; 
  std::vector<float> jet_softdrop_subjet_genmatch_pt;
  std::vector<float> jet_softdrop_subjet_genmatch_eta;
  std::vector<float> jet_softdrop_subjet_genmatch_phi;
  std::vector<float> jet_softdrop_subjet_genmatch_mass;
  // jet pf candidates
  std::vector<float> jet_pfcandidate_pt;
  std::vector<float> jet_pfcandidate_eta;
  std::vector<float> jet_pfcandidate_phi; 
  std::vector<float> jet_pfcandidate_mass;
  std::vector<float> jet_pfcandidate_energy;
  std::vector<float> jet_pfcandidate_calofraction;
  std::vector<float> jet_pfcandidate_hcalfraction;
  std::vector<float> jet_pfcandidate_dz;
  std::vector<float> jet_pfcandidate_dxy;
  std::vector<float> jet_pfcandidate_dzsig;
  std::vector<float> jet_pfcandidate_dxysig;
  std::vector<unsigned int> jet_pfcandidate_frompv;
  std::vector<unsigned int> jet_pfcandidate_nhits;
  std::vector<unsigned int> jet_pfcandidate_npixhits;
  std::vector<unsigned int> jet_pfcandidate_nstriphits;
  std::vector<unsigned int> jet_pfcandidate_nlosthits;
  std::vector<unsigned int> jet_pfcandidate_npixlayers;
  std::vector<unsigned int> jet_pfcandidate_nstriplayers;
  std::vector<unsigned int> jet_pfcandidate_id;
  std::vector<unsigned int> jet_pfcandidate_ijet;
  std::vector<int> jet_pfcandidate_charge;
  std::vector<float> jet_pfcandidate_puppiw;
  std::vector<float> jet_pfcandidate_candjet_pperp_ratio;
  std::vector<float> jet_pfcandidate_candjet_ppara_ratio;
  std::vector<float> jet_pfcandidate_candjet_deta;
  std::vector<float> jet_pfcandidate_candjet_dphi;
  std::vector<float> jet_pfcandidate_candjet_etarel;
  std::vector<unsigned int> jet_pfcandidate_track_chi2;
  std::vector<unsigned int> jet_pfcandidate_track_qual;
  std::vector<unsigned int> jet_pfcandidate_track_algo;
  std::vector<float> jet_pfcandidate_track_pterr;
  std::vector<float> jet_pfcandidate_track_etaerr;
  std::vector<float> jet_pfcandidate_track_phierr;
  std::vector<float> jet_pfcandidate_trackjet_d3d;
  std::vector<float> jet_pfcandidate_trackjet_d3dsig;
  std::vector<float> jet_pfcandidate_trackjet_dist;
  std::vector<float> jet_pfcandidate_trackjet_decayL;
  std::vector<unsigned int> jet_pfcandidate_tau_signal;
  std::vector<unsigned int> jet_pfcandidate_tau_boosted_signal;
  std::vector<unsigned int> jet_pfcandidate_muon_id;
  std::vector<float> jet_pfcandidate_muon_chi2;
  std::vector<unsigned int> jet_pfcandidate_muon_isglobal;
  std::vector<unsigned int> jet_pfcandidate_muon_nvalidhit;
  std::vector<unsigned int> jet_pfcandidate_muon_nstation;
  std::vector<float> jet_pfcandidate_muon_segcomp;
  std::vector<float> jet_pfcandidate_electron_eOverP;
  std::vector<float> jet_pfcandidate_electron_detaIn;
  std::vector<float> jet_pfcandidate_electron_dphiIn;
  std::vector<float> jet_pfcandidate_electron_r9;
  std::vector<float> jet_pfcandidate_electron_sigIetaIeta;
  std::vector<float> jet_pfcandidate_electron_sigIphiIphi;
  std::vector<float> jet_pfcandidate_electron_convProb;
  std::vector<float> jet_pfcandidate_photon_sigIetaIeta;
  std::vector<float> jet_pfcandidate_photon_eVeto;
  std::vector<float> jet_pfcandidate_photon_r9;
  // jet-to-secondary vertex
  std::vector<float> jet_sv_pt;
  std::vector<float> jet_sv_eta;
  std::vector<float> jet_sv_phi;
  std::vector<float> jet_sv_mass;
  std::vector<float> jet_sv_energy;
  std::vector<float> jet_sv_chi2;
  std::vector<float> jet_sv_dxy;
  std::vector<float> jet_sv_dxysig;
  std::vector<float> jet_sv_d3d;
  std::vector<float> jet_sv_d3dsig;
  std::vector<unsigned int> jet_sv_ntrack;
  std::vector<unsigned int> jet_sv_ijet;
  // jet lost tracks candidates                                                                                                                                                                       
  std::vector<float> jet_losttrack_pt;
  std::vector<float> jet_losttrack_eta;
  std::vector<float> jet_losttrack_phi;
  std::vector<float> jet_losttrack_mass;
  std::vector<float> jet_losttrack_energy;
  std::vector<float> jet_losttrack_dz;
  std::vector<float> jet_losttrack_dxy;
  std::vector<float> jet_losttrack_dzsig;
  std::vector<float> jet_losttrack_dxysig;
  std::vector<unsigned int> jet_losttrack_frompv;
  std::vector<unsigned int> jet_losttrack_nhits;
  std::vector<unsigned int> jet_losttrack_npixhits;
  std::vector<unsigned int> jet_losttrack_nstriphits;
  std::vector<unsigned int> jet_losttrack_nlosthits;
  std::vector<unsigned int> jet_losttrack_npixlayers;
  std::vector<unsigned int> jet_losttrack_nstriplayers;
  std::vector<unsigned int> jet_losttrack_ijet;
  std::vector<int> jet_losttrack_charge;
  std::vector<float> jet_losttrack_candjet_deta;
  std::vector<float> jet_losttrack_candjet_dphi;
  std::vector<float> jet_losttrack_candjet_etarel;
  std::vector<unsigned int> jet_losttrack_track_chi2;
  std::vector<unsigned int> jet_losttrack_track_qual;
  std::vector<unsigned int> jet_losttrack_track_algo;
  std::vector<float> jet_losttrack_track_pterr;
  std::vector<float> jet_losttrack_track_etaerr;
  std::vector<float> jet_losttrack_track_phierr;
  std::vector<float> jet_losttrack_trackjet_d3d;
  std::vector<float> jet_losttrack_trackjet_d3dsig;
  std::vector<float> jet_losttrack_trackjet_dist;
  std::vector<float> jet_losttrack_trackjet_decayL;

  // TTree carrying the event weight information
  TTree* tree;

  // Sorters to order object collections in decreasing order of pT
  template<typename T> 
  class PatPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i.pt() > j.pt());
    }
  };

  PatPtSorter<pat::Muon>     muonSorter;
  PatPtSorter<pat::Electron> electronSorter;
  PatPtSorter<pat::Photon>   photonSorter;
  PatPtSorter<pat::Tau>      tauSorter;
  PatPtSorter<pat::PackedCandidate>  packedPFCandidateSorter;

  template<typename T>
  class CandPtrPtSorter {
  public:
    bool operator()(const T & i, const T & j) const {
      return (i->pt() > j->pt());
    }
  };
  CandPtrPtSorter<reco::CandidatePtr>  candidatePtrSorter;

  template<typename T> 
  class PatRefPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i->pt() > j->pt());
    }
  };

  PatRefPtSorter<pat::JetRef>  jetRefSorter;
  PatRefPtSorter<reco::GenJetRef> genJetRefSorter;
  PatRefPtSorter<edm::Ptr<pat::Jet> > jetPtrSorter;

  template<typename T, typename A>
  static Measurement1D vertexDxy(const T &i, const A & j){
    VertexDistanceXY dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.distance(svtx,j);
  }

  template<typename T, typename A>
  static Measurement1D vertexDxySigned(const T &i, const A & j, const GlobalVector & ref){
    VertexDistanceXY dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.signedDistance(svtx,j,ref);
  }


  template<typename T, typename A>
  static Measurement1D vertexD3d(const T &i, const A & j){
    VertexDistance3D dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.distance(svtx,j);
  }

  template<typename T, typename A>
  static Measurement1D vertexD3dSigned(const T &i, const A & j, const GlobalVector & ref){
    VertexDistance3D dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.signedDistance(svtx,j,ref);
  }


  template<typename T, typename A>
  class VertexSorterDxy {
  public:
    
    VertexSorterDxy (const A & pv):
      pv_(pv){};

    bool operator()(const T& i, const T& j) {      
      Measurement1D idxy = vertexDxy<T,A>(i,pv_);
      Measurement1D jdxy = vertexDxy<T,A>(j,pv_);
      float isig = abs(idxy.significance());
      float jsig = abs(jdxy.significance());
      if(std::isinf(isig) or std::isnan(isig)) isig = 0;
      if(std::isinf(jsig) or std::isnan(jsig)) jsig = 0;
      return isig > jsig;
    }

  private :
    A pv_;
  };
      
};


TrainingTreeMakerAK8::TrainingTreeMakerAK8(const edm::ParameterSet& iConfig): 
  pileupInfoToken          (mayConsume<std::vector<PileupSummaryInfo> > (iConfig.getParameter<edm::InputTag>("pileUpInfo"))),
  gensToken                (mayConsume<reco::GenParticleCollection>     (iConfig.getParameter<edm::InputTag>("genParticles"))),
  genEvtInfoToken          (mayConsume<GenEventInfoProduct>           (iConfig.getParameter<edm::InputTag>("genEventInfo"))),
  lheInfoToken             (mayConsume<LHEEventProduct>               (iConfig.getParameter<edm::InputTag>("lheInfo"))),
  filterResultsTag         (iConfig.getParameter<edm::InputTag>("filterResults")),
  filterResultsToken       (consumes<edm::TriggerResults>      (filterResultsTag)),
  primaryVerticesToken     (consumes<reco::VertexCollection>           (iConfig.getParameter<edm::InputTag>("pVertices"))),
  secondaryVerticesToken   (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("sVertices"))),
  rhoToken                 (consumes<double>(iConfig.getParameter<edm::InputTag>("rho"))),
  muonsToken               (consumes<pat::MuonCollection>             (iConfig.getParameter<edm::InputTag>("muons"))), 
  electronsToken           (consumes<pat::ElectronCollection>         (iConfig.getParameter<edm::InputTag>("electrons"))), 
  photonsToken             (consumes<pat::PhotonCollection>         (iConfig.getParameter<edm::InputTag>("photons"))),
  tausToken                (consumes<pat::TauCollection>              (iConfig.getParameter<edm::InputTag>("taus"))), 
  boostedTausToken         (consumes<pat::TauCollection>              (iConfig.getParameter<edm::InputTag>("boostedTaus"))), 
  jetsToken                (consumes<pat::JetCollection >             (iConfig.getParameter<edm::InputTag>("jets"))),
  subJetCollectionName     (iConfig.existsAs<std::string>("subJetCollectionName") ? iConfig.getParameter<std::string>("subJetCollectionName") : "SoftDropPuppi"),
  metToken                 (consumes<pat::METCollection >             (iConfig.getParameter<edm::InputTag>("met"))),
  lostTracksToken          (consumes<pat::PackedCandidateCollection>  (iConfig.getParameter<edm::InputTag>("lostTracks"))),
  genJetsWnuToken          (mayConsume<reco::GenJetCollection >       (iConfig.getParameter<edm::InputTag>("genJetsWnu"))),
  genJetsToken             (consumes<reco::GenJetCollection >         (iConfig.getParameter<edm::InputTag>("genJets"))),
  genSoftDropSubJetsToken  (consumes<reco::GenJetCollection >         (iConfig.getParameter<edm::InputTag>("genSoftDropSubJets"))),
  trackBuilderToken        (esConsumes<TransientTrackBuilder, TransientTrackRecord>(edm::ESInputTag("","TransientTrackBuilder"))), 
  muonPtMin                (iConfig.existsAs<double>("muonPtMin")    ? iConfig.getParameter<double>("muonPtMin") : 15.),
  muonEtaMax               (iConfig.existsAs<double>("muonEtaMax")   ? iConfig.getParameter<double>("muonEtaMax") : 2.4),
  electronPtMin            (iConfig.existsAs<double>("electronPtMin")   ? iConfig.getParameter<double>("electronPtMin") : 15.),
  electronEtaMax           (iConfig.existsAs<double>("electronEtaMax")  ? iConfig.getParameter<double>("electronEtaMax") : 2.5),
  photonPtMin              (iConfig.existsAs<double>("photonPtMin")   ? iConfig.getParameter<double>("photonPtMin") : 15.),
  photonEtaMax             (iConfig.existsAs<double>("photonEtaMax")  ? iConfig.getParameter<double>("photonEtaMax") : 2.5),
  tauPtMin                 (iConfig.existsAs<double>("tauPtMin")   ? iConfig.getParameter<double>("tauPtMin") : 20.),
  tauEtaMax                (iConfig.existsAs<double>("tauEtaMax")   ? iConfig.getParameter<double>("tauEtaMax") : 2.5),
  boostedTauPtMin          (iConfig.existsAs<double>("boostedTauPtMin")   ? iConfig.getParameter<double>("boostedTauPtMin") : 100.),
  boostedTauEtaMax         (iConfig.existsAs<double>("boostedTauEtaMax")  ? iConfig.getParameter<double>("boostedTauEtaMax") : 2.5),
  jetPtMin                 (iConfig.existsAs<double>("jetPtMin")       ? iConfig.getParameter<double>("jetPtMin") : 200.),
  jetSoftDropMassMin       (iConfig.existsAs<double>("jetSoftDropMassMin")       ? iConfig.getParameter<double>("jetSoftDropMassMin") : 0.),
  jetEtaMax                (iConfig.existsAs<double>("jetEtaMax")      ? iConfig.getParameter<double>("jetEtaMax") : 2.5),
  jetEtaMin                (iConfig.existsAs<double>("jetEtaMin")      ? iConfig.getParameter<double>("jetEtaMin") : 0.0),
  jetPFCandidatePtMin      (iConfig.existsAs<double>("jetPFCandidatePtMin")    ? iConfig.getParameter<double>("jetPFCandidatePtMin") : 0.1),
  dRJetGenMatch            (iConfig.existsAs<double>("dRJetGenMatch")    ? iConfig.getParameter<double>("dRJetGenMatch") : 0.8),
  filterEvents             (iConfig.existsAs<bool>("filterEvents") ? iConfig.getParameter<bool>  ("filterEvents") : true),
  lostTracksPtMin          (iConfig.existsAs<double>("lostTracksPtMin")    ? iConfig.getParameter<double>("lostTracksPtMin") : 1.0),
  dRLostTrackJet           (iConfig.existsAs<double>("dRLostTrackJet")    ? iConfig.getParameter<double>("dRLostTrackJet") : 0.4),
  dumpOnlyJetMatchedToGen  (iConfig.existsAs<bool>("dumpOnlyJetMatchedToGen")  ? iConfig.getParameter<bool>  ("dumpOnlyJetMatchedToGen") : false),
  saveLHEObjects           (iConfig.existsAs<bool>("saveLHEObjects")  ? iConfig.getParameter<bool>  ("saveLHEObjects") : false),
  isMC                     (iConfig.existsAs<bool>("isMC")   ? iConfig.getParameter<bool>  ("isMC") : true),
  pnetDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorLabels") : std::vector<std::string> ()),
  pnetDiscriminatorNames  (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorNames") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorNames") : std::vector<std::string> ()),
  parTDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("parTDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("parTDiscriminatorLabels") : std::vector<std::string> ()),
  parTDiscriminatorNames  (iConfig.existsAs<std::vector<std::string> > ("parTDiscriminatorNames") ? iConfig.getParameter<std::vector<std::string>>("parTDiscriminatorNames") : std::vector<std::string> ()),
  xsec                     (iConfig.existsAs<double>("xsec") ? iConfig.getParameter<double>("xsec") * 1000.0 : 1.){

  usesResource("TFileService");

}

TrainingTreeMakerAK8::~TrainingTreeMakerAK8() {}

void TrainingTreeMakerAK8::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  // MET filters
  edm::Handle<edm::TriggerResults>      filterResultsH;
  iEvent.getByToken(filterResultsToken, filterResultsH); 

  // Vertexes
  edm::Handle<reco::VertexCollection > primaryVerticesH;
  iEvent.getByToken(primaryVerticesToken, primaryVerticesH);

  edm::Handle<reco::VertexCompositePtrCandidateCollection> secondaryVerticesH;
  iEvent.getByToken(secondaryVerticesToken, secondaryVerticesH);
  reco::VertexCompositePtrCandidateCollection svColl = *secondaryVerticesH;

  edm::Handle<double> rho_val;
  iEvent.getByToken(rhoToken,rho_val);  

  // Muons / Electrons / Taus
  edm::Handle<pat::MuonCollection> muonsH;
  iEvent.getByToken(muonsToken,muonsH);
  pat::MuonCollection muonsColl = *muonsH;

  edm::Handle<pat::ElectronCollection> electronsH;
  iEvent.getByToken(electronsToken,electronsH);
  pat::ElectronCollection electronsColl = *electronsH;

  edm::Handle<pat::PhotonCollection> photonsH;
  iEvent.getByToken(photonsToken,photonsH);
  pat::PhotonCollection photonsColl = *photonsH;

  edm::Handle<pat::TauCollection> tausH;
  iEvent.getByToken(tausToken,tausH);
  pat::TauCollection tausColl = *tausH;

  edm::Handle<pat::TauCollection> boostedTausH;
  iEvent.getByToken(boostedTausToken,boostedTausH);
  pat::TauCollection boostedTausColl = *boostedTausH;

  // Jets 
  edm::Handle<pat::JetCollection> jetsH;
  iEvent.getByToken(jetsToken, jetsH);

  // MET 
  edm::Handle<pat::METCollection> metH;
  iEvent.getByToken(metToken, metH);

  // Lost tracks                                                                                                                                                                                      
  edm::Handle<pat::PackedCandidateCollection> lostTracksH;
  iEvent.getByToken(lostTracksToken, lostTracksH);

  // GEN Level info and LHE
  edm::Handle<std::vector<PileupSummaryInfo> > pileupInfoH;
  edm::Handle<GenEventInfoProduct> genEvtInfoH;
  edm::Handle<reco::GenParticleCollection> gensH;
  edm::Handle<LHEEventProduct> lheInfoH;
  edm::Handle<reco::GenJetCollection> genJetsWnuH;
  edm::Handle<reco::GenJetCollection> genJetsH;
  edm::Handle<reco::GenJetCollection> genSoftDropSubJetsH;

  if(isMC){
    iEvent.getByToken(pileupInfoToken, pileupInfoH);  
    iEvent.getByToken(genEvtInfoToken, genEvtInfoH);  
    iEvent.getByToken(gensToken, gensH);
    iEvent.getByToken(lheInfoToken,lheInfoH);
    iEvent.getByToken(genJetsWnuToken, genJetsWnuH);
    iEvent.getByToken(genJetsToken, genJetsH);
    iEvent.getByToken(genSoftDropSubJetsToken, genSoftDropSubJetsH);
  }

  edm::ESHandle<TransientTrackBuilder> trackBuilderH;
  trackBuilderH = iSetup.getHandle(trackBuilderToken);
  
  initializeBranches();

  // apply event filters
  if(filterEvents){
    bool acceptEvent = false;
    for (auto jets_iter = jetsH->begin(); jets_iter != jetsH->end(); ++jets_iter) { 
      if(jets_iter->pt() > jetPtMin and
	 fabs(jets_iter->eta()) < jetEtaMax and
	 jets_iter->userFloat("ak8PFJetsPuppiSoftDropMass") > jetSoftDropMassMin)
	acceptEvent = true;
    }
    if(not acceptEvent) return;
  }

  // Event information - MC weight, event ID (run, lumi, event) and so on
  event = iEvent.id().event();
  run   = iEvent.id().run();
  lumi  = iEvent.luminosityBlock();

  // MC weight
  wgt = 1.0;
  if(lheInfoH.isValid() and not lheInfoH->weights().empty())
    wgt = lheInfoH->weights()[0].wgt;
  else if(genEvtInfoH.isValid())
    wgt = genEvtInfoH->weight(); 
 
  // Pileup information
  putrue = 0;
  if(pileupInfoH.isValid()){
    for (auto pileupInfo_iter = pileupInfoH->begin(); pileupInfo_iter != pileupInfoH->end(); ++pileupInfo_iter) {
      if (pileupInfo_iter->getBunchCrossing() == 0) 
	putrue = (unsigned int) pileupInfo_iter->getTrueNumInteractions();
    }
  }
    
  // Vertices RECO
  npv  = (unsigned int) primaryVerticesH->size();
  nsv  = (unsigned int) secondaryVerticesH->size();
  
  // Rho value
  rho = *rho_val;

  // Missing energy
  met      = metH->front().corPt();
  met_phi  = metH->front().corPhi();

  // MET filter info
  unsigned int flagvtx       = 1 * 1;
  unsigned int flaghalo      = 1 * 2;
  unsigned int flaghbhe      = 1 * 4;
  unsigned int flaghbheiso   = 1 * 8;
  unsigned int flagecaltp    = 1 * 16;
  unsigned int flagbadmuon   = 1 * 32; 
  unsigned int flagbadhad    = 1 * 64;
 
  // Which MET filters passed
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    if (filterPathsMap[filterPathsVector[i]] == -1) continue;
    if (i == 0  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagvtx       = 0; // goodVertices
    if (i == 1  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghalo      = 0; // CSCTightHaloFilter
    if (i == 2  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbhe      = 0; // HBHENoiseFilter
    if (i == 3  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbheiso   = 0; // HBHENoiseIsoFilter
    if (i == 4  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagecaltp    = 0; // EcalDeadCellTriggerPrimitiveFilter
    if (i == 5  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadmuon   = 0; // badmuon
    if (i == 6  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadhad    = 0; // badhadrons
  }

  flags = flagvtx + flaghalo + flaghbhe + flaghbheiso + flagecaltp + flagbadmuon + flagbadhad;

  // LHE level information
  if(lheInfoH.isValid() and saveLHEObjects){
    const auto& hepeup = lheInfoH->hepeup();
    const auto& pup    = hepeup.PUP;
    for (unsigned int i = 0, n = pup.size(); i < n; ++i) {
      int status = hepeup.ISTUP[i];
      int id     = hepeup.IDUP[i];
      TLorentzVector p4(pup[i][0], pup[i][1], pup[i][2], pup[i][3]);
      if(abs(id) == 25 or abs(id) == 23 or abs(id) == 24 or abs(id)==6){ // Higgs, Z or W or top                                                                                             
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 10 && abs(id) <= 17 and status == 1){ // final state leptons + neutrinos
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 1 && abs(id) <= 5 and status == 1){ // final state quarks                                                                                                    
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
	lhe_particle_id.push_back(id);
      }
      else if(abs(id) == 21 and status == 1){ // final state gluons                                                                                                               
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
    }
  }


  // GEN particle info
  if(gensH.isValid()){
    unsigned int igen = 0;
    for (auto gens_iter = gensH->begin(); gens_iter != gensH->end(); ++gens_iter) {      
      // Higgs, Z and W beforethe decay and after the shower effects
      if((abs(gens_iter->pdgId()) == 25 or abs(gens_iter->pdgId()) == 24 or abs(gens_iter->pdgId()) == 23) and	
	 gens_iter->isLastCopy() and 
	 gens_iter->statusFlags().fromHardProcess()){ 
	
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

        for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
          gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
          gen_particle_daughters_igen.push_back(igen);
          gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
          gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
          gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
          gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
        }
        igen++;
      }
      
      // Final states Leptons (e,mu) and Neutrinos --> exclude taus. They need to be prompt or from Tau decay      
      if (abs(gens_iter->pdgId()) > 10 and abs(gens_iter->pdgId()) < 17 and abs(gens_iter->pdgId()) != 15  and 
	  (gens_iter->isPromptFinalState() or 
	   gens_iter->isDirectPromptTauDecayProductFinalState())) { 

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// No need to save daughters here
        igen++;
      }
      
      // Final state quarks or gluons from the hard process before the shower --> partons in which H/Z/W/top decay into
      if (((abs(gens_iter->pdgId()) >= 1 and abs(gens_iter->pdgId()) <= 5) or abs(gens_iter->pdgId()) == 21) and 
	  gens_iter->statusFlags().fromHardProcess() and 
	  gens_iter->statusFlags().isFirstCopy()){
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());
	igen++;
	// no need to save daughters
      }

      // Special case of taus: last-copy, from hard process and, prompt and decayed
      if(abs(gens_iter->pdgId()) == 15 and 
	 gens_iter->isLastCopy() and
	 gens_iter->statusFlags().fromHardProcess() and
	 gens_iter->isPromptDecayed()){ // hadronic taus

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// only store the final decay particles
	for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
	  if(not dynamic_cast<const reco::GenParticle*>(gens_iter->daughter(idau))->statusFlags().isPromptTauDecayProduct()) continue;
	  gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
	  gen_particle_daughters_igen.push_back(igen);
	  gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
	  gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
	  gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
	  gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());	    
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
	}
	igen++;
      }	
    }
  }

  // Sorting muons based on pT
  sort(muonsColl.begin(),muonsColl.end(),muonSorter);

  for (size_t i = 0; i < muonsColl.size(); i++) {

    if(muonsColl[i].pt() < muonPtMin) continue;
    if(fabs(muonsColl[i].eta()) > muonEtaMax) continue;

    muon_pt.push_back(muonsColl[i].pt());
    muon_eta.push_back(muonsColl[i].eta());
    muon_phi.push_back(muonsColl[i].phi());
    muon_mass.push_back(muonsColl[i].mass());
    muon_energy.push_back(muonsColl[i].energy());
  
    // Muon isolation
    int isoval = 0;
    if(muonsColl[i].passed(reco::Muon::PFIsoLoose))
      isoval += 1;
    if(muonsColl[i].passed(reco::Muon::PFIsoMedium))
      isoval += 2;
    if(muonsColl[i].passed(reco::Muon::PFIsoTight))
      isoval += 4;
    if(muonsColl[i].passed(reco::Muon::PFIsoVeryTight))
      isoval += 8;
    if(muonsColl[i].passed(reco::Muon::MiniIsoLoose))
      isoval += 16;
    if(muonsColl[i].passed(reco::Muon::MiniIsoMedium))
      isoval += 32;
    if(muonsColl[i].passed(reco::Muon::MiniIsoTight))
      isoval += 64;
    
    muon_iso.push_back(isoval);
    
    // Muon id
    int midval = 0;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdLoose))
       midval += 1;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdMedium))
       midval += 2;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdTight))
       midval += 4;
    if(muonsColl[i].passed(reco::Muon::MvaLoose))
       midval += 8;
    if(muonsColl[i].passed(reco::Muon::MvaMedium))
       midval += 16;
    if(muonsColl[i].passed(reco::Muon::MvaTight))
       midval += 32;
    
    muon_id.push_back(midval);
    muon_charge.push_back(muonsColl[i].charge());
    muon_d0.push_back(muonsColl[i].muonBestTrack()->dxy(primaryVerticesH->at(0).position()));
    muon_dz.push_back(muonsColl[i].muonBestTrack()->dz(primaryVerticesH->at(0).position()));
  }
  
  // Sorting electrons based on pT
  sort(electronsColl.begin(),electronsColl.end(),electronSorter);

  for (size_t i = 0; i < electronsColl.size(); i++) {

    if(electronsColl[i].pt() < electronPtMin) continue;
    if(fabs(electronsColl[i].eta()) > electronEtaMax) continue;
   
    electron_pt.push_back(electronsColl[i].pt());
    if(electronsColl[i].hasUserFloat("ecalTrkEnergyPostCorr"))
      electron_pt_corr.push_back(electronsColl[i].pt()*electronsColl[i].userFloat("ecalTrkEnergyPostCorr")/electronsColl[i].energy());   
    electron_eta.push_back(electronsColl[i].eta());
    electron_phi.push_back(electronsColl[i].phi());
    electron_mass.push_back(electronsColl[i].mass());
    electron_energy.push_back(electronsColl[i].energy());
	
    int eidval = 0;   
    if(electronsColl[i].isElectronIDAvailable("cutBasedElectronID-Fall17-94X-V2-loose") and electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-loose"))
      eidval += 1;
    if(electronsColl[i].isElectronIDAvailable("cutBasedElectronID-Fall17-94X-V2-medium") and electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-medium"))
      eidval += 2;
    if(electronsColl[i].isElectronIDAvailable("cutBasedElectronID-Fall17-94X-V2-tight") and electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-tight"))
      eidval += 4;
    if(electronsColl[i].isElectronIDAvailable("mvaEleID-Fall17-iso-V2-wpLoose") and electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wpLoose"))
      eidval += 8;
    if(electronsColl[i].isElectronIDAvailable("mvaEleID-Fall17-iso-V2-wp90") and electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp90"))
      eidval += 16;
    if(electronsColl[i].isElectronIDAvailable("mvaEleID-Fall17-iso-V2-wp80") and electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp80"))
      eidval += 32;

    electron_id.push_back(eidval);
    if(electronsColl[i].hasUserFloat("ElectronMVAEstimatorRun2Fall17IsoV2Values"))
      electron_idscore.push_back(electronsColl[i].userFloat("ElectronMVAEstimatorRun2Fall17IsoV2Values"));
    electron_charge.push_back(electronsColl[i].charge());
    electron_d0.push_back(electronsColl[i].gsfTrack()->dxy(primaryVerticesH->at(0).position()));
    electron_dz.push_back(electronsColl[i].gsfTrack()->dz(primaryVerticesH->at(0).position()));
  }

  // Sorting photons based on pT                                                                                                                                                                      
  sort(photonsColl.begin(),photonsColl.end(),photonSorter);

  for (size_t i = 0; i < photonsColl.size(); i++) {

    if(photonsColl[i].pt() < photonPtMin) continue;
    if(fabs(photonsColl[i].eta()) > photonEtaMax) continue;

    photon_pt.push_back(photonsColl[i].pt());
    if(photonsColl[i].hasUserFloat("ecalEnergyPostCorr"))
      photon_pt_corr.push_back(photonsColl[i].pt()*photonsColl[i].userFloat("ecalEnergyPostCorr")/photonsColl[i].energy());
    photon_eta.push_back(photonsColl[i].eta());
    photon_phi.push_back(photonsColl[i].phi());
    photon_mass.push_back(photonsColl[i].mass());
    photon_energy.push_back(photonsColl[i].energy());

    int pidval = 0;
    if(photonsColl[i].isPhotonIDAvailable("cutBasedPhotonID-Fall17-94X-V2-loose") and photonsColl[i].photonID("cutBasedPhotonID-Fall17-94X-V2-loose"))
      pidval += 1;
    if(photonsColl[i].isPhotonIDAvailable("cutBasedPhotonID-Fall17-94X-V2-medium") and photonsColl[i].photonID("cutBasedPhotonID-Fall17-94X-V2-medium"))
      pidval += 2;
    if(photonsColl[i].isPhotonIDAvailable("cutBasedPhotonID-Fall17-94X-V2-tight") and photonsColl[i].photonID("cutBasedPhotonID-Fall17-94X-V2-tight"))
      pidval += 4;
    if(photonsColl[i].isPhotonIDAvailable("mvaPhoID-RunIIFall17-v2-wp90") and photonsColl[i].photonID("mvaPhoID-RunIIFall17-v2-wp90"))
      pidval += 8;
    if(photonsColl[i].isPhotonIDAvailable("mvaPhoID-RunIIFall17-v2-wp80") and photonsColl[i].photonID("mvaPhoID-RunIIFall17-v2-wp80"))
      pidval += 16;

    photon_id.push_back(pidval);
    if(photonsColl[i].hasUserFloat("PhotonMVAEstimatorRunIIFall17v2Values"))
      photon_idscore.push_back(photonsColl[i].userFloat("PhotonMVAEstimatorRunIIFall17v2Values"));
  }

  // Sorting taus based on pT
  sort(tausColl.begin(),tausColl.end(),tauSorter);
  std::vector<math::XYZTLorentzVector> tau_pfcandidates;  // in order to match PF and HPS candidates
  
  for (size_t i = 0; i < tausColl.size(); i++) {
    
    if(tausColl[i].pt() < tauPtMin) continue;
    if(fabs(tausColl[i].eta()) > tauEtaMax) continue;

    tau_pt.push_back(tausColl[i].pt());
    tau_eta.push_back(tausColl[i].eta());
    tau_phi.push_back(tausColl[i].phi());
    tau_mass.push_back(tausColl[i].mass());
    tau_energy.push_back(tausColl[i].energy());
    tau_dxy.push_back(tausColl[i].dxy());
    tau_dz.push_back((tausColl[i].leadChargedHadrCand().get() ? dynamic_cast<const pat::PackedCandidate*>(tausColl[i].leadChargedHadrCand().get())->dz() : 0.));
    tau_decaymode.push_back(tausColl[i].decayMode());
    tau_charge.push_back(tausColl[i].charge());

    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSjetraw"))
      tau_idjet.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSjetraw"));
    else
      tau_idjet.push_back(-1);
    
    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSmuraw"))
      tau_idmu.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSmuraw"));
    else
      tau_idmu.push_back(-1);
    
    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSeraw"))
      tau_idele.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSeraw"));
    else
      tau_idele.push_back(-1);

    int tauvsjetid = 0;
    if(tausColl[i].isTauIDAvailable("byVVVLooseDeepTau2018v2p5VSjet") and tausColl[i].tauID("byVVVLooseDeepTau2018v2p5VSjet"))
      tauvsjetid += 1;
    if(tausColl[i].isTauIDAvailable("byVVLooseDeepTau2018v2p5VSjet") and tausColl[i].tauID("byVVLooseDeepTau2018v2p5VSjet"))
      tauvsjetid += 2;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2018v2p5VSjet") and tausColl[i].tauID("byVLooseDeepTau2018v2p5VSjet"))
      tauvsjetid += 4;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2018v2p5VSjet") and tausColl[i].tauID("byLooseDeepTau2018v2p5VSjet"))
      tauvsjetid += 8;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2018v2p5VSjet") and tausColl[i].tauID("byMediumDeepTau2018v2p5VSjet"))
      tauvsjetid += 16;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2018v2p5VSjet") and tausColl[i].tauID("byTightDeepTau2018v2p5VSjet"))
      tauvsjetid += 32;
    tau_idjet_wp.push_back(tauvsjetid);
    
    int tauvsmuid = 0;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2018v2p5VSmu") and tausColl[i].tauID("byVLooseDeepTau2018v2p5VSmu"))
      tauvsmuid += 1;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2018v2p5VSmu") and tausColl[i].tauID("byLooseDeepTau2018v2p5VSmu"))
      tauvsmuid += 2;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2018v2p5VSmu") and tausColl[i].tauID("byMediumDeepTau2018v2p5VSmu"))
      tauvsmuid += 4;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2018v2p5VSmu") and tausColl[i].tauID("byTightDeepTau2018v2p5VSmu"))
      tauvsmuid += 8;
    tau_idmu_wp.push_back(tauvsmuid);

    int tauvselid = 0;
    if(tausColl[i].isTauIDAvailable("byVVVLooseDeepTau2018v2p5VSe") and tausColl[i].tauID("byVVVLooseDeepTau2018v2p5VSe"))
      tauvselid += 1;
    if(tausColl[i].isTauIDAvailable("byVVLooseDeepTau2018v2p5VSe") and tausColl[i].tauID("byVVLooseDeepTau2018v2p5VSe"))
      tauvselid += 2;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2018v2p5VSe") and tausColl[i].tauID("byVLooseDeepTau2018v2p5VSe"))
      tauvselid += 4;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2018v2p5VSe") and tausColl[i].tauID("byLooseDeepTau2018v2p5VSe"))
      tauvselid += 8;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2018v2p5VSe") and tausColl[i].tauID("byMediumDeepTau2018v2p5VSe"))
      tauvselid += 16;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2018v2p5VSe") and tausColl[i].tauID("byTightDeepTau2018v2p5VSe"))
      tauvselid += 32;
    tau_idele_wp.push_back(tauvselid);
       
    if(tausColl[i].genJet()){
      tau_genmatch_pt.push_back(tausColl[i].genJet()->pt());
      tau_genmatch_eta.push_back(tausColl[i].genJet()->eta());
      tau_genmatch_phi.push_back(tausColl[i].genJet()->phi());
      tau_genmatch_mass.push_back(tausColl[i].genJet()->mass());
      // reconstruct the decay mode of the gen-jet
      unsigned int tau_ch = 0;
      unsigned int tau_ph = 0;
      unsigned int tau_nh = 0;
      auto gen_constituents = tausColl[i].genJet()->getGenConstituents();
      for(size_t iconst = 0; iconst < gen_constituents.size(); iconst++){
	auto part = gen_constituents[iconst];
	if(part->status() != 1) continue;
	if(part->charge() == 0 and abs(part->pdgId()) == 22) tau_ph++;
	if(part->charge() == 0 and abs(part->pdgId()) != 22) tau_nh++;
	if(part->charge() != 0 and abs(part->pdgId()) != 11 and abs(part->pdgId()) != 13) tau_ch++;
      }
      tau_genmatch_decaymode.push_back(5*(tau_ch-1)+tau_ph/2+tau_nh);
    }    
    else{
      tau_genmatch_pt.push_back(-1);
      tau_genmatch_eta.push_back(-1);
      tau_genmatch_phi.push_back(-1);
      tau_genmatch_mass.push_back(-1);
      tau_genmatch_decaymode.push_back(-1);
    }
    for(unsigned ipart = 0; ipart < tausColl[i].signalCands().size(); ipart++){
      const pat::PackedCandidate* pfcand = dynamic_cast<const pat::PackedCandidate*> (tausColl[i].signalCands()[ipart].get());      
      tau_pfcandidates.push_back(pfcand->p4());
    }
  }


  // Sorting taus based on pT
  sort(boostedTausColl.begin(),boostedTausColl.end(),tauSorter);
  std::vector<math::XYZTLorentzVector> tau_boosted_pfcandidates;  // in order to match PF and HPS candidates
  
  for (size_t i = 0; i < boostedTausColl.size(); i++) {
    
    if(boostedTausColl[i].pt() < boostedTauPtMin) continue;
    if(fabs(boostedTausColl[i].eta()) > boostedTauEtaMax) continue;

    tau_boosted_pt.push_back(boostedTausColl[i].pt());
    tau_boosted_eta.push_back(boostedTausColl[i].eta());
    tau_boosted_phi.push_back(boostedTausColl[i].phi());
    tau_boosted_mass.push_back(boostedTausColl[i].mass());
    tau_boosted_energy.push_back(boostedTausColl[i].energy());
    tau_boosted_dxy.push_back(boostedTausColl[i].dxy());
    tau_boosted_dz.push_back((boostedTausColl[i].leadChargedHadrCand().get() ? dynamic_cast<const pat::PackedCandidate*>(boostedTausColl[i].leadChargedHadrCand().get())->dz() : 0.));
    tau_boosted_decaymode.push_back(boostedTausColl[i].decayMode());
    tau_boosted_charge.push_back(boostedTausColl[i].charge());

    if(boostedTausColl[i].isTauIDAvailable("byDeepTau2018v2p7VSjetraw"))
      tau_boosted_idjet.push_back(boostedTausColl[i].tauID("byDeepTau2018v2p7VSjetraw"));
    else
      tau_boosted_idjet.push_back(-1);
    
    if(boostedTausColl[i].isTauIDAvailable("byDeepTau2018v2p7VSmuraw"))
      tau_boosted_idmu.push_back(boostedTausColl[i].tauID("byDeepTau2018v2p7VSmuraw"));
    else
      tau_boosted_idmu.push_back(-1);

    if(boostedTausColl[i].isTauIDAvailable("byDeepTau2018v2p7VSeraw"))
      tau_boosted_idele.push_back(boostedTausColl[i].tauID("byDeepTau2018v2p7VSeraw"));
    else
      tau_boosted_idele.push_back(-1);

    int tauvsjetid = 0;
    if(boostedTausColl[i].isTauIDAvailable("byVVVLooseDeepTau2018v2p7VSjet") and boostedTausColl[i].tauID("byVVVLooseDeepTau2018v2p7VSjet"))
      tauvsjetid += 1;
    if(boostedTausColl[i].isTauIDAvailable("byVVLooseDeepTau2018v2p7VSjet") and boostedTausColl[i].tauID("byVVLooseDeepTau2018v2p7VSjet"))
      tauvsjetid += 2;
    if(boostedTausColl[i].isTauIDAvailable("byVLooseDeepTau2018v2p7VSjet") and boostedTausColl[i].tauID("byVLooseDeepTau2018v2p7VSjet"))
      tauvsjetid += 4;
    if(boostedTausColl[i].isTauIDAvailable("byLooseDeepTau2018v2p7VSjet") and boostedTausColl[i].tauID("byLooseDeepTau2018v2p7VSjet"))
      tauvsjetid += 8;
    if(boostedTausColl[i].isTauIDAvailable("byMediumDeepTau2018v2p7VSjet") and boostedTausColl[i].tauID("byMediumDeepTau2018v2p7VSjet"))
      tauvsjetid += 16;
    if(boostedTausColl[i].isTauIDAvailable("byTightDeepTau2018v2p7VSjet") and boostedTausColl[i].tauID("byTightDeepTau2018v2p7VSjet"))
      tauvsjetid += 32;

    tau_boosted_idjet_wp.push_back(tauvsjetid);

    int tauvsmuid = 0;
    if(boostedTausColl[i].isTauIDAvailable("byVLooseDeepTau2018v2p7VSmu") and boostedTausColl[i].tauID("byVLooseDeepTau2018v2p7VSmu"))
      tauvsmuid += 1;
    if(boostedTausColl[i].isTauIDAvailable("byLooseDeepTau2018v2p7VSmu") and boostedTausColl[i].tauID("byLooseDeepTau2018v2p7VSmu"))
      tauvsmuid += 2;
    if(boostedTausColl[i].isTauIDAvailable("byMediumDeepTau2018v2p7VSmu") and boostedTausColl[i].tauID("byMediumDeepTau2018v2p7VSmu"))
      tauvsmuid += 4;
    if(boostedTausColl[i].isTauIDAvailable("byTightDeepTau2018v2p7VSmu") and boostedTausColl[i].tauID("byTightDeepTau2018v2p7VSmu"))
      tauvsmuid += 8;

    tau_boosted_idmu_wp.push_back(tauvsmuid);

    int tauvselid = 0;
    if(boostedTausColl[i].isTauIDAvailable("byVVVLooseDeepTau2018v2p7VSe") and boostedTausColl[i].tauID("byVVVLooseDeepTau2018v2p7VSe"))
      tauvselid += 1;
    if(boostedTausColl[i].isTauIDAvailable("byVVLooseDeepTau2018v2p7VSe") and boostedTausColl[i].tauID("byVVLooseDeepTau2018v2p7VSe"))
      tauvselid += 2;
    if(boostedTausColl[i].isTauIDAvailable("byVLooseDeepTau2018v2p7VSe") and boostedTausColl[i].tauID("byVLooseDeepTau2018v2p7VSe"))
      tauvselid += 4;
    if(boostedTausColl[i].isTauIDAvailable("byLooseDeepTau2018v2p7VSe") and boostedTausColl[i].tauID("byLooseDeepTau2018v2p7VSe"))
      tauvselid += 8;
    if(boostedTausColl[i].isTauIDAvailable("byMediumDeepTau2018v2p7VSe") and boostedTausColl[i].tauID("byMediumDeepTau2018v2p7VSe"))
      tauvselid += 16;
    if(boostedTausColl[i].isTauIDAvailable("byTightDeepTau2018v2p7VSe") and boostedTausColl[i].tauID("byTightDeepTau2018v2p7VSe"))
      tauvselid += 32;

    tau_boosted_idele_wp.push_back(tauvselid);

    if(boostedTausColl[i].genJet()){
      tau_boosted_genmatch_pt.push_back(boostedTausColl[i].genJet()->pt());
      tau_boosted_genmatch_eta.push_back(boostedTausColl[i].genJet()->eta());
      tau_boosted_genmatch_phi.push_back(boostedTausColl[i].genJet()->phi());
      tau_boosted_genmatch_mass.push_back(boostedTausColl[i].genJet()->mass());
      // reconstruct the decay mode of the gen-jet
      unsigned int tau_ch = 0;
      unsigned int tau_ph = 0;
      unsigned int tau_nh = 0;
      auto gen_constituents = boostedTausColl[i].genJet()->getGenConstituents();
      for(size_t iconst = 0; iconst < gen_constituents.size(); iconst++){
	auto part = gen_constituents[iconst];
	if(part->status() != 1) continue;
	if(part->charge() == 0 and abs(part->pdgId()) == 22) tau_ph++;
	if(part->charge() == 0 and abs(part->pdgId()) != 22) tau_nh++;
	if(part->charge() != 0 and abs(part->pdgId()) != 11 and abs(part->pdgId()) != 13) tau_ch++;
      }
      tau_boosted_genmatch_decaymode.push_back(5*(tau_ch-1)+tau_ph/2+tau_nh);
    }    
    else{
      tau_boosted_genmatch_pt.push_back(-1);
      tau_boosted_genmatch_eta.push_back(-1);
      tau_boosted_genmatch_phi.push_back(-1);
      tau_boosted_genmatch_mass.push_back(-1);
      tau_boosted_genmatch_decaymode.push_back(-1);
    }

    for(unsigned ipart = 0; ipart < boostedTausColl[i].signalCands().size(); ipart++){
      const pat::PackedCandidate* pfcand = dynamic_cast<const pat::PackedCandidate*> (boostedTausColl[i].signalCands()[ipart].get());      
      tau_boosted_pfcandidates.push_back(pfcand->p4());
    }
  }
  
  // Standard gen-jets excluding the neutrinos
  std::vector<reco::GenJetRef> jetv_gen;   
  if(genJetsH.isValid()){
    for (auto jets_iter = genJetsH->begin(); jets_iter != genJetsH->end(); ++jets_iter) {                                                                                                   
      reco::GenJetRef jref (genJetsH,jets_iter-genJetsH->begin());                                                                                                                      
      jetv_gen.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen.begin(), jetv_gen.end(), genJetRefSorter);
  }

  // GEN jets with neutrinos
  std::vector<reco::GenJetRef> jetv_gen_wnu;   
  if(genJetsWnuH.isValid()){
    for (auto jets_iter = genJetsWnuH->begin(); jets_iter != genJetsWnuH->end(); ++jets_iter) {                                                                                           
      reco::GenJetRef jref  (genJetsWnuH, jets_iter-genJetsWnuH->begin());                                                                                                                 
      jetv_gen_wnu.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen_wnu.begin(), jetv_gen_wnu.end(), genJetRefSorter);
  }
  
  // Offline jets
  std::vector<pat::JetRef> jetv;   
  for (auto jets_iter = jetsH->begin(); jets_iter != jetsH->end(); ++jets_iter) {                                                                                                                     
    pat::JetRef jref(jetsH, jets_iter - jetsH->begin());                                                                                                                                            
    if (jref->pt() < jetPtMin) continue;
    if (jref->userFloat("ak8PFJetsPuppiSoftDropMass") < jetSoftDropMassMin) continue;
    if (fabs(jref->eta()) > jetEtaMax) continue;
    if (fabs(jref->eta()) < jetEtaMin) continue;
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
    jetv.push_back(jref);                                                                                                                                                                           
  }    
  sort(jetv.begin(), jetv.end(), jetRefSorter);
 
  // Store jet info
  for (size_t i = 0; i < jetv.size(); i++) {

    jet_pt.push_back(jetv[i]->pt());
    jet_eta.push_back(jetv[i]->eta());
    jet_phi.push_back(jetv[i]->phi());
    jet_mass.push_back(jetv[i]->mass());

    jet_pt_raw.push_back(jetv[i]->correctedJet("Uncorrected").pt());
    jet_mass_raw.push_back(jetv[i]->correctedJet("Uncorrected").mass());

    jet_pnet_probHbb.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probXbb"));
    jet_pnet_probHcc.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probXcc"));
    jet_pnet_probHqq.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probXqq"));
    jet_pnet_probQCDbb.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDbb"));
    jet_pnet_probQCDcc.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDcc"));
    jet_pnet_probQCDb.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDb"));
    jet_pnet_probQCDc.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDc"));
    jet_pnet_probQCDothers.push_back(jetv[i]->bDiscriminator("pfMassDecorrelatedParticleNetJetTags:probQCDothers"));
    jet_pnet_regmass.push_back(jetv[i]->bDiscriminator("pfParticleNetMassRegressionJetTags:mass"));

    for (size_t ilabel = 0; ilabel < pnetDiscriminatorLabels.size(); ilabel++)
      jet_pnetlast_score[pnetDiscriminatorLabels[ilabel]].push_back(jetv[i]->bDiscriminator(pnetDiscriminatorNames[ilabel].c_str()));
    
    for (size_t ilabel = 0; ilabel < parTDiscriminatorLabels.size(); ilabel++)
      jet_parTlast_score[parTDiscriminatorLabels[ilabel]].push_back(jetv[i]->bDiscriminator(parTDiscriminatorNames[ilabel].c_str()));
    
    // JetId    
    int jetid = 0;    
    if(applyJetID(*jetv[i],"tight")) jetid += 1;
    jet_id.push_back(jetid);

    // Energy fractions
    jet_chf.push_back(jetv[i]->chargedHadronEnergyFraction());
    jet_nhf.push_back(jetv[i]->neutralHadronEnergyFraction());
    jet_elf.push_back(jetv[i]->electronEnergyFraction());
    jet_phf.push_back(jetv[i]->photonEnergyFraction());
    jet_muf.push_back(jetv[i]->muonEnergyFraction());

    // PF components
    jet_ncand.push_back(jetv[i]->chargedHadronMultiplicity()+jetv[i]->neutralHadronMultiplicity()+jetv[i]->electronMultiplicity()+jetv[i]->photonMultiplicity()+jetv[i]->muonMultiplicity());
    jet_nch.push_back(jetv[i]->chargedHadronMultiplicity());
    jet_nnh.push_back(jetv[i]->neutralHadronMultiplicity());
    jet_nel.push_back(jetv[i]->electronMultiplicity());
    jet_nph.push_back(jetv[i]->photonMultiplicity());
    jet_nmu.push_back(jetv[i]->muonMultiplicity());
    jet_hflav.push_back(jetv[i]->hadronFlavour());
    jet_pflav.push_back(jetv[i]->partonFlavour());
    jet_nbhad.push_back(jetv[i]->jetFlavourInfo().getbHadrons().size());
    jet_nchad.push_back(jetv[i]->jetFlavourInfo().getcHadrons().size());
    
    // Matching with gen-jets
    int pos_gen_matched = -1;
    float minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4()) < minDR){
	pos_gen_matched = igen;
	minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4());
      }
    }
      
    if(pos_gen_matched >= 0){      
      jet_genmatch_pt.push_back(jetv_gen[pos_gen_matched]->pt());
      jet_genmatch_eta.push_back(jetv_gen[pos_gen_matched]->eta());
      jet_genmatch_phi.push_back(jetv_gen[pos_gen_matched]->phi());
      jet_genmatch_mass.push_back(jetv_gen[pos_gen_matched]->mass());
    }
    else{
      jet_genmatch_pt.push_back(0);
      jet_genmatch_eta.push_back(0);
      jet_genmatch_phi.push_back(0);
      jet_genmatch_mass.push_back(0);
    }
      
    //////////
    int pos_matched = -1;
    minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen_wnu.size(); igen++){
      if(reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[i]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[i]->p4());
      }
    }
    
    if(pos_matched >= 0){      
      jet_genmatch_wnu_pt.push_back(jetv_gen_wnu[pos_matched]->pt());
      jet_genmatch_wnu_eta.push_back(jetv_gen_wnu[pos_matched]->eta());
      jet_genmatch_wnu_phi.push_back(jetv_gen_wnu[pos_matched]->phi());
      jet_genmatch_wnu_mass.push_back(jetv_gen_wnu[pos_matched]->mass());
    }
    else{
      jet_genmatch_wnu_pt.push_back(0);
      jet_genmatch_wnu_eta.push_back(0);
      jet_genmatch_wnu_phi.push_back(0);
      jet_genmatch_wnu_mass.push_back(0);
    }

    TLorentzVector jet_softdrop_4V;
    TLorentzVector jet_softdrop_4V_raw;
    TLorentzVector genjet_softdrop_4V;

    if(jetv[i]->nSubjetCollections() > 0){
      auto subjets = jetv[i]->subjets(subJetCollectionName);
      std::sort(subjets.begin(), subjets.end(), jetPtrSorter);
      std::vector<unsigned int> subjetToSkip;
      for(size_t isub = 0; isub < subjets.size(); isub++){
	jet_softdrop_subjet_pt.push_back(subjets.at(isub)->pt());
	jet_softdrop_subjet_eta.push_back(subjets.at(isub)->eta());
	jet_softdrop_subjet_phi.push_back(subjets.at(isub)->phi());
	jet_softdrop_subjet_mass.push_back(subjets.at(isub)->mass());
	jet_softdrop_subjet_pt_raw.push_back(subjets.at(isub)->correctedJet("Uncorrected").pt());
	jet_softdrop_subjet_mass_raw.push_back(subjets.at(isub)->correctedJet("Uncorrected").mass());
	jet_softdrop_subjet_ijet.push_back(i);
	jet_softdrop_subjet_nbhad.push_back(subjets.at(isub)->jetFlavourInfo().getbHadrons().size());
	jet_softdrop_subjet_nchad.push_back(subjets.at(isub)->jetFlavourInfo().getcHadrons().size());
	jet_softdrop_subjet_hflav.push_back(subjets.at(isub)->hadronFlavour());
	jet_softdrop_subjet_pflav.push_back(subjets.at(isub)->partonFlavour());  
	TLorentzVector subjet_4v, subjet_raw_4v;
	subjet_4v.SetPtEtaPhiM(subjets.at(isub)->pt(),subjets.at(isub)->eta(),subjets.at(isub)->phi(),subjets.at(isub)->mass());
	subjet_raw_4v.SetPtEtaPhiM(subjets.at(isub)->correctedJet("Uncorrected").pt(),subjets.at(isub)->correctedJet("Uncorrected").eta(),
				   subjets.at(isub)->correctedJet("Uncorrected").phi(),subjets.at(isub)->correctedJet("Uncorrected").mass());
	jet_softdrop_4V += subjet_4v;
	jet_softdrop_4V_raw += subjet_raw_4v;
	minDR = 1000;
	int pos_gen_subjet_matched = -1;
	if(pos_gen_matched >= 0){
	  for(size_t jsub = 0; jsub < genSoftDropSubJetsH->size(); jsub++){
	    if(std::find(subjetToSkip.begin(),subjetToSkip.end(),jsub) != subjetToSkip.end()) continue;
	    if(reco::deltaR(subjets.at(isub)->p4(),(*genSoftDropSubJetsH)[jsub].p4()) < minDR){
		pos_gen_subjet_matched = jsub;
		minDR = reco::deltaR(subjets.at(isub)->p4(),(*genSoftDropSubJetsH)[jsub].p4());
	    }
	  }
	}
	if(pos_gen_matched >= 0 and pos_gen_subjet_matched >= 0){
	  subjetToSkip.push_back(pos_gen_subjet_matched);
	  TLorentzVector gen_subjet_4v;
	  gen_subjet_4v.SetPtEtaPhiM((*genSoftDropSubJetsH)[pos_gen_subjet_matched].pt(),(*genSoftDropSubJetsH)[pos_gen_subjet_matched].eta(),
				     (*genSoftDropSubJetsH)[pos_gen_subjet_matched].phi(),(*genSoftDropSubJetsH)[pos_gen_subjet_matched].mass());
	  genjet_softdrop_4V += gen_subjet_4v;  
	  jet_softdrop_subjet_genmatch_pt.push_back((*genSoftDropSubJetsH)[pos_gen_subjet_matched].pt());
	  jet_softdrop_subjet_genmatch_eta.push_back((*genSoftDropSubJetsH)[pos_gen_subjet_matched].eta());
	  jet_softdrop_subjet_genmatch_phi.push_back((*genSoftDropSubJetsH)[pos_gen_subjet_matched].phi());
	  jet_softdrop_subjet_genmatch_mass.push_back((*genSoftDropSubJetsH)[pos_gen_subjet_matched].mass());
	}
	else{
	  jet_softdrop_subjet_genmatch_pt.push_back(0);
	  jet_softdrop_subjet_genmatch_eta.push_back(0);
	  jet_softdrop_subjet_genmatch_phi.push_back(0);
	  jet_softdrop_subjet_genmatch_mass.push_back(0);
	}
      }      
    }

    jet_softdrop_pt.push_back(jet_softdrop_4V.Pt());
    jet_softdrop_eta.push_back(jet_softdrop_4V.Eta());
    jet_softdrop_phi.push_back(jet_softdrop_4V.Phi());
    jet_softdrop_mass.push_back(jet_softdrop_4V.M());
    jet_softdrop_pt_raw.push_back(jet_softdrop_4V_raw.Pt());
    jet_softdrop_mass_raw.push_back(jet_softdrop_4V_raw.M());

    if(isMC){
      jet_softdrop_genmatch_pt.push_back(genjet_softdrop_4V.Pt());
      jet_softdrop_genmatch_eta.push_back(genjet_softdrop_4V.Eta());
      jet_softdrop_genmatch_phi.push_back(genjet_softdrop_4V.Phi());
      jet_softdrop_genmatch_mass.push_back(genjet_softdrop_4V.M());
    }
    
    // PF candidates dump
    math::XYZVector jetDir      = jetv[i]->momentum().Unit();
    TVector3 jet_direction      (jetv[i]->momentum().Unit().x(),jetv[i]->momentum().Unit().y(),jetv[i]->momentum().Unit().z());
    GlobalVector jet_global_vec (jetv[i]->px(),jetv[i]->py(),jetv[i]->pz());	     

    // secondary vertex that are inside the jet cone + sorting in significance
    std::vector<reco::VertexCompositePtrCandidate> jetSVs;    
    for(size_t isv = 0; isv < svColl.size(); isv++){
      if(reco::deltaR(svColl.at(isv),*jetv[i]) < dRJetGenMatch){
	jetSVs.push_back(svColl.at(isv));
      }
    }
    
    VertexSorterDxy<reco::VertexCompositePtrCandidate,reco::Vertex> svSorter (primaryVerticesH->front());
    std::sort(jetSVs.begin(),jetSVs.end(),svSorter);
    
    for(auto const & svcand : jetSVs){
      jet_sv_pt.push_back(svcand.pt());
      jet_sv_eta.push_back(svcand.eta());
      jet_sv_phi.push_back(svcand.phi());
      jet_sv_mass.push_back(svcand.mass());
      jet_sv_energy.push_back(svcand.energy());
      jet_sv_chi2.push_back(svcand.vertexNormalizedChi2());
      auto dxy = vertexDxySigned(svcand,primaryVerticesH->front(),jet_global_vec);
      jet_sv_dxy.push_back(dxy.value());
      jet_sv_dxysig.push_back(fabs(dxy.significance()));
      auto d3d = vertexD3dSigned(svcand,primaryVerticesH->front(),jet_global_vec);
      jet_sv_d3d.push_back(d3d.value());
      jet_sv_d3dsig.push_back(fabs(d3d.significance()));
      jet_sv_ntrack.push_back(svcand.numberOfDaughters());
      jet_sv_ijet.push_back(i);
    }    


    // lost tracks inside the jet                                                                                                                                                                   
    std::vector<pat::PackedCandidate> jetLostTracks;
    for(size_t itrk = 0; itrk < lostTracksH->size(); itrk++){
      if(reco::deltaR(lostTracksH->at(itrk).p4(),jetv[i]->p4()) < dRLostTrackJet and
         lostTracksH->at(itrk).pt() > lostTracksPtMin ){
        jetLostTracks.push_back(lostTracksH->at(itrk));
      }
    }
    std::sort(jetLostTracks.begin(),jetLostTracks.end(),packedPFCandidateSorter);

    for(auto const & ltrack : jetLostTracks){

      jet_losttrack_pt.push_back(ltrack.pt());
      jet_losttrack_eta.push_back(ltrack.eta());
      jet_losttrack_phi.push_back(ltrack.phi());
      jet_losttrack_mass.push_back(ltrack.mass());
      jet_losttrack_energy.push_back(ltrack.energy());
      jet_losttrack_charge.push_back(ltrack.charge());
      jet_losttrack_ijet.push_back(i);

      jet_losttrack_frompv.push_back(ltrack.fromPV());
      jet_losttrack_dz.push_back(ltrack.dz(primaryVerticesH->front().position()));
      jet_losttrack_dxy.push_back(ltrack.dxy(primaryVerticesH->front().position()));

      TVector3 ltrack_momentum (ltrack.momentum().x(),ltrack.momentum().y(),ltrack.momentum().z());
      jet_losttrack_candjet_dphi.push_back(jet_direction.DeltaPhi(ltrack_momentum));
      jet_losttrack_candjet_deta.push_back(jet_direction.Eta()-ltrack_momentum.Eta());
      jet_losttrack_candjet_etarel.push_back(reco::btau::etaRel(jetDir,ltrack.momentum()));

      // track specific                                                                                                                                                                           
      const reco::Track* track = ltrack.bestTrack();
      if(track){
        jet_losttrack_dzsig.push_back(fabs(ltrack.dz(primaryVerticesH->front().position()))/ltrack.dzError());
        jet_losttrack_dxysig.push_back(fabs(ltrack.dxy(primaryVerticesH->front().position()))/ltrack.dxyError());
        jet_losttrack_track_chi2.push_back(track->normalizedChi2());
        jet_losttrack_track_qual.push_back(track->qualityMask());
	jet_losttrack_track_algo.push_back(track->algoMaskUL());
	jet_losttrack_track_pterr.push_back(track->ptError()/track->pt());
	jet_losttrack_track_etaerr.push_back(track->etaError());
	jet_losttrack_track_phierr.push_back(track->phiError());
	
	jet_losttrack_nhits.push_back(track->hitPattern().numberOfValidHits());
	jet_losttrack_npixhits.push_back(track->hitPattern().numberOfValidPixelHits());
	jet_losttrack_nstriphits.push_back(track->hitPattern().numberOfValidStripHits());
	jet_losttrack_nlosthits.push_back(track->hitPattern().numberOfLostHits(reco::HitPattern::TRACK_HITS)+
					  track->hitPattern().numberOfLostHits(reco::HitPattern::MISSING_INNER_HITS)+
					  track->hitPattern().numberOfLostHits(reco::HitPattern::MISSING_OUTER_HITS));
	
	jet_losttrack_npixlayers.push_back(track->hitPattern().pixelLayersWithMeasurement());
	jet_losttrack_nstriplayers.push_back(track->hitPattern().stripLayersWithMeasurement());
	
	reco::TransientTrack transientTrack = trackBuilderH->build(*track);
        Measurement1D meas_ip3d    = IPTools::signedImpactParameter3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
        Measurement1D meas_jetdist = IPTools::jetTrackDistance(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
        Measurement1D meas_decayl  = IPTools::signedDecayLength3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;

        jet_losttrack_trackjet_d3d.push_back(meas_ip3d.value());
        jet_losttrack_trackjet_d3dsig.push_back(fabs(meas_ip3d.significance()));
        jet_losttrack_trackjet_dist.push_back(-meas_jetdist.value());
        jet_losttrack_trackjet_decayL.push_back(meas_decayl.value());
      }
      else{
        jet_losttrack_track_chi2.push_back(0);
        jet_losttrack_track_qual.push_back(0);
        jet_losttrack_track_algo.push_back(0);
        jet_losttrack_trackjet_d3d.push_back(0.);
        jet_losttrack_trackjet_d3dsig.push_back(0.);
        jet_losttrack_trackjet_dist.push_back(0.);
        jet_losttrack_trackjet_decayL.push_back(0.);
	jet_losttrack_track_pterr.push_back(0.);
	jet_losttrack_track_etaerr.push_back(0.);
	jet_losttrack_track_phierr.push_back(0.);
	jet_losttrack_track_algo.push_back(0.);
	jet_losttrack_nhits.push_back(0.);
	jet_losttrack_npixhits.push_back(0.);
	jet_losttrack_nstriphits.push_back(0.);
	jet_losttrack_nlosthits.push_back(0.);
	jet_losttrack_npixlayers.push_back(0.);
	jet_losttrack_nstriplayers.push_back(0.);
      }
    }

    // PF candidates
    std::vector<pat::PackedCandidate> vectorOfConstituents;
    for(unsigned ipart = 0; ipart < jetv[i]->numberOfDaughters(); ipart++){
      const pat::PackedCandidate* pfPart = dynamic_cast<const pat::PackedCandidate*> (jetv[i]->daughter(ipart));      
      vectorOfConstituents.push_back(*pfPart);
    }
    
    std::sort(vectorOfConstituents.begin(),vectorOfConstituents.end(),packedPFCandidateSorter);
    std::vector<unsigned int> muonsToSkip;
      
    for(auto const & pfcand : vectorOfConstituents){
      
      // basic quantities
      if(pfcand.pt() < jetPFCandidatePtMin) continue;
      
      jet_pfcandidate_pt.push_back(pfcand.pt());
      jet_pfcandidate_eta.push_back(pfcand.eta());
      jet_pfcandidate_phi.push_back(pfcand.phi());
      jet_pfcandidate_mass.push_back(pfcand.mass());
      jet_pfcandidate_energy.push_back(pfcand.energy());      
      jet_pfcandidate_calofraction.push_back(pfcand.caloFraction());
      jet_pfcandidate_hcalfraction.push_back(pfcand.hcalFraction());
	
      jet_pfcandidate_id.push_back(abs(pfcand.pdgId()));
      jet_pfcandidate_charge.push_back(pfcand.charge());
      jet_pfcandidate_ijet.push_back(i);
	
      // PV association and distance from PV
      jet_pfcandidate_frompv.push_back(pfcand.fromPV());
      jet_pfcandidate_dz.push_back(pfcand.dz(primaryVerticesH->front().position()));
      jet_pfcandidate_dxy.push_back(pfcand.dxy(primaryVerticesH->front().position()));

      TVector3 pfcand_momentum (pfcand.momentum().x(),pfcand.momentum().y(),pfcand.momentum().z());
      jet_pfcandidate_candjet_pperp_ratio.push_back(jet_direction.Perp(pfcand_momentum)/pfcand_momentum.Mag());
      jet_pfcandidate_candjet_ppara_ratio.push_back(jet_direction.Dot(pfcand_momentum)/pfcand_momentum.Mag());
      jet_pfcandidate_candjet_dphi.push_back(jet_direction.DeltaPhi(pfcand_momentum));
      jet_pfcandidate_candjet_deta.push_back(jet_direction.Eta()-pfcand_momentum.Eta());
      jet_pfcandidate_candjet_etarel.push_back(reco::btau::etaRel(jetDir,pfcand.momentum()));
      jet_pfcandidate_puppiw.push_back(pfcand.puppiWeight());

      // tau specific
      if(std::find(tau_pfcandidates.begin(),tau_pfcandidates.end(),pfcand.p4()) != tau_pfcandidates.end())
	jet_pfcandidate_tau_signal.push_back(1);
      else
	jet_pfcandidate_tau_signal.push_back(0);

      if(std::find(tau_boosted_pfcandidates.begin(),tau_boosted_pfcandidates.end(),pfcand.p4()) != tau_boosted_pfcandidates.end())
	jet_pfcandidate_tau_boosted_signal.push_back(1);
      else
	jet_pfcandidate_tau_boosted_signal.push_back(0);
	
      // muon specific
      if(abs(pfcand.pdgId()) == 13){
	float minDR = 1000;
	int ipos = -1;
	for (size_t i = 0; i < muonsColl.size(); i++) {
	  if(not muonsColl[i].isPFMuon()) continue;
	  if(std::find(muonsToSkip.begin(),muonsToSkip.end(),i) != muonsToSkip.end()) continue;
	  float dR = reco::deltaR(muonsColl[i].p4(),pfcand.p4());
	  if(dR < dRJetGenMatch and dR < minDR){
	    minDR = dR;
	    ipos = i;
	  }
	}
	
	if(ipos >= 0){
	  muonsToSkip.push_back(ipos);
	  int muonId = 0;
	  if(muonsColl[ipos].passed(reco::Muon::CutBasedIdLoose)) muonId++;
	  if(muonsColl[ipos].passed(reco::Muon::CutBasedIdMedium)) muonId++;
	  if(muonsColl[ipos].passed(reco::Muon::CutBasedIdTight)) muonId++;
	  if(muonsColl[ipos].passed(reco::Muon::CutBasedIdGlobalHighPt)) muonId++;
	  if(muonsColl[ipos].passed(reco::Muon::CutBasedIdTrkHighPt)) muonId++;
	  jet_pfcandidate_muon_id.push_back(muonId);
	  jet_pfcandidate_muon_isglobal.push_back(muonsColl[ipos].isGlobalMuon());
          jet_pfcandidate_muon_chi2.push_back((muonsColl[ipos].isGlobalMuon()) ? muonsColl[ipos].globalTrack()->normalizedChi2(): 0);
          jet_pfcandidate_muon_nvalidhit.push_back((muonsColl[ipos].isGlobalMuon()) ? muonsColl[ipos].globalTrack()->hitPattern().numberOfValidMuonHits() : 0);
          jet_pfcandidate_muon_nstation.push_back(muonsColl[ipos].numberOfMatchedStations());
          jet_pfcandidate_muon_segcomp.push_back(muon::segmentCompatibility(muonsColl[ipos]));
	}
	else {	  
	  jet_pfcandidate_muon_id.push_back(0);
	  jet_pfcandidate_muon_isglobal.push_back(0);
          jet_pfcandidate_muon_chi2.push_back(0);
          jet_pfcandidate_muon_nvalidhit.push_back(0);
          jet_pfcandidate_muon_nstation.push_back(0);
          jet_pfcandidate_muon_segcomp.push_back(0);
	}
      }
      else{
	jet_pfcandidate_muon_id.push_back(0);
	jet_pfcandidate_muon_isglobal.push_back(0);
	jet_pfcandidate_muon_chi2.push_back(0);
	jet_pfcandidate_muon_nvalidhit.push_back(0);
	jet_pfcandidate_muon_nstation.push_back(0);
	jet_pfcandidate_muon_segcomp.push_back(0);
      }

      // electron specific
      if(abs(pfcand.pdgId()) == 11){	
	int ipos = -1;
        for (size_t i = 0; i < electronsColl.size(); i++) {
          if(electronsColl[i].isPF()){
            for(const auto & element : electronsColl[i].associatedPackedPFCandidates()){
              if(abs(element->pdgId()) == 11 and element->p4() == pfcand.p4()){
                ipos = i;
                break;
              }
              if(ipos != -1) break;
            }
          }
        }
	if(ipos >= 0){
          jet_pfcandidate_electron_eOverP.push_back(electronsColl[ipos].eSuperClusterOverP());
          jet_pfcandidate_electron_detaIn.push_back(electronsColl[ipos].deltaEtaSuperClusterTrackAtVtx());
          jet_pfcandidate_electron_dphiIn.push_back(electronsColl[ipos].deltaPhiSuperClusterTrackAtVtx());
          jet_pfcandidate_electron_sigIetaIeta.push_back(electronsColl[ipos].full5x5_sigmaIetaIeta());
          jet_pfcandidate_electron_sigIphiIphi.push_back(electronsColl[ipos].full5x5_sigmaIphiIphi());
          jet_pfcandidate_electron_r9.push_back(electronsColl[ipos].full5x5_r9());
          jet_pfcandidate_electron_convProb.push_back(electronsColl[ipos].convVtxFitProb());
        }
        else{
          jet_pfcandidate_electron_eOverP.push_back(0);
          jet_pfcandidate_electron_detaIn.push_back(0);
          jet_pfcandidate_electron_dphiIn.push_back(0);
          jet_pfcandidate_electron_r9.push_back(0);
          jet_pfcandidate_electron_sigIetaIeta.push_back(0);
          jet_pfcandidate_electron_sigIphiIphi.push_back(0);
          jet_pfcandidate_electron_convProb.push_back(0);
        }
      }
      else{
        jet_pfcandidate_electron_eOverP.push_back(0);
        jet_pfcandidate_electron_detaIn.push_back(0);
        jet_pfcandidate_electron_dphiIn.push_back(0);
        jet_pfcandidate_electron_r9.push_back(0);
        jet_pfcandidate_electron_sigIetaIeta.push_back(0);
        jet_pfcandidate_electron_sigIphiIphi.push_back(0);
        jet_pfcandidate_electron_convProb.push_back(0);
      }

      // photon specific                                                                                                                                                                              
      if(abs(pfcand.pdgId()) == 22){
        int ipos = -1;
        for (size_t i = 0; i < photonsColl.size(); i++) {
          for(const auto & element : photonsColl[i].associatedPackedPFCandidates()){
            if(abs(element->pdgId()) == 22 and element->p4() == pfcand.p4()){
              ipos = i;
              break;
            }
            if(ipos != -1) break;
          }
        }
        if(ipos >= 0){
          jet_pfcandidate_photon_sigIetaIeta.push_back(photonsColl[ipos].full5x5_sigmaIetaIeta());
          jet_pfcandidate_photon_r9.push_back(photonsColl[ipos].full5x5_r9());
          jet_pfcandidate_photon_eVeto.push_back(photonsColl[ipos].passElectronVeto());
        }
        else{
          jet_pfcandidate_photon_sigIetaIeta.push_back(0);
          jet_pfcandidate_photon_r9.push_back(0);
          jet_pfcandidate_photon_eVeto.push_back(0);
        }
      }
      else{
        jet_pfcandidate_photon_sigIetaIeta.push_back(0);
        jet_pfcandidate_photon_r9.push_back(0);
        jet_pfcandidate_photon_eVeto.push_back(0);
      }
      
      // track specific                                                                                                                                                                              
      const reco::Track* track = pfcand.bestTrack();
      if(track){
        jet_pfcandidate_dzsig.push_back(fabs(pfcand.dz(primaryVerticesH->front().position()))/pfcand.dzError());
        jet_pfcandidate_dxysig.push_back(fabs(pfcand.dxy(primaryVerticesH->front().position()))/pfcand.dxyError());
        jet_pfcandidate_track_chi2.push_back(track->normalizedChi2());
        jet_pfcandidate_track_qual.push_back(track->qualityMask());
	jet_pfcandidate_track_algo.push_back(track->algoMaskUL());
	jet_pfcandidate_track_pterr.push_back(track->ptError()/track->pt());
	jet_pfcandidate_track_etaerr.push_back(track->etaError());
	jet_pfcandidate_track_phierr.push_back(track->phiError());

	jet_pfcandidate_nhits.push_back(track->hitPattern().numberOfValidHits());
	jet_pfcandidate_npixhits.push_back(track->hitPattern().numberOfValidPixelHits());
	jet_pfcandidate_nstriphits.push_back(track->hitPattern().numberOfValidStripHits());
	jet_pfcandidate_nlosthits.push_back(track->hitPattern().numberOfLostHits(reco::HitPattern::TRACK_HITS)+
					  track->hitPattern().numberOfLostHits(reco::HitPattern::MISSING_INNER_HITS)+
					  track->hitPattern().numberOfLostHits(reco::HitPattern::MISSING_OUTER_HITS));
	jet_pfcandidate_npixlayers.push_back(track->hitPattern().pixelLayersWithMeasurement());
	jet_pfcandidate_nstriplayers.push_back(track->hitPattern().stripLayersWithMeasurement());

	reco::TransientTrack transientTrack = trackBuilderH->build(*track);
        Measurement1D meas_ip3d    = IPTools::signedImpactParameter3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
        Measurement1D meas_jetdist = IPTools::jetTrackDistance(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
        Measurement1D meas_decayl  = IPTools::signedDecayLength3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;

        jet_pfcandidate_trackjet_d3d.push_back(meas_ip3d.value());
        jet_pfcandidate_trackjet_d3dsig.push_back(fabs(meas_ip3d.significance()));
        jet_pfcandidate_trackjet_dist.push_back(-meas_jetdist.value());
        jet_pfcandidate_trackjet_decayL.push_back(meas_decayl.value());
      }
      else{
	jet_pfcandidate_dzsig.push_back(0.);
	jet_pfcandidate_dxysig.push_back(0.);	
	jet_pfcandidate_track_chi2.push_back(0);
	jet_pfcandidate_track_qual.push_back(0);
	jet_pfcandidate_track_pterr.push_back(0.);
	jet_pfcandidate_track_etaerr.push_back(0.);
	jet_pfcandidate_track_phierr.push_back(0.);
	jet_pfcandidate_track_algo.push_back(0.);
	jet_pfcandidate_trackjet_d3d.push_back(0.);
	jet_pfcandidate_trackjet_d3dsig.push_back(0.);
	jet_pfcandidate_trackjet_dist.push_back(0.);
	jet_pfcandidate_trackjet_decayL.push_back(0.);
	jet_pfcandidate_nhits.push_back(0.);
	jet_pfcandidate_npixhits.push_back(0.);
	jet_pfcandidate_nstriphits.push_back(0.);
	jet_pfcandidate_nlosthits.push_back(0.);
	jet_pfcandidate_npixlayers.push_back(0.);
	jet_pfcandidate_nstriplayers.push_back(0.);
      }
    }
  }    
    
  tree->Fill();
}


void TrainingTreeMakerAK8::beginJob() {

  // Access the TFileService
  edm::Service<TFileService> fs; 
  // Create the TTree
  tree = fs->make<TTree>("tree","tree");

  // Branches
  tree->Branch("event", &event, "event/i");
  tree->Branch("run", &run, "run/i");
  tree->Branch("lumi", &lumi, "lumi/i");
  if(isMC){
    tree->Branch("xsec", &xsec, "xsec/F");
    tree->Branch("wgt" , &wgt , "wgt/F");
    tree->Branch("putrue", &putrue, "putrue/i");
  }  
  tree->Branch("rho", &rho, "rho/F");
  tree->Branch("met",&met,"met/F");
  tree->Branch("met_phi", &met_phi, "met_phi/F");
  tree->Branch("npv", &npv, "npv/i");
  tree->Branch("nsv", &nsv, "nsv/i");

  if(isMC and saveLHEObjects){
    tree->Branch("lhe_particle_pt", "std::vector<float>", &lhe_particle_pt);
    tree->Branch("lhe_particle_eta", "std::vector<float>", &lhe_particle_eta);
    tree->Branch("lhe_particle_phi", "std::vector<float>", &lhe_particle_phi);
    tree->Branch("lhe_particle_mass", "std::vector<float>", &lhe_particle_mass);
    tree->Branch("lhe_particle_id", "std::vector<int>", &lhe_particle_id);
    tree->Branch("lhe_particle_status", "std::vector<unsigned int>", &lhe_particle_status);
  }
  if(isMC){
    tree->Branch("gen_particle_pt","std::vector<float>", &gen_particle_pt);
    tree->Branch("gen_particle_eta","std::vector<float>", &gen_particle_eta);
    tree->Branch("gen_particle_phi","std::vector<float>", &gen_particle_phi);
    tree->Branch("gen_particle_mass","std::vector<float>", &gen_particle_mass);
    tree->Branch("gen_particle_id","std::vector<int>", &gen_particle_id);
    tree->Branch("gen_particle_status","std::vector<unsigned int>", &gen_particle_status);
    tree->Branch("gen_particle_daughters_id","std::vector<int>", &gen_particle_daughters_id);
    tree->Branch("gen_particle_daughters_igen","std::vector<unsigned int>", &gen_particle_daughters_igen);
    tree->Branch("gen_particle_daughters_pt","std::vector<float>", &gen_particle_daughters_pt);
    tree->Branch("gen_particle_daughters_eta","std::vector<float>", &gen_particle_daughters_eta);
    tree->Branch("gen_particle_daughters_phi","std::vector<float>", &gen_particle_daughters_phi);
    tree->Branch("gen_particle_daughters_mass","std::vector<float>", &gen_particle_daughters_mass);
    tree->Branch("gen_particle_daughters_status","std::vector<unsigned int>", &gen_particle_daughters_status);
    tree->Branch("gen_particle_daughters_charge","std::vector<int>", &gen_particle_daughters_charge);
  }
  
  tree->Branch("muon_pt", "std::vector<float>", &muon_pt);
  tree->Branch("muon_eta", "std::vector<float>", &muon_eta);
  tree->Branch("muon_phi", "std::vector<float>", &muon_phi);
  tree->Branch("muon_mass", "std::vector<float>", &muon_mass);
  tree->Branch("muon_energy", "std::vector<float>", &muon_energy);
  tree->Branch("muon_id", "std::vector<unsigned int>" , &muon_id);
  tree->Branch("muon_iso", "std::vector<unsigned int>" , &muon_iso);
  tree->Branch("muon_charge", "std::vector<int>" , &muon_charge);
  tree->Branch("muon_d0", "std::vector<float>" , &muon_d0);
  tree->Branch("muon_dz", "std::vector<float>" , &muon_dz);

  tree->Branch("electron_pt", "std::vector<float>", &electron_pt);
  tree->Branch("electron_pt_corr", "std::vector<float>", &electron_pt_corr);
  tree->Branch("electron_eta", "std::vector<float>", &electron_eta);
  tree->Branch("electron_phi", "std::vector<float>", &electron_phi);
  tree->Branch("electron_mass", "std::vector<float>", &electron_mass);
  tree->Branch("electron_energy", "std::vector<float>", &electron_energy);
  tree->Branch("electron_id", "std::vector<unsigned int>" , &electron_id);
  tree->Branch("electron_charge", "std::vector<int>" , &electron_charge);
  tree->Branch("electron_idscore", "std::vector<float>" , &electron_idscore);
  tree->Branch("electron_d0", "std::vector<float>" , &electron_d0);
  tree->Branch("electron_dz", "std::vector<float>" , &electron_dz);

  tree->Branch("photon_pt", "std::vector<float>", &photon_pt);
  tree->Branch("photon_pt_corr", "std::vector<float>", &photon_pt_corr);
  tree->Branch("photon_eta", "std::vector<float>", &photon_eta);
  tree->Branch("photon_phi", "std::vector<float>", &photon_phi);
  tree->Branch("photon_mass", "std::vector<float>", &photon_mass);
  tree->Branch("photon_energy", "std::vector<float>", &photon_energy);
  tree->Branch("photon_id", "std::vector<unsigned int>" , &photon_id);
  tree->Branch("photon_idscore", "std::vector<float>" , &photon_idscore);

  tree->Branch("tau_pt", "std::vector<float>" , &tau_pt);
  tree->Branch("tau_eta", "std::vector<float>" , &tau_eta);
  tree->Branch("tau_phi", "std::vector<float>" , &tau_phi);
  tree->Branch("tau_mass", "std::vector<float>" , &tau_mass);
  tree->Branch("tau_energy", "std::vector<float>" , &tau_energy);
  tree->Branch("tau_dxy", "std::vector<float>" , &tau_dxy);
  tree->Branch("tau_dz", "std::vector<float>" , &tau_dz);
  tree->Branch("tau_decaymode", "std::vector<unsigned int>" , &tau_decaymode);
  tree->Branch("tau_idjet_wp", "std::vector<unsigned int>" , &tau_idjet_wp);
  tree->Branch("tau_idmu_wp", "std::vector<unsigned int>" , &tau_idmu_wp);
  tree->Branch("tau_idele_wp", "std::vector<unsigned int>" , &tau_idele_wp);
  tree->Branch("tau_idjet", "std::vector<float>" , &tau_idjet);
  tree->Branch("tau_idele", "std::vector<float>" , &tau_idele);
  tree->Branch("tau_idmu", "std::vector<float>" , &tau_idmu);
  tree->Branch("tau_charge", "std::vector<int>" , &tau_charge);
  if(isMC){
    tree->Branch("tau_genmatch_pt", "std::vector<float>" , &tau_genmatch_pt);
    tree->Branch("tau_genmatch_eta", "std::vector<float>" , &tau_genmatch_eta);
    tree->Branch("tau_genmatch_phi", "std::vector<float>" , &tau_genmatch_phi);
    tree->Branch("tau_genmatch_mass", "std::vector<float>" , &tau_genmatch_mass);
    tree->Branch("tau_genmatch_decaymode", "std::vector<int>" , &tau_genmatch_decaymode);
  }

  tree->Branch("tau_boosted_pt", "std::vector<float>" , &tau_boosted_pt);
  tree->Branch("tau_boosted_eta", "std::vector<float>" , &tau_boosted_eta);
  tree->Branch("tau_boosted_phi", "std::vector<float>" , &tau_boosted_phi);
  tree->Branch("tau_boosted_mass", "std::vector<float>" , &tau_boosted_mass);
  tree->Branch("tau_boosted_energy", "std::vector<float>" , &tau_boosted_energy);
  tree->Branch("tau_boosted_dxy", "std::vector<float>" , &tau_boosted_dxy);
  tree->Branch("tau_boosted_dz", "std::vector<float>" , &tau_boosted_dz);
  tree->Branch("tau_boosted_decaymode", "std::vector<unsigned int>" , &tau_boosted_decaymode);
  tree->Branch("tau_boosted_idjet_wp", "std::vector<unsigned int>" , &tau_boosted_idjet_wp);
  tree->Branch("tau_boosted_idmu_wp", "std::vector<unsigned int>" , &tau_boosted_idmu_wp);
  tree->Branch("tau_boosted_idele_wp", "std::vector<unsigned int>" , &tau_boosted_idele_wp);
  tree->Branch("tau_boosted_idjet", "std::vector<float>" , &tau_boosted_idjet);
  tree->Branch("tau_boosted_idele", "std::vector<float>" , &tau_boosted_idele);
  tree->Branch("tau_boosted_idmu", "std::vector<float>" , &tau_boosted_idmu);
  tree->Branch("tau_boosted_charge", "std::vector<int>" , &tau_boosted_charge);
  if(isMC){
    tree->Branch("tau_boosted_genmatch_pt", "std::vector<float>" , &tau_boosted_genmatch_pt);
    tree->Branch("tau_boosted_genmatch_eta", "std::vector<float>" , &tau_boosted_genmatch_eta);
    tree->Branch("tau_boosted_genmatch_phi", "std::vector<float>" , &tau_boosted_genmatch_phi);
    tree->Branch("tau_boosted_genmatch_mass", "std::vector<float>" , &tau_boosted_genmatch_mass);
    tree->Branch("tau_boosted_genmatch_decaymode", "std::vector<int>" , &tau_boosted_genmatch_decaymode);
  }

  tree->Branch("jet_pt", "std::vector<float>" , &jet_pt);
  tree->Branch("jet_eta", "std::vector<float>" , &jet_eta);
  tree->Branch("jet_phi", "std::vector<float>" , &jet_phi);
  tree->Branch("jet_mass", "std::vector<float>" , &jet_mass);
  tree->Branch("jet_pt_raw", "std::vector<float>" , &jet_pt_raw);
  tree->Branch("jet_mass_raw", "std::vector<float>" , &jet_mass_raw);
  tree->Branch("jet_chf", "std::vector<float>" , &jet_chf);
  tree->Branch("jet_nhf", "std::vector<float>" , &jet_nhf);
  tree->Branch("jet_elf", "std::vector<float>" , &jet_elf);
  tree->Branch("jet_phf", "std::vector<float>" , &jet_phf);
  tree->Branch("jet_muf", "std::vector<float>" , &jet_muf);
  tree->Branch("jet_pnet_probHbb","std::vector<float>",&jet_pnet_probHbb);
  tree->Branch("jet_pnet_probHcc","std::vector<float>",&jet_pnet_probHcc);
  tree->Branch("jet_pnet_probHqq","std::vector<float>",&jet_pnet_probHqq);
  tree->Branch("jet_pnet_probQCDbb","std::vector<float>",&jet_pnet_probQCDbb);
  tree->Branch("jet_pnet_probQCDcc","std::vector<float>",&jet_pnet_probQCDcc);
  tree->Branch("jet_pnet_probQCDb","std::vector<float>",&jet_pnet_probQCDb);
  tree->Branch("jet_pnet_probQCDc","std::vector<float>",&jet_pnet_probQCDc);
  tree->Branch("jet_pnet_probQCDothers","std::vector<float>",&jet_pnet_probQCDothers);
  tree->Branch("jet_pnet_regmass","std::vector<float>",&jet_pnet_regmass);

  for(const auto & label : pnetDiscriminatorLabels){
    jet_pnetlast_score[label] = std::vector<float>();
    tree->Branch(("jet_pnetlast_"+label).c_str(),"std::vector<float>", &jet_pnetlast_score[label]);
  }

  for(const auto & label : parTDiscriminatorLabels){
    jet_parTlast_score[label] = std::vector<float>();
    tree->Branch(("jet_parTlast_"+label).c_str(),"std::vector<float>", &jet_parTlast_score[label]);
  }

  tree->Branch("jet_id", "std::vector<unsigned int>" , &jet_id);
  tree->Branch("jet_ncand", "std::vector<unsigned int>" , &jet_ncand);
  tree->Branch("jet_nch", "std::vector<unsigned int>" , &jet_nch);
  tree->Branch("jet_nnh", "std::vector<unsigned int>" , &jet_nnh);
  tree->Branch("jet_nel", "std::vector<unsigned int>" , &jet_nel);
  tree->Branch("jet_nph", "std::vector<unsigned int>" , &jet_nph);
  tree->Branch("jet_nmu", "std::vector<unsigned int>" , &jet_nmu);
  tree->Branch("jet_hflav", "std::vector<unsigned int>" , &jet_hflav);
  tree->Branch("jet_pflav", "std::vector<int>" , &jet_pflav);
  tree->Branch("jet_nbhad", "std::vector<unsigned int>" , &jet_nbhad);
  tree->Branch("jet_nchad", "std::vector<unsigned int>" , &jet_nchad);
  if(isMC){
    tree->Branch("jet_genmatch_pt","std::vector<float>" , &jet_genmatch_pt);
    tree->Branch("jet_genmatch_eta","std::vector<float>" , &jet_genmatch_eta);
    tree->Branch("jet_genmatch_phi","std::vector<float>" , &jet_genmatch_phi);
    tree->Branch("jet_genmatch_mass","std::vector<float>" , &jet_genmatch_mass);
    tree->Branch("jet_genmatch_wnu_pt","std::vector<float>" , &jet_genmatch_wnu_pt);
    tree->Branch("jet_genmatch_wnu_eta","std::vector<float>" , &jet_genmatch_wnu_eta);
    tree->Branch("jet_genmatch_wnu_phi","std::vector<float>" , &jet_genmatch_wnu_phi);
    tree->Branch("jet_genmatch_wnu_mass","std::vector<float>" , &jet_genmatch_wnu_mass);
  }

  tree->Branch("jet_softdrop_pt","std::vector<float>", &jet_softdrop_pt);
  tree->Branch("jet_softdrop_pt_raw","std::vector<float>", &jet_softdrop_pt_raw);
  tree->Branch("jet_softdrop_eta","std::vector<float>", &jet_softdrop_eta);
  tree->Branch("jet_softdrop_phi","std::vector<float>", &jet_softdrop_phi);
  tree->Branch("jet_softdrop_mass","std::vector<float>", &jet_softdrop_mass);
  tree->Branch("jet_softdrop_mass_raw","std::vector<float>", &jet_softdrop_mass_raw);

  tree->Branch("jet_softdrop_subjet_pt","std::vector<float>", &jet_softdrop_subjet_pt);
  tree->Branch("jet_softdrop_subjet_pt_raw","std::vector<float>", &jet_softdrop_subjet_pt_raw);
  tree->Branch("jet_softdrop_subjet_eta","std::vector<float>", &jet_softdrop_subjet_eta);
  tree->Branch("jet_softdrop_subjet_phi","std::vector<float>", &jet_softdrop_subjet_phi);
  tree->Branch("jet_softdrop_subjet_mass","std::vector<float>", &jet_softdrop_subjet_mass);
  tree->Branch("jet_softdrop_subjet_mass_raw","std::vector<float>", &jet_softdrop_subjet_mass_raw);
  tree->Branch("jet_softdrop_subjet_ijet","std::vector<unsigned int>", &jet_softdrop_subjet_ijet);
  tree->Branch("jet_softdrop_subjet_nbhad","std::vector<unsigned int>", &jet_softdrop_subjet_nbhad);
  tree->Branch("jet_softdrop_subjet_nchad","std::vector<unsigned int>", &jet_softdrop_subjet_nchad);
  tree->Branch("jet_softdrop_subjet_hflav","std::vector<unsigned int>", &jet_softdrop_subjet_hflav);
  tree->Branch("jet_softdrop_subjet_pflav","std::vector<int>", &jet_softdrop_subjet_pflav);

  if(isMC){
    tree->Branch("jet_softdrop_genmatch_pt","std::vector<float>", &jet_softdrop_genmatch_pt);
    tree->Branch("jet_softdrop_genmatch_eta","std::vector<float>", &jet_softdrop_genmatch_eta);
    tree->Branch("jet_softdrop_genmatch_phi","std::vector<float>", &jet_softdrop_genmatch_phi);
    tree->Branch("jet_softdrop_genmatch_mass","std::vector<float>", &jet_softdrop_genmatch_mass);
    tree->Branch("jet_softdrop_subjet_genmatch_pt","std::vector<float>", &jet_softdrop_subjet_genmatch_pt);
    tree->Branch("jet_softdrop_subjet_genmatch_eta","std::vector<float>", &jet_softdrop_subjet_genmatch_eta);
    tree->Branch("jet_softdrop_subjet_genmatch_phi","std::vector<float>", &jet_softdrop_subjet_genmatch_phi);
    tree->Branch("jet_softdrop_subjet_genmatch_mass","std::vector<float>", &jet_softdrop_subjet_genmatch_mass);
  }
  
  tree->Branch("jet_sv_pt", "std::vector<float>" , &jet_sv_pt);
  tree->Branch("jet_sv_eta", "std::vector<float>" , &jet_sv_eta);
  tree->Branch("jet_sv_phi", "std::vector<float>" , &jet_sv_phi);
  tree->Branch("jet_sv_mass", "std::vector<float>" , &jet_sv_mass);
  tree->Branch("jet_sv_energy", "std::vector<float>" , &jet_sv_energy);
  tree->Branch("jet_sv_chi2", "std::vector<float>" , &jet_sv_chi2);
  tree->Branch("jet_sv_dxy", "std::vector<float>" , &jet_sv_dxy);
  tree->Branch("jet_sv_dxysig", "std::vector<float>" , &jet_sv_dxysig);
  tree->Branch("jet_sv_d3d", "std::vector<float>" , &jet_sv_d3d);
  tree->Branch("jet_sv_d3dsig", "std::vector<float>" , &jet_sv_d3dsig);
  tree->Branch("jet_sv_ntrack", "std::vector<unsigned int>" , &jet_sv_ntrack);
  tree->Branch("jet_sv_ijet", "std::vector<unsigned int>" , &jet_sv_ijet);
    
  tree->Branch("jet_pfcandidate_pt","std::vector<float>",&jet_pfcandidate_pt);
  tree->Branch("jet_pfcandidate_eta","std::vector<float>",&jet_pfcandidate_eta);
  tree->Branch("jet_pfcandidate_phi","std::vector<float>",&jet_pfcandidate_phi);
  tree->Branch("jet_pfcandidate_mass","std::vector<float>",&jet_pfcandidate_mass);
  tree->Branch("jet_pfcandidate_energy","std::vector<float>",&jet_pfcandidate_energy);
  tree->Branch("jet_pfcandidate_calofraction","std::vector<float>",&jet_pfcandidate_calofraction);
  tree->Branch("jet_pfcandidate_hcalfraction","std::vector<float>",&jet_pfcandidate_hcalfraction);
  tree->Branch("jet_pfcandidate_dz","std::vector<float>",&jet_pfcandidate_dz);
  tree->Branch("jet_pfcandidate_dzsig","std::vector<float>",&jet_pfcandidate_dzsig);
  tree->Branch("jet_pfcandidate_dxy","std::vector<float>",&jet_pfcandidate_dxy);
  tree->Branch("jet_pfcandidate_dxysig","std::vector<float>",&jet_pfcandidate_dxysig);
  tree->Branch("jet_pfcandidate_puppiw","std::vector<float>",&jet_pfcandidate_puppiw);
  tree->Branch("jet_pfcandidate_frompv","std::vector<unsigned int>",&jet_pfcandidate_frompv);
  tree->Branch("jet_pfcandidate_id","std::vector<unsigned int>",&jet_pfcandidate_id);
  tree->Branch("jet_pfcandidate_charge","std::vector<int>",&jet_pfcandidate_charge);
  tree->Branch("jet_pfcandidate_ijet","std::vector<unsigned int>",&jet_pfcandidate_ijet);
  tree->Branch("jet_pfcandidate_candjet_pperp_ratio","std::vector<float>",&jet_pfcandidate_candjet_pperp_ratio);
  tree->Branch("jet_pfcandidate_candjet_ppara_ratio","std::vector<float>",&jet_pfcandidate_candjet_ppara_ratio);
  tree->Branch("jet_pfcandidate_candjet_deta","std::vector<float>",&jet_pfcandidate_candjet_deta);
  tree->Branch("jet_pfcandidate_candjet_dphi","std::vector<float>",&jet_pfcandidate_candjet_dphi);
  tree->Branch("jet_pfcandidate_candjet_etarel","std::vector<float>",&jet_pfcandidate_candjet_etarel);
  tree->Branch("jet_pfcandidate_track_chi2","std::vector<unsigned int>",&jet_pfcandidate_track_chi2);
  tree->Branch("jet_pfcandidate_track_qual","std::vector<unsigned int>",&jet_pfcandidate_track_qual);
  tree->Branch("jet_pfcandidate_track_pterr","std::vector<float>",&jet_pfcandidate_track_pterr);
  tree->Branch("jet_pfcandidate_track_etaerr","std::vector<float>",&jet_pfcandidate_track_etaerr);
  tree->Branch("jet_pfcandidate_track_phierr","std::vector<float>",&jet_pfcandidate_track_phierr);
  tree->Branch("jet_pfcandidate_track_algo","std::vector<unsigned int>",&jet_pfcandidate_track_algo);
  tree->Branch("jet_pfcandidate_trackjet_d3d","std::vector<float>",&jet_pfcandidate_trackjet_d3d);
  tree->Branch("jet_pfcandidate_trackjet_d3dsig","std::vector<float>",&jet_pfcandidate_trackjet_d3dsig);
  tree->Branch("jet_pfcandidate_trackjet_dist","std::vector<float>",&jet_pfcandidate_trackjet_dist);
  tree->Branch("jet_pfcandidate_trackjet_decayL","std::vector<float>",&jet_pfcandidate_trackjet_decayL);
  tree->Branch("jet_pfcandidate_nhits","std::vector<unsigned int>",&jet_pfcandidate_nhits);
  tree->Branch("jet_pfcandidate_npixhits","std::vector<unsigned int>",&jet_pfcandidate_npixhits);
  tree->Branch("jet_pfcandidate_nstriphits","std::vector<unsigned int>",&jet_pfcandidate_nstriphits);
  tree->Branch("jet_pfcandidate_nlosthits","std::vector<unsigned int>",&jet_pfcandidate_nlosthits);
  tree->Branch("jet_pfcandidate_npixlayers","std::vector<unsigned int>",&jet_pfcandidate_npixlayers);
  tree->Branch("jet_pfcandidate_nstriplayers","std::vector<unsigned int>",&jet_pfcandidate_nstriplayers);
  tree->Branch("jet_pfcandidate_tau_signal",",std::vector<unsigned int>",&jet_pfcandidate_tau_signal);
  tree->Branch("jet_pfcandidate_tau_boosted_signal",",std::vector<unsigned int>",&jet_pfcandidate_tau_boosted_signal);
  tree->Branch("jet_pfcandidate_muon_id","std::vector<unsigned int>",&jet_pfcandidate_muon_id);
  tree->Branch("jet_pfcandidate_muon_isglobal","std::vector<unsigned int>",&jet_pfcandidate_muon_isglobal);
  tree->Branch("jet_pfcandidate_muon_chi2","std::vector<float>",&jet_pfcandidate_muon_chi2);
  tree->Branch("jet_pfcandidate_muon_nvalidhit","std::vector<unsigned int>",&jet_pfcandidate_muon_nvalidhit);
  tree->Branch("jet_pfcandidate_muon_nstation","std::vector<unsigned int>",&jet_pfcandidate_muon_nstation);
  tree->Branch("jet_pfcandidate_muon_segcomp","std::vector<float>",&jet_pfcandidate_muon_segcomp);
  tree->Branch("jet_pfcandidate_electron_eOverP","std::vector<float>",&jet_pfcandidate_electron_eOverP);
  tree->Branch("jet_pfcandidate_electron_detaIn","std::vector<float>",&jet_pfcandidate_electron_detaIn);
  tree->Branch("jet_pfcandidate_electron_dphiIn","std::vector<float>",&jet_pfcandidate_electron_dphiIn);
  tree->Branch("jet_pfcandidate_electron_r9","std::vector<float>",&jet_pfcandidate_electron_r9);
  tree->Branch("jet_pfcandidate_electron_sigIetaIeta","std::vector<float>",&jet_pfcandidate_electron_sigIetaIeta);
  tree->Branch("jet_pfcandidate_electron_sigIphiIphi","std::vector<float>",&jet_pfcandidate_electron_sigIphiIphi);
  tree->Branch("jet_pfcandidate_electron_convProb","std::vector<float>",&jet_pfcandidate_electron_convProb);
  tree->Branch("jet_pfcandidate_photon_sigIetaIeta","std::vector<float>",&jet_pfcandidate_photon_sigIetaIeta);
  tree->Branch("jet_pfcandidate_photon_eVeto","std::vector<float>",&jet_pfcandidate_photon_eVeto);
  tree->Branch("jet_pfcandidate_photon_r9","std::vector<float>",&jet_pfcandidate_photon_r9);

  tree->Branch("jet_losttrack_pt","std::vector<float>",&jet_losttrack_pt);
  tree->Branch("jet_losttrack_eta","std::vector<float>",&jet_losttrack_eta);
  tree->Branch("jet_losttrack_phi","std::vector<float>",&jet_losttrack_phi);
  tree->Branch("jet_losttrack_mass","std::vector<float>",&jet_losttrack_mass);
  tree->Branch("jet_losttrack_energy","std::vector<float>",&jet_losttrack_energy);
  tree->Branch("jet_losttrack_dz","std::vector<float>",&jet_losttrack_dz);
  tree->Branch("jet_losttrack_dzsig","std::vector<float>",&jet_losttrack_dzsig);
  tree->Branch("jet_losttrack_dxy","std::vector<float>",&jet_losttrack_dxy);
  tree->Branch("jet_losttrack_dxysig","std::vector<float>",&jet_losttrack_dxysig);
  tree->Branch("jet_losttrack_frompv","std::vector<unsigned int>",&jet_losttrack_frompv);
  tree->Branch("jet_losttrack_charge","std::vector<int>",&jet_losttrack_charge);
  tree->Branch("jet_losttrack_ijet","std::vector<unsigned int>",&jet_losttrack_ijet);
  tree->Branch("jet_losttrack_candjet_deta","std::vector<float>",&jet_losttrack_candjet_deta);
  tree->Branch("jet_losttrack_candjet_dphi","std::vector<float>",&jet_losttrack_candjet_dphi);
  tree->Branch("jet_losttrack_candjet_etarel","std::vector<float>",&jet_losttrack_candjet_etarel);
  tree->Branch("jet_losttrack_trackjet_d3d","std::vector<float>",&jet_losttrack_trackjet_d3d);
  tree->Branch("jet_losttrack_trackjet_d3dsig","std::vector<float>",&jet_losttrack_trackjet_d3dsig);
  tree->Branch("jet_losttrack_trackjet_dist","std::vector<float>",&jet_losttrack_trackjet_dist);
  tree->Branch("jet_losttrack_trackjet_decayL","std::vector<float>",&jet_losttrack_trackjet_decayL);
  tree->Branch("jet_losttrack_track_chi2","std::vector<unsigned int>",&jet_losttrack_track_chi2);
  tree->Branch("jet_losttrack_track_pterr","std::vector<float>",&jet_losttrack_track_pterr);
  tree->Branch("jet_losttrack_track_etaerr","std::vector<float>",&jet_losttrack_track_etaerr);
  tree->Branch("jet_losttrack_track_phierr","std::vector<float>",&jet_losttrack_track_phierr);
  tree->Branch("jet_losttrack_track_algo","std::vector<unsigned int>",&jet_losttrack_track_algo);
  tree->Branch("jet_losttrack_track_qual","std::vector<unsigned int>",&jet_losttrack_track_qual);
  tree->Branch("jet_losttrack_nhits","std::vector<unsigned int>",&jet_losttrack_nhits);
  tree->Branch("jet_losttrack_npixhits","std::vector<unsigned int>",&jet_losttrack_npixhits);
  tree->Branch("jet_losttrack_nstriphits","std::vector<unsigned int>",&jet_losttrack_nstriphits);
  tree->Branch("jet_losttrack_nlosthits","std::vector<unsigned int>",&jet_losttrack_nlosthits);
  tree->Branch("jet_losttrack_npixlayers","std::vector<unsigned int>",&jet_losttrack_npixlayers);
  tree->Branch("jet_losttrack_nstriplayers","std::vector<unsigned int>",&jet_losttrack_nstriplayers);

}

void TrainingTreeMakerAK8::endJob() {}

void TrainingTreeMakerAK8::beginRun(edm::Run const& iRun, edm::EventSetup const& iSetup) {

  HLTConfigProvider fltrConfig;
  bool flag = false;
  fltrConfig.init(iRun, iSetup, filterResultsTag.process(), flag);
  
  // MET filter Paths
  filterPathsMap.clear();
  filterPathsVector.clear();
  filterPathsVector.push_back("Flag_goodVertices");
  filterPathsVector.push_back("Flag_globalSuperTightHalo2016Filter");
  filterPathsVector.push_back("Flag_HBHENoiseFilter");
  filterPathsVector.push_back("Flag_HBHENoiseIsoFilter");
  filterPathsVector.push_back("Flag_EcalDeadCellTriggerPrimitiveFilter");
  filterPathsVector.push_back("Flag_BadPFMuonFilter");
  filterPathsVector.push_back("Flag_BadChargedCandidateFilter");
  
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    filterPathsMap[filterPathsVector[i]] = -1;
  }
  
  for(size_t i = 0; i < filterPathsVector.size(); i++){
    TPRegexp pattern(filterPathsVector[i]);
    for(size_t j = 0; j < fltrConfig.triggerNames().size(); j++){
      std::string pathName = fltrConfig.triggerNames()[j];
      if(TString(pathName).Contains(pattern)){
	filterPathsMap[filterPathsVector[i]] = j;
      }
    }
  }
}
 
void TrainingTreeMakerAK8::endRun(edm::Run const&, edm::EventSetup const&) {}

void TrainingTreeMakerAK8::initializeBranches(){

  event  = 0;
  run    = 0;
  lumi   = 0;
  putrue = 0;
  flags  = 0;
  wgt    = 0.;
  rho    = 0.;

  gen_particle_pt.clear();
  gen_particle_eta.clear();
  gen_particle_phi.clear();
  gen_particle_mass.clear();
  gen_particle_id.clear();
  gen_particle_status.clear();
  gen_particle_daughters_id.clear();
  gen_particle_daughters_igen.clear();
  gen_particle_daughters_pt.clear();
  gen_particle_daughters_eta.clear();
  gen_particle_daughters_phi.clear();
  gen_particle_daughters_mass.clear();
  gen_particle_daughters_status.clear();
  gen_particle_daughters_charge.clear();

  lhe_particle_pt.clear();
  lhe_particle_eta.clear();
  lhe_particle_phi.clear();
  lhe_particle_mass.clear();
  lhe_particle_id.clear();
  lhe_particle_status.clear();

  muon_pt.clear();
  muon_eta.clear();
  muon_phi.clear();
  muon_mass.clear();
  muon_energy.clear();
  muon_id.clear();
  muon_charge.clear();
  muon_iso.clear();
  muon_d0.clear();
  muon_dz.clear();

  electron_pt.clear();
  electron_pt_corr.clear();
  electron_eta.clear();
  electron_phi.clear();
  electron_mass.clear();
  electron_energy.clear();
  electron_id.clear();
  electron_idscore.clear();
  electron_charge.clear();
  electron_d0.clear();
  electron_dz.clear();

  photon_pt.clear();
  photon_pt_corr.clear();
  photon_eta.clear();
  photon_phi.clear();
  photon_mass.clear();
  photon_energy.clear();
  photon_id.clear();
  photon_idscore.clear();

  tau_pt.clear();
  tau_eta.clear();
  tau_phi.clear();
  tau_mass.clear();
  tau_energy.clear();
  tau_dxy.clear();
  tau_dz.clear();
  tau_decaymode.clear();
  tau_idjet.clear();
  tau_idele.clear();
  tau_idmu.clear();
  tau_idjet_wp.clear();
  tau_idmu_wp.clear();
  tau_idele_wp.clear();
  tau_charge.clear();
  tau_genmatch_pt.clear();
  tau_genmatch_eta.clear();
  tau_genmatch_phi.clear();
  tau_genmatch_mass.clear();
  tau_genmatch_decaymode.clear();

  tau_boosted_pt.clear();
  tau_boosted_eta.clear();
  tau_boosted_phi.clear();
  tau_boosted_mass.clear();
  tau_boosted_energy.clear();
  tau_boosted_dxy.clear();
  tau_boosted_dz.clear();
  tau_boosted_decaymode.clear();
  tau_boosted_idjet.clear();
  tau_boosted_idele.clear();
  tau_boosted_idmu.clear();
  tau_boosted_idjet_wp.clear();
  tau_boosted_idmu_wp.clear();
  tau_boosted_idele_wp.clear();
  tau_boosted_charge.clear();
  tau_boosted_genmatch_pt.clear();
  tau_boosted_genmatch_eta.clear();
  tau_boosted_genmatch_phi.clear();
  tau_boosted_genmatch_mass.clear();
  tau_boosted_genmatch_decaymode.clear();

  met = 0.;
  met_phi = 0;

  npv = 0;
  nsv = 0;

  jet_pt.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_mass.clear();
  jet_pt_raw.clear();
  jet_mass_raw.clear();
  jet_chf.clear();
  jet_nhf.clear();
  jet_elf.clear();
  jet_phf.clear();
  jet_muf.clear();

  jet_pnet_probHbb.clear();
  jet_pnet_probHcc.clear();
  jet_pnet_probHqq.clear();
  jet_pnet_probQCDbb.clear();
  jet_pnet_probQCDcc.clear();
  jet_pnet_probQCDb.clear();
  jet_pnet_probQCDc.clear();
  jet_pnet_probQCDothers.clear();
  jet_pnet_regmass.clear();

  for(auto & imap : jet_pnetlast_score)
    imap.second.clear();
  for(auto & imap : jet_parTlast_score)
    imap.second.clear();

  jet_id.clear();
  jet_ncand.clear();
  jet_nch.clear();
  jet_nnh.clear();
  jet_nel.clear();
  jet_nph.clear();
  jet_nmu.clear();
  jet_hflav.clear();
  jet_pflav.clear();
  jet_nbhad.clear();
  jet_nchad.clear();
  jet_genmatch_pt.clear();
  jet_genmatch_eta.clear();
  jet_genmatch_phi.clear();
  jet_genmatch_mass.clear();
  jet_genmatch_wnu_pt.clear();
  jet_genmatch_wnu_eta.clear();
  jet_genmatch_wnu_phi.clear();
  jet_genmatch_wnu_mass.clear();

  jet_softdrop_pt.clear();
  jet_softdrop_eta.clear();
  jet_softdrop_phi.clear();
  jet_softdrop_mass.clear();
  jet_softdrop_pt_raw.clear();
  jet_softdrop_mass_raw.clear();
  jet_softdrop_subjet_pt.clear();
  jet_softdrop_subjet_pt_raw.clear();
  jet_softdrop_subjet_eta.clear();
  jet_softdrop_subjet_phi.clear();
  jet_softdrop_subjet_mass.clear();
  jet_softdrop_subjet_mass_raw.clear();
  jet_softdrop_subjet_ijet.clear(); 
  jet_softdrop_subjet_nbhad.clear();
  jet_softdrop_subjet_nchad.clear();
  jet_softdrop_subjet_hflav.clear();
  jet_softdrop_subjet_pflav.clear();
  jet_softdrop_genmatch_pt.clear();
  jet_softdrop_genmatch_eta.clear();
  jet_softdrop_genmatch_phi.clear();
  jet_softdrop_genmatch_mass.clear();  
  jet_softdrop_subjet_genmatch_pt.clear();
  jet_softdrop_subjet_genmatch_eta.clear();
  jet_softdrop_subjet_genmatch_phi.clear();
  jet_softdrop_subjet_genmatch_mass.clear(); 
 
  jet_sv_pt.clear();
  jet_sv_eta.clear();
  jet_sv_phi.clear();
  jet_sv_mass.clear();
  jet_sv_energy.clear();
  jet_sv_chi2.clear();
  jet_sv_dxy.clear();
  jet_sv_dxysig.clear();
  jet_sv_d3d.clear();
  jet_sv_d3dsig.clear();
  jet_sv_ntrack.clear();
  jet_sv_ijet.clear();

  jet_pfcandidate_pt.clear();
  jet_pfcandidate_eta.clear();
  jet_pfcandidate_phi.clear();
  jet_pfcandidate_mass.clear();
  jet_pfcandidate_energy.clear();
  jet_pfcandidate_charge.clear();
  jet_pfcandidate_calofraction.clear();
  jet_pfcandidate_hcalfraction.clear();
  jet_pfcandidate_dz.clear();
  jet_pfcandidate_dxy.clear();
  jet_pfcandidate_dzsig.clear();
  jet_pfcandidate_dxysig.clear();
  jet_pfcandidate_frompv.clear();
  jet_pfcandidate_candjet_pperp_ratio.clear();
  jet_pfcandidate_candjet_ppara_ratio.clear();
  jet_pfcandidate_candjet_deta.clear();
  jet_pfcandidate_candjet_dphi.clear();
  jet_pfcandidate_candjet_etarel.clear();
  jet_pfcandidate_track_chi2.clear();
  jet_pfcandidate_track_pterr.clear();
  jet_pfcandidate_track_etaerr.clear();
  jet_pfcandidate_track_phierr.clear();
  jet_pfcandidate_track_algo.clear();
  jet_pfcandidate_track_qual.clear();
  jet_pfcandidate_trackjet_d3d.clear();
  jet_pfcandidate_trackjet_d3dsig.clear();
  jet_pfcandidate_trackjet_dist.clear();
  jet_pfcandidate_trackjet_decayL.clear();
  jet_pfcandidate_nhits.clear();
  jet_pfcandidate_npixhits.clear();
  jet_pfcandidate_nstriphits.clear();
  jet_pfcandidate_nlosthits.clear();
  jet_pfcandidate_npixlayers.clear();
  jet_pfcandidate_nstriplayers.clear();
  jet_pfcandidate_id.clear();
  jet_pfcandidate_ijet.clear();
  jet_pfcandidate_puppiw.clear();
  jet_pfcandidate_tau_signal.clear();
  jet_pfcandidate_tau_boosted_signal.clear();
  jet_pfcandidate_muon_id.clear();
  jet_pfcandidate_muon_isglobal.clear();
  jet_pfcandidate_muon_chi2.clear();
  jet_pfcandidate_muon_nvalidhit.clear();
  jet_pfcandidate_muon_nstation.clear();
  jet_pfcandidate_muon_segcomp.clear();
  jet_pfcandidate_electron_eOverP.clear();
  jet_pfcandidate_electron_detaIn.clear();
  jet_pfcandidate_electron_dphiIn.clear();
  jet_pfcandidate_electron_r9.clear();
  jet_pfcandidate_electron_sigIetaIeta.clear();
  jet_pfcandidate_electron_sigIphiIphi.clear();
  jet_pfcandidate_electron_convProb.clear();
  jet_pfcandidate_photon_sigIetaIeta.clear();
  jet_pfcandidate_photon_eVeto.clear();
  jet_pfcandidate_photon_r9.clear();

  jet_losttrack_pt.clear();
  jet_losttrack_eta.clear();
  jet_losttrack_phi.clear();
  jet_losttrack_mass.clear();
  jet_losttrack_energy.clear();
  jet_losttrack_charge.clear();
  jet_losttrack_dz.clear();
  jet_losttrack_dxy.clear();
  jet_losttrack_dzsig.clear();
  jet_losttrack_dxysig.clear();
  jet_losttrack_frompv.clear();
  jet_losttrack_track_chi2.clear();
  jet_losttrack_track_pterr.clear();
  jet_losttrack_track_etaerr.clear();
  jet_losttrack_track_phierr.clear();
  jet_losttrack_track_algo.clear();
  jet_losttrack_track_qual.clear();
  jet_losttrack_candjet_deta.clear();
  jet_losttrack_candjet_dphi.clear();
  jet_losttrack_candjet_etarel.clear();
  jet_losttrack_trackjet_d3d.clear();
  jet_losttrack_trackjet_d3dsig.clear();
  jet_losttrack_trackjet_dist.clear();
  jet_losttrack_trackjet_decayL.clear();
  jet_losttrack_nhits.clear();
  jet_losttrack_npixhits.clear();
  jet_losttrack_nstriphits.clear();
  jet_losttrack_nlosthits.clear();
  jet_losttrack_npixlayers.clear();
  jet_losttrack_nstriplayers.clear();
  jet_losttrack_ijet.clear();
}

void TrainingTreeMakerAK8::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

// to apply jet ID: https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL#Preliminary_Recommendations_for
bool TrainingTreeMakerAK8::applyJetID(const pat::Jet & jet, const std::string & level){
  
  if(level != "tight" and level != "tightLepVeto")
    return true;

  if(level != "tight" and level != "tightLepVeto")
    return true;

  double eta  = jet.eta();
  double nhf  = jet.neutralHadronEnergyFraction();
  double nemf = jet.neutralEmEnergyFraction();
  double chf  = jet.chargedHadronEnergyFraction();
  double muf  = jet.muonEnergyFraction();
  double cemf = jet.chargedEmEnergyFraction();
  int np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();
  int nnp  = jet.neutralMultiplicity();
  int nch  = jet.chargedMultiplicity();
  int jetid = 0;

  if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
  if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
  if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0) jetid += 1;
  if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
  if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 1;
  if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 2;
  if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 1;
  if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 2;

  if(level == "tight" and jetid > 1) return true;
  else if(level == "tightLepVeto" and jetid > 2) return true;
  else return false;
  
}

DEFINE_FWK_MODULE(TrainingTreeMakerAK8);
