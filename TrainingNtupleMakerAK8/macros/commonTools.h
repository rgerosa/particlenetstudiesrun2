#include "CMS_style.h"

#ifndef commonTools_h
#define commonTools_h

// Constant used in all macros
vector<int> colors = {kBlack,kRed,kBlue,kOrange+1,kCyan+1,kViolet,kGreen+2,kYellow+1};
vector<int> style  = {1,9,7,2,3,4,5};
vector<int> markers  = {20,21,22,23,29,33,34,49};

enum class sample_type {undefined=-1,qcd=0,qcdb=1,XtoHH=2,Hjet=3,Zjet=4,ggHH=5,bulkG=6};

TGraph* buildROC(shared_ptr<TH1F> sig, shared_ptr<TH1F> bkg, bool interpolate = false){

  TGraph* roc = new TGraph();
  roc->SetName(TString(sig->GetName()).ReplaceAll("hist_sig","roc"));
  roc->SetPoint(0,1,1);
  std::vector<double> xval;
  std::vector<double> yval;
  for(int ibin = 0; ibin < sig->GetNbinsX(); ibin++){
    xval.push_back(sig->Integral(ibin+1,sig->GetNbinsX()+1)/sig->Integral(0,sig->GetNbinsX()+1));
    yval.push_back(bkg->Integral(ibin+1,bkg->GetNbinsX()+1)/bkg->Integral(0,sig->GetNbinsX()+1));
  }
  sort(xval.begin(),xval.end());
  sort(yval.begin(),yval.end());
  if(not interpolate){
    for(int ibin = 0; ibin < xval.size(); ibin++)
      roc->SetPoint(ibin+1,xval.at(ibin),yval.at(ibin));
  }
  else{
    ROOT::Math::Interpolator* interpolator = new ROOT::Math::Interpolator(xval,yval,ROOT::Math::Interpolation::kCSPLINE);
    for(int ibin = 0; ibin < xval.size(); ibin++)
      roc->SetPoint(ibin+1,xval.at(ibin),interpolator->Eval(xval.at(ibin)));
  }
  return roc;
}



void buildRegResponse(const vector<shared_ptr<TH1F> > response,
		      const vector<string> legends,
                      std::map<string,vector<float> > & parameters,
		      const string & xaxisName = "",
		      const string & label     = "",
		      const string & binname   = "",
		      const string & outputDIR = "",
		      const string & plotName  = "",
                      const string & postfix   = "",
                      const bool & plotGenOverReco = false,
		      const bool & doAnalyticalFit = false,
		      const float & rebinFactor = 3,
		      double q_min_fit = 0.001,
		      double q_max_fit = 0.999,
		      const double & x_min_plot = 0.5,
		      const double & x_max_plot = 1.5
		      ){

  RooMsgService::instance().setSilentMode(kTRUE);
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR) ;
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
  RooFit::Minimizer("Minuit2","minimize");

  TCanvas* canvas = new TCanvas("canvas","canvas",600,600);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();

  TLegend leg (0.7,0.8,0.9,0.92);
  leg.SetBorderSize(0);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);

  vector<TH1F*> h;
  vector<TH1F*> h_sigma_eff;
  float offset   = 0;
  double maximum = 0;

  for(size_t ihist = 0; ihist < response.size(); ihist++){

    bool isSoftDrop = false;
    if(TString(response.at(ihist)->GetName()).Contains("SoftDrop"))
      isSoftDrop = true;

    RooRealVar xvar (("x_"+legends.at(ihist)).c_str(),("x_"+legends.at(ihist)).c_str(),
		     response.at(ihist)->GetMean(),response.at(ihist)->GetXaxis()->GetXmin(),response.at(ihist)->GetXaxis()->GetXmax());
    xvar.setBins(response.at(ihist)->GetNbinsX());

    RooDataHist  hist (Form("%s_datahist",response.at(ihist)->GetName()),"",RooArgList(xvar),response.at(ihist).get());
    RooHistPdf pdf_hist (("pdf_hist_"+legends.at(ihist)).c_str(),"",RooArgSet(xvar),hist,4);

    // pdf for reco/gen and gen/reco                                                                                                                                                                  
    RooRealVar m0  (("m0_"+legends.at(ihist)).c_str(),"",response.at(ihist)->GetMean(),
                    response.at(ihist)->GetMean()-response.at(ihist)->GetRMS(),response.at(ihist)->GetMean()+response.at(ihist)->GetRMS());
    RooRealVar s0  (("s0_"+legends.at(ihist)).c_str(),"",response.at(ihist)->GetRMS(),response.at(ihist)->GetRMS()/10,response.at(ihist)->GetRMS()*2);
    RooRealVar w0  (("w0_"+legends.at(ihist)).c_str(),"",response.at(ihist)->GetRMS(),response.at(ihist)->GetRMS()/10,response.at(ihist)->GetRMS()*2);
    RooRealVar as  (("as_"+legends.at(ihist)).c_str(),"",0.1,-10,10);
    RooRealVar r1  (("r1_"+legends.at(ihist)).c_str(),"",-0.01,-10,10);
    RooRealVar r2  (("r2_"+legends.at(ihist)).c_str(),"",0.01,-10,10);
    RooBukinPdf bk (("bk_"+legends.at(ihist)).c_str(),"",xvar,m0,s0,as,r1,r2);
    RooVoigtian vg (("vg_"+legends.at(ihist)).c_str(),"",xvar,m0,w0,s0);

    RooAbsPdf* pdf = &bk;
    if(TString(response.at(ihist)->GetName()).Contains("Htt") or 
       TString(response.at(ihist)->GetName()).Contains("Htm") or 
       TString(response.at(ihist)->GetName()).Contains("Hte")){
      pdf = &vg;
      q_min_fit = 0.01;
      q_max_fit = 0.99;
    }

    double x_qmin  = 0;
    double x_qmax  = 0;
    // make a fit in \pm 3-sigma                                                                                                                                                                    
    response.at(ihist)->GetQuantiles(1,&x_qmin,&q_min_fit);
    response.at(ihist)->GetQuantiles(1,&x_qmax,&q_max_fit);
    if(isSoftDrop)
      x_qmin = 0.2;
    xvar.setRange("fitRange",x_qmin,x_qmax);   

    if(doAnalyticalFit and not isSoftDrop){
      pdf->fitTo(hist,RooFit::SumW2Error(kTRUE),RooFit::Optimize(1));
      pdf->fitTo(hist,RooFit::SumW2Error(kTRUE),RooFit::Optimize(1),RooFit::Range("fitRange"));
    }
    
    // start the plotting part                                                                                                                                                                        
    response.at(ihist)->GetXaxis()->SetTitle(xaxisName.c_str());
    response.at(ihist)->GetXaxis()->SetTitle(xaxisName.c_str());
    response.at(ihist)->GetYaxis()->SetTitle("a.u.");
    if(ihist == 0)
      response.at(ihist)->Draw();

    maximum = TMath::Max(maximum,response.at(ihist)->GetMaximum());

    if(doAnalyticalFit and not isSoftDrop)
      h.push_back((TH1F*) pdf->createHistogram(("h_"+legends.at(ihist)).c_str(),xvar,
					       RooFit::Binning(int(response.at(ihist)->FindBin(x_max_plot)-response.at(ihist)->FindBin(x_min_plot)),x_min_plot,x_max_plot)));
    else
      h.push_back((TH1F*) pdf_hist.createHistogram(("h_"+legends.at(ihist)).c_str(),xvar,
						   RooFit::Binning(int(response.at(ihist)->FindBin(x_max_plot)-response.at(ihist)->FindBin(x_min_plot)),x_min_plot,x_max_plot)));

    for(int ibin = 0; ibin < h.back()->GetNbinsX(); ibin++)
      h.back()->SetBinContent(ibin+1,h.back()->GetBinContent(ibin+1)*response.at(ihist)->Integral(response.at(ihist)->FindBin(x_min_plot),response.at(ihist)->FindBin(x_max_plot))/h.back()->Integral());

    response.at(ihist)->SetLineColor(colors.at(ihist));
    response.at(ihist)->SetMarkerColor(colors.at(ihist));
    response.at(ihist)->SetMarkerStyle(20);
    response.at(ihist)->SetMarkerSize(1);
    response.at(ihist)->SetLineWidth(2);
    response.at(ihist)->Draw("EPsame");

    h.back()->SetLineColor(colors.at(ihist));
    h.back()->SetLineWidth(2);
    h.back()->Draw("Csame");

    if(doAnalyticalFit and not isSoftDrop)
      h_sigma_eff.push_back((TH1F*) pdf->createHistogram(("h_sigma_eff_"+legends.at(ihist)).c_str(),xvar,
							 RooFit::Binning(int(response.at(ihist)->FindBin(x_max_plot)-response.at(ihist)->FindBin(x_min_plot))*rebinFactor,x_min_plot,x_max_plot)));
    else
      h_sigma_eff.push_back((TH1F*) pdf_hist.createHistogram(("h_sigma_eff_"+legends.at(ihist)).c_str(),xvar,
							     RooFit::Binning(int(response.at(ihist)->FindBin(x_max_plot)-response.at(ihist)->FindBin(x_min_plot)),x_min_plot,x_max_plot)));

    for(int ibin = 0; ibin < h_sigma_eff.back()->GetNbinsX(); ibin++)
      h_sigma_eff.back()->SetBinContent(ibin+1,h_sigma_eff.back()->GetBinContent(ibin+1)*response.at(ihist)->Integral(response.at(ihist)->FindBin(x_min_plot),response.at(ihist)->FindBin(x_max_plot))/h_sigma_eff.back()->Integral());

    
    // FWHM                                                                                                                                                                                  
    double fwhm = h_sigma_eff.back()->GetBinCenter(h_sigma_eff.back()->FindLastBinAbove(h_sigma_eff.back()->GetMaximum()/2.))-
      h_sigma_eff.back()->GetBinCenter(h_sigma_eff.back()->FindFirstBinAbove(h_sigma_eff.back()->GetMaximum()/2.));
    // MPV                                                                                                                                                                                           
    float peak = h_sigma_eff.back()->GetBinCenter(h_sigma_eff.back()->GetMaximumBin());
    // Mean                                                                                                                                                                                        
    float mean = h_sigma_eff.back()->GetMean();
    // Median and sigma-eff                                                                                                                                                                 
    double x_qmed = 0;
    double qmin   = 0.16;
    double qmed   = 0.5;
    double qmax   = 0.84;
    h_sigma_eff.back()->GetQuantiles(1,&x_qmin,&qmin);
    h_sigma_eff.back()->GetQuantiles(1,&x_qmax,&qmax);
    h_sigma_eff.back()->GetQuantiles(1,&x_qmed,&qmed);

    TLatex latex;
    latex.SetTextAlign(13);
    latex.SetNDC();
    latex.SetTextSize(0.03);
    latex.SetTextFont(42);
    latex.SetTextColor(colors.at(ihist));

    if(not doAnalyticalFit and not isSoftDrop)
      peak = h.back()->GetBinCenter(h.back()->GetMaximumBin());
    else
      peak = h_sigma_eff.back()->GetBinCenter(h_sigma_eff.back()->GetMaximumBin());
    latex.DrawLatex(0.7,0.77+offset,Form("Peak = %.3f",peak));
    latex.DrawLatex(0.7,0.73+offset,Form("Mean = %.3f",mean));
    latex.DrawLatex(0.7,0.69+offset,Form("#sigma_{eff} = %.3f",(x_qmax-x_qmin)/2));
    latex.DrawLatex(0.7,0.65+offset,Form("FWHM = %.3f",fwhm));
    offset -= 0.17;

    vector<float> par;
    par.push_back(mean);
    par.push_back(peak);
    par.push_back((x_qmax-x_qmin)/2);
    parameters[legends.at(ihist)+"_"+postfix] = par;

    if(ihist==0){				
      latex.SetTextColor(kBlack);
      latex.SetTextFont(62);
      latex.DrawLatex(0.17,0.78,label.c_str());
    }
    leg.AddEntry(response.at(ihist).get(),legends.at(ihist).c_str(),"L");    
  }

  leg.Draw("same");

  response.front()->GetYaxis()->SetRangeUser(0.,maximum*1.3);
  response.front()->GetXaxis()->SetLabelSize(response.front()->GetXaxis()->GetLabelSize()*0.9);
  response.front()->GetYaxis()->SetLabelSize(response.front()->GetYaxis()->GetLabelSize()*0.9);
  response.front()->GetXaxis()->SetTitleOffset(1.1);

  TLatex latex;
  latex.SetTextAlign(13);
  latex.SetNDC();
  latex.SetTextSize(0.03);
  latex.SetTextFont(42);
  latex.DrawLatex(0.17,0.83,binname.c_str());

  CMS_lumi(canvas,"");
  canvas->SaveAs((outputDIR+"/"+plotName+postfix+".pdf").c_str(),"pdf");
  delete canvas;

  for(auto & hist : h) delete hist;
  for(auto & hist : h_sigma_eff) delete hist;
  h.clear();
  h_sigma_eff.clear();
}


void plotParameterVsObs(const std::vector<TGraph*> & graphs,
                        const std::vector<std::string> & legends,
                        const std::string & xaxis_title,
                        const std::string & yaxis_title,
                        const int & periodicity,
                        const std::string & outputDIR,
                        const std::string & plotName){

  TCanvas* canvas = new TCanvas("canvas","canvas",600,600);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();

  TLegend leg (0.65,0.75,0.9,0.9);
  leg.SetBorderSize(0);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);
  int icolor = 0;
  double maximum = -99;
  double minimum = 100;

  for(size_t igr = 0; igr < graphs.size(); igr++){

    if(igr % periodicity == 0 and igr != 0)
      icolor ++;

    graphs.at(igr)->GetXaxis()->SetTitle(xaxis_title.c_str());
    graphs.at(igr)->GetYaxis()->SetTitle(yaxis_title.c_str());
    graphs.at(igr)->SetLineWidth(2);
    graphs.at(igr)->SetMarkerSize(1);
    graphs.at(igr)->SetMarkerStyle(20);
    graphs.at(igr)->SetLineColor(colors.at(icolor));
    graphs.at(igr)->SetMarkerColor(colors.at(icolor));
    maximum = TMath::Max(maximum,TMath::MaxElement(graphs.at(igr)->GetN(),graphs.at(igr)->GetY()));
    minimum = TMath::Min(minimum,TMath::MinElement(graphs.at(igr)->GetN(),graphs.at(igr)->GetY()));
    if(igr==0)
      graphs.at(igr)->Draw("APL");
    else
      graphs.at(igr)->Draw("PLsame");
    leg.AddEntry(graphs.at(igr),legends.at(igr).c_str(),"L");
  }

  leg.Draw("same");

  graphs.at(0)->GetYaxis()->SetRangeUser(minimum*0.9,maximum*1.1);
  graphs.at(0)->GetYaxis()->SetLabelSize(graphs.at(0)->GetYaxis()->GetLabelSize()*0.85);
  graphs.at(0)->GetXaxis()->SetLabelSize(graphs.at(0)->GetXaxis()->GetLabelSize()*0.85);
  graphs.at(0)->GetYaxis()->SetTitleSize(graphs.at(0)->GetYaxis()->GetTitleSize()*0.90);
  graphs.at(0)->GetXaxis()->SetTitleSize(graphs.at(0)->GetXaxis()->GetTitleSize()*0.90);
  graphs.at(0)->GetXaxis()->SetTitleOffset(1.1);

  leg.Draw("same");
  CMS_lumi(canvas,"");

  canvas->SaveAs((outputDIR+"/"+plotName+".pdf").c_str(),"pdf");
  delete canvas;
}


void plotDistributions(const vector<shared_ptr<TH1F> > & histos,
		       const vector<string> & labels,
		       const string & xaxisName = "",
		       const string & yaxisName = "",
		       const string & binname = "",
		       const string & outputDIR = "",
		       const string & plotName = ""){


  TCanvas* canvas = new TCanvas("canvas","canvas",600,600);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();
  canvas->SetRightMargin(0.15);

  TPad* pad1 = new TPad("pad1","pad1",0,0,1,1);
  pad1->SetFillColor(0);
  pad1->SetFillStyle(0);
  pad1->SetTickx(1);
  pad1->SetTicky(1);
  pad1->SetBottomMargin(0.3);
  pad1->SetRightMargin(0.06);
  pad1->Draw();
  pad1->cd();

  TLegend leg (0.7,0.7,0.9,0.9);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  
  for(size_t ihist = 0; ihist < histos.size(); ihist++){
    histos.at(ihist)->GetXaxis()->SetTitle(xaxisName.c_str());
    histos.at(ihist)->GetYaxis()->SetTitle(yaxisName.c_str());
    histos.at(ihist)->SetLineWidth(2);
    histos.at(ihist)->SetMarkerSize(0.8);
    histos.at(ihist)->SetMarkerStyle(markers.at(ihist));
    histos.at(ihist)->SetMarkerColor(colors.at(ihist));
    histos.at(ihist)->SetLineColor(colors.at(ihist));
    if(ihist == 0){
      histos.at(ihist)->Draw("hist");
      histos.at(ihist)->Draw("Psame");
    }
    else{
      histos.at(0)->GetYaxis()->SetRangeUser(0,TMath::Max(histos.at(ihist)->GetMaximum(),histos.at(0)->GetMaximum()));
      histos.at(ihist)->Draw("hist same");
      histos.at(ihist)->Draw("P same");
    }
    leg.AddEntry(histos.at(ihist).get(),labels.at(ihist).c_str(),"PL");
  }
  histos.at(0)->GetYaxis()->SetRangeUser(0,histos.at(0)->GetMaximum()*1.25);
  leg.Draw("same");

  TPad* pad2 = new TPad("pad1","pad1",0,0,1,1);
  pad2->SetFillColor(0);
  pad2->SetFillStyle(0);
  pad2->SetTickx(1);
  pad2->SetTicky(1);
  pad2->SetTopMargin(0.73);
  pad2->SetRightMargin(0.06);
  pad2->Draw();
  pad2->cd();

  for(size_t ihist = 1; ihist < histos.size(); ihist++){
    TH1F* ratio_clone = (TH1F*) histos.at(ihist)->Clone("clone");
    ratio_clone->Divide(histos.at(0).get());
    ratio_clone->GetYaxis()->SetTitle("Ratio");    
    ratio_clone->GetYaxis()->CenterTitle();
    ratio_clone->GetYaxis()->SetTitleSize(ratio_clone->GetYaxis()->GetTitleSize()*0.8);
    ratio_clone->GetYaxis()->SetLabelSize(ratio_clone->GetYaxis()->GetLabelSize()*0.75);
    ratio_clone->GetYaxis()->SetTitleOffset(1.2);
    ratio_clone->GetXaxis()->SetTitle(xaxisName.c_str());
    ratio_clone->GetYaxis()->SetNdivisions(505);
    ratio_clone->GetYaxis()->SetTickLength(ratio_clone->GetYaxis()->GetTickLength()*3);
    ratio_clone->GetYaxis()->SetRangeUser(0.5,1.5);
    if(ihist==1){
      ratio_clone->Draw("EP");
    }
    else{
      ratio_clone->Draw("EPsame");
    }
  }

  histos.at(0)->GetXaxis()->SetTitleSize(0);
  histos.at(0)->GetXaxis()->SetLabelSize(0);
  
  TLatex latex;
  latex.SetTextAlign(13);
  latex.SetNDC();
  latex.SetTextSize(0.03);
  latex.SetTextFont(62);
  latex.DrawLatex(0.17,0.83,binname.c_str());

  CMS_lumi(canvas,"");
  canvas->SaveAs((outputDIR+"/"+plotName+".pdf").c_str(),"pdf");
  delete canvas;
}

void plotCorrelation(const shared_ptr<TH2F> correlation,
                     const string & yaxisName = "",
                     const string & label = "",
                     const string & binname = "",
                     const string & outputDIR = "",
                     const string & plotName = ""){


  TCanvas* canvas = new TCanvas("canvas","canvas",600,600);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();
  canvas->SetRightMargin(0.15);

  correlation->GetXaxis()->SetTitle("p_{T}^{gen} [GeV]");
  correlation->GetYaxis()->SetTitle(yaxisName.c_str());
  correlation->Draw("colz");
  TProfile* correlation_profile = correlation->ProfileX();
  correlation_profile->SetMarkerColor(kBlack);
  correlation_profile->SetMarkerStyle(20);
  correlation_profile->SetMarkerSize(0.8);
  correlation_profile->Draw("EPsame");

  TLatex latex;
  latex.SetTextAlign(13);
  latex.SetNDC();
  latex.SetTextSize(0.03);
  latex.SetTextFont(42);
  latex.DrawLatex(0.17,0.83,binname.c_str());
  latex.SetTextFont(62);
  latex.DrawLatex(0.17,0.78,label.c_str());

  CMS_lumi(canvas,"");
  canvas->SaveAs((outputDIR+"/"+plotName+".pdf").c_str(),"pdf");
  delete canvas;
}

void plotROC(const vector<TF1*> rocs_1 = {},
             const vector<TF1*> rocs_2 = {},
             const vector<TF1*> rocs_3 = {},
             const vector<TF1*> rocs_4 = {},
             const vector<TF1*> rocs_5 = {},
             const vector<TF1*> rocs_6 = {},
             const vector<string> & legends = {},
             const vector<string> & labels = {},
             const string & binname   = "",
             const string & outputDIR = "",
             const string & plotName  = "",
	     const float  & xmin = 0,
	     const float  & xmax = 1,
	     const float  & ymin = 0.0001,
	     const float  & ymax = 1
	     ){

  TCanvas* canvas = new TCanvas("canvas","canvas",700,600);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();

  TH1F* frame = canvas->DrawFrame(xmin,ymin,xmax,ymax,"");
  frame->GetXaxis()->SetTitle("Signal efficiency");
  frame->GetYaxis()->SetTitle("Background efficiency");
  frame->Draw();

  TLegend leg (0.7,0.2,0.9,0.35);
  leg.SetBorderSize(0);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);

  for(size_t iroc = 0; iroc < rocs_1.size(); iroc++){
    if(rocs_1.size() > 1){
      rocs_1.at(iroc)->SetLineColor(colors.at(iroc));
      rocs_1.at(iroc)->SetLineStyle(style.at(0));
    }
    else
      rocs_1.at(iroc)->SetLineColor(colors.at(0));
    rocs_1.at(iroc)->SetLineWidth(2);
    rocs_1.at(iroc)->Draw("Lsame");
    leg.AddEntry(rocs_1.at(iroc),legends.at(iroc).c_str(),"L");
  }
  for(size_t iroc = 0; iroc < rocs_2.size(); iroc++){
    if(rocs_2.size() > 1){
      rocs_2.at(iroc)->SetLineColor(colors.at(iroc));
      rocs_2.at(iroc)->SetLineStyle(style.at(1));
    }
    else
      rocs_2.at(iroc)->SetLineColor(colors.at(1));
    rocs_2.at(iroc)->SetLineWidth(2);
    rocs_2.at(iroc)->Draw("Lsame");
  }

  for(size_t iroc = 0; iroc < rocs_3.size(); iroc++){
    if(rocs_3.size() > 1){
      rocs_3.at(iroc)->SetLineColor(colors.at(iroc));
      rocs_3.at(iroc)->SetLineStyle(style.at(2));
    }
    else
      rocs_3.at(iroc)->SetLineColor(colors.at(2));
    rocs_3.at(iroc)->SetLineWidth(2);
    rocs_3.at(iroc)->Draw("Lsame");
  }

  for(size_t iroc = 0; iroc < rocs_4.size(); iroc++){
    if(rocs_4.size() > 1){
      rocs_4.at(iroc)->SetLineColor(colors.at(iroc));
      rocs_4.at(iroc)->SetLineStyle(style.at(3));
    }
    else
      rocs_4.at(iroc)->SetLineColor(colors.at(3));
    rocs_4.at(iroc)->SetLineWidth(2);
    rocs_4.at(iroc)->Draw("Lsame");
  }
  for(size_t iroc = 0; iroc < rocs_5.size(); iroc++){
    if(rocs_5.size() > 1){
      rocs_5.at(iroc)->SetLineColor(colors.at(iroc));
      rocs_5.at(iroc)->SetLineStyle(style.at(4));
    }
    else
      rocs_5.at(iroc)->SetLineColor(colors.at(4));
    rocs_5.at(iroc)->SetLineWidth(2);
    rocs_5.at(iroc)->Draw("Lsame");
  }

  for(size_t iroc = 0; iroc < rocs_6.size(); iroc++){
    if(rocs_6.size() > 1){
      rocs_6.at(iroc)->SetLineColor(colors.at(iroc));
      rocs_6.at(iroc)->SetLineStyle(style.at(5));
    }
    else
      rocs_6.at(iroc)->SetLineColor(colors.at(5));
    rocs_6.at(iroc)->SetLineWidth(2);
    rocs_6.at(iroc)->Draw("Lsame");
  }

  if(legends.size()>1) leg.Draw("same");
  TLatex latex;
  latex.SetTextAlign(13);
  latex.SetNDC();
  latex.SetTextSize(0.03);
  latex.SetTextFont(42);
  latex.DrawLatex(0.17,0.83,binname.c_str());

  float ylegmin = 0.65;
  float ylegmax = 0.78;
  if(rocs_1.size() > 0 and rocs_2.size() == 0 and rocs_3.size() == 0 and rocs_4.size() == 0 and rocs_5.size() == 0 and rocs_6.size() == 0) ylegmax = 0.68;
  if(rocs_1.size() > 0 and rocs_2.size() > 0 and rocs_3.size() == 0 and rocs_4.size() == 0 and rocs_5.size() == 0 and rocs_6.size() == 0) ylegmax = 0.71;
  if(rocs_1.size() > 0 and rocs_2.size() > 0 and rocs_3.size() > 0 and rocs_4.size() == 0 and rocs_5.size() == 0 and rocs_6.size() == 0) ylegmax = 0.73;
  if(rocs_1.size() > 0 and rocs_2.size() > 0 and rocs_3.size() > 0 and rocs_4.size() > 0 and rocs_5.size() == 0 and rocs_6.size() == 0) ylegmax = 0.76;

  TLegend leg2 (0.17,ylegmin,0.45,ylegmax);
  leg2.SetBorderSize(0);
  leg2.SetFillColor(0);
  leg2.SetFillStyle(0);
  if(rocs_1.size()) leg2.AddEntry(rocs_1.at(0),labels.at(0).c_str(),"L");
  if(rocs_2.size()) leg2.AddEntry(rocs_2.at(0),labels.at(1).c_str(),"L");
  if(rocs_3.size()) leg2.AddEntry(rocs_3.at(0),labels.at(2).c_str(),"L");
  if(rocs_4.size()) leg2.AddEntry(rocs_4.at(0),labels.at(3).c_str(),"L");
  if(rocs_5.size()) leg2.AddEntry(rocs_5.at(0),labels.at(4).c_str(),"L");
  if(rocs_6.size()) leg2.AddEntry(rocs_6.at(0),labels.at(5).c_str(),"L");
  leg2.Draw("same");

  CMS_lumi(canvas,"");
  canvas->SetLogy();
  canvas->SaveAs((outputDIR+"/"+plotName+".pdf").c_str(),"pdf");
  delete canvas;
}

void plotEfficiency(const vector<TGraph*> effs_1 = {},
                    const vector<TGraph*> effs_2 = {},
                    const vector<TGraph*> effs_3 = {},
                    const vector<TGraph*> effs_4 = {},
                    const vector<string> & legends = {},
                    const vector<string> & labels = {},
                    const string & binname = "",
                    const string & outputDIR = "",
                    const string & plotName = "",
                    const float  & xmin = 0.,
                    const float  & xmax = 100.,
                    const float  & ymin = 0.0001,
                    const float  & ymax = 1.){

  TCanvas* canvas = new TCanvas("canvas","canvas",700,600);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();

  TH1F* frame = canvas->DrawFrame(xmin,ymin,xmax,ymax,"");
  if(TString(plotName).Contains("eta"))
     frame->GetXaxis()->SetTitle("jet |#eta|");
  else if(TString(plotName).Contains("mass"))
     frame->GetXaxis()->SetTitle("jet mass");
  else
     frame->GetXaxis()->SetTitle("jet p_{T}");
  frame->GetYaxis()->SetTitle("mistag rate");
  frame->Draw();

  TLegend leg (0.7,0.2,0.9,0.35);
  leg.SetBorderSize(0);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);

  for(size_t ieff = 0; ieff < effs_1.size(); ieff++){

    if(effs_1.size() > 1){
      effs_1.at(ieff)->SetLineColor(colors.at(ieff));
      effs_1.at(ieff)->SetMarkerColor(colors.at(ieff));
      effs_1.at(ieff)->SetLineStyle(style.at(0));
    }
    else{
      effs_1.at(ieff)->SetLineColor(colors.at(0));
      effs_1.at(ieff)->SetMarkerColor(colors.at(0));
    }
    effs_1.at(ieff)->SetLineWidth(2);
    effs_1.at(ieff)->SetMarkerStyle(20);
    effs_1.at(ieff)->SetMarkerSize(1);
    effs_1.at(ieff)->Draw("PLsame");
    leg.AddEntry(effs_1.at(ieff),legends.at(ieff).c_str(),"L");
  }

  for(size_t ieff = 0; ieff < effs_2.size(); ieff++){
    if(effs_2.size() > 1){
      effs_2.at(ieff)->SetLineColor(colors.at(ieff));
      effs_2.at(ieff)->SetMarkerColor(colors.at(ieff));
      effs_2.at(ieff)->SetLineStyle(style.at(1));
    }
    else{
      effs_2.at(ieff)->SetLineColor(colors.at(1));
      effs_2.at(ieff)->SetMarkerColor(colors.at(1));
    }
    effs_2.at(ieff)->SetLineWidth(2);
    effs_2.at(ieff)->SetMarkerStyle(20);
    effs_2.at(ieff)->SetMarkerSize(1);
    effs_2.at(ieff)->Draw("PLsame");
  }

  for(size_t ieff = 0; ieff < effs_3.size(); ieff++){
    if(effs_3.size() > 1){
      effs_3.at(ieff)->SetLineColor(colors.at(ieff));
      effs_3.at(ieff)->SetMarkerColor(colors.at(ieff));
      effs_3.at(ieff)->SetLineStyle(style.at(2));
    }
    else{
      effs_3.at(ieff)->SetLineColor(colors.at(2));
      effs_3.at(ieff)->SetMarkerColor(colors.at(2));
    }
    effs_3.at(ieff)->SetLineWidth(2);
    effs_3.at(ieff)->SetMarkerStyle(20);
    effs_3.at(ieff)->SetMarkerSize(1);
    effs_3.at(ieff)->Draw("PLsame");
  }

  for(size_t ieff = 0; ieff < effs_4.size(); ieff++){
    if(effs_4.size() > 1){
      effs_4.at(ieff)->SetLineColor(colors.at(ieff));
      effs_4.at(ieff)->SetMarkerColor(colors.at(ieff));
      effs_4.at(ieff)->SetLineStyle(style.at(3));
    }
    else{
      effs_4.at(ieff)->SetLineColor(colors.at(3));
      effs_3.at(ieff)->SetMarkerColor(colors.at(3));
    }
    effs_4.at(ieff)->SetLineWidth(2);
    effs_4.at(ieff)->SetMarkerStyle(20);
    effs_4.at(ieff)->SetMarkerSize(1);
    effs_4.at(ieff)->Draw("PLsame");
  }

  if(legends.size()>1) leg.Draw("same");

  TLatex latex;
  latex.SetTextAlign(13);
  latex.SetNDC();
  latex.SetTextSize(0.03);
  latex.SetTextFont(42);
  latex.DrawLatex(0.17,0.83,binname.c_str());

  float ylegmin = 0.65;
  float ylegmax = 0.78;
  if(effs_1.size() > 0 and effs_2.size() == 0 and effs_3.size() == 0 and effs_4.size() == 0) ylegmax = 0.69;
  if(effs_1.size() > 0 and effs_2.size() > 0 and effs_3.size() == 0 and effs_4.size() == 0) ylegmax = 0.72;
  if(effs_1.size() > 0 and effs_2.size() > 0 and effs_3.size() > 0 and effs_4.size() == 0) ylegmax = 0.75;

  TLegend leg2 (0.17,ylegmin,0.45,ylegmax);
  leg2.SetBorderSize(0);
  leg2.SetFillColor(0);
  leg2.SetFillStyle(0);
  if(effs_1.size()) leg2.AddEntry(effs_1.at(0),labels.at(0).c_str(),"PL");
  if(effs_2.size()) leg2.AddEntry(effs_2.at(0),labels.at(1).c_str(),"PL");
  if(effs_3.size()) leg2.AddEntry(effs_3.at(0),labels.at(2).c_str(),"PL");
  if(effs_4.size()) leg2.AddEntry(effs_4.at(0),labels.at(3).c_str(),"PL");
  leg2.Draw("same");

  CMS_lumi(canvas,"");
  canvas->SetLogy();
  canvas->SaveAs((outputDIR+"/"+plotName+".pdf").c_str(),"pdf");
  delete canvas;
}

#endif
