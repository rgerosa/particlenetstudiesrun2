import os,sys
import ROOT
import argparse
import yaml
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-o','--output-file', type=str, default="weight.root", help='output directory for plots')
parser.add_argument('-c','--data-config', type=str, default='data/ak15_points_pf_sv_v0.yaml', help='data config YAML file')
parser.add_argument('-v','--reweight-vars', nargs='*', default=['jet_pt','jet_mass_truth'], help='name of the variables for x and y axis in the re-weight')
args = parser.parse_args()

## import yaml 
with open(args.data_config, 'r') as inputfile:
    data_configuration = yaml.safe_load(inputfile);

## save the re-weight
reweight = {};
if data_configuration["labels"]: 
    classes = data_configuration["labels"]["value"];
else:
    print("No class in defining the loss .. check the reweight part of the configuration file");
    classes = data_configuration["weights"]["reweight_classes"]
print("parseDataConfig --> Classes --> ",classes);

### create Th2 histograms to host the re-weight factors
print("parseDataConfig --> Evaluate reweight factors in TH2");
var1_reweight_bin   = data_configuration['weights']['reweight_vars'][args.reweight_vars[0]];
var2_reweight_bin = data_configuration['weights']['reweight_vars'][args.reweight_vars[1]];
for iclass in classes:
    reweight[iclass] = ROOT.TH2F("reweight_"+iclass,"",
                                 len(var1_reweight_bin)-1,np.array(var1_reweight_bin,dtype=float),
                                 len(var2_reweight_bin)-1,np.array(var2_reweight_bin,dtype=float))
    ### Fill histograms
    reweight_val = data_configuration['weights']['reweight_hists'][iclass];
    for binx in range(0,reweight[iclass].GetNbinsX()):
        for biny in range(0,reweight[iclass].GetNbinsY()):
            reweight[iclass].SetBinContent(binx+1,biny+1,reweight_val[binx][biny]);

outputfile = ROOT.TFile(args.output_file,"RECREATE");
for iclass in classes:
    reweight[iclass].Write();
outputfile.Close();
