# Macros for assessing PNETAK8 performance

A set of c++ or python macros are listed here and can be used to assess the performance of the PNET trainings in terms of ROCs:

* `makeROCCurvesVsQCD.C` used to plot $`X\tobb,~X\tocc,~X\to\tau_{h}\tau_{h},~X\toqq,~X\togg,~X\to\tau_{h}\mu,~X\to\tau_{h}e`$ vs QCD ROCs.
* `makeROCCurvesHbb.C` used to plot $`X\tobb`$ vs $X\tocc,~X\to\tau_{h}\tau_{h},~X\toqq,~X\togg,~X\to\tau_{h}\mu,~X\to\tau_{h}e`$.
* `makeROCCurvesHtt.C` used to plot $`X\to\tau_{h}\tau_{h}`$ vs $X\tobb,~X\tocc,~X\to\tau_{h}\tau_{h},~X\toqq,~X\togg,~X\to\tau_{h}\mu,~X\to\tau_{h}e`$
* `makeROCCurvesSpin.C` used to plot $`X\tobb`$ and $`X\to\tau_{h}\tau_{h}`$ for spin-0 and spin-1 resonances.	
* `makeRegressionResponse.C` plot the effect of mass regression and compare with earlier versions.	

Each macro of them takes common input parameters that are described below:
* First of all the paths and the type of ROCs one wants to display are defined inside the code as member of the inputFileName map: key is an identifier string, value is the path of the directory containing the ntuples produced  by weaver (prediction from weaver).
* `fileNameStringToGrep`: string to grep in the folder in order to collect the input trees/files.
* `dataConfigStringToGrep`: string to grep in order to collect the data-config file from weaver containing the class weights (should be placed in the same directory of the trees).
* `applyReweight`: whether re-weight or not the jet pT and eta spectrum to be flat (as done in the training).
* `ptBins` and `massBins`: binning in order to produce ROC curves.
* `sample_selected`: filter the jets to consider only those belonging to a specific set of physics samples described in the `commonTools.h` file.
* `nThreads`: number of threads that will be used for looping on the input trees, not for building the final rocs and plots.
* `sigEffTargetHbb` or `sigEffTargetHtt`, etc ... indentify the signal-eff WPs that are tested i.e. those for which the fake-rate vs pT and mass is plotted.