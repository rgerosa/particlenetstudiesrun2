### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register (
    'era',"2018",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'era that identifies the data taking period: 2016PreVFP, 2016PostVBF, 2017, 2018');

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register(
    'runPuppiV17',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True run v17 puppi');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'muonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtMin',5.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for AK4 jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for AK4 jets');

options.register (
    'dumpOnlyJetMatchedToGen', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'minimum pt for reco PF candidates');

options.register (
    'lostTrackPtMin',1.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for lost tracks');

options.register (
    'dRLostTrackJet',0.2,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum dR between lost track and jet axis');

options.register (
    'ptMaxCharged',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'PtMaxCharged parameter in puppi');

options.register (
    'dRJetGenMatch',0.4,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets');

options.register (
    'evaluateForwardPNETTraining', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'evalaute the forward PNET network instead of the central one')

options.parseArguments()

if options.era != "2018"  and options.era != "2017"  and options.era != "2016PostVFP" and options.era != "2016PreVFP":
    sys.exit("Invalid era abort program");

# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    if options.era == "2018":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
    elif options.era == "2017":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2017_realistic', '')
    elif options.era == "2016PreVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc_pre_vfp', '')
    elif options.era == "2016PostVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc', '')
else:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')


##########################    
### Evaluate the version of particleNet
##########################

pnetDiscriminators = [];
pnetPuppiDiscriminators = [];

from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
process.pfParticleNetAK4LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
    muons = cms.InputTag("slimmedMuons"),                                                            
    electrons = cms.InputTag("slimmedElectrons"),                                                            
    photons = cms.InputTag("slimmedPhotons"),                                                            
    taus = cms.InputTag("slimmedTaus"),                                                            
    vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
    secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
    jets = cms.InputTag("slimmedJets"),
    losttracks = cms.InputTag("lostTracks"),                                                         
    jet_radius = cms.double(0.4),
    min_jet_pt = cms.double(options.jetPtMin),
    max_jet_eta = cms.double(options.jetEtaMax),
    min_jet_eta = cms.double(options.jetEtaMin),
    min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                            
    min_pt_for_track_properties = cms.double(-1),
    min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
    max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
    min_pt_for_taus = cms.double(options.tauPtMin),
    max_eta_for_taus = cms.double(2.5),
    dump_feature_tree = cms.bool(False)
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK4LastJetTags = boostedJetONNXJetTagsProducer.clone();
process.pfParticleNetAK4LastJetTags.src = cms.InputTag("pfParticleNetAK4LastJetTagInfos");
process.pfParticleNetAK4LastJetTags.flav_names = cms.vstring('probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p','probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow');

if not options.evaluateForwardPNETTraining:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegOneTarget/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegOneTarget/particle-net.onnx');
else:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegOneTarget/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegOneTarget/particle-net.onnx');

pnetDiscriminators.extend([
    "pfParticleNetAK4LastJetTags:probmu",
    "pfParticleNetAK4LastJetTags:probele",
    "pfParticleNetAK4LastJetTags:probtaup1h0p",
    "pfParticleNetAK4LastJetTags:probtaup1h1p",
    "pfParticleNetAK4LastJetTags:probtaup1h2p",
    "pfParticleNetAK4LastJetTags:probtaup3h0p",
    "pfParticleNetAK4LastJetTags:probtaup3h1p",
    "pfParticleNetAK4LastJetTags:probtaum1h0p",
    "pfParticleNetAK4LastJetTags:probtaum1h1p",
    "pfParticleNetAK4LastJetTags:probtaum1h2p",
    "pfParticleNetAK4LastJetTags:probtaum3h0p",
    "pfParticleNetAK4LastJetTags:probtaum3h1p",
    "pfParticleNetAK4LastJetTags:probb",
    "pfParticleNetAK4LastJetTags:probc",
    "pfParticleNetAK4LastJetTags:probuds",
    "pfParticleNetAK4LastJetTags:probg",
    "pfParticleNetAK4LastJetTags:ptcorr",
    "pfParticleNetAK4LastJetTags:ptreslow",
    "pfParticleNetAK4LastJetTags:ptreshigh",
])

## re-make puppi jets with v17 workflow
if options.runPuppiV17:
    process.load('CommonTools/PileupAlgos/Puppi_cff')
    process.puppi.candName = cms.InputTag('packedPFCandidates')
    process.puppi.vertexName = cms.InputTag('offlineSlimmedPrimaryVertices')
    process.puppi.clonePackedCands   = cms.bool(True)
    process.puppi.useExistingWeights = cms.bool(False)
    process.puppi.PtMaxCharged = cms.double(options.ptMaxCharged)

    from RecoJets.JetProducers.ak4PFJets_cfi import ak4PFJets
    process.ak4PuppiJets = ak4PFJets.clone (
        src = 'puppi', 
        doAreaFastjet = True, 
        jetPtMin = 1.
    )
    
    from PhysicsTools.PatAlgos.tools.jetTools import addJetCollection
    addJetCollection(process,
                     labelName = 'Puppi', 
                     jetSource = cms.InputTag('ak4PuppiJets'), 
                     algo = 'AK', 
                     rParam=0.4, 
                     genJetCollection=cms.InputTag('slimmedGenJets'), 
                     jetCorrections = ('AK4PFPuppi', ['L2Relative', 'L3Absolute'], 'None'),
                     pfCandidates = cms.InputTag('packedPFCandidates'),
                     pvSource = cms.InputTag('offlineSlimmedPrimaryVertices'),
                     svSource = cms.InputTag('slimmedSecondaryVertices'),
                     muSource =cms.InputTag( 'slimmedMuons'),
                     elSource = cms.InputTag('slimmedElectrons')
                 )
    process.patJetsPuppi.addGenPartonMatch = cms.bool(options.isMC)                                                                                                                                   
    process.patJetsPuppi.addGenJetMatch = cms.bool(options.isMC)       
    process.patJetPartons.particles = cms.InputTag("prunedGenParticles")
    process.patJetPartonMatchPuppi.matched = cms.InputTag("prunedGenParticles")

process.pfParticleNetPuppiAK4LastJetTagInfos = process.pfParticleNetAK4LastJetTagInfos.clone();
if options.runPuppiV17:
    process.pfParticleNetPuppiAK4LastJetTagInfos.jets = cms.InputTag("patJetsPuppi")
else:
    process.pfParticleNetPuppiAK4LastJetTagInfos.jets = cms.InputTag("slimmedJetsPuppi")
process.pfParticleNetPuppiAK4LastJetTags = process.pfParticleNetAK4LastJetTags.clone();
process.pfParticleNetPuppiAK4LastJetTags.src = cms.InputTag("pfParticleNetPuppiAK4LastJetTagInfos");
if not options.evaluateForwardPNETTraining:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('RecoBTag/Combined/data/ParticleNetAK4/PNETUL/ClassRegPuppi/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('RecoBTag/Combined/data/ParticleNetAK4/PNETUL/ClassRegPuppi/particle-net.onnx');
else:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('RecoBTag/Combined/data/ParticleNetAK4/PNETUL/ClassRegPuppi/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('RecoBTag/Combined/data/ParticleNetAK4/PNETUL/ClassRegPuppi/particle-net.onnx');

pnetPuppiDiscriminators.extend([
    "pfParticleNetPuppiAK4LastJetTags:probmu",
    "pfParticleNetPuppiAK4LastJetTags:probele",
    "pfParticleNetPuppiAK4LastJetTags:probtaup1h0p",
    "pfParticleNetPuppiAK4LastJetTags:probtaup1h1p",
    "pfParticleNetPuppiAK4LastJetTags:probtaup1h2p",
    "pfParticleNetPuppiAK4LastJetTags:probtaup3h0p",
    "pfParticleNetPuppiAK4LastJetTags:probtaup3h1p",
    "pfParticleNetPuppiAK4LastJetTags:probtaum1h0p",
    "pfParticleNetPuppiAK4LastJetTags:probtaum1h1p",
    "pfParticleNetPuppiAK4LastJetTags:probtaum1h2p",
    "pfParticleNetPuppiAK4LastJetTags:probtaum3h0p",
    "pfParticleNetPuppiAK4LastJetTags:probtaum3h1p",
    "pfParticleNetPuppiAK4LastJetTags:probb",
    "pfParticleNetPuppiAK4LastJetTags:probc",
    "pfParticleNetPuppiAK4LastJetTags:probuds",
    "pfParticleNetPuppiAK4LastJetTags:probg",
    "pfParticleNetPuppiAK4LastJetTags:ptcorr",
    "pfParticleNetPuppiAK4LastJetTags:ptreslow",
    "pfParticleNetPuppiAK4LastJetTags:ptreshigh",
])

###                                                                                                                                                                                                   
from RecoJets.JetProducers.PileupJetID_cfi import pileupJetId
process.pileupJetIdUpdated = pileupJetId.clone(
    jets = cms.InputTag("slimmedJets"),
    inputIsCorrected = True,
    applyJec = False,
    vertexes = cms.InputTag("offlineSlimmedPrimaryVertices"),
)

if options.era == "2018":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL18
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL18)
elif options.era == "2017":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL17
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL17)
elif options.era == "2016PostVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16)
elif options.era == "2016PreVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16APV
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16APV)


## Update final jet collection                                                                                                                                                                 
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = 'slimmedJets',
    addJetCorrFactors = False,
    discriminatorSources = pnetDiscriminators
)
process.slimmedJetsUpdated.userData.userInts.src += ['pileupJetIdUpdated:fullId'];

if options.runPuppiV17:
    process.patJetsPuppiUpdated = updatedPatJets.clone(
        jetSource = 'patJetsPuppi',
        addJetCorrFactors = False,
        discriminatorSources = pnetPuppiDiscriminators
    )
else:
    process.patJetsPuppiUpdated = updatedPatJets.clone(
        jetSource = 'slimmedJetsPuppi',
        addJetCorrFactors = False,
        discriminatorSources = pnetPuppiDiscriminators
    )

#### add gen jets with neutrino
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
process.genParticlesForJets = genParticlesForJets.clone(
    src = cms.InputTag("packedGenParticles")
)

from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
process.ak4GenJetsWithNu = ak4GenJets.clone( 
    src = "genParticlesForJets" 
)

## deep tau evaluation
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.                                                                                                                        
tauIdEmbedder.runTauID()


#### Final dumper
process.dnntree = cms.EDAnalyzer('ValidationCHSvsPuppiAK4',
    #### General flags
    xsec  = cms.double(options.xsec),
    dumpOnlyJetMatchedToGen = cms.bool(options.dumpOnlyJetMatchedToGen),
    pnetDiscriminatorLabels = cms.vstring(),                                 
    pnetPuppiDiscriminatorLabels = cms.vstring(),                                 
    ### Object selection 
    muonPtMin         = cms.double(options.muonPtMin),
    electronPtMin     = cms.double(options.electronPtMin),
    tauPtMin          = cms.double(options.tauPtMin),
    jetPtMin          = cms.double(options.jetPtMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    dRJetGenMatch     = cms.double(options.dRJetGenMatch),
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    genParticles      = cms.InputTag("prunedGenParticles"),
    ### miniAOD objects
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    taus              = cms.InputTag("slimmedTausUpdated"),
    jets              = cms.InputTag("slimmedJetsUpdated"),
    jetsPuppi         = cms.InputTag("patJetsPuppiUpdated"),
    met               = cms.InputTag("slimmedMETs"),
    genJets           = cms.InputTag("slimmedGenJets"),                                 
    genJetsWithNu     = cms.InputTag("ak4GenJetsWithNu"),                                 
)

if options.runPuppiV17:
    process.dnntree.puppiWeights = cms.InputTag("puppi")
else:
    process.dnntree.puppiWeights = cms.InputTag("")

for element in pnetDiscriminators:
    element = element.split(":")[-1];
    process.dnntree.pnetDiscriminatorLabels.append(element);

for element in pnetPuppiDiscriminators:
    element = element.split(":")[-1];
    process.dnntree.pnetPuppiDiscriminatorLabels.append(element);

########
process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

########
process.dnnTreePath = cms.Path(process.dnntree,process.edTask)
