// Standard C++ includes
#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>

#include <TPRegexp.h>

// ROOT includes
#include <TTree.h>
#include <TLorentzVector.h>

// CMSSW framework includes
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/ESGetToken.h"

// CMSSW data formats
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/METReco/interface/PFMET.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/Candidate/interface/VertexCompositePtrCandidate.h"
#include "DataFormats/Candidate/interface/CompositeCandidate.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"
#include "DataFormats/JetMatching/interface/JetFlavourInfoMatching.h"


// Other relevant CMSSW includes
#include "CommonTools/UtilAlgos/interface/TFileService.h" 
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

// For dumping b-tagging info
#include "DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h"
#include "RecoBTag/FeatureTools/interface/TrackInfoBuilder.h"
#include "RecoVertex/VertexTools/interface/VertexDistanceXY.h"
#include "RecoVertex/VertexTools/interface/VertexDistance3D.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"

class TrainingTreeMakerAK4 : public edm::one::EDAnalyzer<edm::one::SharedResources, edm::one::WatchRuns> {

public:
  explicit TrainingTreeMakerAK4(const edm::ParameterSet&);
  ~TrainingTreeMakerAK4();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  bool applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi);
  bool applyPileupJetID(const pat::Jet & jet, const std::string & level);
  

private:

  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;  
  virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
  virtual void endRun(edm::Run const&, edm::EventSetup const&) override;

  void initializeBranches();

  // MC information
  const edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupInfoToken;
  const edm::EDGetTokenT<reco::GenParticleCollection > gensToken;
  const edm::EDGetTokenT<GenEventInfoProduct> genEvtInfoToken;
  const edm::EDGetTokenT<LHEEventProduct> lheInfoToken;

  // Trigger
  const edm::InputTag triggerResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> triggerResultsToken;

  // Filter result
  const edm::InputTag filterResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> filterResultsToken;

  // Vertices from miniAOD
  const edm::EDGetTokenT<reco::VertexCollection > primaryVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> secondaryVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> kaonVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> lambdaVerticesToken;
  edm::EDGetTokenT< double > rhoToken;

  // Pat objects from miniAOD
  const edm::EDGetTokenT<pat::MuonCollection> muonsToken;
  const edm::EDGetTokenT<pat::ElectronCollection> electronsToken;
  const edm::EDGetTokenT<pat::PhotonCollection> photonsToken;
  const edm::EDGetTokenT<pat::TauCollection> tausToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsToken;
  const edm::EDGetTokenT<pat::METCollection> metToken;
  const edm::EDGetTokenT<pat::PackedCandidateCollection> lostTracksToken;
  const edm::EDGetTokenT<edm::View<reco::CompositeCandidate> > leptonPairsToken;
  const edm::InputTag leptonPairsTag;

  // GEN level jets
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsWnuToken;
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsToken;
  const edm::EDGetTokenT<reco::JetFlavourInfoMatchingCollection> genJetsFlavourToken;

  // Transient track
  const edm::ESGetToken<TransientTrackBuilder, TransientTrackRecord> trackBuilderToken;

  // trigger filters
  std::map<std::string,int> triggerPathsMap;

  // MET filters
  std::vector<std::string>  filterPathsVector;
  std::map<std::string,int> filterPathsMap;

  // Flag for the analyzer
  float muonPtMin;
  float muonEtaMax;
  float electronPtMin;
  float electronEtaMax;
  float photonPtMin;
  float photonEtaMax;
  float tauPtMin;
  float tauEtaMax;
  float jetPtMin;
  float jetEtaMax;
  float jetEtaMin;
  float jetPFCandidatePtMin;
  float lostTracksPtMin;
  float dRLostTrackJet;
  float dRJetGenMatch;
  bool  dumpOnlyJetMatchedToGen;
  bool  usePuppiJets;
  bool  saveLHEObjects;
  bool  isMC;
  std::vector<std::string> pnetDiscriminatorLabels;
  std::vector<std::string> pnetDiscriminatorNames;
  std::vector<std::string> parTDiscriminatorLabels;
  std::vector<std::string> parTDiscriminatorNames;
  
  // Event coordinates
  unsigned event, run, lumi;
  // Pileup information
  unsigned int putrue;
  // Flags for various event filters
  unsigned int flags;
  // Cross-section and event weight information for MC events
  float xsec, wgt;
  // Rho
  float rho;
  //Trigger
  std::vector<unsigned int> trigger_hlt_pass;
  std::vector<std::string>  trigger_hlt_path;
  // LHE particles
  std::vector<float>  lhe_particle_pt;
  std::vector<float>  lhe_particle_eta;
  std::vector<float>  lhe_particle_phi;
  std::vector<float>  lhe_particle_mass;
  std::vector<int>    lhe_particle_id;
  std::vector<unsigned int>  lhe_particle_status;
  // Generator-level information (Gen and LHE particles)
  std::vector<float>  gen_particle_pt;
  std::vector<float>  gen_particle_eta;
  std::vector<float>  gen_particle_phi;
  std::vector<float>  gen_particle_mass;
  std::vector<int>    gen_particle_id;
  std::vector<unsigned int>  gen_particle_status;
  std::vector<int>    gen_particle_daughters_id;
  std::vector<unsigned int> gen_particle_daughters_igen;
  std::vector<unsigned int> gen_particle_daughters_status;
  std::vector<float>  gen_particle_daughters_pt;
  std::vector<float>  gen_particle_daughters_eta;
  std::vector<float>  gen_particle_daughters_phi;
  std::vector<float>  gen_particle_daughters_mass;
  std::vector<int>    gen_particle_daughters_charge;
  // Vertex RECO
  unsigned int npv;
  unsigned int nsv;
  unsigned int nkaon;
  unsigned int nlambda;
  // Collection of muon 4-vectors, muon ID bytes, muon isolation values
  std::vector<float>  muon_pt;
  std::vector<float>  muon_eta;
  std::vector<float>  muon_phi;
  std::vector<float>  muon_energy;
  std::vector<unsigned int> muon_iso;
  std::vector<unsigned int> muon_id;
  std::vector<int>    muon_charge;
  // Collection of electron 4-vectors, electron ID bytes, electron isolation values
  std::vector<float>  electron_pt;
  std::vector<float>  electron_pt_corr;
  std::vector<float>  electron_eta;
  std::vector<float>  electron_phi;
  std::vector<float>  electron_energy;
  std::vector<unsigned int> electron_id;
  std::vector<float>  electron_idscore;
  std::vector<int>    electron_charge;
  // Collection of photon 4-vectors, photon ID bytes, photon isolation values
  std::vector<float>  photon_pt;
  std::vector<float>  photon_pt_corr;
  std::vector<float>  photon_eta;
  std::vector<float>  photon_phi;
  std::vector<float>  photon_energy;
  std::vector<unsigned int> photon_id;
  std::vector<float>  photon_idscore;
  // Collection of taus 4-vectors, tau ID bytes, tau isolation values
  std::vector<float>  tau_pt;
  std::vector<float>  tau_eta;
  std::vector<float>  tau_phi;
  std::vector<float>  tau_mass;
  std::vector<float>  tau_energy;
  std::vector<float>  tau_dxy;
  std::vector<float>  tau_dz;
  std::vector<unsigned int> tau_decaymode;
  std::vector<unsigned int> tau_idjet_wp;
  std::vector<unsigned int> tau_idmu_wp;
  std::vector<unsigned int> tau_idele_wp;
  std::vector<float>  tau_idjet;
  std::vector<float>  tau_idele;
  std::vector<float>  tau_idmu;
  std::vector<int>    tau_charge;  
  std::vector<float>  tau_genmatch_pt;
  std::vector<float>  tau_genmatch_eta;
  std::vector<float>  tau_genmatch_phi;
  std::vector<float>  tau_genmatch_mass;
  std::vector<int> tau_genmatch_decaymode;
  // MET
  float met, met_phi;
  // Lepton pair properties if selected
  std::vector<float>  leppair_pt;
  std::vector<float>  leppair_eta;
  std::vector<float>  leppair_phi;
  std::vector<float>  leppair_mass;
  std::vector<float>  leppair_pt1;
  std::vector<float>  leppair_eta1;
  std::vector<float>  leppair_phi1;
  std::vector<float>  leppair_mass1;
  std::vector<float>  leppair_pdg1;
  std::vector<float>  leppair_pt2;
  std::vector<float>  leppair_eta2;
  std::vector<float>  leppair_phi2;
  std::vector<float>  leppair_mass2;
  std::vector<float>  leppair_pdg2;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_pt;
  std::vector<float> jet_eta;
  std::vector<float> jet_phi;
  std::vector<float> jet_mass;
  std::vector<float> jet_pt_raw;
  std::vector<float> jet_mass_raw;
  std::vector<float> jet_chf;
  std::vector<float> jet_nhf;
  std::vector<float> jet_elf;
  std::vector<float> jet_phf;
  std::vector<float> jet_muf;
  std::vector<float> jet_deepjet_probb;
  std::vector<float> jet_deepjet_probbb;
  std::vector<float> jet_deepjet_problepb;
  std::vector<float> jet_deepjet_probc;
  std::vector<float> jet_deepjet_probg;
  std::vector<float> jet_deepjet_probuds;
  std::vector<float> jet_pnet_probb;
  std::vector<float> jet_pnet_probbb;
  std::vector<float> jet_pnet_probc;
  std::vector<float> jet_pnet_probcc;
  std::vector<float> jet_pnet_probuds;
  std::vector<float> jet_pnet_probg;
  std::vector<float> jet_pnet_probpu;
  std::vector<float> jet_pnet_probundef;
  std::map<std::string,std::vector<float> > jet_pnetlast_score;
  std::map<std::string,std::vector<float> > jet_parTlast_score;
  std::vector<unsigned int> jet_id;
  std::vector<unsigned int> jet_puid;
  std::vector<unsigned int> jet_ncand;
  std::vector<unsigned int> jet_nch;
  std::vector<unsigned int> jet_nnh;
  std::vector<unsigned int> jet_nel;
  std::vector<unsigned int> jet_nph;
  std::vector<unsigned int> jet_nmu;
  std::vector<unsigned int> jet_hflav;
  std::vector<int> jet_pflav;
  std::vector<unsigned int> jet_nbhad;
  std::vector<unsigned int> jet_nchad;
  std::vector<float> jet_genmatch_pt;
  std::vector<float> jet_genmatch_eta;
  std::vector<float> jet_genmatch_phi;
  std::vector<float> jet_genmatch_mass;
  std::vector<unsigned int> jet_genmatch_hflav;
  std::vector<int> jet_genmatch_pflav;
  std::vector<unsigned int> jet_genmatch_nbhad;
  std::vector<unsigned int> jet_genmatch_nchad;
  std::vector<float> jet_genmatch_wnu_pt;
  std::vector<float> jet_genmatch_wnu_eta;
  std::vector<float> jet_genmatch_wnu_phi;
  std::vector<float> jet_genmatch_wnu_mass;
  // jet pf candidates
  std::vector<float> jet_pfcand_ch_pt;
  std::vector<float> jet_pfcand_ch_eta;
  std::vector<float> jet_pfcand_ch_phi; 
  std::vector<float> jet_pfcand_ch_mass;
  std::vector<float> jet_pfcand_ch_energy;
  std::vector<float> jet_pfcand_ch_calofraction;
  std::vector<float> jet_pfcand_ch_hcalfraction;
  std::vector<float> jet_pfcand_ch_dz;
  std::vector<float> jet_pfcand_ch_dxy;
  std::vector<float> jet_pfcand_ch_dzsig;
  std::vector<float> jet_pfcand_ch_dxysig;
  std::vector<unsigned int> jet_pfcand_ch_frompv;
  std::vector<unsigned int> jet_pfcand_ch_id;
  std::vector<unsigned int> jet_pfcand_ch_ijet;
  std::vector<int> jet_pfcand_ch_charge;
  std::vector<float> jet_pfcand_ch_puppiw;
  std::vector<float> jet_pfcand_ch_candjet_pperp;
  std::vector<float> jet_pfcand_ch_candjet_ppara;
  std::vector<float> jet_pfcand_ch_candjet_deta;
  std::vector<float> jet_pfcand_ch_candjet_dphi;
  std::vector<float> jet_pfcand_ch_candjet_ptrel;
  std::vector<float> jet_pfcand_ch_candjet_etarel;
  std::vector<unsigned int> jet_pfcand_ch_track_chi2;
  std::vector<unsigned int> jet_pfcand_ch_track_qual;
  std::vector<float> jet_pfcand_ch_track_pterr;
  std::vector<float> jet_pfcand_ch_track_etaerr;
  std::vector<float> jet_pfcand_ch_track_phierr;
  std::vector<float> jet_pfcand_ch_trackjet_sip2d;
  std::vector<float> jet_pfcand_ch_trackjet_sip2dsig;
  std::vector<float> jet_pfcand_ch_trackjet_sip3d;
  std::vector<float> jet_pfcand_ch_trackjet_sip3dsig;
  std::vector<float> jet_pfcand_ch_trackjet_dist;
  std::vector<float> jet_pfcand_ch_trackjet_decayL;
  std::vector<unsigned int> jet_pfcand_ch_nhits;
  std::vector<unsigned int> jet_pfcand_ch_npixhits;
  std::vector<unsigned int> jet_pfcand_ch_npixbarrelhits;
  std::vector<unsigned int> jet_pfcand_ch_nstriphits;
  std::vector<unsigned int> jet_pfcand_ch_nstriptibhits;
  std::vector<unsigned int> jet_pfcand_ch_nstriptobhits;
  std::vector<int> jet_pfcand_ch_nlosthits;
  std::vector<unsigned int> jet_pfcand_ch_nlayers;
  std::vector<unsigned int> jet_pfcand_ch_npixlayers;
  std::vector<unsigned int> jet_pfcand_ch_nstriplayers;
  std::vector<unsigned int> jet_pfcand_ch_tau_signal;
  std::vector<float> jet_pfcand_neu_pt;
  std::vector<float> jet_pfcand_neu_eta;
  std::vector<float> jet_pfcand_neu_phi; 
  std::vector<float> jet_pfcand_neu_mass;
  std::vector<float> jet_pfcand_neu_energy;
  std::vector<float> jet_pfcand_neu_calofraction;
  std::vector<float> jet_pfcand_neu_hcalfraction;
  std::vector<float> jet_pfcand_neu_dz;
  std::vector<float> jet_pfcand_neu_dxy;
  std::vector<unsigned int> jet_pfcand_neu_frompv;
  std::vector<unsigned int> jet_pfcand_neu_id;
  std::vector<unsigned int> jet_pfcand_neu_ijet;
  std::vector<float> jet_pfcand_neu_puppiw;
  std::vector<float> jet_pfcand_neu_candjet_pperp;
  std::vector<float> jet_pfcand_neu_candjet_ppara;
  std::vector<float> jet_pfcand_neu_candjet_deta;
  std::vector<float> jet_pfcand_neu_candjet_dphi;
  std::vector<float> jet_pfcand_neu_candjet_ptrel;
  std::vector<float> jet_pfcand_neu_candjet_etarel;
  std::vector<unsigned int> jet_pfcand_neu_tau_signal;
  // Leptons
  std::vector<float> jet_pfcand_muon_pt;
  std::vector<float> jet_pfcand_muon_eta;
  std::vector<float> jet_pfcand_muon_phi;
  std::vector<float> jet_pfcand_muon_energy;
  std::vector<float> jet_pfcand_muon_chi2;
  std::vector<float> jet_pfcand_muon_dxy;
  std::vector<float> jet_pfcand_muon_dz;
  std::vector<float> jet_pfcand_muon_segcomp;
  std::vector<float> jet_pfcand_muon_validfraction;
  std::vector<float> jet_pfcand_muon_trkKink;
  std::vector<float> jet_pfcand_muon_pterr;  
  std::vector<unsigned int> jet_pfcand_muon_type;
  std::vector<unsigned int> jet_pfcand_muon_nvalidhits;
  std::vector<unsigned int> jet_pfcand_muon_nstations;
  std::vector<unsigned int> jet_pfcand_muon_nlayers;
  std::vector<unsigned int> jet_pfcand_muon_nhits;
  std::vector<unsigned int> jet_pfcand_muon_ijet;
  std::vector<float> jet_pfcand_electron_pt;
  std::vector<float> jet_pfcand_electron_eta;
  std::vector<float> jet_pfcand_electron_phi;
  std::vector<float> jet_pfcand_electron_energy;
  std::vector<float> jet_pfcand_electron_dxy;
  std::vector<float> jet_pfcand_electron_dz;
  std::vector<float> jet_pfcand_electron_eOverP;
  std::vector<float> jet_pfcand_electron_detaIn;
  std::vector<float> jet_pfcand_electron_dphiIn;
  std::vector<float> jet_pfcand_electron_r9;
  std::vector<float> jet_pfcand_electron_hOverE;
  std::vector<float> jet_pfcand_electron_sigIetaIeta;
  std::vector<float> jet_pfcand_electron_convProb;
  std::vector<unsigned int> jet_pfcand_electron_ijet;
  std::vector<float> jet_pfcand_photon_pt;
  std::vector<float> jet_pfcand_photon_eta;
  std::vector<float> jet_pfcand_photon_phi;
  std::vector<float> jet_pfcand_photon_energy;
  std::vector<float> jet_pfcand_photon_hOverE;
  std::vector<float> jet_pfcand_photon_sigIetaIeta;
  std::vector<float> jet_pfcand_photon_eVeto;
  std::vector<float> jet_pfcand_photon_r9;  
  std::vector<unsigned int> jet_pfcand_photon_ijet;
  // jet-to-secondary vertex
  std::vector<float> jet_sv_pt;
  std::vector<float> jet_sv_eta;
  std::vector<float> jet_sv_phi;
  std::vector<float> jet_sv_mass;
  std::vector<float> jet_sv_energy;
  std::vector<float> jet_sv_chi2;
  std::vector<float> jet_sv_dxy;
  std::vector<float> jet_sv_dxysig;
  std::vector<float> jet_sv_d3d;
  std::vector<float> jet_sv_d3dsig;
  std::vector<int> jet_sv_dxy_sign;
  std::vector<int> jet_sv_d3d_sign;
  std::vector<float> jet_sv_costheta;
  std::vector<unsigned int> jet_sv_ntrack;
  std::vector<unsigned int> jet_sv_ijet;
  // jet-to-kaon vertex
  std::vector<float> jet_kaon_pt;
  std::vector<float> jet_kaon_eta;
  std::vector<float> jet_kaon_phi;
  std::vector<float> jet_kaon_mass;
  std::vector<float> jet_kaon_energy;
  std::vector<float> jet_kaon_chi2;
  std::vector<float> jet_kaon_dxy;
  std::vector<float> jet_kaon_dxysig;
  std::vector<float> jet_kaon_d3d;
  std::vector<float> jet_kaon_d3dsig;
  std::vector<int> jet_kaon_dxy_sign;
  std::vector<int> jet_kaon_d3d_sign;
  std::vector<float> jet_kaon_costheta;
  std::vector<unsigned int> jet_kaon_ntrack;
  std::vector<unsigned int> jet_kaon_ijet;
  // jet-to-lambda vertex
  std::vector<float> jet_lambda_pt;
  std::vector<float> jet_lambda_eta;
  std::vector<float> jet_lambda_phi;
  std::vector<float> jet_lambda_mass;
  std::vector<float> jet_lambda_energy;
  std::vector<float> jet_lambda_chi2;
  std::vector<float> jet_lambda_dxy;
  std::vector<float> jet_lambda_dxysig;
  std::vector<float> jet_lambda_d3d;
  std::vector<float> jet_lambda_d3dsig;
  std::vector<int> jet_lambda_dxy_sign;
  std::vector<int> jet_lambda_d3d_sign;
  std::vector<float> jet_lambda_costheta;
  std::vector<unsigned int> jet_lambda_ntrack;
  std::vector<unsigned int> jet_lambda_ijet;
  // jet lost tracks candidates
  std::vector<float> jet_losttrack_pt;
  std::vector<float> jet_losttrack_eta;
  std::vector<float> jet_losttrack_phi; 
  std::vector<float> jet_losttrack_mass;
  std::vector<float> jet_losttrack_energy;
  std::vector<float> jet_losttrack_dz;
  std::vector<float> jet_losttrack_dxy;
  std::vector<float> jet_losttrack_dzsig;
  std::vector<float> jet_losttrack_dxysig;
  std::vector<float> jet_losttrack_candjet_pperp;
  std::vector<float> jet_losttrack_candjet_ppara;
  std::vector<float> jet_losttrack_candjet_deta;
  std::vector<float> jet_losttrack_candjet_dphi;
  std::vector<float> jet_losttrack_candjet_etarel;
  std::vector<float> jet_losttrack_candjet_ptrel;  
  std::vector<float> jet_losttrack_track_pterr;
  std::vector<float> jet_losttrack_track_etaerr;
  std::vector<float> jet_losttrack_track_phierr;
  std::vector<float> jet_losttrack_trackjet_sip2d;
  std::vector<float> jet_losttrack_trackjet_sip2dsig;
  std::vector<float> jet_losttrack_trackjet_sip3d;
  std::vector<float> jet_losttrack_trackjet_sip3dsig;
  std::vector<float> jet_losttrack_trackjet_dist;
  std::vector<float> jet_losttrack_trackjet_decayL;
  std::vector<int> jet_losttrack_charge;
  std::vector<unsigned int> jet_losttrack_frompv;
  std::vector<unsigned int> jet_losttrack_ijet;
  std::vector<unsigned int> jet_losttrack_nhits;
  std::vector<unsigned int> jet_losttrack_npixhits;
  std::vector<unsigned int> jet_losttrack_npixbarrelhits;
  std::vector<unsigned int> jet_losttrack_nstriphits;
  std::vector<unsigned int> jet_losttrack_nstriptibhits;
  std::vector<unsigned int> jet_losttrack_nstriptobhits;
  std::vector<int> jet_losttrack_nlosthits;
  std::vector<unsigned int> jet_losttrack_nlayers;
  std::vector<unsigned int> jet_losttrack_npixlayers;
  std::vector<unsigned int> jet_losttrack_nstriplayers;
  std::vector<unsigned int> jet_losttrack_track_chi2;
  std::vector<unsigned int> jet_losttrack_track_qual;
  

  // TTree carrying the event weight information
  TTree* tree;

  // Sorters to order object collections in decreasing order of pT
  template<typename T> 
  class PatPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i.pt() > j.pt());
    }
  };

  PatPtSorter<pat::Muon>     muonSorter;
  PatPtSorter<pat::Electron> electronSorter;
  PatPtSorter<pat::Photon>   photonSorter;
  PatPtSorter<pat::Tau>      tauSorter;
  PatPtSorter<pat::PackedCandidate>  packedPFCandidateSorter;

  template<typename T>
  class CandPtrPtSorter {
  public:
    bool operator()(const T & i, const T & j) const {
      return (i->pt() > j->pt());
    }
  };
  CandPtrPtSorter<reco::CandidatePtr>  candidatePtrSorter;

  template<typename T> 
  class PatRefPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i->pt() > j->pt());
    }
  };

  PatRefPtSorter<pat::JetRef>  jetRefSorter;
  PatRefPtSorter<reco::GenJetRef> genJetRefSorter;

  template<typename T, typename A>
  static Measurement1D vertexDxy(const T &i, const A & j){
    VertexDistanceXY dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.distance(svtx,j);
  }

  template<typename T, typename A>
  static Measurement1D vertexDxySigned(const T &i, const A & j, const GlobalVector & ref){
    VertexDistanceXY dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.signedDistance(svtx,j,ref);
  }


  template<typename T, typename A>
  static Measurement1D vertexD3d(const T &i, const A & j){
    VertexDistance3D dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.distance(svtx,j);
  }

  template<typename T, typename A>
  static Measurement1D vertexD3dSigned(const T &i, const A & j, const GlobalVector & ref){
    VertexDistance3D dist;
    reco::Vertex::CovarianceMatrix csv;
    i.fillVertexCovariance(csv);
    reco::Vertex svtx(i.vertex(), csv);
    return dist.signedDistance(svtx,j,ref);
  }

  template<typename T, typename A>
  static float vertexDdotP (const T &i, const A & j){
    reco::Candidate::Vector p = i.momentum();
    reco::Candidate::Vector d(i.vx() - j.x(), i.vy() - j.y(), i.vz() - j.z());
    return p.Unit().Dot(d.Unit());
  }


  template<typename T, typename A>
  class VertexSorterDxy {
  public:
    
    VertexSorterDxy (const A & pv):
      pv_(pv){};

    bool operator()(const T& i, const T& j) {      
      Measurement1D idxy = vertexDxy<T,A>(i,pv_);
      Measurement1D jdxy = vertexDxy<T,A>(j,pv_);
      float isig = abs(idxy.significance());
      float jsig = abs(jdxy.significance());
      if(std::isinf(isig) or std::isnan(isig)) isig = 0;
      if(std::isinf(jsig) or std::isnan(jsig)) jsig = 0;
      return isig > jsig;
    }

  private :
    A pv_;
  };
      
};


TrainingTreeMakerAK4::TrainingTreeMakerAK4(const edm::ParameterSet& iConfig): 
  pileupInfoToken          (mayConsume<std::vector<PileupSummaryInfo> > (iConfig.getParameter<edm::InputTag>("pileUpInfo"))),
  gensToken                (mayConsume<reco::GenParticleCollection>     (iConfig.getParameter<edm::InputTag>("genParticles"))),
  genEvtInfoToken          (mayConsume<GenEventInfoProduct>           (iConfig.getParameter<edm::InputTag>("genEventInfo"))),
  lheInfoToken             (mayConsume<LHEEventProduct>               (iConfig.getParameter<edm::InputTag>("lheInfo"))),
  triggerResultsTag        (iConfig.getParameter<edm::InputTag>("triggerResults")),
  triggerResultsToken      (consumes<edm::TriggerResults>      (triggerResultsTag)),
  filterResultsTag         (iConfig.getParameter<edm::InputTag>("filterResults")),
  filterResultsToken       (consumes<edm::TriggerResults>      (filterResultsTag)),
  primaryVerticesToken     (consumes<reco::VertexCollection>           (iConfig.getParameter<edm::InputTag>("pVertices"))),
  secondaryVerticesToken   (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("sVertices"))),
  kaonVerticesToken        (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("kVertices"))),
  lambdaVerticesToken      (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("lVertices"))),
  rhoToken                 (consumes<double>(iConfig.getParameter<edm::InputTag>("rho"))),
  muonsToken               (consumes<pat::MuonCollection>           (iConfig.getParameter<edm::InputTag>("muons"))), 
  electronsToken           (consumes<pat::ElectronCollection>       (iConfig.getParameter<edm::InputTag>("electrons"))), 
  photonsToken             (consumes<pat::PhotonCollection>         (iConfig.getParameter<edm::InputTag>("photons"))), 
  tausToken                (consumes<pat::TauCollection>            (iConfig.getParameter<edm::InputTag>("taus"))), 
  jetsToken                (consumes<pat::JetCollection >           (iConfig.getParameter<edm::InputTag>("jets"))),
  metToken                 (consumes<pat::METCollection >             (iConfig.getParameter<edm::InputTag>("met"))),
  lostTracksToken          (consumes<pat::PackedCandidateCollection>  (iConfig.getParameter<edm::InputTag>("lostTracks"))),
  leptonPairsToken         (mayConsume<edm::View<reco::CompositeCandidate> >(iConfig.getParameter<edm::InputTag>("leptonPairs"))),
  leptonPairsTag           (iConfig.getParameter<edm::InputTag>("leptonPairs")),
  genJetsWnuToken          (mayConsume<reco::GenJetCollection >       (iConfig.getParameter<edm::InputTag>("genJetsWnu"))),
  genJetsToken             (consumes<reco::GenJetCollection >         (iConfig.getParameter<edm::InputTag>("genJets"))),
  genJetsFlavourToken      (consumes<reco::JetFlavourInfoMatchingCollection >    (iConfig.getParameter<edm::InputTag>("genJetsFlavour"))),
  trackBuilderToken        (esConsumes<TransientTrackBuilder, TransientTrackRecord>(edm::ESInputTag("","TransientTrackBuilder"))), 
  muonPtMin                (iConfig.existsAs<double>("muonPtMin")    ? iConfig.getParameter<double>("muonPtMin") : 15.),
  muonEtaMax               (iConfig.existsAs<double>("muonEtaMax")   ? iConfig.getParameter<double>("muonEtaMax") : 2.4),
  electronPtMin            (iConfig.existsAs<double>("electronPtMin")   ? iConfig.getParameter<double>("electronPtMin") : 15.),
  electronEtaMax           (iConfig.existsAs<double>("electronEtaMax")  ? iConfig.getParameter<double>("electronEtaMax") : 2.5),
  photonPtMin              (iConfig.existsAs<double>("photonPtMin")   ? iConfig.getParameter<double>("photonPtMin") : 15.),
  photonEtaMax             (iConfig.existsAs<double>("photonEtaMax")  ? iConfig.getParameter<double>("photonEtaMax") : 2.5),
  tauPtMin                 (iConfig.existsAs<double>("tauPtMin")   ? iConfig.getParameter<double>("tauPtMin") : 20.),
  tauEtaMax                (iConfig.existsAs<double>("tauEtaMax")  ? iConfig.getParameter<double>("tauEtaMax") : 2.5),
  jetPtMin                 (iConfig.existsAs<double>("jetPtMin")       ? iConfig.getParameter<double>("jetPtMin") : 25.),
  jetEtaMax                (iConfig.existsAs<double>("jetEtaMax")      ? iConfig.getParameter<double>("jetEtaMax") : 2.5),
  jetEtaMin                (iConfig.existsAs<double>("jetEtaMin")      ? iConfig.getParameter<double>("jetEtaMin") : 0.0),
  jetPFCandidatePtMin      (iConfig.existsAs<double>("jetPFCandidatePtMin")    ? iConfig.getParameter<double>("jetPFCandidatePtMin") : 0.1),
  lostTracksPtMin          (iConfig.existsAs<double>("lostTracksPtMin")    ? iConfig.getParameter<double>("lostTracksPtMin") : 1.0),
  dRLostTrackJet           (iConfig.existsAs<double>("dRLostTrackJet")    ? iConfig.getParameter<double>("dRLostTrackJet") : 0.2),
  dRJetGenMatch            (iConfig.existsAs<double>("dRJetGenMatch")    ? iConfig.getParameter<double>("dRJetGenMatch") : 0.4),
  dumpOnlyJetMatchedToGen  (iConfig.existsAs<bool>("dumpOnlyJetMatchedToGen")  ? iConfig.getParameter<bool>  ("dumpOnlyJetMatchedToGen") : false),
  usePuppiJets             (iConfig.existsAs<bool>("usePuppiJets")  ? iConfig.getParameter<bool>  ("usePuppiJets") : false),
  saveLHEObjects           (iConfig.existsAs<bool>("saveLHEObjects")  ? iConfig.getParameter<bool>  ("saveLHEObjects") : false),
  isMC                     (iConfig.existsAs<bool>("isMC")   ? iConfig.getParameter<bool>  ("isMC") : true),
  pnetDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorLabels") : std::vector<std::string> ()),
  pnetDiscriminatorNames  (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorNames") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorNames") : std::vector<std::string> ()),
  parTDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("parTDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("parTDiscriminatorLabels") : std::vector<std::string> ()),
  parTDiscriminatorNames  (iConfig.existsAs<std::vector<std::string> > ("parTDiscriminatorNames") ? iConfig.getParameter<std::vector<std::string>>("parTDiscriminatorNames") : std::vector<std::string> ()),
  xsec                     (iConfig.existsAs<double>("xsec") ? iConfig.getParameter<double>("xsec") * 1000.0 : 1.){

  usesResource("TFileService");
}

TrainingTreeMakerAK4::~TrainingTreeMakerAK4() {}

void TrainingTreeMakerAK4::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  // MET filters
  edm::Handle<edm::TriggerResults>      filterResultsH;
  iEvent.getByToken(filterResultsToken, filterResultsH); 

  // triggers
  edm::Handle<edm::TriggerResults> triggerResultsH;
  iEvent.getByToken(triggerResultsToken, triggerResultsH);
 
  // Vertexes
  edm::Handle<reco::VertexCollection > primaryVerticesH;
  iEvent.getByToken(primaryVerticesToken, primaryVerticesH);

  edm::Handle<reco::VertexCompositePtrCandidateCollection> secondaryVerticesH;
  iEvent.getByToken(secondaryVerticesToken, secondaryVerticesH);
  reco::VertexCompositePtrCandidateCollection svColl = *secondaryVerticesH;

  edm::Handle<reco::VertexCompositePtrCandidateCollection> kaonVerticesH;
  iEvent.getByToken(kaonVerticesToken, kaonVerticesH);
  reco::VertexCompositePtrCandidateCollection kaonColl = *kaonVerticesH;
  
  edm::Handle<reco::VertexCompositePtrCandidateCollection> lambdaVerticesH;
  iEvent.getByToken(lambdaVerticesToken, lambdaVerticesH);
  reco::VertexCompositePtrCandidateCollection lambdaColl = *lambdaVerticesH;

  // rho
  edm::Handle<double> rho_val;
  iEvent.getByToken(rhoToken,rho_val);  

  // Muons / Electrons / Taus
  edm::Handle<pat::MuonCollection> muonsH;
  iEvent.getByToken(muonsToken,muonsH);
  pat::MuonCollection muonsColl = *muonsH;

  edm::Handle<pat::ElectronCollection> electronsH;
  iEvent.getByToken(electronsToken,electronsH);
  pat::ElectronCollection electronsColl = *electronsH;

  edm::Handle<pat::PhotonCollection> photonsH;
  iEvent.getByToken(photonsToken,photonsH);
  pat::PhotonCollection photonsColl = *photonsH;

  edm::Handle<pat::TauCollection> tausH;
  iEvent.getByToken(tausToken,tausH);
  pat::TauCollection tausColl = *tausH;

  // Jets 
  edm::Handle<pat::JetCollection> jetsH;
  iEvent.getByToken(jetsToken, jetsH);

  // MET 
  edm::Handle<pat::METCollection> metH;
  iEvent.getByToken(metToken, metH);

  // Lost tracks 
  edm::Handle<pat::PackedCandidateCollection> lostTracksH;
  iEvent.getByToken(lostTracksToken, lostTracksH);

  // Lepton pairs
  edm::Handle<edm::View<reco::CompositeCandidate>> leptonPairsH;
  iEvent.getByToken(leptonPairsToken, leptonPairsH);

  // GEN Level info and LHE
  edm::Handle<std::vector<PileupSummaryInfo> > pileupInfoH;
  edm::Handle<GenEventInfoProduct> genEvtInfoH;
  edm::Handle<reco::GenParticleCollection> gensH;
  edm::Handle<LHEEventProduct> lheInfoH;
  edm::Handle<reco::GenJetCollection> genJetsWnuH;
  edm::Handle<reco::GenJetCollection> genJetsH;
  edm::Handle<reco::JetFlavourInfoMatchingCollection> genJetsFlavourH;

  if(isMC){
    iEvent.getByToken(pileupInfoToken, pileupInfoH);  
    iEvent.getByToken(genEvtInfoToken, genEvtInfoH);  
    iEvent.getByToken(gensToken, gensH);
    iEvent.getByToken(lheInfoToken,lheInfoH);
    iEvent.getByToken(genJetsWnuToken, genJetsWnuH);
    iEvent.getByToken(genJetsToken, genJetsH);
    iEvent.getByToken(genJetsFlavourToken, genJetsFlavourH);
  }

  edm::ESHandle<TransientTrackBuilder> trackBuilderH;
  trackBuilderH = iSetup.getHandle(trackBuilderToken);
  
  initializeBranches();
     
  // Event information - MC weight, event ID (run, lumi, event) and so on
  event = iEvent.id().event();
  run   = iEvent.id().run();
  lumi  = iEvent.luminosityBlock();

  // MC weight
  wgt = 1.0;
  if(lheInfoH.isValid() and not lheInfoH->weights().empty())
    wgt = lheInfoH->weights()[0].wgt;
  else if(genEvtInfoH.isValid())
    wgt = genEvtInfoH->weight(); 
  

  // Pileup information
  putrue = 0;
  if(pileupInfoH.isValid()){
    for (auto pileupInfo_iter = pileupInfoH->begin(); pileupInfo_iter != pileupInfoH->end(); ++pileupInfo_iter) {
      if (pileupInfo_iter->getBunchCrossing() == 0) 
	putrue = (unsigned int) pileupInfo_iter->getTrueNumInteractions();
    }
  }
    
  // Vertices RECO
  npv  = (unsigned int) primaryVerticesH->size();
  nsv  = (unsigned int) secondaryVerticesH->size();
  nkaon  = (unsigned int) kaonVerticesH->size();
  nlambda  = (unsigned int) lambdaVerticesH->size();
  
  // Rho value
  rho = *rho_val;

  // Missing energy
  met      = metH->front().corPt();
  met_phi  = metH->front().corPhi();

  // MET filter info
  unsigned int flagvtx       = 1 * 1;
  unsigned int flaghalo      = 1 * 2;
  unsigned int flaghbhe      = 1 * 4;
  unsigned int flaghbheiso   = 1 * 8;
  unsigned int flagecaltp    = 1 * 16;
  unsigned int flagbadmuon   = 1 * 32; 
  unsigned int flagbadhad    = 1 * 64;
 
  // Which MET filters passed
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    if (filterPathsMap[filterPathsVector[i]] == -1) continue;
    if (i == 0  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagvtx       = 0; // goodVertices
    if (i == 1  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghalo      = 0; // CSCTightHaloFilter
    if (i == 2  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbhe      = 0; // HBHENoiseFilter
    if (i == 3  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbheiso   = 0; // HBHENoiseIsoFilter
    if (i == 4  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagecaltp    = 0; // EcalDeadCellTriggerPrimitiveFilter
    if (i == 5  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadmuon   = 0; // badmuon
    if (i == 6  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadhad    = 0; // badhadrons
  }

  flags = flagvtx + flaghalo + flaghbhe + flaghbheiso + flagecaltp + flagbadmuon + flagbadhad;

  // HLT triggers                                                                                                                                                                                     
  for(auto key : triggerPathsMap){
    trigger_hlt_path.push_back(key.first);
    trigger_hlt_pass.push_back(triggerResultsH->accept(key.second));
  }

  // LHE level information
  if(lheInfoH.isValid() and saveLHEObjects){
    const auto& hepeup = lheInfoH->hepeup();
    const auto& pup    = hepeup.PUP;
    for (unsigned int i = 0, n = pup.size(); i < n; ++i) {
      int status = hepeup.ISTUP[i];
      int id     = hepeup.IDUP[i];
      TLorentzVector p4(pup[i][0], pup[i][1], pup[i][2], pup[i][3]);
      if(abs(id) == 25 or abs(id) == 23 or abs(id) == 24 or abs(id)==6){ // Higgs, Z or W or top                                                                                             
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 10 && abs(id) <= 17 and status == 1){ // final state leptons + neutrinos
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
      else if(abs(id) >= 1 && abs(id) <= 5 and status == 1){ // final state quarks                                                                                                    
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
	lhe_particle_id.push_back(id);
      }
      else if(abs(id) == 21 and status == 1){ // final state gluons                                                                                                               
        lhe_particle_pt.push_back(p4.Pt());
        lhe_particle_eta.push_back(p4.Eta());
        lhe_particle_phi.push_back(p4.Phi());
        lhe_particle_mass.push_back(p4.M());
        lhe_particle_status.push_back(status);
        lhe_particle_id.push_back(id);
      }
    }
  }

  // GEN particle info
  if(gensH.isValid()){
    unsigned int igen = 0;
    for (auto gens_iter = gensH->begin(); gens_iter != gensH->end(); ++gens_iter) {      
      // Higgs, Z and W beforethe decay and after the shower effects
      if((abs(gens_iter->pdgId()) == 25 or abs(gens_iter->pdgId()) == 24 or abs(gens_iter->pdgId()) == 23) and	
	 gens_iter->isLastCopy() and 
	 gens_iter->statusFlags().fromHardProcess()){ 
	
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

        for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
          gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
          gen_particle_daughters_igen.push_back(igen);
          gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
          gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
          gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
          gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
        }
        igen++;
      }
      
      // Final states Leptons (e,mu) and Neutrinos --> exclude taus. They need to be prompt or from Tau decay      
      if (abs(gens_iter->pdgId()) > 10 and abs(gens_iter->pdgId()) < 17 and abs(gens_iter->pdgId()) != 15  and 
	  (gens_iter->isPromptFinalState() or 
	   gens_iter->isDirectPromptTauDecayProductFinalState())) { 

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// No need to save daughters here
        igen++;
      }
      
      // Final state quarks or gluons from the hard process before the shower --> partons in which H/Z/W/top decay into
      if (((abs(gens_iter->pdgId()) >= 1 and abs(gens_iter->pdgId()) <= 5) or abs(gens_iter->pdgId()) == 21) and 
	  gens_iter->statusFlags().fromHardProcess() and 
	  gens_iter->statusFlags().isFirstCopy()){
	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());
	igen++;
	// no need to save daughters
      }

      // Special case of taus: last-copy, from hard process and, prompt and decayed
      if(abs(gens_iter->pdgId()) == 15 and 
	 gens_iter->isLastCopy() and
	 gens_iter->statusFlags().fromHardProcess() and
	 gens_iter->isPromptDecayed()){ // hadronic taus

	gen_particle_pt.push_back(gens_iter->pt());
	gen_particle_eta.push_back(gens_iter->eta());
	gen_particle_phi.push_back(gens_iter->phi());
	gen_particle_mass.push_back(gens_iter->mass());
	gen_particle_id.push_back(gens_iter->pdgId());
	gen_particle_status.push_back(gens_iter->status());

	// only store the final decay particles
	for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
	  if(not dynamic_cast<const reco::GenParticle*>(gens_iter->daughter(idau))->statusFlags().isPromptTauDecayProduct()) continue;
	  gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
	  gen_particle_daughters_igen.push_back(igen);
	  gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
	  gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
	  gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
	  gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());	    
	  gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
	  gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
	}
	igen++;
      }	
    }
  }

  // Sorting muons based on pT
  sort(muonsColl.begin(),muonsColl.end(),muonSorter);

  for (size_t i = 0; i < muonsColl.size(); i++) {

    if(muonsColl[i].pt() < muonPtMin) continue;
    if(fabs(muonsColl[i].eta()) > muonEtaMax) continue;

    muon_pt.push_back(muonsColl[i].pt());
    muon_eta.push_back(muonsColl[i].eta());
    muon_phi.push_back(muonsColl[i].phi());
    muon_energy.push_back(muonsColl[i].energy());
    
    // Muon isolation
    int isoval = 0;
    if(muonsColl[i].passed(reco::Muon::PFIsoLoose))
      isoval += 1;
    if(muonsColl[i].passed(reco::Muon::PFIsoMedium))
      isoval += 2;
    if(muonsColl[i].passed(reco::Muon::PFIsoTight))
      isoval += 4;
    if(muonsColl[i].passed(reco::Muon::PFIsoVeryTight))
      isoval += 8;
    if(muonsColl[i].passed(reco::Muon::MiniIsoLoose))
      isoval += 16;
    if(muonsColl[i].passed(reco::Muon::MiniIsoMedium))
      isoval += 32;
    if(muonsColl[i].passed(reco::Muon::MiniIsoTight))
      isoval += 64;
    
    muon_iso.push_back(isoval);
    
    // Muon id
    int midval = 0;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdLoose))
       midval += 1;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdMedium))
       midval += 2;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdTight))
       midval += 4;
    if(muonsColl[i].passed(reco::Muon::MvaLoose))
       midval += 8;
    if(muonsColl[i].passed(reco::Muon::MvaMedium))
       midval += 16;
    if(muonsColl[i].passed(reco::Muon::MvaTight))
       midval += 32;
    
    muon_id.push_back(midval);
    muon_charge.push_back(muonsColl[i].charge());
  }

  // Sorting electrons based on pT
  sort(electronsColl.begin(),electronsColl.end(),electronSorter);

  for (size_t i = 0; i < electronsColl.size(); i++) {

    if(electronsColl[i].pt() < electronPtMin) continue;
    if(fabs(electronsColl[i].eta()) > electronEtaMax) continue;
   
    electron_pt.push_back(electronsColl[i].pt());
    if(electronsColl[i].hasUserFloat("ecalTrkEnergyPostCorr"))
      electron_pt_corr.push_back(electronsColl[i].pt()*electronsColl[i].userFloat("ecalTrkEnergyPostCorr")/electronsColl[i].energy());
    electron_eta.push_back(electronsColl[i].eta());
    electron_phi.push_back(electronsColl[i].phi());
    electron_energy.push_back(electronsColl[i].energy());

    int eidval = 0;   
    if(electronsColl[i].isElectronIDAvailable("cutBasedElectronID-Fall17-94X-V2-loose") and electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-loose"))
      eidval += 1;
    if(electronsColl[i].isElectronIDAvailable("cutBasedElectronID-Fall17-94X-V2-medium") and electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-medium"))
      eidval += 2;
    if(electronsColl[i].isElectronIDAvailable("cutBasedElectronID-Fall17-94X-V2-tight") and electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-tight"))
      eidval += 4;
    if(electronsColl[i].isElectronIDAvailable("mvaEleID-Fall17-iso-V2-wpLoose") and electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wpLoose"))
      eidval += 8;
    if(electronsColl[i].isElectronIDAvailable("mvaEleID-Fall17-iso-V2-wp90") and electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp90"))
      eidval += 16;
    if(electronsColl[i].isElectronIDAvailable("mvaEleID-Fall17-iso-V2-wp80") and electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp80"))
      eidval += 32;

    electron_id.push_back(eidval);
    if(electronsColl[i].hasUserFloat("ElectronMVAEstimatorRun2Fall17IsoV2Values"))
      electron_idscore.push_back(electronsColl[i].userFloat("ElectronMVAEstimatorRun2Fall17IsoV2Values"));
    electron_charge.push_back(electronsColl[i].charge());
  }

  // Sorting photons based on pT
  sort(photonsColl.begin(),photonsColl.end(),photonSorter);

  for (size_t i = 0; i < photonsColl.size(); i++) {

    if(photonsColl[i].pt() < photonPtMin) continue;
    if(fabs(photonsColl[i].eta()) > photonEtaMax) continue;
   
    photon_pt.push_back(photonsColl[i].pt());
    if(photonsColl[i].hasUserFloat("ecalEnergyPostCorr"))
      photon_pt_corr.push_back(photonsColl[i].pt()*photonsColl[i].userFloat("ecalEnergyPostCorr")/photonsColl[i].energy());
    photon_eta.push_back(photonsColl[i].eta());
    photon_phi.push_back(photonsColl[i].phi());
    photon_energy.push_back(photonsColl[i].energy());

    int pidval = 0;   
    if(photonsColl[i].isPhotonIDAvailable("cutBasedPhotonID-Fall17-94X-V2-loose") and photonsColl[i].photonID("cutBasedPhotonID-Fall17-94X-V2-loose"))
      pidval += 1;
    if(photonsColl[i].isPhotonIDAvailable("cutBasedPhotonID-Fall17-94X-V2-medium") and photonsColl[i].photonID("cutBasedPhotonID-Fall17-94X-V2-medium"))
      pidval += 2;
    if(photonsColl[i].isPhotonIDAvailable("cutBasedPhotonID-Fall17-94X-V2-tight") and photonsColl[i].photonID("cutBasedPhotonID-Fall17-94X-V2-tight"))
      pidval += 4;
    if(photonsColl[i].isPhotonIDAvailable("mvaPhoID-RunIIFall17-v2-wp90") and photonsColl[i].photonID("mvaPhoID-RunIIFall17-v2-wp90"))
      pidval += 8;
    if(photonsColl[i].isPhotonIDAvailable("mvaPhoID-RunIIFall17-v2-wp80") and photonsColl[i].photonID("mvaPhoID-RunIIFall17-v2-wp80"))
      pidval += 16;

    photon_id.push_back(pidval);
    if(photonsColl[i].hasUserFloat("PhotonMVAEstimatorRunIIFall17v2Values"))
      photon_idscore.push_back(photonsColl[i].userFloat("PhotonMVAEstimatorRunIIFall17v2Values"));
  }

  // Sorting taus based on pT
  sort(tausColl.begin(),tausColl.end(),tauSorter);
  std::vector<math::XYZTLorentzVector> tau_pfcandidates;  // in order to match PF and HPS candidates

  for (size_t i = 0; i < tausColl.size(); i++) {
    
    if(tausColl[i].pt() < tauPtMin) continue;
    if(fabs(tausColl[i].eta()) > tauEtaMax) continue;

    tau_pt.push_back(tausColl[i].pt());
    tau_eta.push_back(tausColl[i].eta());
    tau_phi.push_back(tausColl[i].phi());
    tau_mass.push_back(tausColl[i].mass());
    tau_energy.push_back(tausColl[i].energy());
    tau_dxy.push_back(tausColl[i].dxy());
    tau_dz.push_back((tausColl[i].leadChargedHadrCand().get() ? dynamic_cast<const pat::PackedCandidate*>(tausColl[i].leadChargedHadrCand().get())->dz() : 0.));
    tau_decaymode.push_back(tausColl[i].decayMode());
    tau_charge.push_back(tausColl[i].charge());
    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSjetraw"))
      tau_idjet.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSjetraw"));
    else if(tausColl[i].isTauIDAvailable("byDeepTau2017v2p1VSjetraw"))
      tau_idjet.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSjetraw"));

    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSmuraw"))
      tau_idmu.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSmuraw"));
    else if(tausColl[i].isTauIDAvailable("byDeepTau2017v2p1VSmuraw"))
      tau_idmu.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSmuraw"));

    if(tausColl[i].isTauIDAvailable("byDeepTau2018v2p5VSeraw"))
      tau_idele.push_back(tausColl[i].tauID("byDeepTau2018v2p5VSeraw"));
    else if(tausColl[i].isTauIDAvailable("byDeepTau2017v2p1VSeraw"))
      tau_idele.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSeraw"));

    int tauvsjetid = 0;
    if(tausColl[i].isTauIDAvailable("byVVVLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byVVVLooseDeepTau2017v2p1VSjet")) 
      tauvsjetid += 1;
    if(tausColl[i].isTauIDAvailable("byVVLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byVVLooseDeepTau2017v2p1VSjet")) 
      tauvsjetid += 2;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byVLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 4;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSjet") and tausColl[i].tauID("byLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 8;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSjet") and tausColl[i].tauID("byMediumDeepTau2017v2p1VSjet"))
      tauvsjetid += 16;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2017v2p1VSjet") and tausColl[i].tauID("byTightDeepTau2017v2p1VSjet"))
      tauvsjetid += 32;

    tau_idjet_wp.push_back(tauvsjetid);

    int tauvsmuid = 0;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSmu") and tausColl[i].tauID("byVLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 1;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSmu") and tausColl[i].tauID("byLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 2;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSmu") and tausColl[i].tauID("byMediumDeepTau2017v2p1VSmu"))
      tauvsmuid += 4;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2017v2p1VSmu") and tausColl[i].tauID("byTightDeepTau2017v2p1VSmu"))
      tauvsmuid += 8;

    tau_idmu_wp.push_back(tauvsmuid);

    int tauvselid = 0;
    if(tausColl[i].isTauIDAvailable("byVVVLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byVVVLooseDeepTau2017v2p1VSe")) 
      tauvselid += 1;
    if(tausColl[i].isTauIDAvailable("byVVLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byVVLooseDeepTau2017v2p1VSe")) 
      tauvselid += 2;
    if(tausColl[i].isTauIDAvailable("byVLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byVLooseDeepTau2017v2p1VSe"))
      tauvselid += 4;
    if(tausColl[i].isTauIDAvailable("byLooseDeepTau2017v2p1VSe") and tausColl[i].tauID("byLooseDeepTau2017v2p1VSe"))
      tauvselid += 8;
    if(tausColl[i].isTauIDAvailable("byMediumDeepTau2017v2p1VSe") and tausColl[i].tauID("byMediumDeepTau2017v2p1VSe"))
      tauvselid += 16;
    if(tausColl[i].isTauIDAvailable("byTightDeepTau2017v2p1VSe") and tausColl[i].tauID("byTightDeepTau2017v2p1VSe"))
      tauvselid += 32;

    tau_idele_wp.push_back(tauvselid);

    if(tausColl[i].genJet()){
      tau_genmatch_pt.push_back(tausColl[i].genJet()->pt());
      tau_genmatch_eta.push_back(tausColl[i].genJet()->eta());
      tau_genmatch_phi.push_back(tausColl[i].genJet()->phi());
      tau_genmatch_mass.push_back(tausColl[i].genJet()->mass());
      // reconstruct the decay mode of the gen-jet
      unsigned int tau_ch = 0;
      unsigned int tau_ph = 0;
      unsigned int tau_nh = 0;
      auto gen_constituents = tausColl[i].genJet()->getGenConstituents();
      for(size_t iconst = 0; iconst < gen_constituents.size(); iconst++){
	auto part = gen_constituents[iconst];
	if(part->status() != 1) continue;
	if(part->charge() == 0 and abs(part->pdgId()) == 22) tau_ph++;
	if(part->charge() == 0 and abs(part->pdgId()) != 22) tau_nh++;
	if(part->charge() != 0 and abs(part->pdgId()) != 11 and abs(part->pdgId()) != 13) tau_ch++;
      }
      tau_genmatch_decaymode.push_back(5*(tau_ch-1)+tau_ph/2+tau_nh);
    }
    else{
      tau_genmatch_pt.push_back(-1);
      tau_genmatch_eta.push_back(-1);
      tau_genmatch_phi.push_back(-1);
      tau_genmatch_mass.push_back(-1);
      tau_genmatch_decaymode.push_back(-1);
    }
    for(unsigned ipart = 0; ipart < tausColl[i].signalCands().size(); ipart++){
      const pat::PackedCandidate* pfcand = dynamic_cast<const pat::PackedCandidate*> (tausColl[i].signalCands()[ipart].get());      
      tau_pfcandidates.push_back(pfcand->p4());
    }
  }

  // save lepton paurs
  if(leptonPairsH.isValid()){
    leppair_pt.push_back(leptonPairsH->at(0).pt());
    leppair_eta.push_back(leptonPairsH->at(0).eta());
    leppair_phi.push_back(leptonPairsH->at(0).phi());
    leppair_mass.push_back(leptonPairsH->at(0).mass());
    leppair_pt1.push_back(leptonPairsH->at(0).daughter(0)->pt());
    leppair_eta1.push_back(leptonPairsH->at(0).daughter(0)->eta());
    leppair_phi1.push_back(leptonPairsH->at(0).daughter(0)->phi());
    leppair_mass1.push_back(leptonPairsH->at(0).daughter(0)->mass());
    leppair_pdg1.push_back(leptonPairsH->at(0).daughter(0)->pdgId());
    leppair_pt2.push_back(leptonPairsH->at(0).daughter(1)->pt());
    leppair_eta2.push_back(leptonPairsH->at(0).daughter(1)->eta());
    leppair_phi2.push_back(leptonPairsH->at(0).daughter(1)->phi());
    leppair_mass2.push_back(leptonPairsH->at(0).daughter(1)->mass());
    leppair_pdg2.push_back(leptonPairsH->at(0).daughter(1)->pdgId());
  }
  
  // Standard gen-jets excluding the neutrinos
  std::vector<reco::GenJetRef> jetv_gen;   
  if(genJetsH.isValid()){
    for (auto jets_iter = genJetsH->begin(); jets_iter != genJetsH->end(); ++jets_iter) {                                                                                                   
      reco::GenJetRef jref (genJetsH,jets_iter-genJetsH->begin());                                                                                                                      
      jetv_gen.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen.begin(), jetv_gen.end(), genJetRefSorter);
  }

  // GEN jets with neutrinos
  std::vector<reco::GenJetRef> jetv_gen_wnu;   
  if(genJetsWnuH.isValid()){
    for (auto jets_iter = genJetsWnuH->begin(); jets_iter != genJetsWnuH->end(); ++jets_iter) {                                                                                           
      reco::GenJetRef jref  (genJetsWnuH, jets_iter-genJetsWnuH->begin());                                                                                                                 
      jetv_gen_wnu.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen_wnu.begin(), jetv_gen_wnu.end(), genJetRefSorter);
  }

  // Offline jets
  std::vector<pat::JetRef> jetv;   
  for (auto jets_iter = jetsH->begin(); jets_iter != jetsH->end(); ++jets_iter) {                                                                                                                     
    pat::JetRef jref(jetsH, jets_iter - jetsH->begin());                                                                                                                                            
    if (jref->pt() < jetPtMin and jref->correctedJet("Uncorrected").pt() < jetPtMin) continue; 
    if (fabs(jref->eta()) > jetEtaMax) continue;                 
    if (fabs(jref->eta()) < jetEtaMin) continue;                 
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
    jetv.push_back(jref);                                                                                                                                                                           
  }    
  sort(jetv.begin(), jetv.end(), jetRefSorter);
 
  // Store jet info
  for (size_t ijet = 0; ijet < jetv.size(); ijet++) {

    jet_pt.push_back(jetv[ijet]->pt());
    jet_eta.push_back(jetv[ijet]->eta());
    jet_phi.push_back(jetv[ijet]->phi());
    jet_mass.push_back(jetv[ijet]->mass());

    jet_pt_raw.push_back(jetv[ijet]->correctedJet("Uncorrected").pt());
    jet_mass_raw.push_back(jetv[ijet]->correctedJet("Uncorrected").mass());

    jet_deepjet_probb.push_back(jetv[ijet]->bDiscriminator("pfDeepFlavourJetTags:probb"));
    jet_deepjet_probbb.push_back(jetv[ijet]->bDiscriminator("pfDeepFlavourJetTags:probbb"));
    jet_deepjet_problepb.push_back(jetv[ijet]->bDiscriminator("pfDeepFlavourJetTags:problepb"));
    jet_deepjet_probc.push_back(jetv[ijet]->bDiscriminator("pfDeepFlavourJetTags:probc"));
    jet_deepjet_probg.push_back(jetv[ijet]->bDiscriminator("pfDeepFlavourJetTags:probg"));
    jet_deepjet_probuds.push_back(jetv[ijet]->bDiscriminator("pfDeepFlavourJetTags:probuds"));

    jet_pnet_probb.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probb"));
    jet_pnet_probbb.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probbb"));
    jet_pnet_probc.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probc"));
    jet_pnet_probcc.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probcc"));
    jet_pnet_probuds.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probuds"));
    jet_pnet_probg.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probg"));
    jet_pnet_probpu.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probpu"));
    jet_pnet_probundef.push_back(jetv[ijet]->bDiscriminator("pfParticleNetAK4JetTags:probundef"));

    for (size_t ilabel = 0; ilabel < pnetDiscriminatorLabels.size(); ilabel++)
      jet_pnetlast_score[pnetDiscriminatorLabels[ilabel]].push_back(jetv[ijet]->bDiscriminator(pnetDiscriminatorNames[ilabel].c_str()));

    for (size_t ilabel = 0; ilabel < parTDiscriminatorLabels.size(); ilabel++)
      jet_parTlast_score[parTDiscriminatorLabels[ilabel]].push_back(jetv[ijet]->bDiscriminator(parTDiscriminatorNames[ilabel].c_str()));

    // JetId    
    int jetid = 0;    
    if(applyJetID(*jetv[ijet],"tight",usePuppiJets)) jetid += 1;
    jet_id.push_back(jetid);

    int jetpuid = 0;
    if(applyPileupJetID(*jetv[ijet],"loose"))  jetpuid += 1;
    if(applyPileupJetID(*jetv[ijet],"medium")) jetpuid += 2;
    if(applyPileupJetID(*jetv[ijet],"tight"))  jetpuid += 4;
    jet_puid.push_back(jetpuid);

    // Energy fractions
    jet_chf.push_back(jetv[ijet]->chargedHadronEnergyFraction());
    jet_nhf.push_back(jetv[ijet]->neutralHadronEnergyFraction());
    jet_elf.push_back(jetv[ijet]->electronEnergyFraction());
    jet_phf.push_back(jetv[ijet]->photonEnergyFraction());
    jet_muf.push_back(jetv[ijet]->muonEnergyFraction());

    // PF components
    jet_ncand.push_back(jetv[ijet]->chargedHadronMultiplicity()+jetv[ijet]->neutralHadronMultiplicity()+jetv[ijet]->electronMultiplicity()+jetv[ijet]->photonMultiplicity()+jetv[ijet]->muonMultiplicity());
    jet_nch.push_back(jetv[ijet]->chargedHadronMultiplicity());
    jet_nnh.push_back(jetv[ijet]->neutralHadronMultiplicity());
    jet_nel.push_back(jetv[ijet]->electronMultiplicity());
    jet_nph.push_back(jetv[ijet]->photonMultiplicity());
    jet_nmu.push_back(jetv[ijet]->muonMultiplicity());    
    jet_hflav.push_back(jetv[ijet]->hadronFlavour());
    jet_pflav.push_back(jetv[ijet]->partonFlavour());
    jet_nbhad.push_back(jetv[ijet]->jetFlavourInfo().getbHadrons().size());
    jet_nchad.push_back(jetv[ijet]->jetFlavourInfo().getcHadrons().size());
    
    // Matching with gen-jets
    int pos_matched = -1;
    float minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv[ijet]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv[ijet]->p4());
      }
    }
      
    if(pos_matched >= 0){      
      jet_genmatch_pt.push_back(jetv_gen[pos_matched]->pt());
      jet_genmatch_eta.push_back(jetv_gen[pos_matched]->eta());
      jet_genmatch_phi.push_back(jetv_gen[pos_matched]->phi());
      jet_genmatch_mass.push_back(jetv_gen[pos_matched]->mass());
      jet_genmatch_hflav.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getHadronFlavour());
      jet_genmatch_pflav.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getPartonFlavour());      
      jet_genmatch_nbhad.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getbHadrons().size());      
      jet_genmatch_nchad.push_back((*genJetsFlavourH)[edm::RefToBase<reco::Jet>(jetv_gen[pos_matched])].getcHadrons().size());      
    }
    else{
      jet_genmatch_pt.push_back(0);
      jet_genmatch_eta.push_back(0);
      jet_genmatch_phi.push_back(0);
      jet_genmatch_mass.push_back(0);
      jet_genmatch_hflav.push_back(0);
      jet_genmatch_pflav.push_back(0);
      jet_genmatch_nbhad.push_back(0);
      jet_genmatch_nchad.push_back(0);
    }
      
    //////////
    pos_matched = -1;
    minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen_wnu.size(); igen++){
      if(reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[ijet]->p4()) < minDR){
	pos_matched = igen;
	minDR = reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[ijet]->p4());
      }
    }
    
    if(pos_matched >= 0){      
      jet_genmatch_wnu_pt.push_back(jetv_gen_wnu[pos_matched]->pt());
      jet_genmatch_wnu_eta.push_back(jetv_gen_wnu[pos_matched]->eta());
      jet_genmatch_wnu_phi.push_back(jetv_gen_wnu[pos_matched]->phi());
      jet_genmatch_wnu_mass.push_back(jetv_gen_wnu[pos_matched]->mass());
    }
    else{
      jet_genmatch_wnu_pt.push_back(0);
      jet_genmatch_wnu_eta.push_back(0);
      jet_genmatch_wnu_phi.push_back(0);
      jet_genmatch_wnu_mass.push_back(0);
    }

    // PF candidates dump
    math::XYZVector jetDir = jetv[ijet]->momentum().Unit();
    TVector3 jet_direction (jetv[ijet]->momentum().Unit().x(),jetv[ijet]->momentum().Unit().y(),jetv[ijet]->momentum().Unit().z());
    GlobalVector jet_global_vec (jetv[ijet]->px(),jetv[ijet]->py(),jetv[ijet]->pz());	     

    // secondary vertex that are inside the jet cone + sorting in significance
    std::vector<reco::VertexCompositePtrCandidate> jetSVs;    
    for(size_t isv = 0; isv < svColl.size(); isv++){
      if(reco::deltaR(svColl.at(isv),*jetv[ijet]) < dRJetGenMatch){
	jetSVs.push_back(svColl.at(isv));
      }
    }
    
    VertexSorterDxy<reco::VertexCompositePtrCandidate,reco::Vertex> svSorter (primaryVerticesH->front());
    std::sort(jetSVs.begin(),jetSVs.end(),svSorter);

    for(auto const & svcand : jetSVs){
      jet_sv_pt.push_back(svcand.pt());
      jet_sv_eta.push_back(svcand.eta());
      jet_sv_phi.push_back(svcand.phi());
      jet_sv_mass.push_back(svcand.mass());
      jet_sv_energy.push_back(svcand.energy());
      jet_sv_chi2.push_back(svcand.vertexNormalizedChi2());
      jet_sv_ntrack.push_back(svcand.numberOfDaughters());
      jet_sv_ijet.push_back(ijet);
      auto dxy = vertexDxy(svcand,primaryVerticesH->front());
      jet_sv_dxy.push_back(dxy.value());
      jet_sv_dxysig.push_back(dxy.significance());
      auto dxy_signed = vertexDxySigned(svcand,primaryVerticesH->front(),jet_global_vec);
      jet_sv_dxy_sign.push_back(dxy_signed.value()/dxy.value());
      auto d3d = vertexD3d(svcand,primaryVerticesH->front());
      jet_sv_d3d.push_back(d3d.value());
      jet_sv_d3dsig.push_back(d3d.significance());
      auto d3d_signed = vertexD3dSigned(svcand,primaryVerticesH->front(),jet_global_vec);
      jet_sv_d3d_sign.push_back(d3d_signed.value()/d3d.value());
      jet_sv_costheta.push_back(vertexDdotP(svcand,primaryVerticesH->front()));      
    }    

    // kaon
    std::vector<reco::VertexCompositePtrCandidate> jetKaons;    
    for(size_t ik = 0; ik < kaonColl.size(); ik++){
      if(reco::deltaR(kaonColl.at(ik),*jetv[ijet]) < dRJetGenMatch){
	jetKaons.push_back(kaonColl.at(ik));
      }
    }
    
    VertexSorterDxy<reco::VertexCompositePtrCandidate,reco::Vertex> kaonSorter (primaryVerticesH->front());
    std::sort(jetKaons.begin(),jetKaons.end(),kaonSorter);

    for(auto const & kcand : jetKaons){
      jet_kaon_pt.push_back(kcand.pt());
      jet_kaon_eta.push_back(kcand.eta());
      jet_kaon_phi.push_back(kcand.phi());
      jet_kaon_mass.push_back(kcand.mass());
      jet_kaon_energy.push_back(kcand.energy());
      jet_kaon_chi2.push_back(kcand.vertexNormalizedChi2());
      jet_kaon_ntrack.push_back(kcand.numberOfDaughters());
      jet_kaon_ijet.push_back(ijet);
      auto dxy = vertexDxy(kcand,primaryVerticesH->front());
      jet_kaon_dxy.push_back(dxy.value());
      jet_kaon_dxysig.push_back(dxy.significance());
      auto dxy_signed = vertexDxySigned(kcand,primaryVerticesH->front(),jet_global_vec);
      jet_kaon_dxy_sign.push_back(dxy_signed.value()/dxy.value());
      auto d3d = vertexD3d(kcand,primaryVerticesH->front());
      jet_kaon_d3d.push_back(d3d.value());
      jet_kaon_d3dsig.push_back(d3d.significance());
      auto d3d_signed = vertexD3dSigned(kcand,primaryVerticesH->front(),jet_global_vec);
      jet_kaon_d3d_sign.push_back(d3d_signed.value()/d3d.value());
      jet_kaon_costheta.push_back(vertexDdotP(kcand,primaryVerticesH->front()));      
    }    

    // lambda
    std::vector<reco::VertexCompositePtrCandidate> jetLambdas;    
    for(size_t il = 0; il < lambdaColl.size(); il++){
      if(reco::deltaR(lambdaColl.at(il),*jetv[ijet]) < dRJetGenMatch){
	jetLambdas.push_back(lambdaColl.at(il));
      }
    }
    
    VertexSorterDxy<reco::VertexCompositePtrCandidate,reco::Vertex> lambdaSorter (primaryVerticesH->front());
    std::sort(jetLambdas.begin(),jetLambdas.end(),lambdaSorter);

    for(auto const & lcand : jetLambdas){
      jet_lambda_pt.push_back(lcand.pt());
      jet_lambda_eta.push_back(lcand.eta());
      jet_lambda_phi.push_back(lcand.phi());
      jet_lambda_mass.push_back(lcand.mass());
      jet_lambda_energy.push_back(lcand.energy());
      jet_lambda_chi2.push_back(lcand.vertexNormalizedChi2());
      jet_lambda_ntrack.push_back(lcand.numberOfDaughters());
      jet_lambda_ijet.push_back(ijet);
      auto dxy = vertexDxy(lcand,primaryVerticesH->front());
      jet_lambda_dxy.push_back(dxy.value());
      jet_lambda_dxysig.push_back(dxy.significance());
      auto dxy_signed = vertexDxySigned(lcand,primaryVerticesH->front(),jet_global_vec);
      jet_lambda_dxy_sign.push_back(dxy_signed.value()/dxy.value());
      auto d3d = vertexD3d(lcand,primaryVerticesH->front());
      jet_lambda_d3d.push_back(d3d.value());
      jet_lambda_d3dsig.push_back(d3d.significance());
      auto d3d_signed = vertexD3dSigned(lcand,primaryVerticesH->front(),jet_global_vec);
      jet_lambda_d3d_sign.push_back(d3d_signed.value()/d3d.value());
      jet_lambda_costheta.push_back(vertexDdotP(lcand,primaryVerticesH->front()));      
    }    

    // lost tracks inside the jet
    std::vector<pat::PackedCandidate> jetLostTracks;
    for(size_t itrk = 0; itrk < lostTracksH->size(); itrk++){      
      if(reco::deltaR(lostTracksH->at(itrk).p4(),jetv[ijet]->p4()) < dRLostTrackJet and
	 lostTracksH->at(itrk).pt() > lostTracksPtMin ){
        jetLostTracks.push_back(lostTracksH->at(itrk));
      }
    }
    std::sort(jetLostTracks.begin(),jetLostTracks.end(),packedPFCandidateSorter);
    
    for(auto const & ltrack : jetLostTracks){

      jet_losttrack_pt.push_back(ltrack.pt());
      jet_losttrack_eta.push_back(ltrack.eta());
      jet_losttrack_phi.push_back(ltrack.phi());
      jet_losttrack_mass.push_back(ltrack.mass());
      jet_losttrack_energy.push_back(ltrack.energy());      
      jet_losttrack_charge.push_back(ltrack.charge());
      jet_losttrack_ijet.push_back(ijet);	
      jet_losttrack_frompv.push_back(ltrack.fromPV());
      jet_losttrack_dz.push_back(ltrack.dz(primaryVerticesH->front().position()));
      jet_losttrack_dxy.push_back(ltrack.dxy(primaryVerticesH->front().position()));
      TVector3 ltrack_momentum (ltrack.momentum().x(),ltrack.momentum().y(),ltrack.momentum().z());
      jet_losttrack_candjet_pperp.push_back(jet_direction.Perp(ltrack_momentum)/ltrack_momentum.Mag());
      jet_losttrack_candjet_ppara.push_back(jet_direction.Dot(ltrack_momentum)/ltrack_momentum.Mag());
      jet_losttrack_candjet_dphi.push_back(jet_direction.DeltaPhi(ltrack_momentum));
      jet_losttrack_candjet_deta.push_back(jetv[ijet]->eta()-ltrack.eta());
      jet_losttrack_candjet_etarel.push_back(reco::btau::etaRel(jetDir,ltrack.momentum()));
      jet_losttrack_candjet_ptrel.push_back(ltrack_momentum.Perp(jet_direction));
      jet_losttrack_nlosthits.push_back(ltrack.lostInnerHits());
      	
      // track specific
      const reco::Track* track = ltrack.bestTrack();
      if(track){ 
	jet_losttrack_dzsig.push_back(ltrack.dzError()/ltrack.dz(primaryVerticesH->front().position()));
	jet_losttrack_dxysig.push_back(ltrack.dxyError()/ltrack.dxy(primaryVerticesH->front().position()));	      
	jet_losttrack_track_chi2.push_back(track->normalizedChi2());
	jet_losttrack_track_qual.push_back(track->qualityMask());	
	jet_losttrack_track_pterr.push_back(track->ptError()/track->pt());
	jet_losttrack_track_etaerr.push_back(track->etaError());
	jet_losttrack_track_phierr.push_back(track->phiError());	
	jet_losttrack_nhits.push_back(track->hitPattern().numberOfValidHits());
	jet_losttrack_npixhits.push_back(track->hitPattern().numberOfValidPixelHits());
	jet_losttrack_npixbarrelhits.push_back(track->hitPattern().numberOfValidPixelBarrelHits());
	jet_losttrack_nstriphits.push_back(track->hitPattern().numberOfValidStripHits());
	jet_losttrack_nstriptibhits.push_back(track->hitPattern().numberOfValidStripTIBHits()+track->hitPattern().numberOfValidStripTIDHits());
	jet_losttrack_nstriptobhits.push_back(track->hitPattern().numberOfValidStripTOBHits());
	jet_losttrack_nlayers.push_back(track->hitPattern().trackerLayersWithMeasurement());
	jet_losttrack_npixlayers.push_back(track->hitPattern().pixelLayersWithMeasurement());
	jet_losttrack_nstriplayers.push_back(track->hitPattern().stripLayersWithMeasurement());
	reco::TransientTrack transientTrack = trackBuilderH->build(*track);
	Measurement1D meas_ip2d    = IPTools::signedTransverseImpactParameter(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	Measurement1D meas_ip3d    = IPTools::signedImpactParameter3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	Measurement1D meas_jetdist = IPTools::jetTrackDistance(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	Measurement1D meas_decayl  = IPTools::signedDecayLength3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;	
	jet_losttrack_trackjet_sip2d.push_back(meas_ip2d.value());	
	jet_losttrack_trackjet_sip2dsig.push_back(meas_ip2d.significance());
	jet_losttrack_trackjet_sip3d.push_back(meas_ip3d.value());	
	jet_losttrack_trackjet_sip3dsig.push_back(meas_ip3d.significance());
	jet_losttrack_trackjet_dist.push_back(-meas_jetdist.value());
	jet_losttrack_trackjet_decayL.push_back(meas_decayl.value());
      }      
      else{
	jet_losttrack_dzsig.push_back(0);
	jet_losttrack_dxysig.push_back(0);	
	jet_losttrack_track_chi2.push_back(0);
	jet_losttrack_track_qual.push_back(0);
	jet_losttrack_track_pterr.push_back(0.);
	jet_losttrack_track_etaerr.push_back(0.);
	jet_losttrack_track_phierr.push_back(0.);
	jet_losttrack_nhits.push_back(0.);
	jet_losttrack_npixhits.push_back(0.);
	jet_losttrack_npixbarrelhits.push_back(0.);
	jet_losttrack_nstriphits.push_back(0.);
	jet_losttrack_nstriptibhits.push_back(0.);
	jet_losttrack_nstriptobhits.push_back(0.);
	jet_losttrack_nlayers.push_back(0.);
	jet_losttrack_npixlayers.push_back(0.);
	jet_losttrack_nstriplayers.push_back(0.);
	jet_losttrack_trackjet_sip2d.push_back(0.);
	jet_losttrack_trackjet_sip2dsig.push_back(0.);
	jet_losttrack_trackjet_sip3d.push_back(0.);
	jet_losttrack_trackjet_sip3dsig.push_back(0.);
	jet_losttrack_trackjet_dist.push_back(0.);
	jet_losttrack_trackjet_decayL.push_back(0.);
      }      
    }

    // PF candidates
    std::vector<pat::PackedCandidate> vectorOfConstituents;
    std::vector<reco::CandidatePtr> vectorOfConstituentsPtr;
    for(unsigned ipart = 0; ipart < jetv[ijet]->numberOfDaughters(); ipart++){
      const pat::PackedCandidate* pfPart = dynamic_cast<const pat::PackedCandidate*> (jetv[ijet]->daughter(ipart));      
      vectorOfConstituents.push_back(*pfPart);
      const reco::CandidatePtr & pfPartPtr = jetv[ijet]->daughterPtr(ipart);
      vectorOfConstituentsPtr.push_back(pfPartPtr);
    }
    
    std::sort(vectorOfConstituents.begin(),vectorOfConstituents.end(),packedPFCandidateSorter);
    std::sort(vectorOfConstituentsPtr.begin(),vectorOfConstituentsPtr.end(),candidatePtrSorter);

    std::vector<int> muonsToSkip;
    std::vector<int> electronsToSkip;
    std::vector<int> photonsToSkip;

    for(auto const & pfcand : vectorOfConstituents){
      
      // basic quantities
      if(pfcand.pt() < jetPFCandidatePtMin) continue;

      // charged candidates
      if(pfcand.charge()!=0){
	jet_pfcand_ch_pt.push_back(pfcand.pt());
	jet_pfcand_ch_eta.push_back(pfcand.eta());
	jet_pfcand_ch_phi.push_back(pfcand.phi());
	jet_pfcand_ch_mass.push_back(pfcand.mass());
	jet_pfcand_ch_energy.push_back(pfcand.energy());      
	jet_pfcand_ch_calofraction.push_back(pfcand.caloFraction());
	jet_pfcand_ch_hcalfraction.push_back(pfcand.hcalFraction());	
	jet_pfcand_ch_id.push_back(abs(pfcand.pdgId()));
	jet_pfcand_ch_charge.push_back(pfcand.charge());
	jet_pfcand_ch_ijet.push_back(ijet);
	jet_pfcand_ch_frompv.push_back(pfcand.fromPV());
	jet_pfcand_ch_dz.push_back(pfcand.dz(primaryVerticesH->front().position()));
	jet_pfcand_ch_dxy.push_back(pfcand.dxy(primaryVerticesH->front().position()));
	TVector3 pfcand_momentum (pfcand.momentum().x(),pfcand.momentum().y(),pfcand.momentum().z());
	jet_pfcand_ch_candjet_pperp.push_back(jet_direction.Perp(pfcand_momentum)/pfcand_momentum.Mag());
	jet_pfcand_ch_candjet_ppara.push_back(jet_direction.Dot(pfcand_momentum)/pfcand_momentum.Mag());
	jet_pfcand_ch_candjet_dphi.push_back(jet_direction.DeltaPhi(pfcand_momentum));
	jet_pfcand_ch_candjet_deta.push_back(jet_direction.Eta()-pfcand_momentum.Eta());
	jet_pfcand_ch_candjet_etarel.push_back(reco::btau::etaRel(jetDir,pfcand.momentum()));
	jet_pfcand_ch_candjet_ptrel.push_back(pfcand_momentum.Perp(jet_direction));
	jet_pfcand_ch_puppiw.push_back(pfcand.puppiWeight());
	jet_pfcand_ch_nlosthits.push_back(pfcand.lostInnerHits());
	
	// tau specific
	if(std::find(tau_pfcandidates.begin(),tau_pfcandidates.end(),pfcand.p4()) != tau_pfcandidates.end())
	  jet_pfcand_ch_tau_signal.push_back(1);
	else
	  jet_pfcand_ch_tau_signal.push_back(0);

	// muon specific
	if(abs(pfcand.pdgId()) == 13){
	  int ipos = -1;
	  float minDR = 1000;
	  for (size_t imu = 0; imu < muonsColl.size(); imu++) {
	    if(std::find(muonsToSkip.begin(),muonsToSkip.end(),imu) != muonsToSkip.end()) continue;
	    if(not muonsColl[imu].isPFMuon()) continue;
	    float dR = reco::deltaR(muonsColl[imu].p4(),pfcand.p4());
	    if(dR < dRJetGenMatch and dR < minDR){
	      minDR = dR;
	      ipos = imu;
	    }
	  }	
	  if(ipos >= 0){
	    muonsToSkip.push_back(ipos);
	    jet_pfcand_muon_pt.push_back(muonsColl[ipos].pt());
	    jet_pfcand_muon_eta.push_back(muonsColl[ipos].eta());
	    jet_pfcand_muon_phi.push_back(muonsColl[ipos].phi());
	    jet_pfcand_muon_energy.push_back(muonsColl[ipos].energy());
	    int type = 0;
	    if(muonsColl[ipos].isStandAloneMuon()) type += 1;
	    if(muonsColl[ipos].isTrackerMuon()) type += 2;
	    if(muonsColl[ipos].isPFMuon()) type += 4;
	    if(muonsColl[ipos].isGlobalMuon()) type += 8;
	    jet_pfcand_muon_type.push_back(type);
	    if(muonsColl[ipos].isGlobalMuon()){
	      jet_pfcand_muon_nvalidhits.push_back(muonsColl[ipos].globalTrack()->hitPattern().numberOfValidMuonHits());
	      jet_pfcand_muon_chi2.push_back(muonsColl[ipos].globalTrack()->normalizedChi2());
	    }
	    else if(muonsColl[ipos].innerTrack().isNonnull()){
	      jet_pfcand_muon_nvalidhits.push_back(muonsColl[ipos].innerTrack()->hitPattern().numberOfValidMuonHits());
	      jet_pfcand_muon_chi2.push_back(muonsColl[ipos].innerTrack()->normalizedChi2());
	    }
	    else{
	      jet_pfcand_muon_nvalidhits.push_back(0.);
	      jet_pfcand_muon_chi2.push_back(0.);
	    }
	    jet_pfcand_muon_nstations.push_back(muonsColl[ipos].numberOfMatchedStations());
	    jet_pfcand_muon_segcomp.push_back(muon::segmentCompatibility(muonsColl[ipos]));
	    jet_pfcand_muon_trkKink.push_back(muonsColl[ipos].combinedQuality().trkKink);
	    jet_pfcand_muon_nlayers.push_back((muonsColl[ipos].innerTrack().isNonnull()) ? muonsColl[ipos].innerTrack()->hitPattern().stripLayersWithMeasurement()+muonsColl[ipos].innerTrack()->hitPattern().pixelLayersWithMeasurement() : 0);
	    jet_pfcand_muon_nhits.push_back((muonsColl[ipos].innerTrack().isNonnull()) ? muonsColl[ipos].innerTrack()->hitPattern().numberOfValidStripHits()+muonsColl[ipos].innerTrack()->hitPattern().numberOfValidPixelHits() : 0);
	    jet_pfcand_muon_validfraction.push_back((muonsColl[ipos].innerTrack().isNonnull()) ? muonsColl[ipos].innerTrack()->validFraction() : 0);
	    jet_pfcand_muon_dxy.push_back((muonsColl[ipos].muonBestTrack().isNonnull()) ? muonsColl[ipos].muonBestTrack()->dxy(primaryVerticesH->front().position()) : 0);
	    jet_pfcand_muon_dz.push_back((muonsColl[ipos].muonBestTrack().isNonnull()) ? muonsColl[ipos].muonBestTrack()->dz(primaryVerticesH->front().position()) : 0);
	    jet_pfcand_muon_pterr.push_back((muonsColl[ipos].tunePMuonBestTrack().isNonnull()) ? muonsColl[ipos].tunePMuonBestTrack()->ptError()/muonsColl[ipos].tunePMuonBestTrack()->pt() : 0);
	    jet_pfcand_muon_ijet.push_back(ijet);
	  }
	}
	
	// electron specific
	if(abs(pfcand.pdgId()) == 11){	
	  int ipos    = -1;
	  for (size_t iel = 0; iel < electronsColl.size(); iel++) {
	    if(std::find(electronsToSkip.begin(),electronsToSkip.end(),iel) != electronsToSkip.end()) continue;
	    if(electronsColl[iel].isPF()){
	      for(const auto & element : electronsColl[iel].associatedPackedPFCandidates()){
		if(abs(element->pdgId()) == 11 and element->p4() == pfcand.p4()){
		  ipos = iel;
		  break;
		}
		if(ipos != -1) break;
	      }
	    }
	  }	  
	  if(ipos >= 0){
	    electronsToSkip.push_back(ipos);
	    jet_pfcand_electron_pt.push_back(electronsColl[ipos].pt());
	    jet_pfcand_electron_eta.push_back(electronsColl[ipos].eta());
	    jet_pfcand_electron_phi.push_back(electronsColl[ipos].phi());
	    jet_pfcand_electron_energy.push_back(electronsColl[ipos].energy());
	    jet_pfcand_electron_dxy.push_back((electronsColl[ipos].gsfTrack().isNonnull()) ? electronsColl[ipos].gsfTrack()->dxy(primaryVerticesH->front().position()) : 0);
	    jet_pfcand_electron_dz.push_back((electronsColl[ipos].gsfTrack().isNonnull()) ? electronsColl[ipos].gsfTrack()->dz(primaryVerticesH->front().position()) : 0);
	    jet_pfcand_electron_eOverP.push_back(electronsColl[ipos].eSuperClusterOverP());
	    jet_pfcand_electron_detaIn.push_back(electronsColl[ipos].deltaEtaSuperClusterTrackAtVtx());
	    jet_pfcand_electron_dphiIn.push_back(electronsColl[ipos].deltaPhiSuperClusterTrackAtVtx());
	    jet_pfcand_electron_sigIetaIeta.push_back(electronsColl[ipos].full5x5_sigmaIetaIeta());
	    jet_pfcand_electron_r9.push_back(electronsColl[ipos].full5x5_r9());
	    jet_pfcand_electron_hOverE.push_back(electronsColl[ipos].hadronicOverEm());
	    jet_pfcand_electron_convProb.push_back(electronsColl[ipos].convVtxFitProb());
	    jet_pfcand_electron_ijet.push_back(ijet);
	  }
	}

	// track specific
	const reco::Track* track = pfcand.bestTrack();      	        
	if(track){ 
	  jet_pfcand_ch_dzsig.push_back(pfcand.dz(primaryVerticesH->front().position())/pfcand.dzError());
	  jet_pfcand_ch_dxysig.push_back(pfcand.dxy(primaryVerticesH->front().position())/pfcand.dxyError());	
	  jet_pfcand_ch_track_chi2.push_back(track->normalizedChi2());
	  jet_pfcand_ch_track_qual.push_back(track->qualityMask());
	  jet_pfcand_ch_track_pterr.push_back(track->ptError()/track->pt());
	  jet_pfcand_ch_track_etaerr.push_back(track->etaError());
	  jet_pfcand_ch_track_phierr.push_back(track->phiError());
	  jet_pfcand_ch_nhits.push_back(track->hitPattern().numberOfValidHits());
	  jet_pfcand_ch_npixhits.push_back(track->hitPattern().numberOfValidPixelHits());
	  jet_pfcand_ch_npixbarrelhits.push_back(track->hitPattern().numberOfValidPixelBarrelHits());
	  jet_pfcand_ch_nstriphits.push_back(track->hitPattern().numberOfValidStripHits());
	  jet_pfcand_ch_nstriptibhits.push_back(track->hitPattern().numberOfValidStripTIBHits()+track->hitPattern().numberOfValidStripTIDHits());
	  jet_pfcand_ch_nstriptobhits.push_back(track->hitPattern().numberOfValidStripTOBHits());
	  jet_pfcand_ch_nlayers.push_back(track->hitPattern().trackerLayersWithMeasurement());
	  jet_pfcand_ch_npixlayers.push_back(track->hitPattern().pixelLayersWithMeasurement());
	  jet_pfcand_ch_nstriplayers.push_back(track->hitPattern().stripLayersWithMeasurement());
	  reco::TransientTrack transientTrack = trackBuilderH->build(*track);
	  Measurement1D meas_ip2d    = IPTools::signedTransverseImpactParameter(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	  Measurement1D meas_ip3d    = IPTools::signedImpactParameter3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	  Measurement1D meas_jetdist = IPTools::jetTrackDistance(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	  Measurement1D meas_decayl  = IPTools::signedDecayLength3D(transientTrack,jet_global_vec,primaryVerticesH->front()).second;
	  jet_pfcand_ch_trackjet_sip2d.push_back(meas_ip2d.value());
	  jet_pfcand_ch_trackjet_sip2dsig.push_back(meas_ip2d.significance());
	  jet_pfcand_ch_trackjet_sip3d.push_back(meas_ip3d.value());
	  jet_pfcand_ch_trackjet_sip3dsig.push_back(meas_ip3d.significance());
	  jet_pfcand_ch_trackjet_dist.push_back(-meas_jetdist.value());
	  jet_pfcand_ch_trackjet_decayL.push_back(meas_decayl.value());
	}
	else{
	  jet_pfcand_ch_dzsig.push_back(0);
	  jet_pfcand_ch_dxysig.push_back(0);	
	  jet_pfcand_ch_track_chi2.push_back(0);
	  jet_pfcand_ch_track_qual.push_back(0);
	  jet_pfcand_ch_track_pterr.push_back(0.);
	  jet_pfcand_ch_track_etaerr.push_back(0.);
	  jet_pfcand_ch_track_phierr.push_back(0.);
	  jet_pfcand_ch_nhits.push_back(0.);
	  jet_pfcand_ch_npixhits.push_back(0.);
	  jet_pfcand_ch_npixbarrelhits.push_back(0.);
	  jet_pfcand_ch_nstriphits.push_back(0.);
	  jet_pfcand_ch_nstriptibhits.push_back(0.);
	  jet_pfcand_ch_nstriptobhits.push_back(0.);
	  jet_pfcand_ch_nlayers.push_back(0.);
	  jet_pfcand_ch_npixlayers.push_back(0.);
	  jet_pfcand_ch_nstriplayers.push_back(0.);
	  jet_pfcand_ch_trackjet_sip2d.push_back(0.);
	  jet_pfcand_ch_trackjet_sip2dsig.push_back(0.);
	  jet_pfcand_ch_trackjet_sip3d.push_back(0.);
	  jet_pfcand_ch_trackjet_sip3dsig.push_back(0.);
	  jet_pfcand_ch_trackjet_dist.push_back(0.);
	  jet_pfcand_ch_trackjet_decayL.push_back(0.);
	}      
      }
      // neutral candidates
      else{
	jet_pfcand_neu_pt.push_back(pfcand.pt());
	jet_pfcand_neu_eta.push_back(pfcand.eta());
	jet_pfcand_neu_phi.push_back(pfcand.phi());
	jet_pfcand_neu_mass.push_back(pfcand.mass());
	jet_pfcand_neu_energy.push_back(pfcand.energy());      
	jet_pfcand_neu_calofraction.push_back(pfcand.caloFraction());
	jet_pfcand_neu_hcalfraction.push_back(pfcand.hcalFraction());	
	jet_pfcand_neu_id.push_back(abs(pfcand.pdgId()));
	jet_pfcand_neu_ijet.push_back(ijet);
	jet_pfcand_neu_frompv.push_back(pfcand.fromPV());
	jet_pfcand_neu_dz.push_back(pfcand.dz(primaryVerticesH->front().position()));
	jet_pfcand_neu_dxy.push_back(pfcand.dxy(primaryVerticesH->front().position()));
	TVector3 pfcand_momentum (pfcand.momentum().x(),pfcand.momentum().y(),pfcand.momentum().z());
	jet_pfcand_neu_candjet_pperp.push_back(jet_direction.Perp(pfcand_momentum)/pfcand_momentum.Mag());
	jet_pfcand_neu_candjet_ppara.push_back(jet_direction.Dot(pfcand_momentum)/pfcand_momentum.Mag());
	jet_pfcand_neu_candjet_dphi.push_back(jet_direction.DeltaPhi(pfcand_momentum));
	jet_pfcand_neu_candjet_deta.push_back(jet_direction.Eta()-pfcand_momentum.Eta());
	jet_pfcand_neu_candjet_etarel.push_back(reco::btau::etaRel(jetDir,pfcand.momentum()));
	jet_pfcand_neu_candjet_ptrel.push_back(pfcand_momentum.Perp(jet_direction));
	jet_pfcand_neu_puppiw.push_back(pfcand.puppiWeight());
	// tau specific
	if(std::find(tau_pfcandidates.begin(),tau_pfcandidates.end(),pfcand.p4()) != tau_pfcandidates.end())
	  jet_pfcand_neu_tau_signal.push_back(1);
	else
	  jet_pfcand_neu_tau_signal.push_back(0);
	if(abs(pfcand.pdgId()) == 22){	
	  int ipos = -1;
	  for (size_t iph = 0; iph < photonsColl.size(); iph++) {
	    if(std::find(photonsToSkip.begin(),photonsToSkip.end(),iph) != photonsToSkip.end()) continue;
	    for(const auto & element : photonsColl[iph].associatedPackedPFCandidates()){
	      if(abs(element->pdgId()) == 22 and element->p4() == pfcand.p4()){
		ipos = iph;
		break;
	      }
	      if(ipos != -1) break;
	    }
	  }
	  if(ipos >= 0){
	    photonsToSkip.push_back(ipos);
	    jet_pfcand_photon_pt.push_back(photonsColl[ipos].pt());
	    jet_pfcand_photon_eta.push_back(photonsColl[ipos].eta());
	    jet_pfcand_photon_phi.push_back(photonsColl[ipos].phi());
	    jet_pfcand_photon_energy.push_back(photonsColl[ipos].energy());
	    jet_pfcand_photon_hOverE.push_back(photonsColl[ipos].hadronicOverEm());
	    jet_pfcand_photon_sigIetaIeta.push_back(photonsColl[ipos].full5x5_sigmaIetaIeta());
	    jet_pfcand_photon_r9.push_back(photonsColl[ipos].full5x5_r9());
	    jet_pfcand_photon_eVeto.push_back(photonsColl[ipos].passElectronVeto());
	    jet_pfcand_photon_ijet.push_back(ijet);
	  }
	}
      }
    }
  }
  
  tree->Fill();
}


void TrainingTreeMakerAK4::beginJob() {

  // Access the TFileService
  edm::Service<TFileService> fs; 
  // Create the TTree
  tree = fs->make<TTree>("tree","tree");

  // Branches
  tree->Branch("event", &event, "event/i");
  tree->Branch("run", &run, "run/i");
  tree->Branch("lumi", &lumi, "lumi/i");
  tree->Branch("flags", &flags, "flags/i");
  if(isMC){
    tree->Branch("xsec", &xsec, "xsec/F");
    tree->Branch("wgt" , &wgt , "wgt/F");
    tree->Branch("putrue", &putrue, "putrue/i");
  }  
  tree->Branch("rho", &rho, "rho/F");
  tree->Branch("met",&met,"met/F");
  tree->Branch("met_phi", &met_phi, "met_phi/F");
  tree->Branch("npv", &npv, "npv/i");
  tree->Branch("nsv", &nsv, "nsv/i");
  tree->Branch("nkaon", &nkaon, "nkaon/i");
  tree->Branch("nlambda", &nlambda, "nlambda/i");

  tree->Branch("trigger_hlt_path","std::vector<std::string>",&trigger_hlt_path);
  tree->Branch("trigger_hlt_pass","std::vector<unsigned int>",&trigger_hlt_pass);

  if(isMC and saveLHEObjects){
    tree->Branch("lhe_particle_pt", "std::vector<float>", &lhe_particle_pt);
    tree->Branch("lhe_particle_eta", "std::vector<float>", &lhe_particle_eta);
    tree->Branch("lhe_particle_phi", "std::vector<float>", &lhe_particle_phi);
    tree->Branch("lhe_particle_mass", "std::vector<float>", &lhe_particle_mass);
    tree->Branch("lhe_particle_id", "std::vector<int>", &lhe_particle_id);
    tree->Branch("lhe_particle_status", "std::vector<unsigned int>", &lhe_particle_status);
  }
  if(isMC){
    tree->Branch("gen_particle_pt","std::vector<float>", &gen_particle_pt);
    tree->Branch("gen_particle_eta","std::vector<float>", &gen_particle_eta);
    tree->Branch("gen_particle_phi","std::vector<float>", &gen_particle_phi);
    tree->Branch("gen_particle_mass","std::vector<float>", &gen_particle_mass);
    tree->Branch("gen_particle_id","std::vector<int>", &gen_particle_id);
    tree->Branch("gen_particle_status","std::vector<unsigned int>", &gen_particle_status);
    tree->Branch("gen_particle_daughters_id","std::vector<int>", &gen_particle_daughters_id);
    tree->Branch("gen_particle_daughters_igen","std::vector<unsigned int>", &gen_particle_daughters_igen);
    tree->Branch("gen_particle_daughters_pt","std::vector<float>", &gen_particle_daughters_pt);
    tree->Branch("gen_particle_daughters_eta","std::vector<float>", &gen_particle_daughters_eta);
    tree->Branch("gen_particle_daughters_phi","std::vector<float>", &gen_particle_daughters_phi);
    tree->Branch("gen_particle_daughters_mass","std::vector<float>", &gen_particle_daughters_mass);
    tree->Branch("gen_particle_daughters_status","std::vector<unsigned int>", &gen_particle_daughters_status);
    tree->Branch("gen_particle_daughters_charge","std::vector<int>", &gen_particle_daughters_charge);
  }
  

  tree->Branch("muon_pt", "std::vector<float>", &muon_pt);
  tree->Branch("muon_eta", "std::vector<float>", &muon_eta);
  tree->Branch("muon_phi", "std::vector<float>", &muon_phi);
  tree->Branch("muon_energy", "std::vector<float>", &muon_energy);
  tree->Branch("muon_id", "std::vector<unsigned int>" , &muon_id);
  tree->Branch("muon_iso", "std::vector<unsigned int>" , &muon_iso);
  tree->Branch("muon_charge", "std::vector<int>" , &muon_charge);

  tree->Branch("electron_pt", "std::vector<float>", &electron_pt);
  tree->Branch("electron_pt_corr", "std::vector<float>", &electron_pt_corr);
  tree->Branch("electron_eta", "std::vector<float>", &electron_eta);
  tree->Branch("electron_phi", "std::vector<float>", &electron_phi);
  tree->Branch("electron_energy", "std::vector<float>", &electron_energy);
  tree->Branch("electron_id", "std::vector<unsigned int>" , &electron_id);
  tree->Branch("electron_idscore", "std::vector<float>" , &electron_idscore);
  tree->Branch("electron_charge", "std::vector<int>" , &electron_charge);

  tree->Branch("photon_pt", "std::vector<float>", &photon_pt);
  tree->Branch("photon_pt_corr", "std::vector<float>", &photon_pt_corr);
  tree->Branch("photon_eta", "std::vector<float>", &photon_eta);
  tree->Branch("photon_phi", "std::vector<float>", &photon_phi);
  tree->Branch("photon_energy", "std::vector<float>", &photon_energy);
  tree->Branch("photon_id", "std::vector<unsigned int>" , &photon_id);
  tree->Branch("photon_idscore", "std::vector<float>" , &photon_idscore);

  tree->Branch("tau_pt", "std::vector<float>" , &tau_pt);
  tree->Branch("tau_eta", "std::vector<float>" , &tau_eta);
  tree->Branch("tau_phi", "std::vector<float>" , &tau_phi);
  tree->Branch("tau_mass", "std::vector<float>" , &tau_mass);
  tree->Branch("tau_energy", "std::vector<float>" , &tau_energy);
  tree->Branch("tau_dxy", "std::vector<float>" , &tau_dxy);
  tree->Branch("tau_dz", "std::vector<float>" , &tau_dz);
  tree->Branch("tau_decaymode", "std::vector<unsigned int>" , &tau_decaymode);
  tree->Branch("tau_idjet_wp", "std::vector<unsigned int>" , &tau_idjet_wp);
  tree->Branch("tau_idmu_wp", "std::vector<unsigned int>" , &tau_idmu_wp);
  tree->Branch("tau_idele_wp", "std::vector<unsigned int>" , &tau_idele_wp);
  tree->Branch("tau_idjet", "std::vector<float>" , &tau_idjet);
  tree->Branch("tau_idele", "std::vector<float>" , &tau_idele);
  tree->Branch("tau_idmu", "std::vector<float>" , &tau_idmu);
  tree->Branch("tau_charge", "std::vector<int>" , &tau_charge);
  if(isMC){
    tree->Branch("tau_genmatch_pt", "std::vector<float>" , &tau_genmatch_pt);
    tree->Branch("tau_genmatch_eta", "std::vector<float>" , &tau_genmatch_eta);
    tree->Branch("tau_genmatch_phi", "std::vector<float>" , &tau_genmatch_phi);
    tree->Branch("tau_genmatch_mass", "std::vector<float>" , &tau_genmatch_mass);
    tree->Branch("tau_genmatch_decaymode", "std::vector<int>" , &tau_genmatch_decaymode);
  }

  if(not leptonPairsTag.label().empty()){
    tree->Branch("leppair_pt", "std::vector<float>" , &leppair_pt);
    tree->Branch("leppair_eta", "std::vector<float>" , &leppair_eta);
    tree->Branch("leppair_phi", "std::vector<float>" , &leppair_phi);
    tree->Branch("leppair_mass", "std::vector<float>" , &leppair_mass);
    tree->Branch("leppair_pt1", "std::vector<float>" , &leppair_pt1);
    tree->Branch("leppair_eta1", "std::vector<float>" , &leppair_eta1);
    tree->Branch("leppair_phi1", "std::vector<float>" , &leppair_phi1);
    tree->Branch("leppair_mass1", "std::vector<float>" , &leppair_mass1);
    tree->Branch("leppair_pdg1", "std::vector<float>" , &leppair_pdg1);
    tree->Branch("leppair_pt2", "std::vector<float>" , &leppair_pt2);
    tree->Branch("leppair_eta2", "std::vector<float>" , &leppair_eta2);
    tree->Branch("leppair_phi2", "std::vector<float>" , &leppair_phi2);
    tree->Branch("leppair_mass2", "std::vector<float>" , &leppair_mass2);
    tree->Branch("leppair_pdg2", "std::vector<float>" , &leppair_pdg2);
  }

  tree->Branch("jet_pt", "std::vector<float>" , &jet_pt);
  tree->Branch("jet_eta", "std::vector<float>" , &jet_eta);
  tree->Branch("jet_phi", "std::vector<float>" , &jet_phi);
  tree->Branch("jet_mass", "std::vector<float>" , &jet_mass);
  tree->Branch("jet_pt_raw", "std::vector<float>" , &jet_pt_raw);
  tree->Branch("jet_mass_raw", "std::vector<float>" , &jet_mass_raw);
  tree->Branch("jet_chf", "std::vector<float>" , &jet_chf);
  tree->Branch("jet_nhf", "std::vector<float>" , &jet_nhf);
  tree->Branch("jet_elf", "std::vector<float>" , &jet_elf);
  tree->Branch("jet_phf", "std::vector<float>" , &jet_phf);
  tree->Branch("jet_muf", "std::vector<float>" , &jet_muf);
  tree->Branch("jet_deepjet_probb", "std::vector<float>" , &jet_deepjet_probb);
  tree->Branch("jet_deepjet_probbb", "std::vector<float>" , &jet_deepjet_probbb);
  tree->Branch("jet_deepjet_probc", "std::vector<float>" , &jet_deepjet_probc);
  tree->Branch("jet_deepjet_problepb", "std::vector<float>" , &jet_deepjet_problepb);
  tree->Branch("jet_deepjet_probg", "std::vector<float>" , &jet_deepjet_probg);
  tree->Branch("jet_deepjet_probuds", "std::vector<float>" , &jet_deepjet_probuds);
  tree->Branch("jet_pnet_probb", "std::vector<float>" , &jet_pnet_probb);
  tree->Branch("jet_pnet_probbb", "std::vector<float>" , &jet_pnet_probbb);
  tree->Branch("jet_pnet_probc", "std::vector<float>" , &jet_pnet_probc);
  tree->Branch("jet_pnet_probcc", "std::vector<float>" , &jet_pnet_probcc);
  tree->Branch("jet_pnet_probuds", "std::vector<float>" , &jet_pnet_probuds);
  tree->Branch("jet_pnet_probg", "std::vector<float>" , &jet_pnet_probg);
  tree->Branch("jet_pnet_probpu", "std::vector<float>" , &jet_pnet_probpu);
  tree->Branch("jet_pnet_probundef", "std::vector<float>" , &jet_pnet_probundef);
    
  for(const auto & label : pnetDiscriminatorLabels){
    jet_pnetlast_score[label] = std::vector<float>();
    tree->Branch(("jet_pnetlast_"+label).c_str(),"std::vector<float>", &jet_pnetlast_score[label]);
  }

  for(const auto & label : parTDiscriminatorLabels){
    jet_parTlast_score[label] = std::vector<float>();
    tree->Branch(("jet_parTlast_"+label).c_str(),"std::vector<float>", &jet_parTlast_score[label]);
  }

  tree->Branch("jet_id", "std::vector<unsigned int>" , &jet_id);
  tree->Branch("jet_puid", "std::vector<unsigned int>" , &jet_puid);
  tree->Branch("jet_ncand", "std::vector<unsigned int>" , &jet_ncand);
  tree->Branch("jet_nch", "std::vector<unsigned int>" , &jet_nch);
  tree->Branch("jet_nnh", "std::vector<unsigned int>" , &jet_nnh);
  tree->Branch("jet_nel", "std::vector<unsigned int>" , &jet_nel);
  tree->Branch("jet_nph", "std::vector<unsigned int>" , &jet_nph);
  tree->Branch("jet_nmu", "std::vector<unsigned int>" , &jet_nmu);
  tree->Branch("jet_hflav", "std::vector<unsigned int>" , &jet_hflav);
  tree->Branch("jet_pflav", "std::vector<int>" , &jet_pflav);
  tree->Branch("jet_nbhad", "std::vector<unsigned int>" , &jet_nbhad);
  tree->Branch("jet_nchad", "std::vector<unsigned int>" , &jet_nchad);
  if(isMC){
    tree->Branch("jet_genmatch_pt","std::vector<float>" , &jet_genmatch_pt);
    tree->Branch("jet_genmatch_eta","std::vector<float>" , &jet_genmatch_eta);
    tree->Branch("jet_genmatch_phi","std::vector<float>" , &jet_genmatch_phi);
    tree->Branch("jet_genmatch_mass","std::vector<float>" , &jet_genmatch_mass);
    tree->Branch("jet_genmatch_hflav","std::vector<unsigned int>" , &jet_genmatch_hflav);
    tree->Branch("jet_genmatch_pflav","std::vector<int>" , &jet_genmatch_pflav);
    tree->Branch("jet_genmatch_nbhad","std::vector<unsigned int>" , &jet_genmatch_nbhad);
    tree->Branch("jet_genmatch_nchad","std::vector<unsigned int>" , &jet_genmatch_nchad);
    tree->Branch("jet_genmatch_wnu_pt","std::vector<float>" , &jet_genmatch_wnu_pt);
    tree->Branch("jet_genmatch_wnu_eta","std::vector<float>" , &jet_genmatch_wnu_eta);
    tree->Branch("jet_genmatch_wnu_phi","std::vector<float>" , &jet_genmatch_wnu_phi);
    tree->Branch("jet_genmatch_wnu_mass","std::vector<float>" , &jet_genmatch_wnu_mass);
  }
  
  tree->Branch("jet_sv_pt", "std::vector<float>" , &jet_sv_pt);
  tree->Branch("jet_sv_eta", "std::vector<float>" , &jet_sv_eta);
  tree->Branch("jet_sv_phi", "std::vector<float>" , &jet_sv_phi);
  tree->Branch("jet_sv_mass", "std::vector<float>" , &jet_sv_mass);
  tree->Branch("jet_sv_energy", "std::vector<float>" , &jet_sv_energy);
  tree->Branch("jet_sv_chi2", "std::vector<float>" , &jet_sv_chi2);
  tree->Branch("jet_sv_dxy", "std::vector<float>" , &jet_sv_dxy);
  tree->Branch("jet_sv_dxysig", "std::vector<float>" , &jet_sv_dxysig);
  tree->Branch("jet_sv_d3d", "std::vector<float>" , &jet_sv_d3d);
  tree->Branch("jet_sv_d3dsig", "std::vector<float>" , &jet_sv_d3dsig);
  tree->Branch("jet_sv_dxy_sign", "std::vector<int>" , &jet_sv_dxy_sign);
  tree->Branch("jet_sv_d3d_sign", "std::vector<int>" , &jet_sv_d3d_sign);
  tree->Branch("jet_sv_costheta", "std::vector<float>" , &jet_sv_costheta);
  tree->Branch("jet_sv_ntrack", "std::vector<unsigned int>" , &jet_sv_ntrack);
  tree->Branch("jet_sv_ijet", "std::vector<unsigned int>" , &jet_sv_ijet);

  tree->Branch("jet_kaon_pt", "std::vector<float>" , &jet_kaon_pt);
  tree->Branch("jet_kaon_eta", "std::vector<float>" , &jet_kaon_eta);
  tree->Branch("jet_kaon_phi", "std::vector<float>" , &jet_kaon_phi);
  tree->Branch("jet_kaon_mass", "std::vector<float>" , &jet_kaon_mass);
  tree->Branch("jet_kaon_energy", "std::vector<float>" , &jet_kaon_energy);
  tree->Branch("jet_kaon_chi2", "std::vector<float>" , &jet_kaon_chi2);
  tree->Branch("jet_kaon_dxy", "std::vector<float>" , &jet_kaon_dxy);
  tree->Branch("jet_kaon_dxysig", "std::vector<float>" , &jet_kaon_dxysig);
  tree->Branch("jet_kaon_d3d", "std::vector<float>" , &jet_kaon_d3d);
  tree->Branch("jet_kaon_d3dsig", "std::vector<float>" , &jet_kaon_d3dsig);
  tree->Branch("jet_kaon_dxy_sign", "std::vector<int>" , &jet_kaon_dxy_sign);
  tree->Branch("jet_kaon_d3d_sign", "std::vector<int>" , &jet_kaon_d3d_sign);
  tree->Branch("jet_kaon_costheta", "std::vector<float>" , &jet_kaon_costheta);
  tree->Branch("jet_kaon_ntrack", "std::vector<unsigned int>" , &jet_kaon_ntrack);
  tree->Branch("jet_kaon_ijet", "std::vector<unsigned int>" , &jet_kaon_ijet);

  tree->Branch("jet_lambda_pt", "std::vector<float>" , &jet_lambda_pt);
  tree->Branch("jet_lambda_eta", "std::vector<float>" , &jet_lambda_eta);
  tree->Branch("jet_lambda_phi", "std::vector<float>" , &jet_lambda_phi);
  tree->Branch("jet_lambda_mass", "std::vector<float>" , &jet_lambda_mass);
  tree->Branch("jet_lambda_energy", "std::vector<float>" , &jet_lambda_energy);
  tree->Branch("jet_lambda_chi2", "std::vector<float>" , &jet_lambda_chi2);
  tree->Branch("jet_lambda_dxy", "std::vector<float>" , &jet_lambda_dxy);
  tree->Branch("jet_lambda_dxysig", "std::vector<float>" , &jet_lambda_dxysig);
  tree->Branch("jet_lambda_d3d", "std::vector<float>" , &jet_lambda_d3d);
  tree->Branch("jet_lambda_d3dsig", "std::vector<float>" , &jet_lambda_d3dsig);
  tree->Branch("jet_lambda_dxy_sign", "std::vector<int>" , &jet_lambda_dxy_sign);
  tree->Branch("jet_lambda_d3d_sign", "std::vector<int>" , &jet_lambda_d3d_sign);
  tree->Branch("jet_lambda_costheta", "std::vector<float>" , &jet_lambda_costheta);
  tree->Branch("jet_lambda_ntrack", "std::vector<unsigned int>" , &jet_lambda_ntrack);
  tree->Branch("jet_lambda_ijet", "std::vector<unsigned int>" , &jet_lambda_ijet);

  tree->Branch("jet_pfcand_ch_pt","std::vector<float>",&jet_pfcand_ch_pt);
  tree->Branch("jet_pfcand_ch_eta","std::vector<float>",&jet_pfcand_ch_eta);
  tree->Branch("jet_pfcand_ch_phi","std::vector<float>",&jet_pfcand_ch_phi);
  tree->Branch("jet_pfcand_ch_mass","std::vector<float>",&jet_pfcand_ch_mass);
  tree->Branch("jet_pfcand_ch_energy","std::vector<float>",&jet_pfcand_ch_energy);
  tree->Branch("jet_pfcand_ch_calofraction","std::vector<float>",&jet_pfcand_ch_calofraction);
  tree->Branch("jet_pfcand_ch_hcalfraction","std::vector<float>",&jet_pfcand_ch_hcalfraction);
  tree->Branch("jet_pfcand_ch_dz","std::vector<float>",&jet_pfcand_ch_dz);
  tree->Branch("jet_pfcand_ch_dzsig","std::vector<float>",&jet_pfcand_ch_dzsig);
  tree->Branch("jet_pfcand_ch_dxy","std::vector<float>",&jet_pfcand_ch_dxy);
  tree->Branch("jet_pfcand_ch_dxysig","std::vector<float>",&jet_pfcand_ch_dxysig);
  tree->Branch("jet_pfcand_ch_puppiw","std::vector<float>",&jet_pfcand_ch_puppiw);
  tree->Branch("jet_pfcand_ch_frompv","std::vector<unsigned int>",&jet_pfcand_ch_frompv);
  tree->Branch("jet_pfcand_ch_id","std::vector<unsigned int>",&jet_pfcand_ch_id);
  tree->Branch("jet_pfcand_ch_charge","std::vector<int>",&jet_pfcand_ch_charge);
  tree->Branch("jet_pfcand_ch_ijet","std::vector<unsigned int>",&jet_pfcand_ch_ijet);
  tree->Branch("jet_pfcand_ch_candjet_pperp","std::vector<float>",&jet_pfcand_ch_candjet_pperp);
  tree->Branch("jet_pfcand_ch_candjet_ppara","std::vector<float>",&jet_pfcand_ch_candjet_ppara);
  tree->Branch("jet_pfcand_ch_candjet_deta","std::vector<float>",&jet_pfcand_ch_candjet_deta);
  tree->Branch("jet_pfcand_ch_candjet_dphi","std::vector<float>",&jet_pfcand_ch_candjet_dphi);
  tree->Branch("jet_pfcand_ch_candjet_ptrel","std::vector<float>",&jet_pfcand_ch_candjet_ptrel);
  tree->Branch("jet_pfcand_ch_candjet_etarel","std::vector<float>",&jet_pfcand_ch_candjet_etarel);
  tree->Branch("jet_pfcand_ch_track_chi2","std::vector<unsigned int>",&jet_pfcand_ch_track_chi2);
  tree->Branch("jet_pfcand_ch_track_qual","std::vector<unsigned int>",&jet_pfcand_ch_track_qual);
  tree->Branch("jet_pfcand_ch_track_pterr","std::vector<float>",&jet_pfcand_ch_track_pterr);
  tree->Branch("jet_pfcand_ch_track_etaerr","std::vector<float>",&jet_pfcand_ch_track_etaerr);
  tree->Branch("jet_pfcand_ch_track_phierr","std::vector<float>",&jet_pfcand_ch_track_phierr);
  tree->Branch("jet_pfcand_ch_trackjet_sip2d","std::vector<float>",&jet_pfcand_ch_trackjet_sip2d);
  tree->Branch("jet_pfcand_ch_trackjet_sip2dsig","std::vector<float>",&jet_pfcand_ch_trackjet_sip2dsig);
  tree->Branch("jet_pfcand_ch_trackjet_sip3d","std::vector<float>",&jet_pfcand_ch_trackjet_sip3d);
  tree->Branch("jet_pfcand_ch_trackjet_sip3dsig","std::vector<float>",&jet_pfcand_ch_trackjet_sip3dsig);
  tree->Branch("jet_pfcand_ch_trackjet_dist","std::vector<float>",&jet_pfcand_ch_trackjet_dist);
  tree->Branch("jet_pfcand_ch_trackjet_decayL","std::vector<float>",&jet_pfcand_ch_trackjet_decayL);
  tree->Branch("jet_pfcand_ch_nhits","std::vector<unsigned int>",&jet_pfcand_ch_nhits);
  tree->Branch("jet_pfcand_ch_npixhits","std::vector<unsigned int>",&jet_pfcand_ch_npixhits);
  tree->Branch("jet_pfcand_ch_npixbarrelhits","std::vector<unsigned int>",&jet_pfcand_ch_npixbarrelhits);
  tree->Branch("jet_pfcand_ch_nstriphits","std::vector<unsigned int>",&jet_pfcand_ch_nstriphits);
  tree->Branch("jet_pfcand_ch_nstriptibhits","std::vector<unsigned int>",&jet_pfcand_ch_nstriptibhits);
  tree->Branch("jet_pfcand_ch_nstriptobhits","std::vector<unsigned int>",&jet_pfcand_ch_nstriptobhits);
  tree->Branch("jet_pfcand_ch_nlosthits","std::vector<int>",&jet_pfcand_ch_nlosthits);
  tree->Branch("jet_pfcand_ch_nlayers","std::vector<unsigned int>",&jet_pfcand_ch_nlayers);
  tree->Branch("jet_pfcand_ch_npixlayers","std::vector<unsigned int>",&jet_pfcand_ch_npixlayers);
  tree->Branch("jet_pfcand_ch_nstriplayers","std::vector<unsigned int>",&jet_pfcand_ch_nstriplayers);
  tree->Branch("jet_pfcand_ch_tau_signal",",std::vector<unsigned int>",&jet_pfcand_ch_tau_signal);

  tree->Branch("jet_pfcand_neu_pt","std::vector<float>",&jet_pfcand_neu_pt);
  tree->Branch("jet_pfcand_neu_eta","std::vector<float>",&jet_pfcand_neu_eta);
  tree->Branch("jet_pfcand_neu_phi","std::vector<float>",&jet_pfcand_neu_phi);
  tree->Branch("jet_pfcand_neu_mass","std::vector<float>",&jet_pfcand_neu_mass);
  tree->Branch("jet_pfcand_neu_energy","std::vector<float>",&jet_pfcand_neu_energy);
  tree->Branch("jet_pfcand_neu_calofraction","std::vector<float>",&jet_pfcand_neu_calofraction);
  tree->Branch("jet_pfcand_neu_hcalfraction","std::vector<float>",&jet_pfcand_neu_hcalfraction);
  tree->Branch("jet_pfcand_neu_dz","std::vector<float>",&jet_pfcand_neu_dz);
  tree->Branch("jet_pfcand_neu_dxy","std::vector<float>",&jet_pfcand_neu_dxy);
  tree->Branch("jet_pfcand_neu_puppiw","std::vector<float>",&jet_pfcand_neu_puppiw);
  tree->Branch("jet_pfcand_neu_frompv","std::vector<unsigned int>",&jet_pfcand_neu_frompv);
  tree->Branch("jet_pfcand_neu_id","std::vector<unsigned int>",&jet_pfcand_neu_id);
  tree->Branch("jet_pfcand_neu_ijet","std::vector<unsigned int>",&jet_pfcand_neu_ijet);
  tree->Branch("jet_pfcand_neu_candjet_pperp","std::vector<float>",&jet_pfcand_neu_candjet_pperp);
  tree->Branch("jet_pfcand_neu_candjet_ppara","std::vector<float>",&jet_pfcand_neu_candjet_ppara);
  tree->Branch("jet_pfcand_neu_candjet_deta","std::vector<float>",&jet_pfcand_neu_candjet_deta);
  tree->Branch("jet_pfcand_neu_candjet_dphi","std::vector<float>",&jet_pfcand_neu_candjet_dphi);
  tree->Branch("jet_pfcand_neu_candjet_ptrel","std::vector<float>",&jet_pfcand_neu_candjet_ptrel);
  tree->Branch("jet_pfcand_neu_candjet_etarel","std::vector<float>",&jet_pfcand_neu_candjet_etarel);
  tree->Branch("jet_pfcand_neu_tau_signal",",std::vector<unsigned int>",&jet_pfcand_neu_tau_signal);

  tree->Branch("jet_pfcand_muon_pt","std::vector<float>",&jet_pfcand_muon_pt);
  tree->Branch("jet_pfcand_muon_eta","std::vector<float>",&jet_pfcand_muon_eta);
  tree->Branch("jet_pfcand_muon_phi","std::vector<float>",&jet_pfcand_muon_phi);
  tree->Branch("jet_pfcand_muon_energy","std::vector<float>",&jet_pfcand_muon_energy);
  tree->Branch("jet_pfcand_muon_dxy","std::vector<float>",&jet_pfcand_muon_dxy);
  tree->Branch("jet_pfcand_muon_dz","std::vector<float>",&jet_pfcand_muon_dz);
  tree->Branch("jet_pfcand_muon_type","std::vector<unsigned int>",&jet_pfcand_muon_type);
  tree->Branch("jet_pfcand_muon_chi2","std::vector<float>",&jet_pfcand_muon_chi2);
  tree->Branch("jet_pfcand_muon_nvalidhits","std::vector<unsigned int>",&jet_pfcand_muon_nvalidhits);
  tree->Branch("jet_pfcand_muon_nstations","std::vector<unsigned int>",&jet_pfcand_muon_nstations);
  tree->Branch("jet_pfcand_muon_nhits","std::vector<unsigned int>",&jet_pfcand_muon_nhits);
  tree->Branch("jet_pfcand_muon_nlayers","std::vector<unsigned int>",&jet_pfcand_muon_nlayers);
  tree->Branch("jet_pfcand_muon_segcomp","std::vector<float>",&jet_pfcand_muon_segcomp);
  tree->Branch("jet_pfcand_muon_pterr","std::vector<float>",&jet_pfcand_muon_pterr);
  tree->Branch("jet_pfcand_muon_validfraction","std::vector<float>",&jet_pfcand_muon_validfraction);
  tree->Branch("jet_pfcand_muon_trkKink","std::vector<float>",&jet_pfcand_muon_trkKink);
  tree->Branch("jet_pfcand_muon_ijet","std::vector<unsigned int>",&jet_pfcand_muon_ijet);
  
  tree->Branch("jet_pfcand_electron_pt","std::vector<float>",&jet_pfcand_electron_pt);
  tree->Branch("jet_pfcand_electron_eta","std::vector<float>",&jet_pfcand_electron_eta);
  tree->Branch("jet_pfcand_electron_phi","std::vector<float>",&jet_pfcand_electron_phi);
  tree->Branch("jet_pfcand_electron_energy","std::vector<float>",&jet_pfcand_electron_energy);
  tree->Branch("jet_pfcand_electron_dxy","std::vector<float>",&jet_pfcand_electron_dxy);
  tree->Branch("jet_pfcand_electron_dz","std::vector<float>",&jet_pfcand_electron_dz);
  tree->Branch("jet_pfcand_electron_eOverP","std::vector<float>",&jet_pfcand_electron_eOverP);
  tree->Branch("jet_pfcand_electron_detaIn","std::vector<float>",&jet_pfcand_electron_detaIn);
  tree->Branch("jet_pfcand_electron_dphiIn","std::vector<float>",&jet_pfcand_electron_dphiIn);
  tree->Branch("jet_pfcand_electron_r9","std::vector<float>",&jet_pfcand_electron_r9);
  tree->Branch("jet_pfcand_electron_sigIetaIeta","std::vector<float>",&jet_pfcand_electron_sigIetaIeta);
  tree->Branch("jet_pfcand_electron_hOverE","std::vector<float>",&jet_pfcand_electron_hOverE);
  tree->Branch("jet_pfcand_electron_convProb","std::vector<float>",&jet_pfcand_electron_convProb);
  tree->Branch("jet_pfcand_electron_ijet","std::vector<unsigned int>",&jet_pfcand_electron_ijet);

  tree->Branch("jet_pfcand_photon_pt","std::vector<float>",&jet_pfcand_photon_pt);
  tree->Branch("jet_pfcand_photon_eta","std::vector<float>",&jet_pfcand_photon_eta);
  tree->Branch("jet_pfcand_photon_phi","std::vector<float>",&jet_pfcand_photon_phi);
  tree->Branch("jet_pfcand_photon_energy","std::vector<float>",&jet_pfcand_photon_energy);
  tree->Branch("jet_pfcand_photon_hOverE","std::vector<float>",&jet_pfcand_photon_hOverE);
  tree->Branch("jet_pfcand_photon_sigIetaIeta","std::vector<float>",&jet_pfcand_photon_sigIetaIeta);
  tree->Branch("jet_pfcand_photon_eVeto","std::vector<float>",&jet_pfcand_photon_eVeto);
  tree->Branch("jet_pfcand_photon_r9","std::vector<float>",&jet_pfcand_photon_r9);
  tree->Branch("jet_pfcand_photon_ijet","std::vector<unsigned int>",&jet_pfcand_photon_ijet);

  tree->Branch("jet_losttrack_pt","std::vector<float>",&jet_losttrack_pt);
  tree->Branch("jet_losttrack_eta","std::vector<float>",&jet_losttrack_eta);
  tree->Branch("jet_losttrack_phi","std::vector<float>",&jet_losttrack_phi);
  tree->Branch("jet_losttrack_mass","std::vector<float>",&jet_losttrack_mass);
  tree->Branch("jet_losttrack_energy","std::vector<float>",&jet_losttrack_energy);
  tree->Branch("jet_losttrack_dz","std::vector<float>",&jet_losttrack_dz);
  tree->Branch("jet_losttrack_dxy","std::vector<float>",&jet_losttrack_dxy);
  tree->Branch("jet_losttrack_dzsig","std::vector<float>",&jet_losttrack_dzsig);
  tree->Branch("jet_losttrack_dxysig","std::vector<float>",&jet_losttrack_dxysig);
  tree->Branch("jet_losttrack_frompv","std::vector<unsigned int>",&jet_losttrack_frompv);
  tree->Branch("jet_losttrack_charge","std::vector<int>",&jet_losttrack_charge);
  tree->Branch("jet_losttrack_ijet","std::vector<unsigned int>",&jet_losttrack_ijet);
  tree->Branch("jet_losttrack_candjet_pperp","std::vector<float>",&jet_losttrack_candjet_pperp);
  tree->Branch("jet_losttrack_candjet_ppara","std::vector<float>",&jet_losttrack_candjet_ppara);
  tree->Branch("jet_losttrack_candjet_deta","std::vector<float>",&jet_losttrack_candjet_deta);
  tree->Branch("jet_losttrack_candjet_dphi","std::vector<float>",&jet_losttrack_candjet_dphi);
  tree->Branch("jet_losttrack_candjet_ptrel","std::vector<float>",&jet_losttrack_candjet_ptrel);
  tree->Branch("jet_losttrack_candjet_etarel","std::vector<float>",&jet_losttrack_candjet_etarel);
  tree->Branch("jet_losttrack_trackjet_sip2d","std::vector<float>",&jet_losttrack_trackjet_sip2d);
  tree->Branch("jet_losttrack_trackjet_sip2dsig","std::vector<float>",&jet_losttrack_trackjet_sip2dsig);
  tree->Branch("jet_losttrack_trackjet_sip3d","std::vector<float>",&jet_losttrack_trackjet_sip3d);
  tree->Branch("jet_losttrack_trackjet_sip3dsig","std::vector<float>",&jet_losttrack_trackjet_sip3dsig);
  tree->Branch("jet_losttrack_trackjet_dist","std::vector<float>",&jet_losttrack_trackjet_dist);
  tree->Branch("jet_losttrack_trackjet_decayL","std::vector<float>",&jet_losttrack_trackjet_decayL);
  tree->Branch("jet_losttrack_track_chi2","std::vector<unsigned int>",&jet_losttrack_track_chi2);
  tree->Branch("jet_losttrack_track_pterr","std::vector<float>",&jet_losttrack_track_pterr);
  tree->Branch("jet_losttrack_track_etaerr","std::vector<float>",&jet_losttrack_track_etaerr);
  tree->Branch("jet_losttrack_track_phierr","std::vector<float>",&jet_losttrack_track_phierr);
  tree->Branch("jet_losttrack_track_qual","std::vector<unsigned int>",&jet_losttrack_track_qual);
  tree->Branch("jet_losttrack_nhits","std::vector<unsigned int>",&jet_losttrack_nhits);
  tree->Branch("jet_losttrack_npixhits","std::vector<unsigned int>",&jet_losttrack_npixhits); 
  tree->Branch("jet_losttrack_npixbarrelhits","std::vector<unsigned int>",&jet_losttrack_npixbarrelhits);
  tree->Branch("jet_losttrack_nstriphits","std::vector<unsigned int>",&jet_losttrack_nstriphits);
  tree->Branch("jet_losttrack_nstriptibhits","std::vector<unsigned int>",&jet_losttrack_nstriptibhits);
  tree->Branch("jet_losttrack_nstriptobhits","std::vector<unsigned int>",&jet_losttrack_nstriptobhits);
  tree->Branch("jet_losttrack_nlosthits","std::vector<int>",&jet_losttrack_nlosthits);
  tree->Branch("jet_losttrack_nlayers","std::vector<unsigned int>",&jet_losttrack_nlayers);
  tree->Branch("jet_losttrack_npixlayers","std::vector<unsigned int>",&jet_losttrack_npixlayers);
  tree->Branch("jet_losttrack_nstriplayers","std::vector<unsigned int>",&jet_losttrack_nstriplayers);
}

void TrainingTreeMakerAK4::endJob() {}

void TrainingTreeMakerAK4::beginRun(edm::Run const& iRun, edm::EventSetup const& iSetup) {

  HLTConfigProvider fltrConfig;
  bool flag = false;
  fltrConfig.init(iRun, iSetup, filterResultsTag.process(), flag);
  
  // MET filter Paths
  filterPathsMap.clear();
  filterPathsVector.clear();
  filterPathsVector.push_back("Flag_goodVertices");
  filterPathsVector.push_back("Flag_globalSuperTightHalo2016Filter");
  filterPathsVector.push_back("Flag_HBHENoiseFilter");
  filterPathsVector.push_back("Flag_HBHENoiseIsoFilter");
  filterPathsVector.push_back("Flag_EcalDeadCellTriggerPrimitiveFilter");
  filterPathsVector.push_back("Flag_BadPFMuonFilter");
  filterPathsVector.push_back("Flag_BadChargedCandidateFilter");
  
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    filterPathsMap[filterPathsVector[i]] = -1;
  }
  
  for(size_t i = 0; i < filterPathsVector.size(); i++){
    TPRegexp pattern(filterPathsVector[i]);
    for(size_t j = 0; j < fltrConfig.triggerNames().size(); j++){
      std::string pathName = fltrConfig.triggerNames()[j];
      if(TString(pathName).Contains(pattern)){
	filterPathsMap[filterPathsVector[i]] = j;
      }
    }
  }

  // trigger names
  triggerPathsMap.clear();
  HLTConfigProvider hltConfig;
  hltConfig.init(iRun, iSetup, triggerResultsTag.process(), flag);
  for(size_t i = 0; i < hltConfig.triggerNames().size(); i++){
    std::string pathName = hltConfig.triggerNames()[i];
    if(not (TString(pathName).Contains("IsoMu24_v") or 
	    (TString(pathName).Contains("Ele30") and TString(pathName).Contains("WPTight_Gsf_v")) or
	    (TString(pathName).Contains("Ele32") and TString(pathName).Contains("WPTight_Gsf_v")) or
	    (TString(pathName).Contains("Ele35") and TString(pathName).Contains("WPTight_Gsf_v")) or
	    (TString(pathName).Contains("Ele23_Ele12")) or
	    (TString(pathName).Contains("HLT_ZeroBias_v")) or
	    (TString(pathName).Contains("HLT_ZeroBias_part")) or
	    (TString(pathName).Contains("HLT_PFJet40_v")) or
	    (TString(pathName).Contains("HLT_DiPFJetAve40_v"))
	    )) continue;
    triggerPathsMap[pathName] = i;
  }
}
 
void TrainingTreeMakerAK4::endRun(edm::Run const&, edm::EventSetup const&) {}

void TrainingTreeMakerAK4::initializeBranches(){

  event  = 0;
  run    = 0;
  lumi   = 0;
  putrue = 0;
  flags  = 0;
  wgt    = 0.;
  rho    = 0.;

  trigger_hlt_path.clear();
  trigger_hlt_pass.clear();
  
  gen_particle_pt.clear();
  gen_particle_eta.clear();
  gen_particle_phi.clear();
  gen_particle_mass.clear();
  gen_particle_id.clear();
  gen_particle_status.clear();
  gen_particle_daughters_id.clear();
  gen_particle_daughters_igen.clear();
  gen_particle_daughters_pt.clear();
  gen_particle_daughters_eta.clear();
  gen_particle_daughters_phi.clear();
  gen_particle_daughters_mass.clear();
  gen_particle_daughters_status.clear();
  gen_particle_daughters_charge.clear();

  lhe_particle_pt.clear();
  lhe_particle_eta.clear();
  lhe_particle_phi.clear();
  lhe_particle_mass.clear();
  lhe_particle_id.clear();
  lhe_particle_status.clear();

  muon_pt.clear();
  muon_eta.clear();
  muon_phi.clear();
  muon_energy.clear();
  muon_id.clear();
  muon_charge.clear();
  muon_iso.clear();

  electron_pt.clear();
  electron_pt_corr.clear();
  electron_eta.clear();
  electron_phi.clear();
  electron_energy.clear();
  electron_id.clear();
  electron_idscore.clear();
  electron_charge.clear();

  photon_pt.clear();
  photon_pt_corr.clear();
  photon_eta.clear();
  photon_phi.clear();
  photon_energy.clear();
  photon_id.clear();
  photon_idscore.clear();

  tau_pt.clear();
  tau_eta.clear();
  tau_phi.clear();
  tau_mass.clear();
  tau_energy.clear();
  tau_dxy.clear();
  tau_dz.clear();
  tau_decaymode.clear();
  tau_idjet.clear();
  tau_idele.clear();
  tau_idmu.clear();
  tau_idjet_wp.clear();
  tau_idmu_wp.clear();
  tau_idele_wp.clear();
  tau_charge.clear();
  tau_genmatch_pt.clear();
  tau_genmatch_eta.clear();
  tau_genmatch_phi.clear();
  tau_genmatch_mass.clear();
  tau_genmatch_decaymode.clear();

  leppair_pt.clear();
  leppair_eta.clear();
  leppair_phi.clear();
  leppair_mass.clear();
  leppair_pt1.clear();
  leppair_eta1.clear();
  leppair_phi1.clear();
  leppair_mass1.clear();
  leppair_pdg1.clear();
  leppair_pt2.clear();
  leppair_eta2.clear();
  leppair_phi2.clear();
  leppair_mass2.clear();
  leppair_pdg2.clear();

  met = 0.;
  met_phi = 0;

  npv = 0;
  nsv = 0;
  nkaon = 0;
  nlambda = 0;

  jet_pt.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_mass.clear();
  jet_pt_raw.clear();
  jet_mass_raw.clear();
  jet_chf.clear();
  jet_nhf.clear();
  jet_elf.clear();
  jet_phf.clear();
  jet_muf.clear();
  jet_deepjet_probb.clear();
  jet_deepjet_probbb.clear();
  jet_deepjet_problepb.clear();
  jet_deepjet_probc.clear();
  jet_deepjet_probg.clear();
  jet_deepjet_probuds.clear();
  jet_pnet_probb.clear();
  jet_pnet_probbb.clear();
  jet_pnet_probc.clear();
  jet_pnet_probcc.clear();
  jet_pnet_probg.clear();
  jet_pnet_probuds.clear();
  jet_pnet_probpu.clear();
  jet_pnet_probundef.clear();

  for(auto & imap : jet_pnetlast_score)
    imap.second.clear();
  for(auto & imap : jet_parTlast_score)
    imap.second.clear();

  jet_id.clear();
  jet_puid.clear();
  jet_ncand.clear();
  jet_nch.clear();
  jet_nnh.clear();
  jet_nel.clear();
  jet_nph.clear();
  jet_nmu.clear();
  jet_hflav.clear();
  jet_pflav.clear();
  jet_nbhad.clear();
  jet_nchad.clear();
  jet_genmatch_pt.clear();
  jet_genmatch_eta.clear();
  jet_genmatch_phi.clear();
  jet_genmatch_mass.clear();
  jet_genmatch_wnu_pt.clear();
  jet_genmatch_wnu_eta.clear();
  jet_genmatch_wnu_phi.clear();
  jet_genmatch_wnu_mass.clear();
  jet_genmatch_hflav.clear();
  jet_genmatch_pflav.clear();
  jet_genmatch_nbhad.clear();
  jet_genmatch_nchad.clear();
 
  jet_sv_pt.clear();
  jet_sv_eta.clear();
  jet_sv_phi.clear();
  jet_sv_mass.clear();
  jet_sv_energy.clear();
  jet_sv_chi2.clear();
  jet_sv_dxy.clear();
  jet_sv_dxysig.clear();
  jet_sv_d3d.clear();
  jet_sv_d3dsig.clear();
  jet_sv_dxy_sign.clear();
  jet_sv_d3d_sign.clear();
  jet_sv_costheta.clear();
  jet_sv_ntrack.clear();
  jet_sv_ijet.clear();

  jet_kaon_pt.clear();
  jet_kaon_eta.clear();
  jet_kaon_phi.clear();
  jet_kaon_mass.clear();
  jet_kaon_energy.clear();
  jet_kaon_chi2.clear();
  jet_kaon_dxy.clear();
  jet_kaon_dxysig.clear();
  jet_kaon_d3d.clear();
  jet_kaon_d3dsig.clear();
  jet_kaon_dxy_sign.clear();
  jet_kaon_d3d_sign.clear();
  jet_kaon_costheta.clear();
  jet_kaon_ntrack.clear();
  jet_kaon_ijet.clear();

  jet_lambda_pt.clear();
  jet_lambda_eta.clear();
  jet_lambda_phi.clear();
  jet_lambda_mass.clear();
  jet_lambda_energy.clear();
  jet_lambda_chi2.clear();
  jet_lambda_dxy.clear();
  jet_lambda_dxysig.clear();
  jet_lambda_d3d.clear();
  jet_lambda_d3dsig.clear();
  jet_lambda_dxy_sign.clear();
  jet_lambda_d3d_sign.clear();
  jet_lambda_costheta.clear();
  jet_lambda_ntrack.clear();
  jet_lambda_ijet.clear();

  jet_pfcand_ch_pt.clear();
  jet_pfcand_ch_eta.clear();
  jet_pfcand_ch_phi.clear();
  jet_pfcand_ch_mass.clear();
  jet_pfcand_ch_energy.clear();
  jet_pfcand_ch_charge.clear();
  jet_pfcand_ch_calofraction.clear();
  jet_pfcand_ch_hcalfraction.clear();
  jet_pfcand_ch_dz.clear();
  jet_pfcand_ch_dxy.clear();
  jet_pfcand_ch_dzsig.clear();
  jet_pfcand_ch_dxysig.clear();
  jet_pfcand_ch_frompv.clear();
  jet_pfcand_ch_candjet_pperp.clear();
  jet_pfcand_ch_candjet_ppara.clear();
  jet_pfcand_ch_candjet_deta.clear();
  jet_pfcand_ch_candjet_dphi.clear();
  jet_pfcand_ch_candjet_etarel.clear();
  jet_pfcand_ch_candjet_ptrel.clear();
  jet_pfcand_ch_track_chi2.clear();
  jet_pfcand_ch_track_pterr.clear();
  jet_pfcand_ch_track_etaerr.clear();
  jet_pfcand_ch_track_phierr.clear();
  jet_pfcand_ch_track_qual.clear();
  jet_pfcand_ch_trackjet_sip2d.clear();
  jet_pfcand_ch_trackjet_sip2dsig.clear();
  jet_pfcand_ch_trackjet_sip3d.clear();
  jet_pfcand_ch_trackjet_sip3dsig.clear();
  jet_pfcand_ch_trackjet_dist.clear();
  jet_pfcand_ch_trackjet_decayL.clear();
  jet_pfcand_ch_id.clear();
  jet_pfcand_ch_ijet.clear();
  jet_pfcand_ch_puppiw.clear();
  jet_pfcand_ch_tau_signal.clear();
  jet_pfcand_ch_nhits.clear();
  jet_pfcand_ch_npixhits.clear();
  jet_pfcand_ch_npixbarrelhits.clear();
  jet_pfcand_ch_nstriphits.clear();
  jet_pfcand_ch_nstriptibhits.clear();
  jet_pfcand_ch_nstriptobhits.clear();
  jet_pfcand_ch_nlosthits.clear();
  jet_pfcand_ch_nlayers.clear();
  jet_pfcand_ch_npixlayers.clear();
  jet_pfcand_ch_nstriplayers.clear();

  jet_pfcand_neu_pt.clear();
  jet_pfcand_neu_eta.clear();
  jet_pfcand_neu_phi.clear();
  jet_pfcand_neu_mass.clear();
  jet_pfcand_neu_energy.clear();
  jet_pfcand_neu_calofraction.clear();
  jet_pfcand_neu_hcalfraction.clear();
  jet_pfcand_neu_dz.clear();
  jet_pfcand_neu_dxy.clear();
  jet_pfcand_neu_frompv.clear();
  jet_pfcand_neu_candjet_pperp.clear();
  jet_pfcand_neu_candjet_ppara.clear();
  jet_pfcand_neu_candjet_deta.clear();
  jet_pfcand_neu_candjet_dphi.clear();
  jet_pfcand_neu_candjet_etarel.clear();
  jet_pfcand_neu_candjet_ptrel.clear();
  jet_pfcand_neu_id.clear();
  jet_pfcand_neu_ijet.clear();
  jet_pfcand_neu_puppiw.clear();
  jet_pfcand_neu_tau_signal.clear();


  jet_losttrack_pt.clear();
  jet_losttrack_eta.clear();
  jet_losttrack_phi.clear();
  jet_losttrack_mass.clear();
  jet_losttrack_energy.clear();
  jet_losttrack_charge.clear();
  jet_losttrack_dz.clear();
  jet_losttrack_dxy.clear();
  jet_losttrack_dzsig.clear();
  jet_losttrack_dxysig.clear();
  jet_losttrack_frompv.clear();
  jet_losttrack_track_chi2.clear();
  jet_losttrack_track_pterr.clear();
  jet_losttrack_track_etaerr.clear();
  jet_losttrack_track_phierr.clear();
  jet_losttrack_track_qual.clear();
  jet_losttrack_candjet_pperp.clear();
  jet_losttrack_candjet_ppara.clear();
  jet_losttrack_candjet_deta.clear();
  jet_losttrack_candjet_dphi.clear();
  jet_losttrack_candjet_ptrel.clear();
  jet_losttrack_candjet_etarel.clear();
  jet_losttrack_trackjet_sip2d.clear();
  jet_losttrack_trackjet_sip2dsig.clear();
  jet_losttrack_trackjet_sip3d.clear();
  jet_losttrack_trackjet_sip3dsig.clear();
  jet_losttrack_trackjet_dist.clear();
  jet_losttrack_trackjet_decayL.clear();
  jet_losttrack_nhits.clear();
  jet_losttrack_npixhits.clear();
  jet_losttrack_npixbarrelhits.clear();
  jet_losttrack_nstriphits.clear();
  jet_losttrack_nstriptibhits.clear();
  jet_losttrack_nstriptobhits.clear();
  jet_losttrack_nlosthits.clear();
  jet_losttrack_nlayers.clear();
  jet_losttrack_npixlayers.clear();
  jet_losttrack_nstriplayers.clear();
  jet_losttrack_ijet.clear();

  jet_pfcand_muon_pt.clear();
  jet_pfcand_muon_eta.clear();
  jet_pfcand_muon_phi.clear();
  jet_pfcand_muon_energy.clear();
  jet_pfcand_muon_chi2.clear();
  jet_pfcand_muon_dxy.clear();
  jet_pfcand_muon_dz.clear();
  jet_pfcand_muon_type.clear();
  jet_pfcand_muon_nvalidhits.clear();
  jet_pfcand_muon_nstations.clear();
  jet_pfcand_muon_nlayers.clear();
  jet_pfcand_muon_nhits.clear();
  jet_pfcand_muon_segcomp.clear();
  jet_pfcand_muon_validfraction.clear();
  jet_pfcand_muon_trkKink.clear();
  jet_pfcand_muon_pterr.clear();
  jet_pfcand_muon_ijet.clear();
  
  jet_pfcand_electron_pt.clear();
  jet_pfcand_electron_eta.clear();
  jet_pfcand_electron_phi.clear();
  jet_pfcand_electron_energy.clear();
  jet_pfcand_electron_dxy.clear();
  jet_pfcand_electron_dz.clear();
  jet_pfcand_electron_eOverP.clear();
  jet_pfcand_electron_detaIn.clear();
  jet_pfcand_electron_dphiIn.clear();
  jet_pfcand_electron_r9.clear();
  jet_pfcand_electron_hOverE.clear();
  jet_pfcand_electron_sigIetaIeta.clear();
  jet_pfcand_electron_convProb.clear();
  jet_pfcand_electron_ijet.clear();
  
  jet_pfcand_photon_pt.clear();
  jet_pfcand_photon_eta.clear();
  jet_pfcand_photon_phi.clear();
  jet_pfcand_photon_energy.clear();
  jet_pfcand_photon_hOverE.clear();
  jet_pfcand_photon_sigIetaIeta.clear();
  jet_pfcand_photon_eVeto.clear();
  jet_pfcand_photon_r9.clear();
  jet_pfcand_photon_ijet.clear();

}

void TrainingTreeMakerAK4::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

// to apply jet ID: https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL
bool TrainingTreeMakerAK4::applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi){
  
  if(level != "tight" and level != "tightLepVeto")
    return true;
  
  double eta  = jet.eta();                                                                                                                                                                           
  double nhf  = jet.neutralHadronEnergyFraction();                                                                                                                                                   
  double nemf = jet.neutralEmEnergyFraction();                                                                                                                                                       
  double chf  = jet.chargedHadronEnergyFraction();                                                                                                                                                   
  double muf  = jet.muonEnergyFraction();                                                                                                                                                            
  double cemf = jet.chargedEmEnergyFraction();                                                                                                                            
  
  int jetid  = 0;
  if(isPuppi){ // Puppi jets
    float np   = jet.userFloat("patPuppiJetSpecificProducer:puppiMultiplicity");
    float nnp  = jet.userFloat("patPuppiJetSpecificProducer:neutralPuppiMultiplicity");
    float nch  = np-nnp;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 1;                                                                                               
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 2;                                                                                               
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 1;                                                                                                                        
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 2;                                                                                                                    
  }
  else{ // CHS jets
    int np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();                                                                                                                              
    int nnp  = jet.neutralMultiplicity();                                                                                                                                                        
    int nch  = jet.chargedMultiplicity();                                                                                                                                                          
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;    
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 1;                                                                                               
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 2;                                        
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 1;                                                                                                                      
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 2;                                                          
  }
                    
  if(level == "tight" and jetid > 1) return true;
  else if(level == "tightLepVeto" and jetid > 2) return true;
  else return false;

}


bool TrainingTreeMakerAK4::applyPileupJetID(const pat::Jet & jet, const std::string & level){
  bool passpuid = false;  
  if(jet.hasUserInt("pileupJetIdUpdated:fullId")){
    if(level == "loose"  and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 0)) or jet.pt() > 50)) passpuid = true;
    if(level == "medium" and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 1)) or jet.pt() > 50)) passpuid = true;
    if(level == "tight"  and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 2)) or jet.pt() > 50)) passpuid = true;
  }
  else if (jet.hasUserInt("pileupJetId:fullId")){
    if(level == "loose"  and (bool(jet.userInt("pileupJetId:fullId") & (1 << 0)) or jet.pt() > 50)) passpuid = true;
    if(level == "medium" and (bool(jet.userInt("pileupJetId:fullId") & (1 << 1)) or jet.pt() > 50)) passpuid = true;
    if(level == "tight"  and (bool(jet.userInt("pileupJetId:fullId") & (1 << 2)) or jet.pt() > 50)) passpuid = true;
  }
  return passpuid;
}	




DEFINE_FWK_MODULE(TrainingTreeMakerAK4);
