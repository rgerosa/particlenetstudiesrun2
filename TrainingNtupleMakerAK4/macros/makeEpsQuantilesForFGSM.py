import os
import sys
import glob
import argparse
import subprocess
import shutil
import yaml
from importlib import import_module
import ROOT
import numpy as np
import math

ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='', help='directory with input files');
parser.add_argument('-s', '--string-to-grep', nargs="+", default=[], help='list of string to grep to get fielist');
parser.add_argument('-d', '--data-config', type=str, default='', help="data config file of weaver");
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-q', '--quantile', type=float, default=0.95, help='one sided quantile of the distribution for regular variables');
parser.add_argument('-o', '--output-file', type=str, default='', help='output file');
parser.add_argument('-p', '--output-plot-dir', type=str, default='', help='output plot dir');
parser.add_argument('-c', '--clip-val', type=float, default=5, help='clipping value to be applied as default');
parser.add_argument('-v', '--var-exclude', nargs="+", default=["id","charge","frompv","qual","hits","npix","nstrip","nlost","nlayers","tau_sig","type","stations","ntrack","sign","eVeto"],help='list of variables for which no eps are stored --> fixed to 0');

if __name__ == '__main__':

    ROOT.gInterpreter.ProcessLine('#include "CMS_style.h"')
    ROOT.setTDRStyle();

    nbin = 500;
    rebin_factor = 2000;
    y_scale_min = 1./10**5;
    y_scale_max = 10;

    args = parser.parse_args()
    xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

    ROOT.EnableImplicitMT(args.nthreads);
    ROOT.EnableThreadSafety();

    ## parse yaml file
    with open(args.data_config, 'r') as file:
        data_config = yaml.safe_load(file)

    ## take selections, new variables, and inputs
    selection = data_config['selection'];
    new_variables = data_config['new_variables'];
    inputs = data_config['inputs'];

    ## exclude masks    
    new_variables = {key: value for key, value in new_variables.items() if "mask" not in key };
    inputs = {key: value for key, value in inputs.items() if "mask" not in key };

    ## load the input dataframe                                                                                                                                    
    input_files = [];
    if args.string_to_grep:
        for name in args.string_to_grep:
            if not args.use_xrootd:
                input_files.append(args.input_dir+"/*"+name+"*root");
            else:
                input_files.append(xrootd_eos+"/"+args.input_dir+"/*"+name+"*root");
    else:
        if not args.use_xrootd:
            input_files.append(args.input_dir+"/*root");
        else:
            input_files.append(xrootd_eos+"/"+args.input_dir+"/*root");

    print("###### Create RDataFrame for ",input_files);
    data_frame = ROOT.RDataFrame("tree",input_files);
    
    ## add new colums
    print("###### Add new colums to DataFrame");
    for key,var in new_variables.items():        
        var = var.replace("np.","").replace("&","&&").replace("~","!").replace("_p4_from_ptetaphie","ROOT::VecOps::Construct<ROOT::Math::PtEtaPhiEVector>")
        if ".px" in var or ".py" in var or ".pz" in var or ".energy" in var:
            var1 = var.split(".")[0];
            var2 = var.split(".")[1];
            var = "return ROOT::VecOps::Map("+var1+",[](const ROOT::Math::PtEtaPhiEVector &v){return v."+var2+";})";
            var = var.replace("px","Px()").replace("py","Py()").replace("pz","Pz()").replace("energy","E()");
        print(key," ",var);
        data_frame = data_frame.Define(key,var);
        
    ## edit selection string
    selection = selection.replace("&","&&").replace("~","!").replace("np.","")
    print("###### Add filter to DataFrame ",selection);
    data_frame = data_frame.Filter(selection);
    ## variables with transformation
    print("###### Create histograms for input features --> after transforming when needed");
    histograms = {}
    for key,var in inputs.items():
        for idx,element in enumerate(var['vars']):
            ## all elements are written in the yaml file
            if len(element) >= 5 and element[1] != None and element[2] != None and element[3] != None and element[4] != None:
                print("Transform ",element[0]," as ","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])," defined in [",element[3],",",element[4],"] range");                
                var_new = "(x+"+str(-1*element[1])+")*"+str(element[2]);
                data_frame = data_frame.Define(element[0]+"_norm","return ROOT::VecOps::Map("+element[0]+",[](const float & x){ if ("+var_new+" < "+str(element[3])+") return "+str(element[3])+"; else if ("+var_new+" > "+str(element[4])+") return "+str(element[4])+"; else return"+var_new+";})");
                h = data_frame.Histo1D((element[0]+"_norm","",nbin*rebin_factor,element[3],element[4]),element[0]+"_norm");                
            ## min and max not written, we put it as cip-val
            elif len(element) >= 3 and element[1] != None and element[2] != None:
                print("Transform ",element[0]," as ","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])," defined in [-"+str(np.round(args.clip_val,2))+","+str(np.round(args.clip_val,2))+"] range");
                var_new = "(x+"+str(-1*element[1])+")*"+str(element[2]);
                data_frame = data_frame.Define(element[0]+"_norm","return ROOT::VecOps::Map("+element[0]+",[](const float & x){ if ("+var_new+" < "+str(-args.clip_val)+") return "+str(-args.clip_val)+"; else if ("+var_new+" > "+str(args.clip_val)+") return "+str(args.clip_val)+"; else return"+var_new+";})");
                h = data_frame.Histo1D((element[0]+"_norm","",nbin*rebin_factor,-args.clip_val,args.clip_val),element[0]+"_norm");
            ## no transformation applied
            else:
                min_var, max_var = 0, 0;
                if "px" in element[0] or "py" in element[0] or "pz" in element[0]:
                    min_var , max_var = -250., 250.;
                elif "eta" in element[0]:
                    min_var , max_var = -2.5, 2.5;
                elif "phi" in element[0]:
                    min_var , max_var = -math.pi, math.pi;
                else:
                    min_var , max_var = 0., 350.;
                print("No transformation for ",element[0]," min ",min_var," max ",max_var);
                h = data_frame.Histo1D((element[0],"",nbin*rebin_factor,min_var,max_var),element[0]);                    
                histograms[element[0]] = [h];

    ## compute the quantiles
    print("###### Looping on events and fill histograms");
    for key, item in histograms.items():
        histo = item[0];
        x_q = np.array([0.,0.,0.]);
        y_q = np.array([0.5,1-args.quantile,args.quantile]);            
        histo.GetQuantiles(len(x_q),x_q,y_q);
        x_q = np.around(x_q,5);
        print("Variable ",key," x_q ",x_q," y_q ",y_q," f-underflow ",np.around(histo.GetBinContent(-1)/histo.Integral(-1,histo.GetNbinsX()+1),5)," f-overflow ",np.around(histo.GetBinContent(histo.GetNbinsX()+1)/histo.Integral(-1,histo.GetNbinsX()+1),5));
        item.extend(x_q)
        for k,v in inputs.items():
            for idx,entry in enumerate(v['vars']):                
              if entry[0] == key:
                  ## quantile infos
                  eps_list= [];
                  if any(vars in key for vars in args.var_exclude):
                      eps_list.extend([float(0.),float(0.)]);
                  else:
                      eps_list.extend([float(x_q[0]),float(x_q[1])]);
                  ## if there are already 5 elements just add eps
                  if len(entry) >= 5:
                      data_config['inputs'][k]['vars'][idx].extend(eps_list);
                  elif len(entry) == 3:
                      data_config['inputs'][k]['vars'][idx].extend([float(-args.clip_val),float(args.clip_val)]);
                      data_config['inputs'][k]['vars'][idx].extend(eps_list);
                  else:
                      data_config['inputs'][k]['vars'][idx].extend([None,None,None]);
                      data_config['inputs'][k]['vars'][idx].extend(eps_list);
                  print("Writing --> ",data_config['inputs'][k]['vars'][idx]);

    ## save output files
    print("###### Save output files");
    with open(args.output_file, 'w') as outfile:
        yaml.preserve_quotes = False
        yaml.safe_dump(data_config,outfile,sort_keys=False)
    
    ## save plots
    print("###### Save plots only for variables to use in the attack ");
    os.system("mkdir -p "+args.output_plot_dir);
    ROOT.gStyle.SetOptStat(111111);
    c = ROOT.TCanvas("c","c",600,600);
    for key, item in histograms.items():
        c.cd();
        if any(vars in key for vars in args.var_exclude):
            continue;        
        histo = item[0];
        median = item[1];
        eps_min = item[2];
        eps_max = item[3];
        ## scale and rebing
        histo.Rebin(rebin_factor);
        histo.Scale(1./histo.Integral(-1,histo.GetNbinsX()+1))
	## include underflow and overflow
        histo.GetXaxis().SetRange(-1,histo.GetNbinsX()+1);
        histo.GetXaxis().SetTitle(key);
        histo.GetYaxis().SetTitle("a.u.");
        histo.Draw("hist");
        median_line = ROOT.TLine(median,0.,median,histo.GetMaximum());
        median_line.SetLineColor(ROOT.kRed);
        median_line.SetLineStyle(7);
        median_line.Draw("Lsame");
        eps_min_line = ROOT.TLine(eps_min,0.,eps_min,histo.GetMaximum());
        eps_min_line.SetLineColor(ROOT.kRed);
        eps_min_line.SetLineStyle(7);
        eps_min_line.Draw("Lsame");
        eps_max_line = ROOT.TLine(eps_max,0.,eps_max,histo.GetMaximum());
        eps_max_line.SetLineColor(ROOT.kRed);
        eps_max_line.SetLineStyle(7);
        eps_max_line.Draw("Lsame");
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+histo.GetName()+".png","png");
        histo.SetMinimum(y_scale_min);
        histo.SetMaximum(y_scale_max);
        c.SetLogy(1);
        c.SaveAs(args.output_plot_dir+"/"+histo.GetName()+"_log.png","png");
        c.SetLogy(0);
        
    ## save root file  
    output_file_root = ROOT.TFile(args.output_file.replace(".yaml",".root"),"RECREATE");
    output_file_root.cd();
    for key,item in histograms.items():
        histo = item[0];
        histo.Write();
    output_file_root.Close();
