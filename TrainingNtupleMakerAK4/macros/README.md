# Macros for assessing PNETAK4 performance

A set of c++ or python macros are listed here and can be used to assess the performance of the PNET trainings in terms of ROCs:

* `makeROCCurvesBTagging.C` used to plot b-tagging ROCs: b vs c, b vs uds, b vs g, b vs $`\tau_{h}`$.
* `makeROCCurvesCTagging.C` used to plot c-tagging ROCs: c vs b, c vs uds, c vs g, c vs $`\tau_{h}`$.
* `makeROCCurvesTauTagging.C` used to plot tau-tagging ROCs: $`\tau_{h}`$ vs b, $`\tau_{h}`$ vs c, $`\tau_{h}`$ vs uds, $`\tau_{h}`$ vs g, $`\tau_{h}`$ vs jet, $`\tau_{h}`$ vs $`\mu`$, and $`\tau_{h}`$ vs ele.
* `makeROCCurvesEleTagging.C` used to plot electron-tagging ROCs: ele vs b, ele vs c, ele vs uds, ele vs g, ele vs jet, and ele vs $`\tau_{h}`$.
* `makeRegressionResponse.C` used to plot the pT regression response in pT and eta bins comparing: the training performed, the last PNET regression stored in the ntuples, and the original object pT assignement.

Each macro of them takes common input parameters that are described below:
* First of all the paths and the type of ROCs one wants to display are defined inside the code as member of the inputFileName map: key is an identifier string, value is the path of the directory containing the ntuples produced  by weaver (prediction from weaver).
* `fileNameStringToGrep`: string to grep in the folder in order to collect the input trees/files.
* `dataConfigStringToGrep`: string to grep in order to collect the data-config file from weaver containing the class weights (should be placed in the same directory of the trees).
* `applyReweight`: whether re-weight or not the jet pT and eta spectrum to be flat (as done in the training).
* `ptBins` and `etaBins`: binning in order to produce ROC curves.
* `sample_selected`: filter the jets to consider only those belonging to a specific set of physics samples described in the `commonTools.h` file
* `nThreads`: number of threads that will be used for looping on the input trees, not for building the final rocs and plots.
* `sigEffTargetBJet` or `sigEffTargetCJet`, etc ... indentify the signal-eff WPs that are tested i.e. those for which the fake-rate vs pT is plotted.

Specific for hadronic taus:
* `addJetHPSTauMatching`: require matching between each jet (signal or background) and a reco-tau from HPS with $`p_{T} > 20, |\eta| < 2.3`$.
* `selectOneProngTaus`: consider only signal and background jet with HPS mode [0,1,2].
* `selectThreeProngTaus`: consider only signal and background jet with HPS mode [10,11].
* `addDeepTauSelection`: require specific selection used in the DeepTau performance paper ($`\Delta Z<0.2`$, VVLoose idjet, VVLoose idele, VLoose idmu).

Specific for electrons:
* `addMatchingWithRecoEle`: require matching between each jet (signal or background) and a reco-electron (slimmedElectron) with $`p_{T} > 20, |\eta| < 2.5`$.
