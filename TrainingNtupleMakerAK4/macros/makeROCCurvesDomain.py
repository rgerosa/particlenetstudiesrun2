import os
import sys
import glob
import argparse
import subprocess
import shutil
import time
import ROOT
import numpy as np
from array import array 
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file-name', type=str, default='', help='file name to grep');
parser.add_argument('-o', '--output-dir', type=str, default='', help='output directgory for plots');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-p', '--pt-bins', nargs="+", type=float, default=[30,1000], help='pt bins to build ROCs');
parser.add_argument('-e', '--eta-bins', nargs="+", type=float, default=[-2.5,2.5], help='eta bins to build ROCs');
parser.add_argument('-d', '--domain', nargs="+", type=str, default=["dimuon","emu","mutau"], choices=["dimuon","emu","mutau","dijet","ttcharm"], help='domain to be used in the selection');
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-j', '--use-jec', action='store_true', help='use pt corrected by JECs');

inputs = {
    "ParTNoJECNew3D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_3d_31012024",
    "ParTNoJECNew5D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_31012024",
    "ParTNoJECNewDyn": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_dyn_02032024"
}

start_time = time.time()
ROOT.gInterpreter.ProcessLine('#include "commonTools.h"')
ROOT.setTDRStyle();

args = parser.parse_args()
print(args)
xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

ROOT.EnableImplicitMT(args.nthreads);
ROOT.EnableThreadSafety();

os.system("mkdir -p "+args.output_dir);

preselection = "domain >= 0";
print("Preselection string --> ",preselection);
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)

### histograms
hist_data_dimuon = {};
hist_data_emu = {};
hist_data_mutau = {};
hist_data_ttcharm = {};
hist_data_dijet = {};
hist_mc_dimuon = {};
hist_mc_emu = {};
hist_mc_mutau = {};
hist_mc_ttcharm = {};
hist_mc_dijet = {};
hist_sf_dimuon = {};
hist_sf_emu = {};
hist_sf_mutau = {};
hist_sf_ttcharm = {};
hist_sf_dijet = {};
roc_dimuon = {}
roc_emu = {}
roc_mutau = {}
roc_ttcharm = {}
roc_dijet = {}

## define binnings
pt_bins = array('d',args.pt_bins);
eta_bins = array('d',args.eta_bins);
score_bins = array('d',[i for i in np.arange(0.,1.0005,0.0005)])
sf_bins = array('d',[i for i in np.arange(0.5,2.0,0.01)])
print("pt_bins: ",pt_bins);
print("eta_bins: ",eta_bins);
print("score_bins: from 0 to 1 in step of 0.0001 --> len ",len(score_bins));
print("sf_bins: from 0.5 to 2 in step of 0.01 --> len ",len(sf_bins));

## 
for key,val in inputs.items():
    start_time = time.time()
    print("Run on input files: key ",key," location: ",val);
    ## build the dataframe
    data_frame = ROOT.RDataFrame("Events",xrootd_eos+val+"/*"+args.file_name+"*root" if args.use_xrootd else val+"/*"+args.file_name+"*root");
    ## apply preselection
    data_frame = data_frame.Filter(preselection)

    ## define the histograms as 3D to speed up RDF computation
    histo_list = [];
    pt_name = "jet_pt_raw";
    if args.use_jec:
        pt_name = "jet_pt"

    data_dimuon, data_emu, data_mutau, data_dijet, data_ttcharm = None, None, None, None, None;
    mc_dimuon, mc_emu, mc_mutau, mc_dijet, mc_ttcharm = None, None, None, None, None;
    sf_dimuon, sf_emu, sf_mutau, sf_dijet, sf_ttcharm = None, None, None, None, None;
    
    if "dimuon" in args.domain and data_frame.HasColumn("score_label_data_zll"):
        data_frame = data_frame.Define("score_dimuon","score_label_data_zll/(score_label_data_zll+score_label_mc_zll)");
        data_frame = data_frame.Define("sf_dimuon","score_label_data_zll/score_label_mc_zll");
        data_frame_dimuon_data = data_frame.Filter("domain == 0 && label_data_zll == 1");
        data_frame_dimuon_mc = data_frame.Filter("domain == 0 && label_mc_zll == 1");
        data_dimuon = data_frame_dimuon_data.Histo3D(("data_dimuon_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_dimuon");
        mc_dimuon = data_frame_dimuon_mc.Histo3D(("mc_dimuon_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_dimuon");
        sf_dimuon = data_frame_dimuon_mc.Histo3D(("sf_dimuon_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(sf_bins)-1,sf_bins),pt_name,"jet_eta","sf_dimuon");
        histo_list.extend([data_dimuon,mc_dimuon,sf_dimuon]);

    if "emu" in args.domain and data_frame.HasColumn("score_label_data_emu"):
        data_frame = data_frame.Define("score_emu","score_label_data_emu/(score_label_data_emu+score_label_mc_emu)");
        data_frame = data_frame.Define("sf_emu","score_label_data_emu/score_label_mc_emu");
        data_frame_emu_data = data_frame.Filter("domain == 2 && label_data_emu == 1");
        data_frame_emu_mc = data_frame.Filter("domain == 2 && label_mc_emu == 1");
        data_emu = data_frame_emu_data.Histo3D(("data_emu_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_emu");
        mc_emu = data_frame_emu_mc.Histo3D(("mc_emu_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_emu");
        sf_emu = data_frame_emu_mc.Histo3D(("sf_emu_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(sf_bins)-1,sf_bins),pt_name,"jet_eta","sf_emu");
        histo_list.extend([data_emu,mc_emu,sf_emu]);

    if "mutau" in args.domain and data_frame.HasColumn("score_label_data_mut"):
        data_frame = data_frame.Define("score_mutau","score_label_data_mut/(score_label_data_mut+score_label_mc_mut)");
        data_frame = data_frame.Define("sf_mutau","score_label_data_mut/score_label_mc_mut");
        data_frame_mutau_data = data_frame.Filter("domain == 3 && label_data_mut == 1");
        data_frame_mutau_mc = data_frame.Filter("domain == 3 && label_mc_mut == 1");
        data_mutau = data_frame_mutau_data.Histo3D(("data_mutau_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_mutau");
        mc_mutau = data_frame_mutau_mc.Histo3D(("mc_mutau_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_mutau");
        sf_mutau = data_frame_mutau_mc.Histo3D(("sf_mutau_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(sf_bins)-1,sf_bins),pt_name,"jet_eta","sf_mutau");
        histo_list.extend([data_mutau,mc_mutau,sf_mutau]);

    if "dijet" in args.domain and data_frame.HasColumn("score_label_data_dijet"):
        data_frame = data_frame.Define("score_dijet","score_label_data_dijet/(score_label_data_dijet+score_label_mc_dijet)");
        data_frame = data_frame.Define("sf_dijet","score_label_data_dijet/score_label_mc_dijet");
        data_frame_dijet_data = data_frame.Filter("domain == 5 && label_data_dijet == 1");
        data_frame_dijet_mc = data_frame.Filter("domain == 5 && label_mc_dijet == 1");
        data_dijet = data_frame_dijet_data.Histo3D(("data_dijet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_dijet");
        mc_dijet = data_frame_dijet_mc.Histo3D(("mc_dijet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_dijet");
        sf_dijet = data_frame_dijet_mc.Histo3D(("sf_dijet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(sf_bins)-1,sf_bins),pt_name,"jet_eta","sf_dijet");
        histo_list.extend([data_dijet,mc_dijet,sf_dijet]);

    if "ttcharm" in args.domain and data_frame.HasColumn("score_label_data_ttcharm"):
        data_frame = data_frame.Define("score_ttcharm","score_label_data_ttcharm/(score_label_data_ttcharm+score_label_mc_ttcharm)");
        data_frame = data_frame.Define("sf_ttcharm","score_label_data_ttcharm/score_label_mc_ttcharm");
        data_frame_ttcharm_data = data_frame.Filter("domain == 6 && label_data_ttcharm == 1");
        data_frame_ttcharm_mc = data_frame.Filter("domain == 6 && label_mc_ttcharm == 1");
        data_ttcharm = data_frame_ttcharm_data.Histo3D(("data_ttcharm_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_ttcharm");
        mc_ttcharm = data_frame_ttcharm_mc.Histo3D(("mc_ttcharm_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_ttcharm");
        sf_ttcharm = data_frame_ttcharm_mc.Histo3D(("sf_ttcharm_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(sf_bins)-1,sf_bins),pt_name,"jet_eta","sf_ttcharm");
        histo_list.extend([data_ttcharm,mc_ttcharm,sf_ttcharm]);

    ## Run histogram creation
    ROOT.RDF.RunGraphs(histo_list);
    if "dimuon" in args.domain and data_frame.HasColumn("score_label_data_zll"):
        data_dimuon = data_dimuon.GetPtr();
        mc_dimuon = mc_dimuon.GetPtr();
        sf_dimuon = sf_dimuon.GetPtr();
    if "emu" in args.domain and data_frame.HasColumn("score_label_data_emu"):
        data_emu = data_emu.GetPtr();
        mc_emu = mc_emu.GetPtr();
        sf_emu = sf_emu.GetPtr();
    if "mutau" in args.domain and data_frame.HasColumn("score_label_data_mut"):
        data_mutau = data_mutau.GetPtr();
        mc_mutau = mc_mutau.GetPtr();
        sf_mutau = sf_mutau.GetPtr();
    if "dijet" in args.domain and data_frame.HasColumn("score_label_data_dijet"):
        data_dijet = data_dijet.GetPtr();
        mc_dijet = mc_dijet.GetPtr();
        sf_dijet = sf_dijet.GetPtr();
    if "ttcharm" in args.domain and data_frame.HasColumn("score_label_data_ttcharm"):
        data_ttcharm = data_ttcharm.GetPtr();
        mc_ttcharm = mc_ttcharm.GetPtr();
        sf_ttcharm = sf_ttcharm.GetPtr();
    
    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)
    ## build the ROCs and projecting histograms
    print("Produce the corresponding ROCs by projecting histograms");
    start_time = time.time()
    for ipt in range(0,len(pt_bins)-1):
        for ieta in range(0,len(eta_bins)-1):
            name = key+"_pt_"+str(ipt)+"_eta_"+str(ieta);

            if "dimuon" in args.domain and data_dimuon:
                hist_data_dimuon[name] = ROOT.extractScore(data_dimuon,ipt,ieta,"hist_data_dimuon_"+name)
                hist_mc_dimuon[name] = ROOT.extractScore(mc_dimuon,ipt,ieta,"hist_mc_dimuon_"+name)
                hist_sf_dimuon[name] = ROOT.extractScore(sf_dimuon,ipt,ieta,"hist_sf_dimuon_"+name)
                hist_data_dimuon[name].Scale(1./hist_data_dimuon[name].Integral());
                hist_mc_dimuon[name].Scale(1./hist_mc_dimuon[name].Integral());
                hist_sf_dimuon[name].Scale(1./hist_sf_dimuon[name].Integral());
                roc_dimuon[name] = ROOT.buildROC(hist_data_dimuon[name],hist_mc_dimuon[name]);
            if "emu" in args.domain and data_emu:
                hist_data_emu[name] = ROOT.extractScore(data_emu,ipt,ieta,"hist_data_emu_"+name)
                hist_mc_emu[name] = ROOT.extractScore(mc_emu,ipt,ieta,"hist_mc_emu_"+name)
                hist_sf_emu[name] = ROOT.extractScore(sf_emu,ipt,ieta,"hist_sf_emu_"+name)
                hist_data_emu[name].Scale(1./hist_data_emu[name].Integral());
                hist_mc_emu[name].Scale(1./hist_mc_emu[name].Integral());
                hist_sf_emu[name].Scale(1./hist_sf_emu[name].Integral());
                roc_emu[name] = ROOT.buildROC(hist_data_emu[name],hist_mc_emu[name]);
            if "mutau" in args.domain and data_mutau:
                hist_data_mutau[name] = ROOT.extractScore(data_mutau,ipt,ieta,"hist_data_mutau_"+name)
                hist_mc_mutau[name] = ROOT.extractScore(mc_mutau,ipt,ieta,"hist_mc_mutau_"+name)
                hist_sf_mutau[name] = ROOT.extractScore(sf_mutau,ipt,ieta,"hist_sf_mutau_"+name)
                hist_data_mutau[name].Scale(1./hist_data_mutau[name].Integral());
                hist_mc_mutau[name].Scale(1./hist_mc_mutau[name].Integral());
                hist_sf_mutau[name].Scale(1./hist_sf_mutau[name].Integral());
                roc_mutau[name] = ROOT.buildROC(hist_data_mutau[name],hist_mc_mutau[name]);
            if "dijet" in args.domain and data_dijet:
                hist_data_dijet[name] = ROOT.extractScore(data_dijet,ipt,ieta,"hist_data_dijet_"+name)
                hist_mc_dijet[name] = ROOT.extractScore(mc_dijet,ipt,ieta,"hist_mc_dijet_"+name)
                hist_sf_dijet[name] = ROOT.extractScore(sf_dijet,ipt,ieta,"hist_sf_dijet_"+name)
                hist_data_dijet[name].Scale(1./hist_data_dijet[name].Integral());
                hist_mc_dijet[name].Scale(1./hist_mc_dijet[name].Integral());
                hist_sf_dijet[name].Scale(1./hist_sf_dijet[name].Integral());
                roc_dijet[name] = ROOT.buildROC(hist_data_dijet[name],hist_mc_dijet[name]);
            if "ttcharm" in args.domain and data_ttcharm:
                hist_data_ttcharm[name] = ROOT.extractScore(data_ttcharm,ipt,ieta,"hist_data_ttcharm_"+name)
                hist_mc_ttcharm[name] = ROOT.extractScore(mc_ttcharm,ipt,ieta,"hist_mc_ttcharm_"+name)
                hist_sf_ttcharm[name] = ROOT.extractScore(sf_ttcharm,ipt,ieta,"hist_sf_ttcharm_"+name)
                hist_data_ttcharm[name].Scale(1./hist_data_ttcharm[name].Integral());
                hist_mc_ttcharm[name].Scale(1./hist_mc_ttcharm[name].Integral());
                hist_sf_ttcharm[name].Scale(1./hist_sf_ttcharm[name].Integral());
                roc_ttcharm[name] = ROOT.buildROC(hist_data_ttcharm[name],hist_mc_ttcharm[name]);

    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)

### From TGraph to TF1
print("Plotting final ROCs");
start_time = time.time()
for ipt in range(0,len(args.pt_bins)-1):
    for ieta in range(0,len(args.eta_bins)-1):
        roc_to_plot_dimuon = [];
        roc_to_plot_emu = [];
        roc_to_plot_mutau = [];
        roc_to_plot_dijet = [];
        roc_to_plot_ttcharm = [];
        sf_to_plot_dimuon = [];
        sf_to_plot_emu = [];
        sf_to_plot_mutau = [];
        sf_to_plot_dijet = [];
        sf_to_plot_ttcharm = [];
        legends_dimuon = [];
        legends_emu = [];
        legends_mutau = [];
        legends_dijet = [];
        legends_ttcharm = [];
        bin_label = "pt_"+str(ipt)+"_eta_"+str(ieta);
        bin_name = "%d < p_{T} < %d GeV, %.1f < |#eta| < %.1f"%(args.pt_bins[ipt],args.pt_bins[ipt+1],args.eta_bins[ieta],args.eta_bins[ieta+1]);
        for key in inputs:

            name = key+"_"+bin_label;
            legends_tmp = [];
            legends_tmp.append(name);
            
            if name in roc_dimuon.keys():
                legends_dimuon.append(key);
                labels_tmp = [];
                roc_to_plot_dimuon.append(roc_dimuon[name]);
                sf_to_plot_dimuon.append(hist_sf_dimuon[name]);
                hist_data_dimuon[name].Rebin(10);
                hist_mc_dimuon[name].Rebin(10);
                hist_data_dimuon[name].Scale(1./hist_data_dimuon[name].Integral());
                hist_mc_dimuon[name].Scale(1./hist_mc_dimuon[name].Integral());
                labels_tmp.append("Data #mu#mu");
                labels_tmp.append("MC #mu#mu");
                ROOT.plotScore([hist_data_dimuon[name]],[hist_mc_dimuon[name]],[],[],[],[],legends_tmp,labels_tmp,bin_name,args.output_dir,"score_dimuon_"+name+"_"+bin_label);
                
            if name in roc_emu.keys():
                legends_emu.append(key);
                labels_tmp = [];
                roc_to_plot_emu.append(roc_emu[name]);
                sf_to_plot_emu.append(hist_sf_emu[name]);
                hist_data_emu[name].Rebin(10);
                hist_mc_emu[name].Rebin(10);
                hist_data_emu[name].Scale(1./hist_data_emu[name].Integral());
                hist_mc_emu[name].Scale(1./hist_mc_emu[name].Integral());
                labels_tmp.append("Data e#mu");
                labels_tmp.append("MC e#mu");
                ROOT.plotScore([hist_data_emu[name]],[hist_mc_emu[name]],[],[],[],[],legends_tmp,labels_tmp,bin_name,args.output_dir,"score_emu_"+name+"_"+bin_label);

            if name in roc_mutau.keys():
                legends_mutau.append(key);
                labels_tmp = [];
                roc_to_plot_mutau.append(roc_mutau[name]);
                sf_to_plot_mutau.append(hist_sf_mutau[name]);
                hist_data_mutau[name].Rebin(10);
                hist_mc_mutau[name].Rebin(10);
                hist_data_mutau[name].Scale(1./hist_data_mutau[name].Integral());
                hist_mc_mutau[name].Scale(1./hist_mc_mutau[name].Integral());
                labels_tmp.append("Data #mu#tau");
                labels_tmp.append("MC #mu#tau");
                ROOT.plotScore([hist_data_mutau[name]],[hist_mc_mutau[name]],[],[],[],[],legends_tmp,labels_tmp,bin_name,args.output_dir,"score_mutau_"+name+"_"+bin_label);

            if name in roc_dijet.keys():
                legends_dijet.append(key);
                labels_tmp = [];
                roc_to_plot_dijet.append(roc_dijet[name]);
                sf_to_plot_dijet.append(hist_sf_dijet[name]);
                hist_data_dijet[name].Rebin(10);
                hist_mc_dijet[name].Rebin(10);
                hist_data_dijet[name].Scale(1./hist_data_dijet[name].Integral());
                hist_mc_dijet[name].Scale(1./hist_mc_dijet[name].Integral());
                labels_tmp.append("Data dijet");
                labels_tmp.append("MC dijet");
                ROOT.plotScore([hist_data_dijet[name]],[hist_mc_dijet[name]],[],[],[],[],legends_tmp,labels_tmp,bin_name,args.output_dir,"score_dijet_"+name+"_"+bin_label);

            if name in roc_ttcharm.keys():
                legends_ttcharm.append(key);
                labels_tmp = [];
                roc_to_plot_ttcharm.append(roc_ttcharm[name]);
                sf_to_plot_ttcharm.append(hist_sf_ttcharm[name]);
                hist_data_ttcharm[name].Rebin(10);
                hist_mc_ttcharm[name].Rebin(10);
                hist_data_ttcharm[name].Scale(1./hist_data_ttcharm[name].Integral());
                hist_mc_ttcharm[name].Scale(1./hist_mc_ttcharm[name].Integral());
                labels_tmp.append("Data tt+c");
                labels_tmp.append("MC tt+c");
                ROOT.plotScore([hist_data_ttcharm[name]],[hist_mc_ttcharm[name]],[],[],[],[],legends_tmp,labels_tmp,bin_name,args.output_dir,"score_ttcharm_"+name+"_"+bin_label);
            
        if name in roc_dimuon.keys():
            labels = [];
            labels.append("data vs mc #mu#mu");
            ROOT.plotROC(roc_to_plot_dimuon,[],[],[],[],[],legends_dimuon,labels,bin_name,args.output_dir,"roc_dimuon_"+bin_label,0,1,0,1,False);
            ROOT.plotScore(sf_to_plot_dimuon,[],[],[],[],[],legends_dimuon,labels,bin_name,args.output_dir,"sf_dimuon_"+bin_label,sf_bins[0],sf_bins[-1],0.0001,1,True,"w(data)/w(mc)");
        if name in roc_emu.keys():
            labels = [];
            labels.append("data vs mc e#mu");
            ROOT.plotROC(roc_to_plot_emu,[],[],[],[],[],legends_emu,labels,bin_name,args.output_dir,"roc_emu_"+bin_label,0,1,0,1,False);
            ROOT.plotScore(sf_to_plot_emu,[],[],[],[],[],legends_emu,labels,bin_name,args.output_dir,"sf_emu_"+bin_label,sf_bins[0],sf_bins[-1],0.0001,1,True,"w(data)/w(mc)");
        if name in roc_mutau.keys():
            labels = [];
            labels.append("data vs mc #mu#tau");
            ROOT.plotROC(roc_to_plot_mutau,[],[],[],[],[],legends_mutau,labels,bin_name,args.output_dir,"roc_mutau_"+bin_label,0,1,0,1,False);
            ROOT.plotScore(sf_to_plot_mutau,[],[],[],[],[],legends_mutau,labels,bin_name,args.output_dir,"sf_mutau_"+bin_label,sf_bins[0],sf_bins[-1],0.0001,1,True,"w(data)/w(mc)");
        if name in roc_dijet.keys():
            labels = [];
            labels.append("data vs mc dijet");
            ROOT.plotROC(roc_to_plot_dijet,[],[],[],[],[],legends_dijet,labels,bin_name,args.output_dir,"roc_dijet_"+bin_label,0,1,0,1,False);
            ROOT.plotScore(sf_to_plot_dijet,[],[],[],[],[],legends_dijet,labels,bin_name,args.output_dir,"sf_dijet_"+bin_label,sf_bins[0],sf_bins[-1],0.0001,1,True,"w(data)/w(mc)");
        if name in roc_ttcharm.keys():
            labels = [];
            labels.append("data vs mc tt+c");
            ROOT.plotROC(roc_to_plot_ttcharm,[],[],[],[],[],legends_ttcharm,labels,bin_name,args.output_dir,"roc_ttcharm_"+bin_label,0,1,0,1,False);
            ROOT.plotScore(sf_to_plot_ttcharm,[],[],[],[],[],legends_ttcharm,labels,bin_name,args.output_dir,"sf_ttcharm_"+bin_label,sf_bins[0],sf_bins[-1],0.0001,1,True,"w(data)/w(mc)");
        
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)
