import os
import sys
import glob
import argparse
import subprocess
import shutil
import yaml
from importlib import import_module

import ROOT
import numpy as np
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='', help='directory with input files');
parser.add_argument('-s', '--string-to-grep', nargs="+", default=[], help='list of string to grep to get fielist');
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-o', '--output-file', type=str, default='', help='output file');
parser.add_argument('-p', '--output-plot-dir', type=str, default='', help='output plot dir');
parser.add_argument('-d', '--data-config', type=str, default='', help="data config file of weaver");
parser.add_argument('-l', '--include-lep', action='store_true', help='include tau, mu, el truth lables');

if __name__ == '__main__':

    ROOT.gInterpreter.ProcessLine('#include "CMS_style.h"')
    ROOT.setTDRStyle();

    y_scale_min = 1./10**7;
    y_scale_max = 10;
    
    args = parser.parse_args()
    xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

    ROOT.EnableImplicitMT(args.nthreads);
    ROOT.EnableThreadSafety();

    ## parse yaml file                                                                                                                                                                                          
    with open(args.data_config, 'r') as file:
        data_config = yaml.safe_load(file)

    ## load the input dataframe
    input_files = [];
    if args.string_to_grep:
        for name in args.string_to_grep:
            if not args.use_xrootd:
                input_files.append(args.input_dir+"/*"+name+"*root");
            else:
                input_files.append(xrootd_eos+"/"+args.input_dir+"/*"+name+"*root");
    else:
        if not args.use_xrootd:
            input_files.append(args.input_dir+"/*root");
        else:
            input_files.append(xrootd_eos+"/"+args.input_dir+"/*root");
            
    print("###### Create RDataFrame for ",input_files);
    data_frame = ROOT.RDataFrame("tree",input_files);

    print("###### Add new colums to DataFrame");
    new_variables = data_config['new_variables'];
    new_variables = {key: value for key, value in new_variables.items() if "mask" not in key };

    for key,var in new_variables.items():
        if "_p4_from_ptetaphie" in var: continue;
        if ".px" in var or  ".py" in var or ".pz" in var: continue;
        var = var.replace("np.","").replace("&","&&").replace("~","!").replace("_p4_from_ptetaphie","ROOT::VecOps::Construct<ROOT::Math::PtEtaPhiEVector>")
        print(key," ",var);
        data_frame = data_frame.Define(key,var);

    ## edit selection string
    selection = data_config['selection'];
    selection = selection.replace("&","&&").replace("~","!").replace("np.","")
    selection += " && jet_pt_raw > 30 && jet_parTlast_probb > 0.5";
    print("###### Add filter to DataFrame ",selection);

    data_frame = data_frame.Filter(selection);
    data_frame_b = data_frame.Filter("label_b == 1");
    data_frame_c = data_frame.Filter("label_c == 1");
    data_frame_uds = data_frame.Filter("label_uds == 1");
    if args.include_lep:
        data_frame_mu = data_frame.Filter("label_mu == 1");
        data_frame_el = data_frame.Filter("label_el == 1");
        data_frame_tau = data_frame.Filter("label_taup_1h0p == 1 || label_taup_1h1p == 1 || label_taup_1h2p == 1 || label_taup_3h0p == 1 || label_taup_3h1p == 1 || label_taum_1h0p == 1 || label_taum_1h1p == 1 || label_taum_1h2p == 1 || label_taum_3h0p == 1 || label_taum_3h1p == 1");

    ## variable and binning
    variables = {"jet_pt_raw": [100,30,400],
                 "jet_eta": [50,-2.5,2.5],
                 "jet_ncand": [50,0,50],
                 "jet_nch": [30,0,30],
                 "jet_nnh": [20,0,20],
                 "jet_nph": [30,0,30],
                 "jet_nel": [5,0,5],
                 "jet_nmu": [5,0,5],
                 "jet_chf": [100,0,1],
                 "jet_nhf": [100,0,1],
                 "jet_phf": [100,0,1],
                 "jet_elf": [100,0,1],
                 "jet_muf": [100,0,1],
                 "jet_parTlast_negprobb": [100,0,1],
                 "jet_parTlast_negprobc": [100,0,1],
                 "jet_parTlast_negprobuds": [100,0,1],
                 "jet_parTlast_negprobg": [100,0,1],
                 "jet_parTlast_negprobmu": [100,0,1],
                 "jet_parTlast_negprobele": [100,0,1],
                 "jet_parTlast_probb": [100,0,1],
                 "jet_parTlast_probc": [100,0,1],
                 "jet_parTlast_probuds": [100,0,1],
                 "jet_parTlast_probg": [100,0,1],
                 "jet_parTlast_probmu": [100,0,1],
                 "jet_parTlast_probele": [100,0,1]
    };
    
    histograms_b, histograms_c, histograms_uds, histograms_mu, histograms_el, histograms_tau = {}, {}, {}, {}, {}, {};

    for key,values in variables.items():
        histograms_b[key] = data_frame_b.Histo1D(("h_"+key+"_b","",values[0],values[1],values[2]),key);
        histograms_c[key] = data_frame_c.Histo1D(("h_"+key+"_c","",values[0],values[1],values[2]),key);
        histograms_uds[key] = data_frame_uds.Histo1D(("h_"+key+"_uds","",values[0],values[1],values[2]),key);
        if args.include_lep:
            histograms_mu[key] = data_frame_mu.Histo1D(("h_"+key+"_mu","",values[0],values[1],values[2]),key);
            histograms_el[key] = data_frame_el.Histo1D(("h_"+key+"_el","",values[0],values[1],values[2]),key);
            histograms_tau[key] = data_frame_tau.Histo1D(("h_"+key+"_tau","",values[0],values[1],values[2]),key);
        
    ## save plots
    os.system("mkdir -p "+args.output_plot_dir);
    ROOT.gStyle.SetOptStat(0);
    c = ROOT.TCanvas("c","c",600,600);

    for key,values in variables.items():
        c.cd();        
        histograms_b[key].Scale(1./histograms_b[key].Integral(-1,histograms_b[key].GetNbinsX()+1));
        histograms_c[key].Scale(1./histograms_c[key].Integral(-1,histograms_c[key].GetNbinsX()+1));
        histograms_uds[key].Scale(1./histograms_uds[key].Integral(-1,histograms_uds[key].GetNbinsX()+1));
        ## include underflow and overflow
        histograms_b[key].GetXaxis().SetRange(-1,histograms_b[key].GetNbinsX()+1);
        histograms_c[key].GetXaxis().SetRange(-1,histograms_c[key].GetNbinsX()+1);
        histograms_uds[key].GetXaxis().SetRange(-1,histograms_uds[key].GetNbinsX()+1);
        histograms_b[key].SetLineColor(ROOT.kBlack);
        histograms_c[key].SetLineColor(ROOT.kBlue);
        histograms_uds[key].SetLineColor(ROOT.kRed);
        histograms_b[key].GetXaxis().SetTitle(key);
        histograms_b[key].GetYaxis().SetTitle("a.u.");
        maximum = max([histograms_b[key].GetMaximum(),histograms_c[key].GetMaximum(),histograms_uds[key].GetMaximum()])
        if args.include_lep:
            histograms_mu[key].Scale(1./histograms_mu[key].Integral(-1,histograms_mu[key].GetNbinsX()+1));
            histograms_el[key].Scale(1./histograms_el[key].Integral(-1,histograms_el[key].GetNbinsX()+1));
            histograms_tau[key].Scale(1./histograms_tau[key].Integral(-1,histograms_tau[key].GetNbinsX()+1));
            histograms_mu[key].GetXaxis().SetRange(-1,histograms_mu[key].GetNbinsX()+1);
            histograms_el[key].GetXaxis().SetRange(-1,histograms_el[key].GetNbinsX()+1);
            histograms_tau[key].GetXaxis().SetRange(-1,histograms_tau[key].GetNbinsX()+1);
            histograms_el[key].SetLineColor(ROOT.kGreen+1);
            histograms_mu[key].SetLineColor(ROOT.kOrange+1);
            histograms_tau[key].SetLineColor(ROOT.kViolet);
            maximum = max([histograms_b[key].GetMaximum(),histograms_c[key].GetMaximum(),histograms_uds[key].GetMaximum(),histograms_mu[key].GetMaximum(),histograms_el[key].GetMaximum(),histograms_tau[key].GetMaximum()])
        histograms_b[key].GetYaxis().SetRangeUser(0.,maximum*1.2);
        histograms_b[key].Draw("hist");
        histograms_c[key].Draw("hist same");
        histograms_uds[key].Draw("hist same");
        if args.include_lep:
            histograms_mu[key].Draw("hist same");
            histograms_el[key].Draw("hist same");
            histograms_tau[key].Draw("hist same");
        histograms_b[key].Draw("hist same");
        leg = ROOT.TLegend(0.75,0.75,0.9,0.9);
        leg.SetFillColor(0);
        leg.SetFillStyle(0);
        leg.SetBorderSize(0);
        leg.SetNColumns(2);
        leg.AddEntry(histograms_b[key].GetPtr(),"b","L");
        leg.AddEntry(histograms_c[key].GetPtr(),"c","L");
        leg.AddEntry(histograms_uds[key].GetPtr(),"uds","L");
        if args.include_lep:
            leg.AddEntry(histograms_mu[key].GetPtr(),"#mu","L");
            leg.AddEntry(histograms_el[key].GetPtr(),"e","L");
            leg.AddEntry(histograms_tau[key].GetPtr(),"#tau","L");
        leg.Draw("same");
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+key+".png","png");
        histograms_b[key].SetMinimum(y_scale_min);
        histograms_b[key].SetMaximum(y_scale_max*maximum);
        c.SetLogy(1);
        c.SaveAs(args.output_plot_dir+"/"+key+"_log.png","png");
        c.SetLogy(0);
