import os
import sys
import glob
import argparse
import subprocess
import shutil
import yaml
from importlib import import_module
import ROOT
import numpy as np
from array import array

ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='', help='directory with input files');
parser.add_argument('-m', '--file-name-mc', nargs="+", default=[], help='list of string to grep to get MC fielist');
parser.add_argument('-d', '--file-name-data', nargs="+", default=[], help='list of string to grep to get Data fielist');
parser.add_argument('-o', '--domain-region', type=str, default='dimuon', choices=['dimuon','emu','mutau','dijet','ttcharm'], help='type of domain to be selected');
parser.add_argument('-c', '--config', type=str, default='', help="data config file of weaver");
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-p', '--output-plot-dir', type=str, default='', help='output plot dir');
parser.add_argument('-f', '--output-file', type=str, default='', help='output root file');
parser.add_argument('-v', '--clip-val', type=float, default=5, help='clipping value to be applied as default');
parser.add_argument('-t', '--no-transf', action='store_true', help='do not transform the input features');
parser.add_argument('-w', '--weight-to-data', action='store_true', help='reweight jet pt and eta to match that of data');

if __name__ == '__main__':

    ROOT.gInterpreter.ProcessLine('#include "CMS_style.h"')
    ROOT.setTDRStyle();
    y_scale_min = 1./10**7;
    y_scale_max = 10.;
    
    args = parser.parse_args()
    xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

    ROOT.EnableImplicitMT(args.nthreads);
    ROOT.EnableThreadSafety();

    ## parse yaml file
    with open(args.config, 'r') as file:
        data_config = yaml.safe_load(file)

    ## take selections, new variables, and inputs
    selection_mc = data_config['selection'];
    selection_data = data_config['selection'];
    new_variables = data_config['new_variables'];
    inputs = data_config['inputs'];

    ## exclude masks    label_mc_zll
    new_variables = {key: value for key, value in new_variables.items() if "mask" not in key };
    inputs = {key: value for key, value in inputs.items() if "mask" not in key };

    ## load the input dataframe                                                                                                                                                                       
    input_files_mc = [];
    if args.file_name_mc:
        for name in args.file_name_mc:
            if not args.use_xrootd:
                input_files_mc.append(args.input_dir+"/*"+name+"*root");
            else:
                input_files_mc.append(xrootd_eos+"/"+args.input_dir+"/*"+name+"*root");
    else:
        sys.exit("Please provide a file name for MC");

    input_files_data = [];
    if args.file_name_data:
        for name in args.file_name_data:
            if not args.use_xrootd:
                input_files_data.append(args.input_dir+"/*"+name+"*root");
            else:
                input_files_data.append(xrootd_eos+"/"+args.input_dir+"/*"+name+"*root");
    else:
        sys.exit("Please provide a file name for Data");

    print("###### Create RDataFrame for ",input_files_mc);
    data_frame_mc = ROOT.RDataFrame("tree",input_files_mc);
    print("###### Create RDataFrame for ",input_files_data);
    data_frame_data = ROOT.RDataFrame("tree",input_files_data);        
    ## add new colums
    print("###### Add new colums to DataFrame");
    for key,var in new_variables.items():        
        var = var.replace("np.","").replace("&","&&").replace("~","!").replace("_p4_from_ptetaphie","ROOT::VecOps::Construct<ROOT::Math::PtEtaPhiEVector>")
        if ".px" in var or ".py" in var or ".pz" in var:
            var1 = var.split(".")[0];
            var2 = var.split(".")[1];
            var = "return ROOT::VecOps::Map("+var1+",[](const ROOT::Math::PtEtaPhiEVector &v){return v."+var2+";})";
            var = var.replace("px","Px()").replace("py","Py()").replace("pz","Pz()")
        print(key," ",var);
        data_frame_mc = data_frame_mc.Define(key,var);
        data_frame_data = data_frame_data.Define(key,var);
        
    ## edit selection string
    selection_mc = selection_mc.replace("&","&&").replace("~","!").replace("np.","")
    selection_data = selection_data.replace("&","&&").replace("~","!").replace("np.","")
    if args.domain_region == "dimuon":
        if not "label_mc_zll" in new_variables or not "label_data_zll" in new_variables:
            sys.exit("label_mc_zll or label_data_zll not found as new variables --> please check or update the script to use the new naming convention");
        selection_mc += " && label_mc_zll"
        selection_data += " && label_data_zll"
    elif args.domain_region == "emu":
        if not "label_mc_emu" in new_variables or not "label_data_emu" in new_variables:
            sys.exit("label_mc_emu or label_data_emu not found as new variables --> please check or update the script to use the new naming convention");
        selection_mc += " && label_mc_emu"
        selection_data += " && label_data_emu"
    elif args.domain_region == "mutau":
        if not "label_mc_mut" in new_variables or not "label_data_mut" in new_variables:
            sys.exit("label_mc_mut or label_data_mut not found as new variables --> please check or update the script to use the new naming convention");
        selection_mc += " && label_mc_mut"
        selection_data += " && label_data_mut"
    elif args.domain_region == "dijet":
        if not "label_mc_dijet" in new_variables or not "label_data_dijet" in new_variables:
            sys.exit("label_mc_dijet or label_data_dijet not found as new variables --> please check or update the script to use the new naming convention");
        selection_mc += " && label_mc_dijet"
        selection_data += " && label_data_dijet"
    elif args.domain_region == "ttcharm":
        if not "label_mc_ttcharm" in new_variables or not "label_data_ttcharm" in new_variables:
            sys.exit("label_mc_ttcharm or label_data_ttcharm not found as new variables --> please check or update the script to use the new naming convention");
        selection_mc += " && label_mc_ttcharm"
        selection_data += " && label_data_ttcharm"
    
    print("###### Add filter to MC DataFrame ",selection_mc);
    data_frame_mc = data_frame_mc.Filter(selection_mc);
    print("###### Add filter to Data DataFrame ",selection_data);
    data_frame_data = data_frame_data.Filter(selection_data);

    histo_weight = None;
    
    if args.weight_to_data:
        print("###### build jet pt-eta distribution in data and MC to derive weights");
        pt_bins =  array('d', [30.,40.,55.,80.,100.,130.,180.,250.,500.,2500.]);
        eta_bins =  array('d', [-2.5,-1.75,-1.0,-0.5,0.0,0.5,1.0,1.75,2.5]);        
        histo_pt_eta_mc = data_frame_mc.Histo2D(("histo_pt_eta_mc","",len(pt_bins)-1,pt_bins,len(eta_bins)-1,eta_bins),"jet_pt_raw","jet_eta","wgt");
        histo_pt_eta_data = data_frame_data.Histo2D(("histo_pt_eta_data","",len(pt_bins)-1,pt_bins,len(eta_bins)-1,eta_bins),"jet_pt_raw","jet_eta");
        histo_pt_eta_data = histo_pt_eta_data.GetPtr();
        histo_pt_eta_mc = histo_pt_eta_mc.GetPtr();
        histo_pt_eta_data.Scale(1./histo_pt_eta_data.Integral());
        histo_pt_eta_mc.Scale(1./histo_pt_eta_mc.Integral());
        histo_weight = histo_pt_eta_data.Clone();
        histo_weight.Divide(histo_pt_eta_mc);
        ROOT.gInterpreter.Declare(
            """ 
            struct HistoEvaluator {
            const TH2D histo;
            HistoEvaluator(TH2D *h) : histo(*h) {}
            float operator()(float x, float y) {
            int ibin = histo.FindFixBin(x,y);
            return histo.GetBinContent(ibin);
            }
            };
            """
        );        
        data_frame_mc = data_frame_mc.Define("weight",ROOT.HistoEvaluator(histo_weight),("jet_pt_raw","jet_eta"));
        
    ## if no transformation compute min and max
    stats_mc = {};
    stats_data = {};
    if args.no_transf:
        print("###### find min and max for each observable as no transformation will be applied")
        for key,var in inputs.items():
            for idx,element in enumerate(var['vars']):
                stats_mc[element[0]] = data_frame_mc.Stats(element[0]);
                stats_data[element[0]] = data_frame_data.Stats(element[0]);

    ## variables with transformation
    print("###### Create histograms for input features in MC --> after transforming when needed");
    histograms_mc = {}
    histograms_data = {}
    for key,var in inputs.items():
        for idx,element in enumerate(var['vars']):
            if not args.no_transf:                
                ## all elements are written in the yaml file
                if len(element) >= 5 and element[1] != None and element[2] != None and element[3] != None and element[4] != None:
                    print("Transform ",element[0]," as ","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2]),
                          " defined in [",element[3],",",element[4],"] range");
                    if args.weight_to_data:
                        h_mc = data_frame_mc.Define(element[0]+"_trans","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])).Histo1D(
                            (element[0]+"_mc","",100,element[3],element[4]),element[0]+"_trans", "weight");
                    else:
                        h_mc = data_frame_mc.Define(element[0]+"_trans","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])).Histo1D(
                            (element[0]+"_mc","",100,element[3],element[4]),element[0]+"_trans");                        
                    h_data = data_frame_data.Define(element[0]+"_trans","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])).Histo1D(
                        (element[0]+"_data","",100,element[3],element[4]),element[0]+"_trans");
                ## min and max not written, we put it as cip-val
                elif len(element) >= 3 and element[1] != None and element[2] != None:
                    print("Transform ",element[0]," as ","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2]),
                          " defined in [-"+str(np.round(args.clip_val,2))+","+str(np.round(args.clip_val,2))+"] range");
                    if args.weight_to_data:
                        h_mc = data_frame_mc.Define(element[0]+"_trans","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])).Histo1D(
                            (element[0]+"_mc","",100,-args.clip_val,args.clip_val),element[0]+"_trans","weight");
                    else:
                        h_mc = data_frame_mc.Define(element[0]+"_trans","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])).Histo1D(
                            (element[0]+"_mc","",100,-args.clip_val,args.clip_val),element[0]+"_trans");
                    h_data = data_frame_data.Define(element[0]+"_trans","("+element[0]+"+"+str(-1*element[1])+")*"+str(element[2])).Histo1D(
                        (element[0]+"_data","",100,-args.clip_val,args.clip_val),element[0]+"_trans");
                    ## no transformation applied
                else:
                    print("No transformation for ",element[0]);
                    if "px" in element[0] or "py" in element[0] or "pz" in element[0]:
                        if args.weight_to_data:
                            h_mc = data_frame_mc.Histo1D((element[0]+"_mc","",100,-250,250),element[0],"weight");
                        else:
                            h_mc = data_frame_mc.Histo1D((element[0]+"_mc","",100,-250,250),element[0]);                            
                        h_data = data_frame_data.Histo1D((element[0]+"_data","",100,-250,250),element[0]);
                    else:
                        if args.weight_to_data:
                            h_mc = data_frame_mc.Histo1D((element[0]+"_mc","",100,0,400),element[0], "weight");
                        else:
                            h_mc = data_frame_mc.Histo1D((element[0]+"_mc","",100,0,400),element[0]);
                        h_data = data_frame_data.Histo1D((element[0]+"_data","",100,0,400),element[0]);
            else:
                min_mc = max(stats_mc[element[0]].GetMean()-5.*stats_mc[element[0]].GetRMS(),stats_mc[element[0]].GetMin());
                max_mc = min(stats_mc[element[0]].GetMean()+5.*stats_mc[element[0]].GetRMS(),stats_mc[element[0]].GetMax());
                if args.weight_to_data:
                    h_mc = data_frame_mc.Histo1D((element[0]+"_mc","",100,min_mc,max_mc),element[0],"weight");
                else:
                    h_mc = data_frame_mc.Histo1D((element[0]+"_mc","",100,min_mc,max_mc),element[0]);
                    
                h_data = data_frame_data.Histo1D((element[0]+"_data","",100,min_mc,max_mc),element[0]);
                
            histograms_mc[element[0]] = h_mc;
            histograms_data[element[0]] = h_data;

    ## save plots
    print("###### Looping on events and fill histograms");
    os.system("mkdir -p "+args.output_plot_dir);
    ROOT.gStyle.SetOptStat(0);
    
    c = ROOT.TCanvas("c","c",600,700);
    c.cd();

    pad1 = ROOT.TPad("pad","pad",0,0,1,1);
    pad1.SetFillColor(0);
    pad1.SetFillStyle(0);
    pad1.SetTickx(1);
    pad1.SetTicky(1);
    pad1.SetBottomMargin(0.3);
    pad1.SetRightMargin(0.06);
    pad1.Draw();

    ROOT.CMS_lumi(pad1,"",False,False,True);

    pad2 = ROOT.TPad("pad2","pad2",0,0.,1,0.9);
    pad2.SetFillColor(0);
    pad2.SetGridy(1);
    pad2.SetFillStyle(0);
    pad2.SetTickx(1);
    pad2.SetTicky(1);
    pad2.SetTopMargin(0.7);
    pad2.SetRightMargin(0.06);
    pad2.Draw();

    for key, histo_mc in histograms_mc.items():
        histo_mc = histo_mc.GetPtr();
        histo_data = histograms_data[key].GetPtr();
        histo_data.Sumw2();
        histo_mc.Sumw2();
	## include underflow and overflow                                                                                                                                                     
        histo_mc.GetXaxis().SetRange(-1,histo_mc.GetNbinsX()+1);
        histo_data.GetXaxis().SetRange(-1,histo_data.GetNbinsX()+1);
        ## normalize data and MC to 1
        histo_mc.Scale(1./histo_mc.Integral(-1,histo_mc.GetNbinsX()+1));
        histo_data.Scale(1./histo_data.Integral(-1,histo_data.GetNbinsX()+1));
        ## style options
        histo_mc.SetLineColor(ROOT.kRed);
        histo_mc.SetLineWidth(2);
        histo_data.SetLineColor(ROOT.kBlack);
        histo_data.SetMarkerColor(ROOT.kBlack);
        histo_data.SetMarkerStyle(20);
        histo_data.SetMarkerSize(1);        
        ## plot distributions
        pad1.cd();
        histo_mc.GetYaxis().SetRangeUser(0.,1.2*max(histo_mc.GetMaximum(),histo_data.GetMaximum()));
        histo_mc.GetYaxis().SetLabelSize(histo_mc.GetYaxis().GetLabelSize()*0.85);
        histo_mc.GetXaxis().SetTitle("");
        histo_mc.GetXaxis().SetLabelSize(0);
        histo_mc.GetYaxis().SetTitle("a.u.");
        histo_mc.Draw("hist");
        histo_data.Draw("E0Psame");
        leg = ROOT.TLegend(0.75,0.75,0.9,0.9);
        leg.SetFillColor(0);
        leg.SetFillStyle(0);
        leg.SetBorderSize(0);
        leg.AddEntry(histo_data,"Data","PEL");
        leg.AddEntry(histo_mc,"MC","L");
        leg.Draw("same");
        pad1.RedrawAxis("sameaxis");
        ## plot ratio
        pad2.cd();
        ratio = ROOT.TGraphAsymmErrors()
        ratio.Divide(histo_data,histo_mc,"pois");
        ratio.SetLineColor(ROOT.kBlack);
        ratio.SetMarkerColor(ROOT.kBlack);
        ratio.SetMarkerStyle(20);
        ratio.SetMarkerSize(1);
        ratio_dummy = histo_data.Clone("ratio");
        ratio_dummy.Divide(histo_mc);
        ratio_dummy.GetXaxis().SetTitle(key);
        ratio_dummy.GetYaxis().SetTitle("Ratio");
        ratio_dummy.GetYaxis().SetRangeUser(0.5,2.);
        ratio_dummy.GetYaxis().SetNdivisions(505);
        ratio_dummy.GetYaxis().SetLabelSize(ratio_dummy.GetYaxis().GetLabelSize()*0.85);
        ratio_dummy.Draw("P");
        line = ROOT.TLine(ratio_dummy.GetXaxis().GetXmin(),1.,ratio_dummy.GetXaxis().GetXmax(),1.);
        line.SetLineColor(ROOT.kRed);
        line.SetLineStyle(7);
        line.Draw("Lsame");
        ratio.Draw("E0Psame");
        pad2.RedrawAxis("sameaxis");
        c.SaveAs(args.output_plot_dir+"/"+key+".png","png");
        pad1.cd();
        histo_mc.SetMinimum(y_scale_min);
        histo_mc.SetMaximum(y_scale_max);
        pad1.SetLogy();
        c.SaveAs(args.output_plot_dir+"/"+key+"_log.png","png");
        pad1.SetLogy(0);        
    
    ## save root file                   
    output_file = ROOT.TFile(args.output_plot_dir+"/"+args.output_file,"RECREATE");
    output_file.mkdir("mc");
    output_file.cd("mc");
    for key,histo in histograms_mc.items():
        histo.Write();
    output_file.cd();
    output_file.mkdir("data");
    output_file.cd("data");
    for key,histo in histograms_data.items():
        histo.Write();
    output_file.cd();
    output_file.Close();
