import os
import sys
import glob
import argparse
import subprocess
import shutil
import time
import ROOT
import numpy as np
from array import array 
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file-name', type=str, default='', help='file name to grep');
parser.add_argument('-o', '--output-dir', type=str, default='', help='output directgory for plots');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-p', '--pt-bins', nargs="+", type=float, default=[30,1000], help='pt bins to build ROCs');
parser.add_argument('-e', '--eta-bins', nargs="+", type=float, default=[-2.5,2.5], help='eta bins to build ROCs');
parser.add_argument('-s', '--samples', nargs="+", type=int, default=[],help='sample to select according to sample ID');
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-j', '--use-jec', action='store_true', help='use pt corrected by JECs');
parser.add_argument('-c', '--use-charge-cut', action='store_true', help='apply the charge confidence cut as preselection');
parser.add_argument('-m', '--use-hps-matching', action='store_true', help='apply the matching between jet and hps');
parser.add_argument('-d', '--select-decay-mode', type=int, default=-1, help='select based on HPS decay modes', choices=[0,1,2,10,11]);
parser.add_argument('--min-tau-pt', type=float, default=20., help='min tau pT for HPS matching');
parser.add_argument('--max-tau-eta', type=float, default=2.3, help='max tau eta for HPS matching');

inputs = {
    "ParTNoJEC": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassReg/trainingAK4_taumuel_classreg_transformer_21052023",
    "ParTNoJECNew3D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_3d_31012024",
    "ParTNoJECNew5D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_31012024",
    "ParTNoJECNewDyn": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_dyn_02032024"
}

start_time = time.time()
ROOT.gInterpreter.ProcessLine('#include "commonTools.h"')
ROOT.setTDRStyle();

args = parser.parse_args()
print(args)
xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

ROOT.EnableImplicitMT(args.nthreads);
ROOT.EnableThreadSafety();

os.system("mkdir -p "+args.output_dir);

## preselections based on hps
preselection = str()
if args.use_hps_matching:
    preselection += " jet_taumatch_pt >= "+str(args.min_tau_pt)+" abs(jet_taumatch_eta) <= "+str(args.max_tau_eta);
if args.select_decay_mode and args.select_decay_mode >= 0:
    if preselection != "":
        preselection += " && "
    preselection += " jet_taumatch_decaymode == "+str(args.select_decay_mode);
    
## preselection based on charge reconstruction
if args.use_charge_cut:
    if preselection != "":
        preselection += " && "
    preselection += "abs(score_label_taup_1h0p+score_label_taup_1h1p+score_label_taup_1h2p+score_label_taup_3h0p+score_label_taup_3h1p-score_label_taum_1h0p+score_label_taum_1h1p+score_label_taum_1h2p+score_label_taum_3h0p+score_label_taum_3h1p)/(score_label_taup_1h0p+score_label_taup_1h1p+score_label_taup_1h2p+score_label_taup_3h0p+score_label_taup_3h1p+score_label_taum_1h0p+score_label_taum_1h1p+score_label_taum_1h2p+score_label_taum_3h0p+score_label_taum_3h1p) > 0.5"

## sample preselection
for i,isamp in enumerate(args.samples):
    if i == 0:
        if preselection != "":
            preselection += " && (";
        else:
            preselection += "(";
    if i == len(args.samples)-1:
        preselection += " sample == "+str(isamp)+")";
    else:
        preselection += " sample == "+str(isamp)+" || ";

print("Preselection string --> ",preselection);
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)

### histograms
hist_sig_tau_vs_c = {};
hist_sig_tau_vs_uds = {};
hist_sig_tau_vs_g = {};
hist_sig_tau_vs_b = {};
hist_sig_tau_vs_jet = {};
hist_sig_tau_vs_mu = {};
hist_sig_tau_vs_el = {};
hist_bkg_tau_vs_c = {};
hist_bkg_tau_vs_uds = {};
hist_bkg_tau_vs_g = {};
hist_bkg_tau_vs_b = {};
hist_bkg_tau_vs_jet = {};
hist_bkg_tau_vs_mu = {};
hist_bkg_tau_vs_el = {};
roc_tau_vs_c = {}
roc_tau_vs_uds = {}
roc_tau_vs_g = {}
roc_tau_vs_b = {}
roc_tau_vs_mu = {}
roc_tau_vs_el = {}
roc_tau_vs_jet = {}

## define binnings
pt_bins = array('d',args.pt_bins);
eta_bins = array('d',args.eta_bins);
score_bins = array('d',[i for i in np.arange(0.,1.0005,0.0005)])
print("pt_bins: ",pt_bins);
print("eta_bins: ",eta_bins);
print("score_bins: from 0 to 1 in step of 0.0001 --> len ",len(score_bins));

### tau score
tau_score ="score_label_taup_1h0p+score_label_taup_1h1p+score_label_taup_1h2p+score_label_taup_3h0p+score_label_taup_3h1p+score_label_taum_1h0p+score_label_taum_1h1p+score_label_taum_1h2p+score_label_taum_3h0p+score_label_taum_3h1p";

for key,val in inputs.items():
    start_time = time.time()
    print("Run on input files: key ",key," location: ",val);
    ## build the dataframe
    data_frame = ROOT.RDataFrame("Events",xrootd_eos+val+"/*"+args.file_name+"*root" if args.use_xrootd else val+"/*"+args.file_name+"*root");
    ## apply preselection
    data_frame = data_frame.Filter(preselection)
    ## define scores
    data_frame = data_frame.Define("tau_vs_c",tau_score+"/("+tau_score+"+score_label_c)");
    data_frame = data_frame.Define("tau_vs_b",tau_score+"/("+tau_score+"+score_label_b)");
    data_frame = data_frame.Define("tau_vs_uds",tau_score+"/("+tau_score+"+score_label_uds)");
    data_frame = data_frame.Define("tau_vs_g",tau_score+"/("+tau_score+"+score_label_g)");
    data_frame = data_frame.Define("tau_vs_mu",tau_score+"/("+tau_score+"+score_label_mu)");
    data_frame = data_frame.Define("tau_vs_el",tau_score+"/("+tau_score+"+score_label_el)");
    data_frame = data_frame.Define("tau_vs_jet",tau_score+"/("+tau_score+"+score_label_b+score_label_c+score_label_uds+score_label_g)");
    ## apply truth label filters
    data_frame_b = data_frame.Filter("label_b==1");
    data_frame_c = data_frame.Filter("label_c==1");
    data_frame_uds = data_frame.Filter("label_uds==1");
    data_frame_g = data_frame.Filter("label_g==1");
    data_frame_tau = data_frame.Filter("label_taup_1h0p==1 || label_taup_1h1p==1 || label_taup_1h2p==1 || label_taup_3h0p==1 || label_taup_3h1p==1 || label_taum_1h0p==1 || label_taum_1h1p==1 || label_taum_1h2p==1 || label_taum_3h0p==1 || label_taum_3h1p==1");
    data_frame_jet = data_frame.Filter("label_b==1 || label_c==1 || label_uds==1 || label_g==1");
    data_frame_mu = data_frame.Filter("label_mu==1");
    data_frame_el = data_frame.Filter("label_el==1");
    ## define the histograms as 3D to speed up RDF computation
    histo_list = [];
    pt_name = "jet_pt_raw";
    if args.use_jec:
        pt_name = "jet_pt"
    sig_tau_vs_c = data_frame_tau.Histo3D(("sig_tau_vs_c_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_c");
    sig_tau_vs_uds = data_frame_tau.Histo3D(("sig_tau_vs_uds_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_uds");
    sig_tau_vs_g = data_frame_tau.Histo3D(("sig_tau_vs_g_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_g");
    sig_tau_vs_b = data_frame_tau.Histo3D(("sig_tau_vs_b_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_b");
    sig_tau_vs_mu = data_frame_tau.Histo3D(("sig_tau_vs_mu_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_mu");
    sig_tau_vs_el = data_frame_tau.Histo3D(("sig_tau_vs_el_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_el");
    sig_tau_vs_jet = data_frame_tau.Histo3D(("sig_tau_vs_jet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_jet");
    bkg_tau_vs_c = data_frame_c.Histo3D(("bkg_tau_vs_c_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_c");
    bkg_tau_vs_uds = data_frame_uds.Histo3D(("bkg_tau_vs_uds_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_uds");
    bkg_tau_vs_g = data_frame_g.Histo3D(("bkg_tau_vs_g_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_g");
    bkg_tau_vs_b = data_frame_b.Histo3D(("bkg_tau_vs_b_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_b");
    bkg_tau_vs_mu = data_frame_mu.Histo3D(("bkg_tau_vs_mu_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_mu");    
    bkg_tau_vs_el = data_frame_el.Histo3D(("bkg_tau_vs_el_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_el");    
    bkg_tau_vs_jet = data_frame_jet.Histo3D(("bkg_tau_vs_jet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","tau_vs_jet");    
    histo_list.extend([sig_tau_vs_c,sig_tau_vs_uds,sig_tau_vs_g,sig_tau_vs_b,sig_tau_vs_mu,sig_tau_vs_el,sig_tau_vs_jet]);
    histo_list.extend([bkg_tau_vs_c,bkg_tau_vs_uds,bkg_tau_vs_g,bkg_tau_vs_b,bkg_tau_vs_mu,bkg_tau_vs_el,bkg_tau_vs_jet]);
    ## Run histogram creation
    ROOT.RDF.RunGraphs(histo_list);
    sig_tau_vs_c = sig_tau_vs_c.GetPtr();
    sig_tau_vs_uds = sig_tau_vs_uds.GetPtr();
    sig_tau_vs_g = sig_tau_vs_g.GetPtr();
    sig_tau_vs_b = sig_tau_vs_b.GetPtr();
    sig_tau_vs_mu = sig_tau_vs_mu.GetPtr();
    sig_tau_vs_el = sig_tau_vs_el.GetPtr();
    sig_tau_vs_jet = sig_tau_vs_jet.GetPtr();
    bkg_tau_vs_c = bkg_tau_vs_c.GetPtr();
    bkg_tau_vs_uds = bkg_tau_vs_uds.GetPtr();
    bkg_tau_vs_g = bkg_tau_vs_g.GetPtr();
    bkg_tau_vs_b = bkg_tau_vs_b.GetPtr();    
    bkg_tau_vs_mu = bkg_tau_vs_mu.GetPtr();    
    bkg_tau_vs_el = bkg_tau_vs_el.GetPtr();    
    bkg_tau_vs_jet = bkg_tau_vs_jet.GetPtr();    
    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)
    ## build the ROCs and projecting histograms
    print("Produce the corresponding ROCs by projecting histograms");
    start_time = time.time()
    for ipt in range(0,len(pt_bins)-1):
        for ieta in range(0,len(eta_bins)-1):
            name = key+"_pt_"+str(ipt)+"_eta_"+str(ieta);

            hist_sig_tau_vs_c[name] = ROOT.extractScore(sig_tau_vs_c,ipt,ieta,"hist_sig_tau_vs_c_"+name)
            hist_sig_tau_vs_uds[name] = ROOT.extractScore(sig_tau_vs_uds,ipt,ieta,"hist_sig_tau_vs_uds_"+name)
            hist_sig_tau_vs_g[name] = ROOT.extractScore(sig_tau_vs_g,ipt,ieta,"hist_sig_tau_vs_g_"+name)
            hist_sig_tau_vs_b[name] = ROOT.extractScore(sig_tau_vs_b,ipt,ieta,"hist_sig_tau_vs_b_"+name)
            hist_sig_tau_vs_mu[name] = ROOT.extractScore(sig_tau_vs_mu,ipt,ieta,"hist_sig_tau_vs_mu_"+name)
            hist_sig_tau_vs_el[name] = ROOT.extractScore(sig_tau_vs_el,ipt,ieta,"hist_sig_tau_vs_el_"+name)
            hist_sig_tau_vs_jet[name] = ROOT.extractScore(sig_tau_vs_jet,ipt,ieta,"hist_sig_tau_vs_jet_"+name)
            hist_bkg_tau_vs_c[name] = ROOT.extractScore(bkg_tau_vs_c,ipt,ieta,"hist_bkg_tau_vs_c_"+name)
            hist_bkg_tau_vs_uds[name] = ROOT.extractScore(bkg_tau_vs_uds,ipt,ieta,"hist_bkg_tau_vs_uds_"+name)
            hist_bkg_tau_vs_g[name] = ROOT.extractScore(bkg_tau_vs_g,ipt,ieta,"hist_bkg_tau_vs_g_"+name)
            hist_bkg_tau_vs_b[name] = ROOT.extractScore(bkg_tau_vs_b,ipt,ieta,"hist_bkg_tau_vs_b_"+name)
            hist_bkg_tau_vs_mu[name] = ROOT.extractScore(bkg_tau_vs_mu,ipt,ieta,"hist_bkg_tau_vs_mu_"+name)
            hist_bkg_tau_vs_el[name] = ROOT.extractScore(bkg_tau_vs_el,ipt,ieta,"hist_bkg_tau_vs_el_"+name)
            hist_bkg_tau_vs_jet[name] = ROOT.extractScore(bkg_tau_vs_jet,ipt,ieta,"hist_bkg_tau_vs_jet_"+name)

            hist_sig_tau_vs_c[name].Scale(1./hist_sig_tau_vs_c[name].Integral());
            hist_sig_tau_vs_uds[name].Scale(1./hist_sig_tau_vs_uds[name].Integral());
            hist_sig_tau_vs_g[name].Scale(1./hist_sig_tau_vs_g[name].Integral());
            hist_sig_tau_vs_b[name].Scale(1./hist_sig_tau_vs_b[name].Integral());
            hist_sig_tau_vs_mu[name].Scale(1./hist_sig_tau_vs_mu[name].Integral());
            hist_sig_tau_vs_el[name].Scale(1./hist_sig_tau_vs_el[name].Integral());
            hist_sig_tau_vs_jet[name].Scale(1./hist_sig_tau_vs_jet[name].Integral());
            hist_bkg_tau_vs_c[name].Scale(1./hist_bkg_tau_vs_c[name].Integral());
            hist_bkg_tau_vs_uds[name].Scale(1./hist_bkg_tau_vs_uds[name].Integral());
            hist_bkg_tau_vs_g[name].Scale(1./hist_bkg_tau_vs_g[name].Integral());
            hist_bkg_tau_vs_b[name].Scale(1./hist_bkg_tau_vs_b[name].Integral());
            hist_bkg_tau_vs_jet[name].Scale(1./hist_bkg_tau_vs_jet[name].Integral());
            
            roc_tau_vs_c[name] = ROOT.buildROC(hist_sig_tau_vs_c[name],hist_bkg_tau_vs_c[name]);
            roc_tau_vs_uds[name] = ROOT.buildROC(hist_sig_tau_vs_uds[name],hist_bkg_tau_vs_uds[name]);
            roc_tau_vs_g[name] = ROOT.buildROC(hist_sig_tau_vs_g[name],hist_bkg_tau_vs_g[name]);
            roc_tau_vs_b[name] = ROOT.buildROC(hist_sig_tau_vs_b[name],hist_bkg_tau_vs_b[name]);
            roc_tau_vs_mu[name] = ROOT.buildROC(hist_sig_tau_vs_mu[name],hist_bkg_tau_vs_mu[name]);
            roc_tau_vs_el[name] = ROOT.buildROC(hist_sig_tau_vs_el[name],hist_bkg_tau_vs_el[name]);
            roc_tau_vs_jet[name] = ROOT.buildROC(hist_sig_tau_vs_jet[name],hist_bkg_tau_vs_jet[name]);

    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)

### From TGraph to TF1
print("Plotting final ROCs");
start_time = time.time()
for ipt in range(0,len(args.pt_bins)-1):
    for ieta in range(0,len(args.eta_bins)-1):
        rocs_to_plot_tau_vs_c = [];
        rocs_to_plot_tau_vs_uds = [];
        rocs_to_plot_tau_vs_g = [];
        rocs_to_plot_tau_vs_b = []
        rocs_to_plot_tau_vs_mu = []
        rocs_to_plot_tau_vs_el = []
        rocs_to_plot_tau_vs_jet = []
        legends = [];
        bin_label = "pt_"+str(ipt)+"_eta_"+str(ieta);
        bin_name = "%d < p_{T} < %d GeV, %.1f < |#eta| < %.1f"%(args.pt_bins[ipt],args.pt_bins[ipt+1],args.eta_bins[ieta],args.eta_bins[ieta+1]);
        for key in inputs:

            name = key+"_"+bin_label;
            rocs_to_plot_tau_vs_c.append(roc_tau_vs_c[name]);
            rocs_to_plot_tau_vs_uds.append(roc_tau_vs_uds[name]);
            rocs_to_plot_tau_vs_g.append(roc_tau_vs_g[name]);
            rocs_to_plot_tau_vs_b.append(roc_tau_vs_b[name]);
            rocs_to_plot_tau_vs_mu.append(roc_tau_vs_mu[name]);
            rocs_to_plot_tau_vs_el.append(roc_tau_vs_el[name]);
            rocs_to_plot_tau_vs_jet.append(roc_tau_vs_jet[name]);
            
            hist_sig_tau_vs_c[name].Rebin(10);
            hist_sig_tau_vs_uds[name].Rebin(10);
            hist_sig_tau_vs_g[name].Rebin(10);
            hist_sig_tau_vs_b[name].Rebin(10);
            hist_sig_tau_vs_mu[name].Rebin(10);
            hist_sig_tau_vs_el[name].Rebin(10);
            hist_sig_tau_vs_jet[name].Rebin(10);
            hist_bkg_tau_vs_c[name].Rebin(10);
            hist_bkg_tau_vs_uds[name].Rebin(10);
            hist_bkg_tau_vs_g[name].Rebin(10);
            hist_bkg_tau_vs_b[name].Rebin(10);
            hist_bkg_tau_vs_mu[name].Rebin(10);
            hist_bkg_tau_vs_el[name].Rebin(10);
            hist_bkg_tau_vs_jet[name].Rebin(10);

            hist_sig_tau_vs_c[name].Scale(1./hist_sig_tau_vs_c[name].Integral());
            hist_sig_tau_vs_uds[name].Scale(1./hist_sig_tau_vs_uds[name].Integral());
            hist_sig_tau_vs_g[name].Scale(1./hist_sig_tau_vs_g[name].Integral());
            hist_sig_tau_vs_b[name].Scale(1./hist_sig_tau_vs_b[name].Integral());
            hist_sig_tau_vs_mu[name].Scale(1./hist_sig_tau_vs_mu[name].Integral());
            hist_sig_tau_vs_el[name].Scale(1./hist_sig_tau_vs_el[name].Integral());
            hist_sig_tau_vs_jet[name].Scale(1./hist_sig_tau_vs_jet[name].Integral());
            hist_bkg_tau_vs_c[name].Scale(1./hist_bkg_tau_vs_c[name].Integral());
            hist_bkg_tau_vs_uds[name].Scale(1./hist_bkg_tau_vs_uds[name].Integral());
            hist_bkg_tau_vs_g[name].Scale(1./hist_bkg_tau_vs_g[name].Integral());
            hist_bkg_tau_vs_b[name].Scale(1./hist_bkg_tau_vs_b[name].Integral());
            hist_bkg_tau_vs_mu[name].Scale(1./hist_bkg_tau_vs_mu[name].Integral());
            hist_bkg_tau_vs_el[name].Scale(1./hist_bkg_tau_vs_el[name].Integral());
            hist_bkg_tau_vs_jet[name].Scale(1./hist_bkg_tau_vs_jet[name].Integral());
            
            legends.append(key);
            
            legends_tmp = [];
            legends_tmp.append(name);

            labels_tmp = [];
            labels_tmp.append("Sig: #tau_{h} vs c");
            labels_tmp.append("Sig: #tau_{h} vs uds");
            labels_tmp.append("Sig: #tau_{h} vs g");
            labels_tmp.append("Bkg: #tau_{h} vs c");
            labels_tmp.append("Bkg: #tau_{h}b vs uds");
            labels_tmp.append("Bkg: #tau_{h} vs g");


            ROOT.plotScore([hist_sig_tau_vs_c[name]],[hist_sig_tau_vs_uds[name]],[hist_sig_tau_vs_g[name]],[hist_bkg_tau_vs_c[name]],[hist_bkg_tau_vs_uds[name]],[hist_bkg_tau_vs_g[name]],
                           legends_tmp,labels_tmp,bin_name,args.output_dir,"score_"+name+"_"+bin_label);
            
            
            labels_tmp = [];
            labels_tmp.append("Sig: #tau_{h} vs jet");
            labels_tmp.append("Sig: #tau_{h} vs #mu");
            labels_tmp.append("Sig: #tau_{h} vs e");
            labels_tmp.append("Bkg: #tau_{h} vs jet");
            labels_tmp.append("Bkg: #tau_{h}b vs #mu");
            labels_tmp.append("Bkg: #tau_{h} vs e");
            
            ROOT.plotScore([hist_sig_tau_vs_jet[name]],[hist_sig_tau_vs_mu[name]],[hist_sig_tau_vs_el[name]],[hist_bkg_tau_vs_jet[name]],[hist_bkg_tau_vs_mu[name]],[hist_bkg_tau_vs_el[name]],
                           legends_tmp,labels_tmp,bin_name,args.output_dir,"score_jl_"+name+"_"+bin_label);

        labels = [];
        labels.append("#tau_{h} vs uds");
        labels.append("#tau_{h} vs g");
        labels.append("#tau_{h} vs c");
        labels.append("#tau_{h} vs b");
        ROOT.plotROC(rocs_to_plot_tau_vs_uds,rocs_to_plot_tau_vs_g,rocs_to_plot_tau_vs_c,rocs_to_plot_tau_vs_b,[],[],legends,labels,bin_name,args.output_dir,"roc_btag_"+bin_label);

        labels = [];
        labels.append("#tau_{h} vs jet");
        labels.append("#tau_{h} vs #mu");
        labels.append("#tau_{h} vs e");
        ROOT.plotROC(rocs_to_plot_tau_vs_jet,rocs_to_plot_tau_vs_mu,rocs_to_plot_tau_vs_el,[],[],[],legends,labels,bin_name,args.output_dir,"roc_btag_jl_"+bin_label);

stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)
