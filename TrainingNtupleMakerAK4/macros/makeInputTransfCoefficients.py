import os
import sys
import glob
import argparse
import subprocess
import shutil
import yaml
from importlib import import_module

import ROOT
import numpy as np
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='', help='directory with input files');
parser.add_argument('-s', '--string-to-grep', nargs="+", default=[], help='list of string to grep to get fielist');
parser.add_argument('-d', '--data-config', type=str, default='', help="data config file of weaver");
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-o', '--output-file', type=str, default='', help='output file');
parser.add_argument('-p', '--output-plot-dir', type=str, default='', help='output plot dir');
parser.add_argument('-c', '--clip-val', type=float, default=6, help='clipping value to be applied as default');
parser.add_argument('-v', '--var-exclude', nargs="+", default=[],help='list of variables for which no transformation will be applied');

if __name__ == '__main__':

    ROOT.gInterpreter.ProcessLine('#include "CMS_style.h"')
    ROOT.setTDRStyle();

    nbin = 500;
    rebin_factor = 2000;
    y_scale_min = 1./10**7;
    y_scale_max = 10;
    
    args = parser.parse_args()
    xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

    ROOT.EnableImplicitMT(args.nthreads);
    ROOT.EnableThreadSafety();
    ## parse yaml file
    with open(args.data_config, 'r') as file:
        data_config = yaml.safe_load(file)

    ## take selections, new variables, and inputs
    selection = data_config['selection'];
    inputs = data_config['inputs'];

    ## exclude masks    
    inputs = {key: value for key, value in inputs.items() if "mask" not in key };

    ## load the input dataframe
    input_files = [];
    if args.string_to_grep:
        for name in args.string_to_grep:
            if not args.use_xrootd:
                input_files.append(args.input_dir+"/*"+name+"*root");
            else:
                input_files.append(xrootd_eos+"/"+args.input_dir+"/*"+name+"*root");
    else:
        if not args.use_xrootd:
            input_files.append(args.input_dir+"/*root");
        else:
            input_files.append(xrootd_eos+"/"+args.input_dir+"/*root");
            
    print("###### Create RDataFrame for ",input_files);
    data_frame = ROOT.RDataFrame("tree",input_files);
        
    ## edit selection string
    selection = selection.replace("&","&&").replace("~","!").replace("np.","")
    print("###### Add filter to DataFrame ",selection);
    data_frame = data_frame.Filter(selection);

    ## loop over inputs
    print("###### Compute Mean, Stdv, Max and Min for every input var");
    stats = {}
    for key,var in inputs.items():
        for idx,element in enumerate(var['vars']):            
            if len(element) > 1:
                if None == element[1]: continue;
            ## for integer variables
            stats[element[0]] = data_frame.Stats(element[0]);

    result = {};
    histograms = {}
    histograms_trans = {}
    histograms_norm = {}
    stats_trans = {}
    stats_norm = {}
    
    for key,value in stats.items():
        mean = value.GetMean();
        stddev = value.GetRMS();
        val_max = value.GetMax();
        val_min = value.GetMin();
        if any(vars in key for vars in args.var_exclude):
            result[key] = [float(mean),float(1./stddev),float((val_min-mean)/stddev),float((val_max-mean)/stddev)]
        else:
            result[key] = [float(mean),float(1./stddev),float(max(-args.clip_val,np.round((val_min-mean)/stddev))-1),float(min(args.clip_val,np.round((val_max-mean)/stddev))+1)]
            
        ## define first transformation with clipping
        print("var name = ",key," values = ",result[key]);
        var_new = "(x+"+str(-1*result[key][0])+")*"+str(result[key][1]);        
        data_frame = data_frame.Define(key+"_trans","return ROOT::VecOps::Map("+key+",[](const float & x){ if ("+var_new+" < "+str(result[key][2])+") return "+str(result[key][2])+"; else if ("+var_new+" > "+str(result[key][3])+") return "+str(result[key][3])+"; else return"+var_new+";})"); 
        stats_trans[key] = data_frame.Stats(key+"_trans");
        histograms_trans[key] = data_frame.Histo1D(("h_"+key+"_trans","",nbin*rebin_factor,result[key][2],result[key][3]),key+"_trans");
        histograms[key] = data_frame.Histo1D(("h_"+key,"",nbin*rebin_factor,val_min,val_max),key);
                                             
    print("###### Compute Mean and stddev post transformation and residual re-shape");
    for key,stats in stats_trans.items():
        print("var name = ",key," : mean ",stats.GetMean()," stddev ",stats.GetRMS(), " frac dw ", histograms_trans[key].GetBinContent(-1)/histograms_trans[key].Integral(-1,histograms_trans[key].GetNbinsX()+1)," frac up ",histograms_trans[key].GetBinContent(histograms_trans[key].GetNbinsX()+1)/histograms_trans[key].Integral(-1,histograms_trans[key].GetNbinsX()+1));
        result[key][1] *= float(1./stats.GetRMS());
        var_new = "(x+"+str(-1*result[key][0])+")*"+str(result[key][1]);
        data_frame = data_frame.Define(key+"_norm","return ROOT::VecOps::Map("+key+",[](const float & x){ if ("+var_new+" < "+str(result[key][2])+") return "+str(result[key][2])+"; else if ("+var_new+" > "+str(result[key][3])+") return "+str(result[key][3])+"; else return"+var_new+";})");
        stats_norm[key] = data_frame.Stats(key+"_norm");
        histograms_norm[key] = data_frame.Histo1D(("h_"+key+"_norm","",nbin*rebin_factor,result[key][2],result[key][3]),key+"_norm");

    print("###### Compute final results after final transformation");
    for key,stats in stats_norm.items():
        print("var name = ",key," : mean ",stats.GetMean()," stddev ",stats.GetRMS(), " frac dw ", histograms_norm[key].GetBinContent(-1)/histograms_norm[key].Integral(-1,histograms_norm[key].GetNbinsX()+1)," frac up ",histograms_norm[key].GetBinContent(histograms_norm[key].GetNbinsX()+1)/histograms_norm[key].Integral(-1,histograms_norm[key].GetNbinsX()+1));
       
    ## modify the yaml file
    print("##### Modify the yaml file");
    for key, element in result.items():
        for k,v in inputs.items():
            for idx,entry in enumerate(v['vars']):
                if entry[0] == key:
                    data_config['inputs'][k]['vars'][idx].extend(element);
                    print(data_config['inputs'][k]['vars'][idx])
                    
    ## save output files
    print("###### Save output files");
    with open(args.output_file, 'w') as outfile:
        yaml.preserve_quotes = False
        yaml.safe_dump(data_config,outfile,sort_keys=False)

    ## save plots
    os.system("mkdir -p "+args.output_plot_dir);
    ROOT.gStyle.SetOptStat(111111);    
    c = ROOT.TCanvas("c","c",600,600);
    ## plot transformed histograms
    for key,histo in histograms_norm.items():
        c.cd();
        histo.Rebin(rebin_factor);
        histo.Scale(1./histo.Integral(-1,histo.GetNbinsX()+1));
        ## include underflow and overflow
        histo.GetXaxis().SetRange(-1,histo.GetNbinsX()+1);
        histo.GetXaxis().SetTitle(key);
        histo.GetYaxis().SetTitle("a.u.");
        histo.Draw("hist");
        histo.GetYaxis().SetRangeUser(0.,histo.GetMaximum()*1.2);
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+histo.GetName()+"_transformed.png","png");
        histo.SetMinimum(y_scale_min);
        histo.SetMaximum(y_scale_max);
        c.SetLogy(1);
        c.SaveAs(args.output_plot_dir+"/"+histo.GetName()+"_transformed_log.png","png");
        c.SetLogy(0);
    ## plot original histograms (no transformation)
    for key,histo in histograms.items():
        c.cd();
        histo.Rebin(rebin_factor);
        histo.Scale(1./histo.Integral(-1,histo.GetNbinsX()+1));
        ## include underflow and overflow
        histo.GetXaxis().SetRange(-1,histo.GetNbinsX()+1);
        histo.GetXaxis().SetTitle(key);
        histo.GetYaxis().SetTitle("a.u.");
        histo.Draw("hist");
        histo.GetYaxis().SetRangeUser(0.,histo.GetMaximum()*1.2);
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+histo.GetName()+".png","png");
        histo.SetMinimum(y_scale_min);
        histo.SetMaximum(y_scale_max);
        c.SetLogy(1);
        c.SaveAs(args.output_plot_dir+"/"+histo.GetName()+"_log.png","png");
        c.SetLogy(0);
        
    ## save root file
    output_file_root = ROOT.TFile(args.output_file.replace(".yaml",".root"),"RECREATE");
    output_file_root.cd();
    for key,histo in histograms_norm.items():
        histo.Write();
    output_file_root.Close();
