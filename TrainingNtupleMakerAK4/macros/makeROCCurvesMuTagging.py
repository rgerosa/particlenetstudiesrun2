import os
import sys
import glob
import argparse
import subprocess
import shutil
import time
import ROOT
import numpy as np
from array import array 
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file-name', type=str, default='', help='file name to grep');
parser.add_argument('-o', '--output-dir', type=str, default='', help='output directgory for plots');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-p', '--pt-bins', nargs="+", type=float, default=[30,1000], help='pt bins to build ROCs');
parser.add_argument('-e', '--eta-bins', nargs="+", type=float, default=[-2.5,2.5], help='eta bins to build ROCs');
parser.add_argument('-s', '--samples', nargs="+", type=int, default=[],help='sample to select according to sample ID');
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-j', '--use-jec', action='store_true', help='use pt corrected by JECs');
parser.add_argument('-u', '--use-reco-matching', action='store_true', help='apply mathing between jet and reco-electron');
parser.add_argument('--min-mu-pt', type=float, default=10., help='min muon pT for RECO matching');
parser.add_argument('--max-mu-eta', type=float, default=2.5, help='max muon eta for RECO matching');

inputs = {
    "ParTNoJEC": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassReg/trainingAK4_taumuel_classreg_transformer_21052023",
    "ParTNoJECNew3D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_3d_31012024",
    "ParTNoJECNew5D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_31012024",
    "ParTNoJECNewDyn": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_dyn_02032024"
}

start_time = time.time()
ROOT.gInterpreter.ProcessLine('#include "commonTools.h"')
ROOT.setTDRStyle();

args = parser.parse_args()
print(args)
xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

ROOT.EnableImplicitMT(args.nthreads);
ROOT.EnableThreadSafety();

os.system("mkdir -p "+args.output_dir);

preselection = "label_el != 1";

if args.use_reco_matching:
    preselection += " && jet_mumatch_pt >= "+str(args.min_mu_pt)+" && abs(jet_mumatch_eta) <= "+str(args.max_mu_eta);

for i,isamp in enumerate(args.samples):
    if i == 0:
        preselection += " && (";
    if i == len(args.samples)-1:
        preselection += " sample == "+str(isamp)+")";
    else:
        preselection += " sample == "+str(isamp)+" || ";
print("Preselection string --> ",preselection);
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)

### histograms
hist_sig_mu_vs_b = {};
hist_sig_mu_vs_c = {};
hist_sig_mu_vs_uds = {};
hist_sig_mu_vs_g = {};
hist_sig_mu_vs_jet = {};
hist_sig_mu_vs_tau = {};
hist_bkg_mu_vs_b = {};
hist_bkg_mu_vs_c = {};
hist_bkg_mu_vs_uds = {};
hist_bkg_mu_vs_g = {};
hist_bkg_mu_vs_jet = {};
hist_bkg_mu_vs_tau = {};
roc_mu_vs_b = {}
roc_mu_vs_c = {}
roc_mu_vs_uds = {}
roc_mu_vs_g = {}
roc_mu_vs_jet = {}
roc_mu_vs_tau = {}

## define binnings
pt_bins = array('d',args.pt_bins);
eta_bins = array('d',args.eta_bins);
score_bins = array('d',[i for i in np.arange(0.,1.0005,0.0005)])
print("pt_bins: ",pt_bins);
print("eta_bins: ",eta_bins);
print("score_bins: from 0 to 1 in step of 0.0001 --> len ",len(score_bins));

## 
for key,val in inputs.items():
    start_time = time.time()
    print("Run on input files: key ",key," location: ",val);
    ## build the dataframe
    data_frame = ROOT.RDataFrame("Events",xrootd_eos+val+"/*"+args.file_name+"*root" if args.use_xrootd else val+"/*"+args.file_name+"*root");
    ## apply preselection
    data_frame = data_frame.Filter(preselection)
    ## apply truth label filters
    data_frame_b = data_frame.Filter("label_b==1");
    data_frame_c = data_frame.Filter("label_c==1");
    data_frame_uds = data_frame.Filter("label_uds==1");
    data_frame_g = data_frame.Filter("label_g==1");
    data_frame_mu = data_frame.Filter("label_mu==1");
    data_frame_jet = data_frame.Filter("label_b==1 || label_c==1 || label_uds==1 || label_g==1");
    data_frame_tau = data_frame.Filter("label_taup_1h0p==1 || label_taup_1h1p==1 || label_taup_1h2p==1 || label_taup_3h0p==1 || label_taup_3h1p==1 || label_taum_1h0p==1 || label_taum_1h1p==1 || label_taum_1h2p==1 || label_taum_3h0p==1 || label_taum_3h1p==1");
    histo_list = [];
    pt_name = "jet_pt_raw";
    if args.use_jec:
        pt_name = "jet_pt"
    sig_mu_vs_b = data_frame_mu.Histo3D(("sig_mu_vs_b_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    sig_mu_vs_uds = data_frame_mu.Histo3D(("sig_mu_vs_uds_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    sig_mu_vs_g = data_frame_mu.Histo3D(("sig_mu_vs_g_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    sig_mu_vs_c = data_frame_mu.Histo3D(("sig_mu_vs_c_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    sig_mu_vs_tau = data_frame_mu.Histo3D(("sig_mu_vs_tau_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    sig_mu_vs_jet = data_frame_mu.Histo3D(("sig_mu_vs_jet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    bkg_mu_vs_b = data_frame_b.Histo3D(("bkg_mu_vs_b_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    bkg_mu_vs_c = data_frame_c.Histo3D(("bkg_mu_vs_c_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    bkg_mu_vs_uds = data_frame_uds.Histo3D(("bkg_mu_vs_uds_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    bkg_mu_vs_g = data_frame_g.Histo3D(("bkg_mu_vs_g_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");
    bkg_mu_vs_tau = data_frame_tau.Histo3D(("bkg_mu_vs_tau_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");    
    bkg_mu_vs_jet = data_frame_jet.Histo3D(("bkg_mu_vs_jet_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","score_label_mu");    
    histo_list.extend([sig_mu_vs_b,sig_mu_vs_uds,sig_mu_vs_g,sig_mu_vs_c,sig_mu_vs_tau,sig_mu_vs_jet]);
    histo_list.extend([bkg_mu_vs_b,bkg_mu_vs_uds,bkg_mu_vs_g,bkg_mu_vs_c,bkg_mu_vs_tau,bkg_mu_vs_jet]);
    ## Run histogram creation
    ROOT.RDF.RunGraphs(histo_list);
    sig_mu_vs_b = sig_mu_vs_b.GetPtr();
    sig_mu_vs_uds = sig_mu_vs_uds.GetPtr();
    sig_mu_vs_g = sig_mu_vs_g.GetPtr();
    sig_mu_vs_c = sig_mu_vs_c.GetPtr();
    sig_mu_vs_tau = sig_mu_vs_tau.GetPtr();
    sig_mu_vs_jet = sig_mu_vs_jet.GetPtr();
    bkg_mu_vs_b = bkg_mu_vs_b.GetPtr();
    bkg_mu_vs_uds = bkg_mu_vs_uds.GetPtr();
    bkg_mu_vs_g = bkg_mu_vs_g.GetPtr();
    bkg_mu_vs_c = bkg_mu_vs_c.GetPtr();    
    bkg_mu_vs_tau = bkg_mu_vs_tau.GetPtr();    
    bkg_mu_vs_jet = bkg_mu_vs_jet.GetPtr();    
    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)
    ## build the ROCs and projecting histograms
    print("Produce the corresponding ROCs by projecting histograms");
    start_time = time.time()
    for ipt in range(0,len(pt_bins)-1):
        for ieta in range(0,len(eta_bins)-1):
            name = key+"_pt_"+str(ipt)+"_eta_"+str(ieta);

            hist_sig_mu_vs_b[name] = ROOT.extractScore(sig_mu_vs_b,ipt,ieta,"hist_sig_mu_vs_b_"+name)
            hist_sig_mu_vs_uds[name] = ROOT.extractScore(sig_mu_vs_uds,ipt,ieta,"hist_sig_mu_vs_uds_"+name)
            hist_sig_mu_vs_g[name] = ROOT.extractScore(sig_mu_vs_g,ipt,ieta,"hist_sig_mu_vs_g_"+name)
            hist_sig_mu_vs_c[name] = ROOT.extractScore(sig_mu_vs_c,ipt,ieta,"hist_sig_mu_vs_c_"+name)
            hist_sig_mu_vs_tau[name] = ROOT.extractScore(sig_mu_vs_tau,ipt,ieta,"hist_sig_mu_vs_tau_"+name)
            hist_sig_mu_vs_jet[name] = ROOT.extractScore(sig_mu_vs_jet,ipt,ieta,"hist_sig_mu_vs_jet_"+name)
            hist_bkg_mu_vs_b[name] = ROOT.extractScore(bkg_mu_vs_b,ipt,ieta,"hist_bkg_mu_vs_b_"+name)
            hist_bkg_mu_vs_uds[name] = ROOT.extractScore(bkg_mu_vs_uds,ipt,ieta,"hist_bkg_mu_vs_uds_"+name)
            hist_bkg_mu_vs_g[name] = ROOT.extractScore(bkg_mu_vs_g,ipt,ieta,"hist_bkg_mu_vs_g_"+name)
            hist_bkg_mu_vs_c[name] = ROOT.extractScore(bkg_mu_vs_c,ipt,ieta,"hist_bkg_mu_vs_c_"+name)
            hist_bkg_mu_vs_tau[name] = ROOT.extractScore(bkg_mu_vs_tau,ipt,ieta,"hist_bkg_mu_vs_tau_"+name)
            hist_bkg_mu_vs_jet[name] = ROOT.extractScore(bkg_mu_vs_jet,ipt,ieta,"hist_bkg_mu_vs_jet_"+name)

            hist_sig_mu_vs_b[name].Scale(1./hist_sig_mu_vs_b[name].Integral());
            hist_sig_mu_vs_uds[name].Scale(1./hist_sig_mu_vs_uds[name].Integral());
            hist_sig_mu_vs_g[name].Scale(1./hist_sig_mu_vs_g[name].Integral());
            hist_sig_mu_vs_c[name].Scale(1./hist_sig_mu_vs_c[name].Integral());
            hist_sig_mu_vs_tau[name].Scale(1./hist_sig_mu_vs_tau[name].Integral());
            hist_sig_mu_vs_jet[name].Scale(1./hist_sig_mu_vs_jet[name].Integral());
            hist_bkg_mu_vs_b[name].Scale(1./hist_bkg_mu_vs_b[name].Integral());
            hist_bkg_mu_vs_uds[name].Scale(1./hist_bkg_mu_vs_uds[name].Integral());
            hist_bkg_mu_vs_g[name].Scale(1./hist_bkg_mu_vs_g[name].Integral());
            hist_bkg_mu_vs_c[name].Scale(1./hist_bkg_mu_vs_c[name].Integral());
            hist_bkg_mu_vs_tau[name].Scale(1./hist_bkg_mu_vs_tau[name].Integral());
            hist_bkg_mu_vs_jet[name].Scale(1./hist_bkg_mu_vs_jet[name].Integral());
            
            roc_mu_vs_b[name] = ROOT.buildROC(hist_sig_mu_vs_b[name],hist_bkg_mu_vs_b[name]);
            roc_mu_vs_uds[name] = ROOT.buildROC(hist_sig_mu_vs_uds[name],hist_bkg_mu_vs_uds[name]);
            roc_mu_vs_g[name] = ROOT.buildROC(hist_sig_mu_vs_g[name],hist_bkg_mu_vs_g[name]);
            roc_mu_vs_c[name] = ROOT.buildROC(hist_sig_mu_vs_c[name],hist_bkg_mu_vs_c[name]);
            roc_mu_vs_tau[name] = ROOT.buildROC(hist_sig_mu_vs_tau[name],hist_bkg_mu_vs_tau[name]);
            roc_mu_vs_jet[name] = ROOT.buildROC(hist_sig_mu_vs_jet[name],hist_bkg_mu_vs_jet[name]);

    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)

### From TGraph to TF1
print("Plotting final ROCs");
start_time = time.time()
for ipt in range(0,len(args.pt_bins)-1):
    for ieta in range(0,len(args.eta_bins)-1):
        rocs_to_plot_mu_vs_b = [];
        rocs_to_plot_mu_vs_uds = [];
        rocs_to_plot_mu_vs_g = [];
        rocs_to_plot_mu_vs_c = []
        rocs_to_plot_mu_vs_tau = []
        rocs_to_plot_mu_vs_jet = []
        legends = [];
        bin_label = "pt_"+str(ipt)+"_eta_"+str(ieta);
        bin_name = "%d < p_{T} < %d GeV, %.1f < |#eta| < %.1f"%(args.pt_bins[ipt],args.pt_bins[ipt+1],args.eta_bins[ieta],args.eta_bins[ieta+1]);
        for key in inputs:

            name = key+"_"+bin_label;
            rocs_to_plot_mu_vs_b.append(roc_mu_vs_b[name]);
            rocs_to_plot_mu_vs_c.append(roc_mu_vs_c[name]);
            rocs_to_plot_mu_vs_uds.append(roc_mu_vs_uds[name]);
            rocs_to_plot_mu_vs_g.append(roc_mu_vs_g[name]);
            rocs_to_plot_mu_vs_tau.append(roc_mu_vs_tau[name]);
            rocs_to_plot_mu_vs_jet.append(roc_mu_vs_jet[name]);
            
            hist_sig_mu_vs_b[name].Rebin(10);
            hist_sig_mu_vs_c[name].Rebin(10);
            hist_sig_mu_vs_uds[name].Rebin(10);
            hist_sig_mu_vs_g[name].Rebin(10);
            hist_sig_mu_vs_tau[name].Rebin(10);
            hist_sig_mu_vs_jet[name].Rebin(10);
            hist_bkg_mu_vs_b[name].Rebin(10);
            hist_bkg_mu_vs_c[name].Rebin(10);
            hist_bkg_mu_vs_uds[name].Rebin(10);
            hist_bkg_mu_vs_g[name].Rebin(10);
            hist_bkg_mu_vs_tau[name].Rebin(10);
            hist_bkg_mu_vs_jet[name].Rebin(10);

            hist_sig_mu_vs_b[name].Scale(1./hist_sig_mu_vs_b[name].Integral());
            hist_sig_mu_vs_uds[name].Scale(1./hist_sig_mu_vs_uds[name].Integral());
            hist_sig_mu_vs_g[name].Scale(1./hist_sig_mu_vs_g[name].Integral());
            hist_sig_mu_vs_c[name].Scale(1./hist_sig_mu_vs_c[name].Integral());
            hist_sig_mu_vs_tau[name].Scale(1./hist_sig_mu_vs_tau[name].Integral());
            hist_sig_mu_vs_jet[name].Scale(1./hist_sig_mu_vs_jet[name].Integral());
            hist_bkg_mu_vs_b[name].Scale(1./hist_bkg_mu_vs_b[name].Integral());
            hist_bkg_mu_vs_uds[name].Scale(1./hist_bkg_mu_vs_uds[name].Integral());
            hist_bkg_mu_vs_g[name].Scale(1./hist_bkg_mu_vs_g[name].Integral());
            hist_bkg_mu_vs_tau[name].Scale(1./hist_bkg_mu_vs_tau[name].Integral());
            hist_bkg_mu_vs_jet[name].Scale(1./hist_bkg_mu_vs_jet[name].Integral());
            
            legends.append(key);
            
            legends_tmp = [];
            legends_tmp.append(name);

            labels_tmp = [];
            labels_tmp.append("Sig: #mu vs c");
            labels_tmp.append("Sig: #mu vs uds");
            labels_tmp.append("Sig: #mu vs g");
            labels_tmp.append("Bkg: #mu vs b");
            labels_tmp.append("Bkg: #mu vs uds");
            labels_tmp.append("Bkg: #mu vs g");

            ROOT.plotScore([hist_sig_mu_vs_c[name]],[hist_sig_mu_vs_uds[name]],[hist_sig_mu_vs_g[name]],[hist_bkg_mu_vs_c[name]],[hist_bkg_mu_vs_uds[name]],[hist_bkg_mu_vs_g[name]],
                           legends_tmp,labels_tmp,bin_name,args.output_dir,"score_"+name+"_"+bin_label,score_bins[0],score_bins[-1]);

            labels_tmp = [];
            labels_tmp.append("Sig: #mu vs jet");
            labels_tmp.append("Sig: #mu vs #tau_{h}");
            labels_tmp.append("Bkg: #mu vs jet");
            labels_tmp.append("Bkg: #mu vs #tau_{h}");

            ROOT.plotScore([hist_sig_mu_vs_jet[name]],[hist_sig_mu_vs_tau[name]],[hist_bkg_mu_vs_jet[name]],[hist_bkg_mu_vs_tau[name]],[],[],
                           legends_tmp,labels_tmp,bin_name,args.output_dir,"score_jt_"+name+"_"+bin_label,score_bins[0],score_bins[-1]);

        labels = [];
        labels.append("#mu vs uds");
        labels.append("#mu vs g");
        labels.append("#mu vs b");
        labels.append("#mu vs c");
        ROOT.plotROC(rocs_to_plot_mu_vs_uds,rocs_to_plot_mu_vs_g,rocs_to_plot_mu_vs_b,rocs_to_plot_mu_vs_c,[],[],legends,labels,bin_name,args.output_dir,"roc_mutag_"+bin_label);

        labels = [];
        labels.append("#mu vs jet");
        labels.append("#mu vs #tau_{h}");
        ROOT.plotROC(rocs_to_plot_mu_vs_jet,rocs_to_plot_mu_vs_tau,[],[],[],[],legends,labels,bin_name,args.output_dir,"roc_mutag_jt_"+bin_label);
        
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)
