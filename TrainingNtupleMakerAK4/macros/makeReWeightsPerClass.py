import os
import sys
import glob
import argparse
import subprocess
import shutil
import yaml
from importlib import import_module
from array import array
import ROOT
import numpy as np
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='', help='directory with input files');
parser.add_argument('-s', '--string-to-grep', nargs="+", default=[], help='list of string to grep to get fielist');
parser.add_argument('-d', '--data-config', type=str, default='', help="data config file of weaver");
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-o', '--output-file', type=str, default='', help='output file');
parser.add_argument('-p', '--output-plot-dir', type=str, default='', help='output plot dir');

if __name__ == '__main__':

    args = parser.parse_args()
    xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

    ROOT.gInterpreter.ProcessLine('#include "CMS_style.h"')
    ROOT.setTDRStyle();
    ROOT.EnableImplicitMT(args.nthreads);
    ROOT.EnableThreadSafety();

    ## parse yaml file
    with open(args.data_config, 'r') as file:
        data_config = yaml.safe_load(file)

    ## take selections, new variables, and inputs
    selection = data_config['selection'];
    ## only build the label variables
    new_variables = data_config['new_variables'];
    new_variables = {key: value for key, value in new_variables.items() if "label" in key };
    ## take the weight dictionary
    weights = data_config['weights'];

    ## load the input dataframe                                                                                                                                    
    input_files = [];
    if args.string_to_grep:
        for name in args.string_to_grep:
            if not args.use_xrootd:
                input_files.append(args.input_dir+"/*"+name+"*root");
            else:
                input_files.append(xrootd_eos+"/"+args.input_dir+"/*"+name+"*root");
    else:
        if not args.use_xrootd:
            input_files.append(args.input_dir+"/*root");
        else:
            input_files.append(xrootd_eos+"/"+args.input_dir+"/*root");

    print("###### Create RDataFrame for ",input_files);
    data_frame = ROOT.RDataFrame("tree",input_files);
    
    ## add new colums
    print("###### Add new colums to DataFrame");
    for key,var in new_variables.items():
        var = var.replace("&","&&").replace("~","!").replace("np.","")
        print(key," ",var);
        data_frame = data_frame.Define(key,var);
        
    ## edit selection string
    selection = selection.replace("&","&&").replace("~","!").replace("np.","")
    print("###### Add filter to DataFrame ",selection);
    data_frame = data_frame.Filter(selection);

    ## reweight infors
    print("###### Reweight information");
    reweight_vars = tuple(weights['reweight_vars'].keys())
    reweight_bins = tuple(weights['reweight_vars'].values())
    reweight_classes = weights['reweight_classes']
    reweight_class_w = weights.get('class_weights', np.ones(len(reweight_classes)));
    reweight_threshold = weights.get('reweight_threshold', 10)
    reweight_domain_classes = weights.get('domain_classes',np.ones(len(data_config["labels_domain"])));
    print("Reweight variables: ",reweight_vars);
    print("Reweight bins: ",reweight_bins);
    print("Reweight classes: ",reweight_classes);
    print("Reweight class weights: ",reweight_class_w);
    print("Reweight domain class: ",reweight_domain_classes);

    ## exclude domains
    exclude_domain = "";
    for i,domain in enumerate(reweight_domain_classes):
        if i != len(reweight_domain_classes) -1:
            exclude_domain += domain+"==0 && ";
        else:
            exclude_domain += domain+"==0";
    data_frame = data_frame.Filter(exclude_domain);
    print("##### Exclude domains ",exclude_domain);

    ## build weights
    print("##### Build weight histograms");
    x_var, y_var = reweight_vars;
    x_bins, y_bins = reweight_bins;
    x_vec, y_vec = {}, {};
    for label_class in reweight_classes:        
        print("Define histogram for class ",label_class);
        x_vec[label_class] = data_frame.Filter(label_class+"==1").Take['float'](x_var);
        y_vec[label_class] = data_frame.Filter(label_class+"==1").Take['float'](y_var);
    
    histo, histo_w, histo_reweighted = {}, {}, {};
    class_evts, class_evets_w = {}, {};
    for key, classwgt in zip(reweight_classes,reweight_class_w):
        hist, _, _ = np.histogram2d(x_vec[key].GetValue(),y_vec[key].GetValue(),bins=reweight_bins);
        histo[key] = hist.astype('float64');
        print('##### Histogram unweighted:',key);
        print(str(hist.astype('int64')));
        class_evts[key] = hist.sum();
        ## median value
        med_val = np.median(hist);
        ## threshold is 0.01*median over the non-null bins
        threshold = np.median(hist[hist > 0]) * 0.01;
        nonzero_vals = hist[hist > threshold];
        min_val = np.min(nonzero_vals);
        ref_val = np.percentile(nonzero_vals,reweight_threshold);
        wgt = np.clip(np.nan_to_num(ref_val/hist,posinf=0),0,1)
        histo_w[key] = wgt.astype('float64');
        class_evets_w[key] = np.sum(hist*wgt) / classwgt;

    ## adjust weights for the class weights
    max_weight = 0.9;
    min_nevt = min(class_evets_w.values()) * max_weight
    for label_class in reweight_classes:
        class_wgt =  float(min_nevt) / class_evets_w[label_class]
        histo_w[label_class] *= class_wgt;
        histo_reweighted[label_class] = (histo[label_class]*histo_w[label_class]).astype('float64');
        print('##### Histogram of weights:',label_class);
        print(str(histo_w[label_class]));
        print('##### Reweighted histograms ',label_class);
        print(str(histo_reweighted[label_class].astype('int64')));

    ## save output files
    print("##### Writing YAML file w/ reweighting info to %s"%(args.output_file));
    data_config['weights']['reweight_hists'] = {k: v.tolist() for k, v in histo_w.items()}
    with open(args.output_file.replace(".yaml",".auto.yaml"), 'w') as outfile:
        yaml.preserve_quotes = False
        yaml.safe_dump(data_config,outfile,sort_keys=False)

    ## save plots
    print("###### Save 2D histograms with weight and yield distributions");
    os.system("mkdir -p "+args.output_plot_dir);
    ROOT.gStyle.SetOptStat(0);
    c = ROOT.TCanvas("c","c",700,600);
    c.SetRightMargin(0.15);
    c.SetLogx();
    for key, hist in histo.items():
        c.cd();
        ## convert to 2D histo
        histogram = ROOT.TH2D("h_"+key,"",len(x_bins)-1,array('d',x_bins),len(y_bins)-1,array('d',y_bins));
        histogram_weight = ROOT.TH2D("h_"+key+"_weight","",len(x_bins)-1,array('d',x_bins),len(y_bins)-1,array('d',y_bins));
        histogram_reweight = ROOT.TH2D("h_"+key+"_reweight","",len(x_bins)-1,array('d',x_bins),len(y_bins)-1,array('d',y_bins));
        for i in range(hist.shape[0]):
            for j in range(hist.shape[1]):
                histogram.SetBinContent(i+1,j+1,histo[key][i,j]);
                histogram_weight.SetBinContent(i+1,j+1,histo_w[key][i,j]);
                histogram_reweight.SetBinContent(i+1,j+1,histo_reweighted[key][i,j]);                
        ## plot nominal
        histogram.GetXaxis().SetTitle(x_var);
        histogram.GetYaxis().SetTitle(y_var);
        histogram.GetZaxis().SetTitle("Events");
        histogram.Draw("colz");
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+histogram.GetName()+".png","png");
        histo[key] = histogram;
        ## plot weights
        histogram_weight.GetXaxis().SetTitle(x_var);
        histogram_weight.GetYaxis().SetTitle(y_var);
        histogram_weight.GetZaxis().SetTitle("Weights");
        histogram_weight.Draw("colz");
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+histogram_weight.GetName()+".png","png");
        histo_w[key] = histogram_weight;
        ## plot reweighted
        histogram_reweight.GetXaxis().SetTitle(x_var);
        histogram_reweight.GetYaxis().SetTitle(y_var);
        histogram_reweight.GetZaxis().SetTitle("Events");
        histogram_reweight.Draw("colz");
        ROOT.CMS_lumi(c,"",False,False,True);
        c.SaveAs(args.output_plot_dir+"/"+histogram_reweight.GetName()+".png","png");
        histo_reweighted[key] = histogram_reweight;
        
    ## save root file  
    output_file_root = ROOT.TFile(args.output_file.replace(".yaml",".root"),"RECREATE");
    output_file_root.cd();
    for key,hist in histo.items():
        hist.Write();
    for key,hist in histo_w.items():
        hist.Write();
    for key,hist in histo_reweighted.items():
        hist.Write();
    output_file_root.Close();
