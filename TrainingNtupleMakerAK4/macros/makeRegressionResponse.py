import os
import sys
import glob
import argparse
import subprocess
import shutil
import time
import ROOT
import numpy as np
from array import array 
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file-name', type=str, default='', help='file name to grep');
parser.add_argument('-o', '--output-dir', type=str, default='', help='output directgory for plots');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-p', '--pt-bins', nargs="+", type=float, default=[30,1000], help='pt bins to build ROCs');
parser.add_argument('-e', '--eta-bins', nargs="+", type=float, default=[0,2.5], help='eta bins to build ROCs');
parser.add_argument('-u', '--pu-bins', nargs="+", type=float, default=[0,18,24,30,35,40,45,60], help='eta bins to build ROCs');
parser.add_argument('-s', '--samples', nargs="+", type=int, default=[],help='sample to select according to sample ID');
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-b', '--use-pu-bins', action='store_true', help='use pileup bins instead of eta');
parser.add_argument('-j', '--use-pt-gen', action='store_true', help='use GEN pT instead of RECO oen for binning');
parser.add_argument('-m', '--use-hps-matching', action='store_true', help='apply the matching between jet and hps for tau candidates');

inputs = {
    "NoRegression": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassReg/trainingAK4_taumuel_classreg_transformer_21052023",
    "ParTNoJECOld": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassReg/trainingAK4_taumuel_classreg_transformer_21052023",
    "ParTNoJECNewDyn": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_dyn_02032024"
}

start_time = time.time()
ROOT.gInterpreter.ProcessLine('#include "commonTools.h"')
ROOT.setTDRStyle();
ROOT.gStyle.SetOptStat(0);

args = parser.parse_args()
print(args)
xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

ROOT.EnableImplicitMT(args.nthreads);
ROOT.EnableThreadSafety();

os.system("mkdir -p "+args.output_dir);

preselection = "label_mu != 1 && label_el !=1";
for i,isamp in enumerate(args.samples):
    if i == 0:
        preselection += " && (";
    if i == len(args.samples)-1:
        preselection += " sample == "+str(isamp)+")";
    else:
        preselection += " sample == "+str(isamp)+" || ";
print("Preselection string --> ",preselection);
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)

### histograms
hist_response_b = {};
hist_response_c = {};
hist_response_uds = {};
hist_response_g = {};
hist_response_jet = {};
hist_response_tau = {};
hist_response_b_vis = {};
hist_response_c_vis = {};
hist_response_uds_vis = {};
hist_response_g_vis = {};
hist_response_jet_vis = {};
hist_response_tau_vis = {};
hist_pt_b = {};
hist_pt_c = {};
hist_pt_uds = {};
hist_pt_g = {};
hist_pt_jet = {};
hist_pt_tau = {};

## define axis and binnings
if args.use_pt_gen:
    pt_j_name = "jet_genmatch_pt"
    pt_tau_name = "jet_genmatch_lep_vis_pt"
else:
    pt_j_name = "jet_pt_raw";        
    pt_tau_name = "jet_pt_raw"
pt_bins = array('d',args.pt_bins);
print("pt_j_name: ",pt_j_name," pt_tau_name ",pt_tau_name," bins ",pt_bins);

if args.use_pu_bins:
    y_name = "npu"
    y_bins = array('d',args.pu_bins);
    suffix = "pu";
else:
    y_name = "jet_eta";
    y_bins = array('d',args.eta_bins);
    suffix = "eta";
print("y_name: ",y_name," y_bins ",y_bins);

response_bins = array('d',[i for i in np.arange(0.30,1.70,(1.70-0.25)/200.)])
print("response_bins: nbins ",len(response_bins)," min ",response_bins[0]," max ",response_bins[-1]);

## for pt-mean
z_bins = array('d',[i for i in np.arange(pt_bins[0],pt_bins[-1],1.)]);

## loop in inputs
for key,val in inputs.items():
    start_time = time.time()
    print("Run on input files: key ",key," location: ",val);

    ## build the dataframe
    data_frame = ROOT.RDataFrame("Events",xrootd_eos+val+"/*"+args.file_name+"*root" if args.use_xrootd else val+"/*"+args.file_name+"*root");
    ## apply preselection
    data_frame = data_frame.Filter(preselection)
    ## define scores
    if "Original" in key or "NoRegression" in key:
        data_frame = data_frame.Define("jet_response","jet_pt/jet_genmatch_wnu_pt");
        data_frame = data_frame.Define("jet_response_vis","jet_pt/jet_genmatch_pt");
        data_frame = data_frame.Define("tau_response","(jet_taumatch_pt>0) ? jet_taumatch_pt/jet_genmatch_lep_pt : jet_pt/jet_genmatch_lep_pt");
        data_frame = data_frame.Define("tau_response_vis","(jet_taumatch_pt>0) ? jet_taumatch_pt/jet_genmatch_lep_vis_pt : jet_pt/jet_genmatch_lep_vis_pt");
    else:
        data_frame = data_frame.Define("jet_response","(jet_pt_raw*score_target_pt*score_target_pt_nu)/jet_genmatch_wnu_pt");
        data_frame = data_frame.Define("jet_response_vis","(jet_pt_raw*score_target_pt)/jet_genmatch_pt");
        data_frame = data_frame.Define("tau_response","(jet_taumatch_pt>0) ? (jet_taumatch_pt*score_target_pt*score_target_pt_nu)/jet_genmatch_lep_pt : (jet_pt_raw*score_target_pt*score_target_pt_nu)/jet_genmatch_lep_pt");
        data_frame = data_frame.Define("tau_response_vis","(jet_taumatch_pt>0) ? (jet_taumatch_pt*score_target_pt)/jet_genmatch_lep_vis_pt : (jet_pt_raw*score_target_pt*score_target_pt_nu)/jet_genmatch_lep_vis_pt");    
    ## apply truth label filters
    data_frame_b = data_frame.Filter("label_b==1");
    data_frame_c = data_frame.Filter("label_c==1");
    data_frame_uds = data_frame.Filter("label_uds==1");
    data_frame_g = data_frame.Filter("label_g==1");
    data_frame_jet = data_frame.Filter("label_g==1 || label_b==1 || label_c==1 || label_uds==1");
    data_frame_tau = data_frame.Filter("label_taup_1h0p==1 || label_taup_1h1p==1 || label_taup_1h2p==1 || label_taup_3h0p==1 || label_taup_3h1p==1 || label_taum_1h0p==1 || label_taum_1h1p==1 || label_taum_1h2p==1 || label_taum_3h0p==1 || label_taum_3h1p==1");
    if args.use_hps_matching:
        data_frame_tau = data_frame_tau.Filter("jet_taumatch_pt>0");
        
    ## define the histograms as 3D to speed up RDF computation
    histo_list = [];
    response_b = data_frame_b.Histo3D(("response_b_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response");
    response_b_vis = data_frame_b.Histo3D(("response_b_vis_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response_vis");
    response_c = data_frame_c.Histo3D(("response_c_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response");
    response_c_vis = data_frame_c.Histo3D(("response_c_vis_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response_vis");
    response_uds = data_frame_uds.Histo3D(("response_uds_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response");
    response_uds_vis = data_frame_uds.Histo3D(("response_uds_vis_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response_vis");
    response_g = data_frame_g.Histo3D(("response_g_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response");
    response_g_vis = data_frame_g.Histo3D(("response_g_vis_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response_vis");
    response_jet = data_frame_jet.Histo3D(("response_jet_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response");
    response_jet_vis = data_frame_jet.Histo3D(("response_jet_vis_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_j_name,y_name,"jet_response_vis");
    response_tau = data_frame_tau.Histo3D(("response_tau_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_tau_name,y_name,"tau_response");
    response_tau_vis = data_frame_tau.Histo3D(("response_tau_vis_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(response_bins)-1,response_bins),pt_tau_name,y_name,"tau_response_vis");

    pt_b = data_frame_b.Histo3D(("pt_b_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(z_bins)-1,z_bins),pt_j_name,y_name,pt_j_name);
    pt_c = data_frame_c.Histo3D(("pt_c_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(z_bins)-1,z_bins),pt_j_name,y_name,pt_j_name);
    pt_uds = data_frame_uds.Histo3D(("pt_uds_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(z_bins)-1,z_bins),pt_j_name,y_name,pt_j_name);
    pt_g = data_frame_g.Histo3D(("pt_g_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(z_bins)-1,z_bins),pt_j_name,y_name,pt_j_name);
    pt_jet = data_frame_jet.Histo3D(("pt_jet_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(z_bins)-1,z_bins),pt_j_name,y_name,pt_j_name);
    pt_tau = data_frame_tau.Histo3D(("pt_tau_"+key,"",len(pt_bins)-1,pt_bins,len(y_bins)-1,y_bins,len(z_bins)-1,z_bins),pt_tau_name,y_name,pt_tau_name);
    
    histo_list.extend([response_b,response_c,response_uds,response_g,response_jet,response_tau]);
    histo_list.extend([response_b_vis,response_c_vis,response_uds_vis,response_g_vis,response_jet_vis,response_tau_vis]);
    histo_list.extend([pt_b,pt_c,pt_uds,pt_g,pt_jet,pt_tau]);
    
    ## Run histogram creation
    ROOT.RDF.RunGraphs(histo_list);
    response_b = response_b.GetPtr();
    response_c = response_c.GetPtr();
    response_uds = response_uds.GetPtr();
    response_g = response_g.GetPtr();
    response_jet = response_jet.GetPtr();
    response_tau = response_tau.GetPtr();
    response_b_vis = response_b_vis.GetPtr();
    response_c_vis = response_c_vis.GetPtr();
    response_uds_vis = response_uds_vis.GetPtr();
    response_g_vis = response_g_vis.GetPtr();
    response_jet_vis = response_jet_vis.GetPtr();
    response_tau_vis = response_tau_vis.GetPtr();
    pt_b = pt_b.GetPtr();
    pt_c = pt_c.GetPtr();
    pt_uds = pt_uds.GetPtr();
    pt_g = pt_g.GetPtr();
    pt_jet = pt_jet.GetPtr();
    pt_tau = pt_tau.GetPtr();
    
    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)

    ## build the ROCs and projecting histograms
    print("Produce the corresponding ROCs by projecting histograms");
    start_time = time.time()
    for ipt in range(0,len(pt_bins)-1):
        for iy in range(0,len(y_bins)-1):
            name = key+"_pt_"+str(ipt)+"_"+suffix+"_"+str(iy);
                
            hist_response_b[name] = ROOT.extractScore(response_b,ipt,iy,"hist_response_b_"+name)
            hist_response_c[name] = ROOT.extractScore(response_c,ipt,iy,"hist_response_c_"+name)
            hist_response_uds[name] = ROOT.extractScore(response_uds,ipt,iy,"hist_response_uds_"+name)
            hist_response_g[name] = ROOT.extractScore(response_g,ipt,iy,"hist_response_g_"+name)
            hist_response_jet[name] = ROOT.extractScore(response_jet,ipt,iy,"hist_response_jet_"+name)
            hist_response_tau[name] = ROOT.extractScore(response_tau,ipt,iy,"hist_response_tau_"+name)

            hist_response_b[name].Scale(1./hist_response_b[name].Integral());
            hist_response_c[name].Scale(1./hist_response_c[name].Integral());
            hist_response_uds[name].Scale(1./hist_response_uds[name].Integral());
            hist_response_g[name].Scale(1./hist_response_g[name].Integral());
            hist_response_jet[name].Scale(1./hist_response_jet[name].Integral());
            hist_response_tau[name].Scale(1./hist_response_tau[name].Integral());

            hist_response_b_vis[name] = ROOT.extractScore(response_b_vis,ipt,iy,"hist_response_b_vis_"+name)
            hist_response_c_vis[name] = ROOT.extractScore(response_c_vis,ipt,iy,"hist_response_c_vis_"+name)
            hist_response_uds_vis[name] = ROOT.extractScore(response_uds_vis,ipt,iy,"hist_response_uds_vis_"+name)
            hist_response_g_vis[name] = ROOT.extractScore(response_g_vis,ipt,iy,"hist_response_g_vis_"+name)
            hist_response_jet_vis[name] = ROOT.extractScore(response_jet_vis,ipt,iy,"hist_response_jet_vis_"+name)
            hist_response_tau_vis[name] = ROOT.extractScore(response_tau_vis,ipt,iy,"hist_response_tau_vis_"+name)

            hist_response_b_vis[name].Scale(1./hist_response_b_vis[name].Integral());
            hist_response_c_vis[name].Scale(1./hist_response_c_vis[name].Integral());
            hist_response_uds_vis[name].Scale(1./hist_response_uds_vis[name].Integral());
            hist_response_g_vis[name].Scale(1./hist_response_g_vis[name].Integral());
            hist_response_jet_vis[name].Scale(1./hist_response_jet_vis[name].Integral());
            hist_response_tau_vis[name].Scale(1./hist_response_tau_vis[name].Integral());

            hist_pt_b[name] = ROOT.extractScore(pt_b,ipt,iy,"hist_pt_b_"+name)
            hist_pt_c[name] = ROOT.extractScore(pt_c,ipt,iy,"hist_pt_c_"+name)
            hist_pt_uds[name] = ROOT.extractScore(pt_uds,ipt,iy,"hist_pt_uds_"+name)
            hist_pt_g[name] = ROOT.extractScore(pt_g,ipt,iy,"hist_pt_g_"+name)
            hist_pt_jet[name] = ROOT.extractScore(pt_jet,ipt,iy,"hist_pt_jet_"+name)
            hist_pt_tau[name] = ROOT.extractScore(pt_tau,ipt,iy,"hist_pt_tau_"+name)           

            hist_pt_b[name].Scale(1./hist_pt_b[name].Integral());
            hist_pt_c[name].Scale(1./hist_pt_c[name].Integral());
            hist_pt_uds[name].Scale(1./hist_pt_uds[name].Integral());
            hist_pt_g[name].Scale(1./hist_pt_g[name].Integral());
            hist_pt_jet[name].Scale(1./hist_pt_jet[name].Integral());
            hist_pt_tau[name].Scale(1./hist_pt_tau[name].Integral());
                        
    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)

### Jet response
print("Plot and fit jet response");
start_time = time.time()

response_b_param = {};
response_c_param = {};
response_uds_param = {};
response_g_param = {};
response_jet_param = {};
response_tau_param = {};

response_b_param_vis = {};
response_c_param_vis = {};
response_uds_param_vis = {};
response_g_param_vis = {};
response_jet_param_vis = {};
response_tau_param_vis = {};

for ipt in range(0,len(args.pt_bins)-1):
    for iy in range(0,len(y_bins)-1):

        response_to_plot_b = [];
        response_to_plot_c = [];
        response_to_plot_uds = [];
        response_to_plot_g = [];
        response_to_plot_jet = [];
        response_to_plot_tau = [];

        response_to_plot_b_vis = [];
        response_to_plot_c_vis = [];
        response_to_plot_uds_vis = [];
        response_to_plot_g_vis = [];
        response_to_plot_jet_vis = [];
        response_to_plot_tau_vis = [];
        
        legends = [];
        bin_label = "pt_"+str(ipt)+"_"+suffix+"_"+str(iy);
        if args.use_pu_bins:
            bin_name = "%d < p_{T} < %d GeV, %d < PU < %d"%(args.pt_bins[ipt],args.pt_bins[ipt+1],args.pu_bins[iy],args.pu_bins[iy+1]);
        else:
            bin_name = "%d < p_{T} < %d GeV, %.1f < |#eta| < %.1f"%(args.pt_bins[ipt],args.pt_bins[ipt+1],args.eta_bins[iy],args.eta_bins[iy+1]);
            
        for key in inputs:
            name = key+"_"+bin_label;
            response_to_plot_b.append(hist_response_b[name]);
            response_to_plot_c.append(hist_response_c[name]);
            response_to_plot_uds.append(hist_response_uds[name]);
            response_to_plot_g.append(hist_response_g[name]);
            response_to_plot_jet.append(hist_response_jet[name]);
            response_to_plot_tau.append(hist_response_tau[name]);

            response_to_plot_b_vis.append(hist_response_b_vis[name]);
            response_to_plot_c_vis.append(hist_response_c_vis[name]);
            response_to_plot_uds_vis.append(hist_response_uds_vis[name]);
            response_to_plot_g_vis.append(hist_response_g_vis[name]);
            response_to_plot_jet_vis.append(hist_response_jet_vis[name]);
            response_to_plot_tau_vis.append(hist_response_tau_vis[name]);

            legends.append(key);

        axis_title = "p_{T}/p_{T}^{gen}";

        response_b_param.update(ROOT.buildRegResponse(response_to_plot_b,legends,axis_title,"b-jet",bin_name,args.output_dir,"response_b_",bin_label,True));
        response_c_param.update(ROOT.buildRegResponse(response_to_plot_c,legends,axis_title,"c-jet",bin_name,args.output_dir,"response_c_",bin_label,True));
        response_uds_param.update(ROOT.buildRegResponse(response_to_plot_uds,legends,axis_title,"uds-jet",bin_name,args.output_dir,"response_uds_",bin_label,True));
        response_g_param.update(ROOT.buildRegResponse(response_to_plot_g,legends,axis_title,"g-jet",bin_name,args.output_dir,"response_g_",bin_label,True));
        response_jet_param.update(ROOT.buildRegResponse(response_to_plot_jet,legends,axis_title,"jets",bin_name,args.output_dir,"response_jet_",bin_label,True));
        response_tau_param.update(ROOT.buildRegResponse(response_to_plot_tau,legends,axis_title,"#tau-jet",bin_name,args.output_dir,"response_tau_",bin_label,False));

        response_b_param_vis.update(ROOT.buildRegResponse(response_to_plot_b_vis,legends,axis_title,"b-jet vis",bin_name,args.output_dir,"response_b_vis_",bin_label,True));
        response_c_param_vis.update(ROOT.buildRegResponse(response_to_plot_c_vis,legends,axis_title,"c-jet vis",bin_name,args.output_dir,"response_c_vis_",bin_label,True));
        response_uds_param_vis.update(ROOT.buildRegResponse(response_to_plot_uds_vis,legends,axis_title,"uds-jet vis",bin_name,args.output_dir,"response_uds_vis_",bin_label,True));
        response_g_param_vis.update(ROOT.buildRegResponse(response_to_plot_g_vis,legends,axis_title,"g-jet vis",bin_name,args.output_dir,"response_g_vis_",bin_label,True));
        response_jet_param_vis.update(ROOT.buildRegResponse(response_to_plot_jet_vis,legends,axis_title,"jets vis",bin_name,args.output_dir,"response_jet_vis_",bin_label,True));
        response_tau_param_vis.update(ROOT.buildRegResponse(response_to_plot_tau_vis,legends,axis_title,"#tau-jet vis",bin_name,args.output_dir,"response_tau_vis_",bin_label,False));

stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)

print("Make final median vs pT and sigma vs pT");
start_time = time.time()

median_vs_pt_b = [];
median_vs_pt_c = [];
median_vs_pt_uds = [];
median_vs_pt_g = [];
median_vs_pt_jet = [];
median_vs_pt_tau = [];
median_vs_pt_b_vis = [];
median_vs_pt_c_vis = [];
median_vs_pt_uds_vis = [];
median_vs_pt_g_vis = [];
median_vs_pt_jet_vis = [];
median_vs_pt_tau_vis = [];
sigma_vs_pt_b = [];
sigma_vs_pt_c = [];
sigma_vs_pt_uds = [];
sigma_vs_pt_g = [];
sigma_vs_pt_jet = [];
sigma_vs_pt_tau = [];
sigma_vs_pt_b_vis = [];
sigma_vs_pt_c_vis = [];
sigma_vs_pt_uds_vis = [];
sigma_vs_pt_g_vis = [];
sigma_vs_pt_jet_vis = [];
sigma_vs_pt_tau_vis = [];
legends = [];

for key in inputs:
    for iy in range(0,len(y_bins)-1):
        if args.use_pu_bins:
            bin_label = "pt_"+str(ipt)+"_pu_"+str(iy);
            legends.append("%s %d < PU < %d"%(key,y_bins[iy],y_bins[iy+1]));
        else:
            bin_label = "pt_"+str(ipt)+"_eta_"+str(iy);
            legends.append("%s %.1f < |#eta| < %.1f"%(key,y_bins[iy],y_bins[iy+1]));

        median_vs_pt_b.append(ROOT.TGraph());        
        median_vs_pt_b[-1].SetName("%s_med_vs_pt_b_%d"%(key,iy));
        median_vs_pt_c.append(ROOT.TGraph());
        median_vs_pt_c[-1].SetName("%s_med_vs_pt_c_%d"%(key,iy));
        median_vs_pt_uds.append(ROOT.TGraph());
        median_vs_pt_uds[-1].SetName("%s_med_vs_pt_uds_%d"%(key,iy));
        median_vs_pt_g.append(ROOT.TGraph());
        median_vs_pt_g[-1].SetName("%s_med_vs_pt_g_%d"%(key,iy));
        median_vs_pt_jet.append(ROOT.TGraph());
        median_vs_pt_jet[-1].SetName("%s_med_vs_pt_jet_%d"%(key,iy));
        median_vs_pt_tau.append(ROOT.TGraph());
        median_vs_pt_tau[-1].SetName("%s_med_vs_pt_tau_%d"%(key,iy));
        
        sigma_vs_pt_b.append(ROOT.TGraph());
        sigma_vs_pt_b[-1].SetName("%s_sig_vs_pt_b_%d"%(key,iy));
        sigma_vs_pt_c.append(ROOT.TGraph());
        sigma_vs_pt_c[-1].SetName("%s_sig_vs_pt_c_%d"%(key,iy));
        sigma_vs_pt_uds.append(ROOT.TGraph());
        sigma_vs_pt_uds[-1].SetName("%s_sig_vs_pt_uds_%d"%(key,iy));
        sigma_vs_pt_g.append(ROOT.TGraph());
        sigma_vs_pt_g[-1].SetName("%s_sig_vs_pt_g_%d"%(key,iy));
        sigma_vs_pt_jet.append(ROOT.TGraph());
        sigma_vs_pt_jet[-1].SetName("%s_sig_vs_pt_jet_%d"%(key,iy));
        sigma_vs_pt_tau.append(ROOT.TGraph());
        sigma_vs_pt_tau[-1].SetName("%s_sig_vs_pt_tau_%d"%(key,iy));
        
        median_vs_pt_b_vis.append(ROOT.TGraph());
        median_vs_pt_b_vis[-1].SetName("%s_med_vs_pt_b_%d_vis"%(key,iy));
        median_vs_pt_c_vis.append(ROOT.TGraph());
        median_vs_pt_c_vis[-1].SetName("%s_med_vs_pt_c_%d_vis"%(key,iy));
        median_vs_pt_uds_vis.append(ROOT.TGraph());
        median_vs_pt_uds_vis[-1].SetName("%s_med_vs_pt_uds_%d_vis"%(key,iy));
        median_vs_pt_g_vis.append(ROOT.TGraph());
        median_vs_pt_g_vis[-1].SetName("%s_med_vs_pt_g_%d_vis"%(key,iy));
        median_vs_pt_jet_vis.append(ROOT.TGraph());
        median_vs_pt_jet_vis[-1].SetName("%s_med_vs_pt_jet_%d_vis"%(key,iy));
        median_vs_pt_tau_vis.append(ROOT.TGraph());
        median_vs_pt_tau_vis[-1].SetName("%s_med_vs_pt_tau_%d_vis"%(key,iy));
        
        sigma_vs_pt_b_vis.append(ROOT.TGraph());
        sigma_vs_pt_b_vis[-1].SetName("%s_sig_vs_pt_b_%d_vis"%(key,iy));
        sigma_vs_pt_c_vis.append(ROOT.TGraph());
        sigma_vs_pt_c_vis[-1].SetName("%s_sig_vs_pt_c_%d_vis"%(key,iy));
        sigma_vs_pt_uds_vis.append(ROOT.TGraph());
        sigma_vs_pt_uds_vis[-1].SetName("%s_sig_vs_pt_uds_%d_vis"%(key,iy));
        sigma_vs_pt_g_vis.append(ROOT.TGraph());
        sigma_vs_pt_g_vis[-1].SetName("%s_sig_vs_pt_g_%d_vis"%(key,iy));
        sigma_vs_pt_jet_vis.append(ROOT.TGraph());
        sigma_vs_pt_jet_vis[-1].SetName("%s_sig_vs_pt_jet_%d_vis"%(key,iy));
        sigma_vs_pt_tau_vis.append(ROOT.TGraph());
        sigma_vs_pt_tau_vis[-1].SetName("%s_sig_vs_pt_tau_%d_vis"%(key,iy));

        for ipt in range(0,len(pt_bins)-1):
            if args.use_pu_bins:
                bin_label = key+"_pt_"+str(ipt)+"_pu_"+str(iy);
            else:
                bin_label = key+"_pt_"+str(ipt)+"_eta_"+str(iy);

            median_vs_pt_b[-1].SetPoint(ipt,hist_pt_b[bin_label].GetMean(),response_b_param[bin_label][1]);
            median_vs_pt_c[-1].SetPoint(ipt,hist_pt_c[bin_label].GetMean(),response_c_param[bin_label][1]);
            median_vs_pt_uds[-1].SetPoint(ipt,hist_pt_uds[bin_label].GetMean(),response_uds_param[bin_label][1]);
            median_vs_pt_g[-1].SetPoint(ipt,hist_pt_g[bin_label].GetMean(),response_g_param[bin_label][1]);
            median_vs_pt_jet[-1].SetPoint(ipt,hist_pt_jet[bin_label].GetMean(),response_jet_param[bin_label][1]);
            median_vs_pt_tau[-1].SetPoint(ipt,hist_pt_tau[bin_label].GetMean(),response_tau_param[bin_label][1]);

            median_vs_pt_b_vis[-1].SetPoint(ipt,hist_pt_b[bin_label].GetMean(),response_b_param_vis[bin_label][1]);
            median_vs_pt_c_vis[-1].SetPoint(ipt,hist_pt_c[bin_label].GetMean(),response_c_param_vis[bin_label][1]);
            median_vs_pt_uds_vis[-1].SetPoint(ipt,hist_pt_uds[bin_label].GetMean(),response_uds_param_vis[bin_label][1]);
            median_vs_pt_g_vis[-1].SetPoint(ipt,hist_pt_g[bin_label].GetMean(),response_g_param_vis[bin_label][1]);
            median_vs_pt_jet_vis[-1].SetPoint(ipt,hist_pt_jet[bin_label].GetMean(),response_jet_param_vis[bin_label][1]);
            median_vs_pt_tau_vis[-1].SetPoint(ipt,hist_pt_tau[bin_label].GetMean(),response_tau_param_vis[bin_label][1]);

            sigma_vs_pt_b[-1].SetPoint(ipt,hist_pt_b[bin_label].GetMean(),response_b_param[bin_label][-1]/response_b_param[bin_label][1]);
            sigma_vs_pt_c[-1].SetPoint(ipt,hist_pt_c[bin_label].GetMean(),response_c_param[bin_label][-1]/response_c_param[bin_label][1]);
            sigma_vs_pt_uds[-1].SetPoint(ipt,hist_pt_uds[bin_label].GetMean(),response_uds_param[bin_label][-1]/response_uds_param[bin_label][1]);
            sigma_vs_pt_g[-1].SetPoint(ipt,hist_pt_g[bin_label].GetMean(),response_g_param[bin_label][-1]/response_g_param[bin_label][1]);
            sigma_vs_pt_jet[-1].SetPoint(ipt,hist_pt_jet[bin_label].GetMean(),response_jet_param[bin_label][-1]/response_jet_param[bin_label][1]);
            sigma_vs_pt_tau[-1].SetPoint(ipt,hist_pt_tau[bin_label].GetMean(),response_tau_param[bin_label][-1]/response_tau_param[bin_label][1]);

            sigma_vs_pt_b_vis[-1].SetPoint(ipt,hist_pt_b[bin_label].GetMean(),response_b_param_vis[bin_label][-1]/response_b_param_vis[bin_label][1]);
            sigma_vs_pt_c_vis[-1].SetPoint(ipt,hist_pt_c[bin_label].GetMean(),response_c_param_vis[bin_label][-1]/response_c_param_vis[bin_label][1]);
            sigma_vs_pt_uds_vis[-1].SetPoint(ipt,hist_pt_uds[bin_label].GetMean(),response_uds_param_vis[bin_label][-1]/response_uds_param_vis[bin_label][1]);
            sigma_vs_pt_g_vis[-1].SetPoint(ipt,hist_pt_g[bin_label].GetMean(),response_g_param_vis[bin_label][-1]/response_g_param_vis[bin_label][1]);
            sigma_vs_pt_jet_vis[-1].SetPoint(ipt,hist_pt_jet[bin_label].GetMean(),response_jet_param_vis[bin_label][-1]/response_jet_param_vis[bin_label][1]);
            sigma_vs_pt_tau_vis[-1].SetPoint(ipt,hist_pt_tau[bin_label].GetMean(),response_tau_param_vis[bin_label][-1]/response_tau_param_vis[bin_label][1]);

##plotting
xaxis_title = "p_{T} [GeV]";
if args.use_pt_gen:
    xaxis_title = "p_{T}^{gen} [GeV]";

yaxis_title = "Median p_{T}/p_{T}^{gen}";
ROOT.plotParameterVsObs(median_vs_pt_b,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_b",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_c,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_c",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_uds,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_uds",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_g,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_g",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_jet,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_jets",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_tau,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_tau",pt_bins[0],pt_bins[-1]);    

ROOT.plotParameterVsObs(median_vs_pt_b_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_b_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_c_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_c_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_uds_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_uds_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_g_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_g_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_jet_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_jets_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(median_vs_pt_tau_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"median_vs_pt_vs_"+suffix+"_tau_vis",pt_bins[0],pt_bins[-1]);    

yaxis_title = "#sigma_{eff}/Median(p_{T}/p_{T}^{gen})";
ROOT.plotParameterVsObs(sigma_vs_pt_b,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_b",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_c,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_c",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_uds,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_uds",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_g,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_g",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_jet,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_jets",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_tau,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_tau",pt_bins[0],pt_bins[-1]);    

ROOT.plotParameterVsObs(sigma_vs_pt_b_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_b_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_c_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_c_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_uds_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_uds_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_g_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_g_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_jet_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_jets_vis",pt_bins[0],pt_bins[-1]);
ROOT.plotParameterVsObs(sigma_vs_pt_tau_vis,legends,xaxis_title,yaxis_title,len(y_bins)-1,args.output_dir,"sigma_vs_pt_vs_"+suffix+"_tau_vis",pt_bins[0],pt_bins[-1]);    

stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)
