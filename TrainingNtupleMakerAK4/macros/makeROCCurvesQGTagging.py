import os
import sys
import glob
import argparse
import subprocess
import shutil
import time
import ROOT
import numpy as np
from array import array 
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file-name', type=str, default='', help='file name to grep');
parser.add_argument('-o', '--output-dir', type=str, default='', help='output directgory for plots');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-p', '--pt-bins', nargs="+", type=float, default=[30,1000], help='pt bins to build ROCs');
parser.add_argument('-e', '--eta-bins', nargs="+", type=float, default=[-2.5,2.5], help='eta bins to build ROCs');
parser.add_argument('-s', '--samples', nargs="+", type=int, default=[],help='sample to select according to sample ID');
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-j', '--use-jec', action='store_true', help='use pt corrected by JECs');
parser.add_argument('-hf', '--exclude-hf', action='store_true', help='exclude heavy flavors');

inputs = {
    "ParTNoJEC": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassReg/trainingAK4_taumuel_classreg_transformer_21052023",
    "ParTNoJECNew3D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_3d_31012024",
    "ParTNoJECNew5D": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_31012024",
    "ParTNoJECNewDyn": "/eos/cms/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/WeaverResult/AK4Models/ClassRegLast/trainingAK4_transformer_domain_cont_ch_dyn_02032024"
}

start_time = time.time()
ROOT.gInterpreter.ProcessLine('#include "commonTools.h"')
ROOT.setTDRStyle();

args = parser.parse_args()
print(args)
xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

ROOT.EnableImplicitMT(args.nthreads);
ROOT.EnableThreadSafety();

os.system("mkdir -p "+args.output_dir);

preselection = "label_mu != 1 && label_el != 1 && label_taup_1h0p !=1 && label_taup_1h1p != 1 && label_taup_1h2p != 1 && label_taup_3h0p != 1 && label_taup_3h1p != 1 && label_taum_1h0p != 1 && label_taum_1h1p != 1 && label_taum_1h2p != 1 && label_taum_3h0p != 1 && label_taum_3h1p != 1";
if args.exclude_hf:
    preselection += " && label_b != 1 && label_c != 1";
for i,isamp in enumerate(args.samples):
    if i == 0:
        preselection += " && (";
    if i == len(args.samples)-1:
        preselection += " sample == "+str(isamp)+")";
    else:
        preselection += " sample == "+str(isamp)+" || ";
print("Preselection string --> ",preselection);
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)

### histograms
hist_sig_q_vs_g = {};
hist_bkg_q_vs_g = {};
roc_q_vs_g = {}

## define binnings
pt_bins = array('d',args.pt_bins);
eta_bins = array('d',args.eta_bins);
score_bins = array('d',[i for i in np.arange(0.,1.0005,0.0005)])
print("pt_bins: ",pt_bins);
print("eta_bins: ",eta_bins);
print("score_bins: from 0 to 1 in step of 0.0001 --> len ",len(score_bins));

## 
for key,val in inputs.items():
    start_time = time.time()
    print("Run on input files: key ",key," location: ",val);
    ## build the dataframe
    data_frame = ROOT.RDataFrame("Events",xrootd_eos+val+"/*"+args.file_name+"*root" if args.use_xrootd else val+"/*"+args.file_name+"*root");
    ## apply preselection
    data_frame = data_frame.Filter(preselection)
    ## define scores
    if args.exclude_hf:
        data_frame = data_frame.Define("q_vs_g","(score_label_b+score_label_c+score_label_uds)/(score_label_b+score_label_c+score_label_uds+score_label_g)");
    else:
        data_frame = data_frame.Define("q_vs_g","(score_label_uds)/(score_label_uds+score_label_g)");
        
    ## apply truth label filters
    data_frame_q = data_frame.Filter("label_b==1 || label_c==1 || label_uds==1");
    data_frame_g = data_frame.Filter("label_g==1");
    ## define the histograms as 3D to speed up RDF computation
    histo_list = [];
    pt_name = "jet_pt_raw";
    if args.use_jec:
        pt_name = "jet_pt"
    sig_q_vs_g = data_frame_q.Histo3D(("sig_q_vs_g_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","q_vs_g");
    bkg_q_vs_g = data_frame_g.Histo3D(("bkg_q_vs_g_"+key,"",len(args.pt_bins)-1,pt_bins,len(args.eta_bins)-1,eta_bins,len(score_bins)-1,score_bins),pt_name,"jet_eta","q_vs_g");
    histo_list.extend([sig_q_vs_g,bkg_q_vs_g]);
    ## Run histogram creation
    ROOT.RDF.RunGraphs(histo_list);
    sig_q_vs_g = sig_q_vs_g.GetPtr();
    bkg_q_vs_g = bkg_q_vs_g.GetPtr();
    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)
    ## build the ROCs and projecting histograms
    print("Produce the corresponding ROCs by projecting histograms");
    start_time = time.time()
    for ipt in range(0,len(pt_bins)-1):
        for ieta in range(0,len(eta_bins)-1):
            name = key+"_pt_"+str(ipt)+"_eta_"+str(ieta);

            hist_sig_q_vs_g[name] = ROOT.extractScore(sig_q_vs_g,ipt,ieta,"hist_sig_q_vs_g_"+name)
            hist_bkg_q_vs_g[name] = ROOT.extractScore(bkg_q_vs_g,ipt,ieta,"hist_bkg_q_vs_g_"+name)

            hist_sig_q_vs_g[name].Scale(1./hist_sig_q_vs_g[name].Integral());
            hist_bkg_q_vs_g[name].Scale(1./hist_bkg_q_vs_g[name].Integral());
            
            roc_q_vs_g[name] = ROOT.buildROC(hist_sig_q_vs_g[name],hist_bkg_q_vs_g[name]);

    stop_time = time.time()
    print("Time taken in seconds: ",stop_time-start_time)

### From TGraph to TF1
print("Plotting final ROCs");
start_time = time.time()
for ipt in range(0,len(args.pt_bins)-1):
    for ieta in range(0,len(args.eta_bins)-1):
        rocs_to_plot_q_vs_g = [];
        legends = [];
        bin_label = "pt_"+str(ipt)+"_eta_"+str(ieta);
        bin_name = "%d < p_{T} < %d GeV, %.1f < |#eta| < %.1f"%(args.pt_bins[ipt],args.pt_bins[ipt+1],args.eta_bins[ieta],args.eta_bins[ieta+1]);
        for key in inputs:

            name = key+"_"+bin_label;
            rocs_to_plot_q_vs_g.append(roc_q_vs_g[name]);
            
            hist_sig_q_vs_g[name].Rebin(10);
            hist_bkg_q_vs_g[name].Rebin(10);

            hist_sig_q_vs_g[name].Scale(1./hist_sig_q_vs_g[name].Integral());
            hist_bkg_q_vs_g[name].Scale(1./hist_bkg_q_vs_g[name].Integral());
            
            legends.append(key);
            
            legends_tmp = [];
            legends_tmp.append(name);

            labels_tmp = [];
            labels_tmp.append("Sig: q vs g");
            labels_tmp.append("Bkg: q vs g");

            ROOT.plotScore([hist_sig_q_vs_g[name]],[hist_bkg_q_vs_g[name]],[],[],[],[],
                           legends_tmp,labels_tmp,bin_name,args.output_dir,"score_"+name+"_"+bin_label);
        labels = [];
        labels.append("q vs g");
        ROOT.plotROC(rocs_to_plot_q_vs_g,[],[],[],[],[],legends,labels,bin_name,args.output_dir,"roc_qtag_"+bin_label,0.,1.,0.,1.,False);
        
stop_time = time.time()
print("Time taken in seconds: ",stop_time-start_time)
