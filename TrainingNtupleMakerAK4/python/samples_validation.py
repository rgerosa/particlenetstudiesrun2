def AddDYSamples(samples):

    samples['DYJetsToLL'] = [
        '/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.401e+03','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        10000000
    ]


def AddHiggsSamples(samples):

    samples['GluGluHToBB_M-125'] = [
        '/GluGluHToBB_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=28.29','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]
    samples['VBFHToBB_M-125'] = [
        '/VBFHToBB_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.203','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['WminusH_HToBB_WToLNu_M-125'] = [
        '/WminusH_HToBB_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.104','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['WplusH_HToBB_WToLNu_M-125'] = [
        '/WplusH_HToBB_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.165','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['ZH_HToBB_ZToLL_M-125'] = [
        '/ZH_HToBB_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.0522','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples ['ZH_HToBB_ZToBB_M-125'] = [
        '/ZH_HToBB_ZToBB_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.0787','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['GluGluHToTauTau_M-125'] = [
        '/GluGluHToTauTau_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.047','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['VBFHToTauTau_M125'] = [
        '/VBFHToTauTau_M125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.237','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['VBFHToCC_M-125'] = [     
        '/VBFHToCC_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.109','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['WminusH_HToCC_WToLNu_M-125'] = [
        '/WminusH_HToCC_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.00518','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['WplusH_HToCC_WToLNu_M-125'] = [
        '/WplusH_HToCC_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.00816','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['ggZH_HToCC_ZToLL_M-125'] = [
        '/ggZH_HToCC_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.00107','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

def AddHHNonResSamples(samples):

    samples['GluGluToHHTo2B2Tau_node_SM'] = [
        '/GluGluToHHTo2B2Tau_TuneCP5_PSWeights_node_SM_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.00134','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]


def AddTTbarSamples(samples):

    samples['TTToSemiLeptonic'] = [
        '/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=384.26','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        10000000
    ]

def AddQCDSamples(samples):

    samples['QCD_Pt_15to30'] = [
        '/QCD_Pt_15to30_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.246e+09','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_30to50'] = [
        '/QCD_Pt_30to50_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.068e+08','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_50to80'] = [
        '/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.569e+07','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_80to120'] = [
        '/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=2.343e+06','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_120to170'] = [
        '/QCD_Pt_120to170_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.062e+05','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_170to300'] = [
        '/QCD_Pt_170to300_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.032e+05','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
	2500000
    ]

    samples['QCD_Pt_300to470'] = [
        '/QCD_Pt_300to470_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=6.831e+03','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_470to600'] = [
        '/QCD_Pt_470to600_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=5.515e+02','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_600to800'] = [
        '/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.567e+02','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_800to1000'] = [
        '/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=2.622e+01','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
	2500000
    ]
    samples['QCD_Pt_1000to1400'] = [
	'/QCD_Pt_1000to1400_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=7.455e+00','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_1400to1800'] = [
        '/QCD_Pt_1400to1800_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=6.477e-01','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_1800to2400'] = [
        '/QCD_Pt_1800to2400_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=8.744e-02','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_2400to3200'] = [
        '/QCD_Pt_2400to3200_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=5.233e-03','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]

    samples['QCD_Pt_3200toInf'] = [
        '/QCD_Pt_3200toInf_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.351e-04','isMC=True','nThreads=2','jetPtMin=15','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','evaluatePNETModels=base'],
        'EventAwareLumiBased',
        50000,
        '',
        2500000
    ]


def AddAllSamples(samples):
    AddDYSamples(samples)
    AddHiggsSamples(samples)
    AddHHNonResSamples(samples)
    AddTTbarSamples(samples)
    AddQCDSamples(samples)
