def AddHiggsSamples(samples):

    samples['VBFHToBB_M-125'] = [
        '/VBFHToBB_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.203','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['VBFHToTauTau_M125'] = [
        '/VBFHToTauTau_M125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.237','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['VBFHToCC_M-125'] = [     
        '/VBFHToCC_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.109','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['VBFHToGG_M-125'] = [
        '/VBFHToGG_M125_TuneCP5_13TeV-amcatnlo-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.0086','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]


def AddQCDSamples(samples):

    samples['QCD_Pt_15to30'] = [
        '/QCD_Pt_15to30_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.246e+09','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]
    
    samples['QCD_Pt_30to50'] = [
        '/QCD_Pt_30to50_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.068e+08','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_50to80'] = [
        '/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.569e+07','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_80to120'] = [
        '/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=2.343e+06','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_120to170'] = [
        '/QCD_Pt_120to170_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=4.062e+05','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_170to300'] = [
        '/QCD_Pt_170to300_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.032e+05','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_300to470'] = [
        '/QCD_Pt_300to470_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=6.831e+03','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_470to600'] = [
        '/QCD_Pt_470to600_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=5.515e+02','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_600to800'] = [
        '/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.567e+02','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_800to1000'] = [
        '/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=2.622e+01','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_1000to1400'] = [
        '/QCD_Pt_1000to1400_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=7.455e+00','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_1400to1800'] = [
        '/QCD_Pt_1400to1800_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=6.477e-01','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_1800to2400'] = [
        '/QCD_Pt_1800to2400_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=8.744e-02','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_2400to3200'] = [
        '/QCD_Pt_2400to3200_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=5.233e-03','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_Pt_3200toInf'] = [
        '/QCD_Pt_3200toInf_TuneCP5_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=1.351e-04','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]


def AddQCDHTSamples(samples):

    samples['QCD_HT50to100'] = [
        '/QCD_HT50to100_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.847e+08','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT100to200'] = [
        '/QCD_HT100to200_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.354e+07','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT200to300'] = [
        '/QCD_HT200to300_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.562e+06','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT300to500'] = [
        '/QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.248e+05','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT500to700'] = [
        '/QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.024e+04','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT700to1000'] = [
        '/QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.429e+03','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT1000to1500'] = [
        '/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.109e+03','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]


    samples['QCD_HT1500to2000'] = [
        '/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.086e+02','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]

    samples['QCD_HT2000toInf'] = [
        '/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.186e+01','isMC=True','jetPtMin=15','jetEtaMax=4.7','jetEtaMin=2.5','dumpOnlyJetMatchedToGen=True','evaluateForwardPNETTraining=True','dRJetGenMatch=0.4'],
        'EventAwareLumiBased',
        65000,
        '',
        10000000
    ]


def AddAllSamples(samples):
    AddHiggsSamples(samples)
    AddQCDSamples(samples)
    AddQCDHTSamples(samples)
    
