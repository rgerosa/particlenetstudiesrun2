import FWCore.ParameterSet.Config as cms

## trigger
triggerResultFilter = cms.EDFilter('TriggerResultsFilter',
        hltResults = cms.InputTag('TriggerResults','',"HLT"),
        l1tResults = cms.InputTag(''),
        l1tIgnoreMaskAndPrescale = cms.bool(False),
        throw = cms.bool(False),
        triggerConditions = cms.vstring('HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_v*')
);

## dielectron selection
tagElectrons = cms.EDFilter("PATElectronSelector",
        src = cms.InputTag("slimmedElectrons"),
        cut = cms.string("pt*(userFloat('ecalTrkEnergyPostCorr')/energy) > 26 && abs(eta) < 2.5 && electronID('mvaEleID-Fall17-iso-V2-wp80')"),
        filter = cms.bool(False)
);

filterTagElectrons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("tagElectrons"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(2)
);

probeElectrons = cms.EDFilter("PATElectronSelector",
        src = cms.InputTag("slimmedElectrons"),
        cut = cms.string("pt*(userFloat('ecalTrkEnergyPostCorr')/energy) > 20 && abs(eta) < 2.5 && electronID('mvaEleID-Fall17-iso-V2-wp90')"),
        filter = cms.bool(False)
);

filterProbeElectrons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("probeElectrons"),
        minNumber = cms.uint32(2),
        maxNumber = cms.uint32(2),
);

## dielectron pair selection
dielectronPairs = cms.EDProducer("CandViewShallowCloneCombiner",
        decay = cms.string("probeElectrons@+ probeElectrons@-"),
        cut = cms.string("mass > 70 && mass < 110 && charge == 0"),
        checkCharge = cms.bool(False)
);

filterDiElectronPairs = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("dielectronPairs"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(1)
);

## final sequence
leptonSelection = cms.Sequence(
    triggerResultFilter+
    tagElectrons+
    filterTagElectrons+
    probeElectrons+
    filterProbeElectrons+
    dielectronPairs+
    filterDiElectronPairs
)

 
    
## require dR between jets and the electrons
from PhysicsTools.PatAlgos.cleaningLayer1.jetCleaner_cfi import cleanPatJets
cleanJets = cms.EDProducer("PATJetCleaner",
    src = cms.InputTag("slimmedJetsUpdated"),
    preselection = cms.string(''),
    checkOverlaps = cms.PSet(
        electrons = cms.PSet(
            src          = cms.InputTag("probeElectrons"),
            algorithm    = cms.string("byDeltaR"),
            preselection = cms.string(""),
            deltaR       = cms.double(0.4),
            checkRecoComponents = cms.bool(False),
            pairCut             = cms.string(""),
            requireNoOverlaps   = cms.bool(True)
        )
    ),
    finalCut = cms.string('')
)

from PhysicsTools.PatAlgos.selectionLayer1.jetSelector_cfi import selectedPatJets
selectedCleanJets = selectedPatJets.clone();
selectedCleanJets.src = cms.InputTag("cleanJets");
selectedCleanJets.cut = cms.string("correctedJet('Uncorrected').pt() > 25 && abs(eta) < 2.5");
selectedCleanJets.filter = cms.bool(False)

filterCleanJets = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("selectedCleanJets"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(99),
);

jetSelection = cms.Sequence(
      cleanJets +
      selectedCleanJets +
      filterCleanJets
)
