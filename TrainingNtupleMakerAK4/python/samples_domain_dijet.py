def AddQCDSamples(samples):

    samples['QCD_Inclusive'] = [
        '/QCD_Pt-15to7000_TuneCP5_Flat2018_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.359e+09','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        -1
    ]

def AddQCDHTSamples(samples):

    samples['QCD_HT50to100'] = [
        '/QCD_HT50to100_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.869e+08','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        10000000
    ]

    samples['QCD_HT100to200'] = [
        '/QCD_HT100to200_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.386e+07','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]
    
    samples['QCD_HT200to300'] = [
        '/QCD_HT200to300_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.560e+06','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]

    samples['QCD_HT300to500'] = [
        '/QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.196e+05','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        2000000
    ]

    samples['QCD_HT500to700'] = [
        '/QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.995e+04','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        2000000
    ]
    
    samples['QCD_HT700to1000'] = [
        '/QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.358e+03','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]

    samples['QCD_HT1000to1500'] = [
        '/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.115e+03','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]

    samples['QCD_HT1500to2000'] = [
        '/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.076e+02','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]
    
    samples['QCD_HT2000toInf'] = [
        '/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.199e+01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]

def AddQCDHerwigSamples(samples):

    
    samples['QCD_HT50to100_herwig'] = [
        '/QCD_HT50to100_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.869e+08','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        10000000
    ]

    samples['QCD_HT100to200_herwig'] = [
        '/QCD_HT100to200_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.386e+07','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]
    
    samples['QCD_HT200to300_herwig'] = [
        '/QCD_HT200to300_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.560e+06','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]

    samples['QCD_HT300to500_herwig'] = [
        '/QCD_HT300to500_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.196e+05','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        2000000
    ]

    samples['QCD_HT500to700_herwig'] = [
        '/QCD_HT500to700_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.995e+04','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        2000000
    ]
    
    samples['QCD_HT700to1000_herwig'] = [
        '/QCD_HT700to1000_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.358e+03','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]

    samples['QCD_HT1000to1500_herwig'] = [
        '/QCD_HT1000to1500_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.115e+03','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]

    samples['QCD_HT1500to2000_herwig'] = [
        '/QCD_HT1500to2000_TuneCH3_13TeV-madgraphMLM-herwig7/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.076e+02','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]
    
    samples['QCD_HT2000toInf_herwig'] = [
        '/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.199e+01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        50000,
        '',
        1000000
    ]


def AddDataSamples(samples):

    samples['ZeroBias2018D'] = [
        '/ZeroBias/Run2018D-UL2018_MiniAODv2-v1/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        100,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]
    '''
    samples['JetHT2018D'] = [
        '/JetHT/Run2018D-UL2018_MiniAODv2-v2/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        100,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]
    '''
def AddAllSamples(samples):
    AddQCDSamples(samples)
    #AddQCDHTSamples(samples)
    #AddQCDHerwigSamples(samples)
    AddDataSamples(samples)
