def AddDYSamples(samples):

    samples['DYJetsToLL'] = [
        '/DYJetsToTauTau_M-50_AtLeastOneEorMuDecay_massWgtFix_TuneCP5_13TeV-powhegMiNNLO-pythia8-photos/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.976e+03','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=etau','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        500000,
        '',
        -1
    ]

def AddWJetSamples(samples):

    samples['WJetsToLNu'] = [
        '/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=20509','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=etau','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        500000,
        '',
        -1
    ]

def AddTTbarSamples(samples):

    samples['TTTo2L2Nu'] = [
        '/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=86.46','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=mutau','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        150000,
        '',
        -1
    ]

    samples['TTToSemiLeptonic'] = [
        '/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=362.01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=mutau','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        150000,
        '',
        -1
    ]

def AddDataSamples(samples):

    samples['EGamma2018A'] = [
        '/EGamma/Run2018A-UL2018_MiniAODv2-v1/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=etau','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        125,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

    samples['EGamma2018B'] = [
        '/EGamma/Run2018B-UL2018_MiniAODv2-v1/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=etau','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        125,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

    samples['EGamma2018C'] = [
        '/EGamma/Run2018B-UL2018_MiniAODv2-v1/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=etau','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        125,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

    samples['EGamma2018D'] = [
        '/EGamma/Run2018D-UL2018_MiniAODv2-v2/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=etau','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        125,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

def AddAllSamples(samples):
    AddDYSamples(samples)
    AddDataSamples(samples)
    
